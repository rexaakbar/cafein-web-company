@extends('layouts.app')

@section('title', Session::get('CompanyName').' | Welcome')

@section('content')
    <!-- start banner Area -->
      <section id="bannerArea" class="banner-area relative" style="background: url('{{ asset('storage/img/header-bg.jpg') }}') no-repeat; background-size:100%;"  id="home">
        <div class="container">
          <div class="row fullscreen d-flex align-items-center justify-content-start">
            {{-- <div class="banner-content col-lg-8 col-md-12">
              <h4 class="text-white text-uppercase">Wide Options of Choice</h4>
              <h1>
                Delicious Receipes          
              </h1>
              <p class="text-white">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp <br> or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
              </p>
              <a href="#" class="primary-btn header-btn text-uppercase">Check Our Menu</a>
            </div>   --}}                      
          </div>
        </div>
      </section>
      <!-- End banner Area -->  

      <!-- Start top-dish Area -->
      <section class="top-dish-area pb-10 mb-0 mt-30 pt-30" id="menu-favorit">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-8">
              <div class="title text-center">
                <h1 class="mb-10">Menu Favorit</h1>
                <p>Menu paling banyak dipesan pelanggan.</p>
              </div>
            </div>
          </div>            
          <div class="row">
            @if(count($dataMenuFavorit) == 0)
              <div class="col-md-12 mb-20">
                <div class="text-center">
                  <h2>
                    Mohon maaf, belum ada menu favorit apapun yang bisa ditampilkan.
                  </h2>
                </div>
              </div>
            @else
              @foreach($dataMenuFavorit as $value)
                <div class="single-dish col-lg-4">
                  <div class="thumb">
                    <img class="img-fluid"  src="{{ asset('storage/img/'.$value->productimage_path) }}" alt="">
                  </div>
                  <h4 class="text-uppercase pt-20 pb-20">{{ $value->productitem_name }}</h4>
                  <p>
                    {{ strip_tags($value->productitem_description) }}
                  </p>
                </div>
              @endforeach  
            @endif              
          </div>
        </div>  
      </section>
      <!-- End top-dish Area -->
      
      <!-- Start video Area -->
      {{-- <section class="video-area">
        <div class="container">
          <div class="row justify-content-center align-items-center flex-column">
            <a class="play-btn" href="http://www.youtube.com/watch?v=0O2aH4XLbto">
              <img src="img/play-btn.png" alt="">
            </a>
            <h3 class="pt-20 pb-20 text-white">We Always serve the vaping hot and delicious foods</h3>
            <p class="text-white">Youtube video will appear in popover</p>
          </div>
        </div>  
      </section> --}}
      <!-- End video Area -->
      

      <!-- Start features Area -->
      {{-- <section class="features-area pt-100" id="feature">
        <div class="container">
          <div class="feature-section">
            <div class="row">
              <div class="single-feature col-lg-3 col-md-6">
                <img src="img/f1.png" alt="">
                <h4 class="pt-20 pb-20">Refreshing Breakfast</h4>
                <p>
                  Lorem ipsum dolor sit ametcons ecteturadipis icing elit.
                </p>
              </div>
              <div class="single-feature col-lg-3 col-md-6">
                <img src="img/f2.png" alt="">
                <h4 class="pt-20 pb-20">Awesome Lunch</h4>
                <p>
                  Lorem ipsum dolor sit ametcons ecteturadipis icing elit.
                </p>
              </div>
              <div class="single-feature col-lg-3 col-md-6">
                <img src="img/f3.png" alt="">
                <h4 class="pt-20 pb-20">Soothing Dinner</h4>
                <p>
                  Lorem ipsum dolor sit ametcons ecteturadipis icing elit.
                </p>
              </div>
              <div class="single-feature col-lg-3 col-md-6">
                <img src="img/f4.png" alt="">
                <h4 class="pt-20 pb-20">Rich Quality Buffet</h4>
                <p>
                  Lorem ipsum dolor sit ametcons ecteturadipis icing elit.
                </p>
              </div>                            
            </div>                      
          </div>
        </div>  
      </section> --}}
      <!-- End features Area -->


      <!-- Start related Area -->
      <section class="related-area pb-0 mb-0 pt-30 mt-10" id="restoran-kami">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-8">
              <div class="title text-center">
                <h1 class="mb-10">Restoran Kami</h1>
                <p>Kini Kami Hadir Di Berbagai Daerah.</p>
              </div>
            </div>
          </div>            
          <div class="row justify-content-center">
            @if(count($dataCabangCompany) > 1)
            <div class="active-realated-carusel">
            @endif
                @foreach($dataCabangCompany as $value)
                    <div class="item row align-items-center">
                        <div class="col-lg-6 rel-left">
                           <h3>
                              {{ $value->branch_name }}
                           </h3>
                           <h4>
                              {{ $value->branch_city.', '.$value->province_name.', '.$value->country_name }}
                           </h4>
                           <p class="pt-30 pb-30">
                              {{ strip_tags($value->branch_description) }}
                           </p>
                          <a href="{{ url('restoran/detail/'.$value->branch_id) }}" class="primary-btn header-btn text-uppercase"><i class="fa fa-fw fa-eye"></i> Lihat Detail Restoran</a>
                        </div>
                        <div class="col-lg-6">
                          <img class="img-fluid" src="{{ asset('storage/img/branch/'.$value->branch_photo) }}" alt="">
                        </div>
                    </div>
                @endforeach             
            @if(count($dataCabangCompany) > 1)
            </div>
            @endif
          </div>
          @if(count($dataCabangCompany) > 1)
          <div class="row">
            <div class="col-md-12 text-center mt-80">
              <a href="{{ url('restoran') }}" class="primary-btn header-btn text-uppercase">
                    <i class="fa fa-fw fa-list-ul"></i> Lihat Lebih Banyak
                </a>
            </div>
          </div>
          @endif
        </div>  
      </section>
      <!-- End related Area --> 


      <!-- Start team Area -->
      {{-- <section class="team-area section-gap" id="chefs">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-8">
              <div class="title text-center">
                <h1 class="mb-10">Meet Our Qualified Chefs</h1>
                <p>Who are in extremely love with eco friendly system.</p>
              </div>
            </div>
          </div>            
          <div class="row justify-content-center d-flex align-items-center">
            <div class="col-md-3 single-team">
                <div class="thumb">
                    <img class="img-fluid" src="img/t1.jpg" alt="">
                    <div class="align-items-center justify-content-center d-flex">
                  <a href="#"><i class="fa fa-facebook"></i></a>
                  <a href="#"><i class="fa fa-twitter"></i></a>
                  <a href="#"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
                <div class="meta-text mt-30 text-center">
                  <h4>Ethel Davis</h4>
                  <p>Managing Director (Sales)</p>                        
                </div>
            </div>
            <div class="col-md-3 single-team">
                <div class="thumb">
                    <img class="img-fluid" src="img/t2.jpg" alt="">
                    <div class="align-items-center justify-content-center d-flex">
                  <a href="#"><i class="fa fa-facebook"></i></a>
                  <a href="#"><i class="fa fa-twitter"></i></a>
                  <a href="#"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
                <div class="meta-text mt-30 text-center">
                  <h4>Rodney Cooper</h4>
                  <p>Creative Art Director (Project)</p>            
                </div>
            </div>  
            <div class="col-md-3 single-team">
                <div class="thumb">
                    <img class="img-fluid" src="img/t3.jpg" alt="">
                    <div class="align-items-center justify-content-center d-flex">
                  <a href="#"><i class="fa fa-facebook"></i></a>
                  <a href="#"><i class="fa fa-twitter"></i></a>
                  <a href="#"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
                <div class="meta-text mt-30 text-center">
                  <h4>Dora Walker</h4>
                  <p>Senior Core Developer</p>            
                </div>
            </div>  
            <div class="col-md-3 single-team">
                <div class="thumb">
                    <img class="img-fluid" src="img/t4.jpg" alt="">
                    <div class="align-items-center justify-content-center d-flex">
                  <a href="#"><i class="fa fa-facebook"></i></a>
                  <a href="#"><i class="fa fa-twitter"></i></a>
                  <a href="#"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
                <div class="meta-text mt-30 text-center">
                  <h4>Lena Keller</h4>
                  <p>Creative Content Developer</p>           
                </div>
            </div>                                    
          </div>
        </div>  
      </section> --}}
      <!-- End team Area -->      

      <!-- start blog Area -->    
      <section class="related-area pt-40 mt-20 mb-0 pb-30" id="berita-terkini">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-8">
              <div class="title text-center">
                <h1 class="mb-10">Berita Terkini</h1>
                <p>Jangan pernah lewatkan satupun berita dari kami.</p>
              </div>
            </div>
          </div>          
          <div class="row">

            @if(count($dataBeritaCompany) == 0)
              
              <div class="col-md-12 mb--20">
                <div class="text-center">
                  <h2>
                    Mohon maaf, belum ada berita apapun yang bisa ditampilkan.
                  </h2>
                </div>
              </div>

            @else

              @foreach($dataBeritaCompany as $value)
                <div class="col-lg-3 col-md-6 single-blog">
                  <div class="thumb">
                    <img class="img-fluid" src="{{ asset('storage/img/news/'.$value->news_photo) }}" alt="">
                  </div>
                  <p class="date">{{ date('d-M-Y',strtotime($value->news_created_at)) }}</p>
                  <a href="{{ url('berita/detail/'.$value->news_id) }}"><h4>{{ $value->news_title }}</h4></a>
                  <p>
                    {{ $value->description_news }}
                  </p>
                  <a href="{{ url('berita/detail/'.$value->news_id) }}">
                    <p>
                      <u>Lihat Selengkapnya</u>
                    </p>
                  </a>
                </div>
              @endforeach

            @endif
          	          
          </div>
          <div class="row">

        	  @if(count($dataBeritaCompany) > 4)
            
              <div class="col-md-12 text-center mt-40 ">
                <a href="{{ url('berita') }}" class="primary-btn header-btn text-uppercase">
                      <i class="fa fa-fw fa-list-ul"></i> Lihat Lebih Banyak
                  </a>
              </div>

            @endif
          </div>
        </div>  
      </section>
      <!-- end blog Area -->  

      <!-- Start Contact Area -->
      {{-- <section class="contact-area" id="contact">
        <div class="container-fluid">
          <div class="row align-items-center d-flex justify-content-start">
            <div class="col-lg-6 col-md-12 contact-left no-padding">
                  <div style=" width:100%;
                  height: 545px;" id="map"></div>
            </div>
            <div class="col-lg-4 col-md-12 pt-100 pb-100">
              <form class="form-area" id="myForm" action="mail.php" method="post" class="contact-form text-right">
                <input name="fname" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" class="common-input mt-10" required="" type="text">
                <input name="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" class="common-input mt-10" required="" type="email">
                <textarea class="common-textarea mt-10" name="message" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
                <button class="primary-btn mt-20">Send Message<span class="lnr lnr-arrow-right"></span></button>
                <div class="mt-10 alert-msg">
                </div>
              </form>
            </div>
          </div>
        </div>
      </section> --}}
      <!-- End Contact Area -->

@endsection

@section('javascripts')

@endsection