<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
{{-- <link rel="shortcut icon" href="img/fav.png"> --}}
<!-- Author Meta -->
<meta name="author" content="codepixer">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
<!--
CSS
============================================= -->
<link rel="stylesheet" href="{{ asset('storage/css/linearicons.css') }}">
<link rel="stylesheet" href="{{ asset('storage/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('storage/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('storage/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('storage/css/nice-select.css') }}">
<link rel="stylesheet" href="{{ asset('storage/css/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('storage/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('storage/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('storage/lightbox/css/lightbox.css') }}">
<!-- DataTables -->
{{-- <link rel="stylesheet" href="{{ asset('storage/css/datatables.net-bs/css/dataTables.bootstrap.min.css') }}"> --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

<style type="text/css">
	@media only screen and (max-width: 600px) {
		#bannerArea.banner-area{
			height: 300px !important;
		}
	} 

	@media only screen and (min-width: 600px) {
		#bannerArea.banner-area{
			height: 350px !important;
		}
	} 

	@media only screen and (min-width: 768px) {
		#bannerArea.banner-area{
			height: 450px !important;
		}
	}

	@media only screen and (min-width: 992px) {
		#bannerArea.banner-area{
			height: 550px !important;
		}
	} 

	@media only screen and (min-width: 1200px) {
		#bannerArea.banner-area{
			height: 700px !important;
		}
	}
</style>