<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{!! Request::is('home*') ? 'active' : '' !!}">
            {{-- <a href="{{ url('mengetuai') }}"><i class="fa fa-book"></i> Mengetuai</a> --}}
            <a href="{{ url('home') }}">
              <i class="fa fa-book"></i> <span>Home</span>
            </a>
          </li>
        @role(['superadmin','sub_leader','supervisor','assist_manager','pga_manager'])
        <li class="treeview {!! Request::is('absensi/*') ? 'active' : '' !!}">
          <a href="#">
            <i class="fa fa-book"></i> <span>Rekap Absensi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            {{-- <li><a href="#"><i class="fa fa-circle-o"></i> Absensi Kehadiran</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Absensi Lembur</a></li> --}}
            @role(['sub_leader','supervisor','assist_manager','pga_manager','superadmin'])
            <li class="{!! Request::is('absensi/pengajuan_lembur*') ? 'active' : '' !!}">
              <a href="{{ url('absensi/pengajuan_lembur') }}"><i class="fa fa-circle-o"></i> Pengajuan Lembur</a>
            </li>
            @endrole
          </ul>
        </li>
        @endrole
        @role('superadmin')
          <li class="treeview {!! Request::is('master/*') ? 'active' : '' !!}">
            <a href="#">
              <i class="fa fa-book"></i> <span>Data Master</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{!! Request::is('master/departemen*') ? 'active' : '' !!}">
                <a href="{{ url('master/departemen') }}"><i class="fa fa-circle-o"></i> Departemen</a>
              </li>
              <li class="{!! Request::is('master/grup*') ? 'active' : '' !!}">
                <a href="{{ url('master/grup') }}"><i class="fa fa-circle-o"></i> Grup</a>
              </li>
              <li class="{!! Request::is('master/unit*') ? 'active' : '' !!}">
                <a href="{{ url('master/unit') }}"><i class="fa fa-circle-o"></i> Unit</a>
              </li>
              <li class="{!! Request::is('master/pegawai*') ? 'active' : '' !!}">
                <a href="{{ url('master/pegawai') }}"><i class="fa fa-circle-o"></i> Pegawai</a>
              </li>
              <li class="{!! Request::is('master/aktivitas*') ? 'active' : '' !!}">
                <a href="{{ url('master/aktivitas') }}"><i class="fa fa-circle-o"></i> Aktivitas</a>
              </li>
              <li class="{!! Request::is('master/role*') ? 'active' : '' !!}">
                <a href="{{ url('master/role') }}"><i class="fa fa-circle-o"></i> Role</a>
              </li>
            </ul>
          </li>
          <li class="{!! Request::is('konfirmasi_profile*') ? 'active' : '' !!}">
            <a href="{{ url('konfirmasi_profile') }}"><i class="fa fa-book"></i> <span>Konfirmasi Profile</span></a>
          </li>
        @endrole
        @role(['sub_leader','supervisor','assist_manager'])
        <li class="{!! Request::is('mengetuai*') ? 'active' : '' !!}">
            {{-- <a href="{{ url('mengetuai') }}"><i class="fa fa-book"></i> Mengetuai</a> --}}
            <a href="{{ url('mengetuai') }}">
              <i class="fa fa-book"></i> <span>Mengetuai</span>
            </a>
          </li>
        @endrole
       <li class="header">MENU LAINNYA</li>
        <li class="{!! Request::is('profile*') ? 'active' : '' !!}">
          <a href="{{ url('profile') }}"><i class="fa fa-circle-o text-green"></i> <span>Biodata</span></a>
        </li>
        <li>
          {{-- <a href="{{ route('logout') }}"
             onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
              <i class="fa fa-circle-o text-red"></i> <span>Sign Out</span>
          </a> --}}
          <a href="#"
             onclick="callAlertLogout();">
              <i class="fa fa-circle-o text-red"></i> <span>Sign Out</span>
          </a>
        </li>
        {{-- <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Helpdesk</span></a></li> --}}
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>