<!-- start footer Area -->		
<footer class="footer-area pb-30 mb-0 pt-30 mt-0">
	<div class="container">
		<div class="row">
			<div class="col-lg-3  col-md-6 col-sm-6">
				<div class="single-footer-widget">
					<h4 class="text-white">Tentang Kami</h4>
					<p>
						{{ is_null(Session::get('CompanyDescription'))?' - ':strip_tags(Session::get('CompanyDescription')) }}
					</p>
				</div>
			</div>
			<div class="col-lg-8 col-md-6 col-sm-6">
				<div class="single-footer-widget">
					<h4 class="text-white">Kontak Kami</h4>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<label class="control-label col-md-3">E - Mail</label>
								<label class="control-label col-md-7">
									{{ is_null(Session::get('CompanyEmail'))?': - ':': '.Session::get('CompanyEmail') }}
								</label>
							</div>
							<div class="row">
								<label class="control-label col-md-3">No. CP</label>
								<label class="control-label col-md-7">
									{{ is_null(Session::get('CompanyCp'))?': - ':': '.Session::get('CompanyCp') }}
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<label class="control-label col-md-3">Handphone</label>
								<label class="control-label col-md-7">
									{{ is_null(Session::get('CompanyPhone'))?': - ':': '.Session::get('CompanyPhone') }}
								</label>
							</div>
							<div class="row">
								<label class="control-label col-md-3">Fax</label>
								<label class="control-label col-md-7">
									{{ is_null(Session::get('CompanyFax'))?': - ':': '.Session::get('CompanyFax') }}
								</label>
							</div>
						</div>
					</div>
					
				</div>
			</div>					
		</div>
		<div class="footer-bottom">
			<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			<div class="row">
				<div class="col-md-12 text-center">
					<p class="footer-text m-0">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
				</div>
			</div>
		<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
		</div>
	</div>
</footer>	
<!-- End footer Area -->