<header id="header" id="home">
  <div class="container">
    <div class="row align-items-center justify-content-between d-flex">
      <div id="logo">
        <a href="{{ url('/') }}"><img src="{{ asset('storage/img/logo.png') }}" alt="" title="" /></a>
      </div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          @if(Request::is('/'))
            <li><a href="#home"><strong>Beranda</strong></a></li>
            <li><a href="#menu-favorit"><strong>Menu Favorit</strong></a></li>
            <li><a href="#restoran-kami"><strong>Restoran</strong></a></li>
            <li><a href="#berita-terkini"><strong>Berita</strong></a></li>
          @else
            <li><a href="{{ url('/') }}"><strong>Beranda</strong></a></li>
            <li><a href="{{ url('restoran') }}"><strong>Restoran</strong></a></li>
            <li><a href="{{ url('berita') }}"><strong>Berita</strong></a></li>
          @endif
          <li><a href="{{ url('order') }}"><strong>Pesan Makanan</strong></a></li>
          {{-- <li><a href="#chefs">Chefs</a></li> --}}
          {{-- <li><a href="#blog">Blog</a></li> --}}
          {{-- <li><a href="#contact">Contact</a></li> --}}
          {{-- <li class="menu-has-children"><a href="">Pages</a>
            <ul>
              <li><a href="generic.html">Generic</a></li>
              <li><a href="elements.html">Elements</a></li>
            </ul>
          </li> --}}
        </ul>
      </nav><!-- #nav-menu-container -->            
    </div>
  </div>
</header><!-- #header -->