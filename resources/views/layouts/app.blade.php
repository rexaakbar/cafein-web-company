<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="no-js">
<head>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  @include('layouts.head')

  <title>@yield('title')</title>
</head>
<body>
  
  @include('layouts.header')

  {{-- @include('layouts.sidebar') --}}

    <!-- Content Header (Page header) -->
    {{-- <section class="content-header">
      @yield('content-header')
    </section> --}}

    @yield('content')

    @include('layouts.footer')

    @include('layouts.javascript')
    @yield('javascripts')
</body>
</html>
