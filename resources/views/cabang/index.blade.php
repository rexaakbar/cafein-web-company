@extends('layouts.app')

@section('title', Session::get('CompanyName').' | Restoran')

@section('content')
	
	<!-- About Generic Start -->
		<div class="main-wrapper">

			<!-- Start team Area -->
			<section class="team-area mt-60 pt-100 {{ (count($dataCabang) == 0 || is_null($dataCabang))?'pb-100 mb-100':'pb-50' }} " id="team">
				<div class="container">
					<h3 class="about-title mb-30 text-center">Restoran Kami</h3>
					<div class="row justify-content-center d-flex align-items-center">

					@if(count($dataCabang) == 0 || is_null($dataCabang))
						<div class="col-md-12 singel-blog mt-30">
							<div class="text-center">
			                  <h2>
			                    Mohon maaf, kami belum membuka cabang dimanapun.
			                  </h2>
			                </div>
						</div>
					@else
						@foreach($dataCabang as $value)
							<div class="col-md-4  single-team">
							    <div class="thumb">
							        <img class="img-fluid" src="{{ asset('storage/img/branch/'.$value->branch_photo) }}" alt="">
							        <div class="align-items-center justify-content-center d-flex">
										<a href="{{ url('restoran/detail/'.$value->branch_id) }}" data-toggle="tooltip" title="Lihat Restoran"><i class="fa fa-eye"></i></a>
										<a rel="lightbox" href="{{ asset('storage/img/branch/'.$value->branch_photo) }}" data-toggle="tooltip" title="Perbesar Gambar"><i class="fa fa-search-plus"></i></a>
							        </div>
							    </div>
							    <div class="meta-text mt-30 text-center">
								    <h4>{{ $value->branch_name }}</h4>
								    <p>{{ $value->branch_city.', '.$value->province_name.', '.$value->country_name }}</p>
							    </div>
							</div>
						@endforeach	
					@endif					
					</div>

					<div class="text-center">
						<div class="form-group">
							{{ $dataCabang->links() }}
						</div>
					</div>

				</div>

			</section>
			<!-- End team Area -->
			
		</div>

@endsection

@section('javascripts')

@endsection