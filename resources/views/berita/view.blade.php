@extends('layouts.app')

@section('title', Session::get('CompanyName').' | Berita - Lihat')

@section('content')
	
	<!-- About Generic Start -->
		<div class="main-wrapper">
			
			<!-- Start Generic Area -->
			<section class="about-generic-area section-gap">
				<div class="container border-top-generic">
					<h3 class="about-title">{{ $dataBerita->news_title }}</h3>
					<p>{{ $waktuPembuatanBerita }} | {{ date('d-m-Y',strtotime($dataBerita->news_created_at)) }} | {{ $dataBerita->news_city.', '.$dataBerita->province_name.', '.$dataBerita->country_name }}</p>
					<div class="mb-30"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="img-text">
								<img src="{{ asset('storage/img/news/'.$dataBerita->news_photo) }}" alt="" class="img-fluid float-left mr-20 mb-20">
								<p>{{ $dataBerita->news_body }}</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Generic Start -->	
			
		</div>

@endsection

@section('javascripts')
	
@endsection