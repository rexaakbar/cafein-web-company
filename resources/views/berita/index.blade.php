@extends('layouts.app')

@section('title', Session::get('CompanyName').' | Berita')

@section('content')
	
	<!-- About Generic Start -->
		<div class="main-wrapper">

			<!-- Start team Area -->
			<section class="team-area mt-60 pt-100 {{ (count($dataBerita) == 0 || is_null($dataBerita))?'pb-100 mb-100':'pb-50' }} " id="team">
				<div class="container">
					<h3 class="about-title mb-30 text-center">Berita</h3>
					<div class="row justify-content-center d-flex align-items-center">
						@if(count($dataBerita) == 0 || is_null($dataBerita))
							<div class="col-md-12 singel-blog mt-30">
								<div class="text-center">
				                  <h2>
				                    Mohon maaf, belum ada berita apapun yang bisa ditampilkan.
				                  </h2>
				                </div>
							</div>
						@else
			          		@foreach($dataBerita as $value)
				          		<div class="col-lg-4 col-md-6 single-blog">
					              <div class="row">
					              	<div class="col-md-4">
					              		<div class="thumb">
							                <img class="img-fluid" src="{{ asset('storage/img/news/'.$value->news_photo) }}" alt="">
							            </div>
					              	</div>
					              </div>
					              <p class="date">{{ date('d-M-Y',strtotime($value->news_created_at)) }}</p>
					              <a href="{{ url('berita/detail/'.$value->news_id) }}"><h4>{{ $value->news_title }}</h4></a>
					              <p>
					                {{ $value->description_news }}
					              </p>
					              <a href="{{ url('berita/detail/'.$value->news_id) }}">
						              <p>
						              	<u>Lihat Selengkapnya</u>
						              </p>
					              </a>
					            </div>
				          	@endforeach
				          @endif          
			          </div>
					{{-- <div class="row justify-content-center d-flex align-items-center">
						@foreach($dataCabang as $value)
							<div class="col-md-4  single-team">
							    <div class="thumb">
							        <img class="img-fluid" src="{{ asset('img/t1.jpg') }}" alt="">
							        <div class="align-items-center justify-content-center d-flex">
										<a href="{{ url('cabang/detail/'.$value->branch_id) }}" data-toggle="tooltip" title="Lihat Cabang"><i class="fa fa-eye"></i></a>
										<a href="#" data-toggle="tooltip" title="Order Makanan"><i class="fa fa-coffee"></i></a>
										<a href="#" data-toggle="tooltip" title="Perbesar Gambar"><i class="fa fa-search-plus"></i></a>
							        </div>
							    </div>
							    <div class="meta-text mt-30 text-center">
								    <h4>{{ $value->branch_name }}</h4>
								    <p>{{ $value->branch_city.', '.$value->province_name.', '.$value->country_name }}</p>
							    </div>
							</div>
						@endforeach						
					</div> --}}

					<div class="text-center">
						<div class="form-group">
							{{ $dataBerita->links() }}
						</div>
					</div>

				</div>

			</section>
			<!-- End team Area -->
			
		</div>

@endsection

@section('javascripts')

@endsection