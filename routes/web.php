<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middlewareGroups' => 'web'], function(){
	Route::get('/', 'WelcomeController@index');
	Route::get('/test', 'WelcomeController@test');
	Route::group(['prefix' => 'restoran'], function(){
		Route::get('/', 'Cabang\CabangController@index');
		Route::get('detail/{id}', 'Cabang\CabangController@view');
	});
	Route::group(['prefix' => 'berita'], function(){
		Route::get('/', 'Berita\BeritaController@index');
		Route::get('detail/{id}', 'Berita\BeritaController@view');
	});
	Route::group(['prefix' => 'order'], function(){
		Route::match(['POST','GET'],'/', 'Order\OrderController@create');
		Route::match(['POST','GET'],'cariMenuPemesan', 'Order\OrderController@cariMenuPemesan');
		Route::match(['POST','GET'],'getMenuPemesan', 'Order\OrderController@getMenuPemesan');
	});
});
