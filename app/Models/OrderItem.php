<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = "trx_order_items";

    protected $primaryKey = "orderitem_id";

    public $timestamps = false;

    protected $fillable = [
        "orderitem_order_id",
        "orderitem_package_id",
        "orderitem_productitem_id",
        "orderitem_quantity",
        "orderitem_quantity_takeaway",
        "orderitem_note",
        "orderitem_normal_price",
        "orderitem_discount_percentage",
        "orderitem_discount_total",
        "orderitem_grand_total",
        "orderitem_cooking_date_start",
        "orderitem_cooking_date_end",
        "orderitem_status",
        "orderitem_itemcooked",
        "orderitem_itemserved",
    ];

}