<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    protected $table = "trx_order_payments";

    protected $primaryKey = "orderpayment_id";

    public $timestamps = false;

    protected $fillable = [
        "orderpayment_code",
        "orderpayment_order_id",
        "orderpayment_total_price",
        "orderpayment_voucher_id",
        "orderpayment_voucher_paid",
        "orderpayment_discount",
        "orderpayment_tax_percentage",
        "orderpayment_tax",
        "orderpayment_service_charge",
        "orderpayment_additional_charge",
        "orderpayment_rounding",
        "orderpayment_grand_total",
        "orderpayment_method",
        "orderpayment_number_card",
        "orderpayment_number_verification",
        "orderpayment_paid",
        "orderpayment_return_cash",
        "orderpayment_create_user_id",
        "orderpayment_create_date",
        "orderpayment_status",
    ];

}