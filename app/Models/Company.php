<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = "ref_companies";

    protected $primaryKey = "company_id";

    protected $fillable = [
        "employee_id",
        "company_code",
        "company_name",
        "company_description",
        "company_address",
        "company_city",
        "company_district",
        "company_province_id",
        "company_country_id",
        "company_email",
        "company_cp",
        "company_phone",
        "company_fax",
        "company_latitude",
        "company_longitude",
    ];

}