<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
    protected $table = "ref_product_categories";

    protected $primaryKey = "productcate_id";

}