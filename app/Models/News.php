<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "ref_news";

    protected $primaryKey = "news_id";

}