<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderTable extends Model
{
    protected $table = "trx_order_tables";

    protected $primaryKey = "ordertable_id";

    public $timestamps = false;

    protected $fillable = [
        "ordertable_order_id",
        "ordertable_branchtable_id",
        "ordertable_status",
    ];

}