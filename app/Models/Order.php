<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "trx_orders";

    protected $primaryKey = "order_id";

    public $timestamps = false;

    protected $fillable = [
        "order_branch_id",
        "order_date_start",
        "order_date_end",
        "order_customer_name",
        "order_pic_user_id",
        "order_status",
        "order_confirm_status",
    ];

}