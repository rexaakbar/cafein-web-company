<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = "ref_branches";

    protected $primaryKey = "branch_id";

    protected $fillable = [
        "branch_company_id",
        "branch_code",
        "branch_photo",
        "branch_name",
        "branch_info",
        "branch_description",
        "branch_rating",
        "branch_address",
        "branch_city",
        "branch_district",
        "branch_province_id",
        "branch_country_id",
        "branch_email",
        "branch_cp",
        "branch_phone",
        "branch_fax",
        "branch_latitude",
        "branch_longitude",
    ];

}