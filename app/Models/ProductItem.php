<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductItem extends Model
{
    protected $table = "ref_product_items";

    protected $primaryKey = "productitem_id";

    public $timestamps = false;

    protected $fillable = [
        "productitem_productcate_id",
        "productitem_unit_id",
        "productitem_code",
        "productitem_name",
        "productitem_availability",
        "productitem_auto_count",
        "productitem_default_availability",
        "productitem_currency_id",
        "productitem_normal_price",
        "productitem_description",
        "productitem_create_user_id",
        "productitem_create_date",
        "productitem_status",
        "productitem_log_id",
    ];

}