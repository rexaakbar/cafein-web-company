<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderOffTable extends Model
{
    protected $table = "trx_order_offtables";

    protected $primaryKey = "orderofftable_id";

    public $timestamps = false;

    protected $fillable = [
        "orderofftable_order_id",
        "orderofftable_name",
        "orderofftable_address",
        "orderofftable_email",
        "orderofftable_phone",
        "orderofftable_latitude",
        "orderofftable_longtitude",
        "orderofftable_description",
        "orderofftable_created_at",
        "orderofftable_deleted_at",
        "orderofftable_status",
        "orderofftable_log_id",
    ];

}