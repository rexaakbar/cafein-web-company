<?php

namespace App\Http\Controllers\Berita;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Branch;
use App\Models\News;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Exceptions\Handler;
use DB;
use DateTime;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $dataBerita = News::join('ref_countries','ref_countries.country_id','=','ref_news.news_country_id')
        ->join('ref_province','ref_province.province_id','=','ref_news.news_province_id')
        ->where([
            ['ref_news.news_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['ref_news.news_status','=',1],
            ['ref_news.news_log_id','=',null],
        ])
        ->orderBy('ref_news.news_id','desc')
        ->paginate(9);

        foreach ($dataBerita as $value) {
            $value->description_news = $this->word_limit($value->news_body,37);
        }

        // dd($dataBerita);

        return view('berita.index',[
            'dataBerita' => $dataBerita
        ]);
    }

    public function view($id)
    {
        $checkId = News::where([
            ['news_id','=',$id],
            ['news_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['news_status','=',1],
            ['news_log_id','=',null],
        ])
        ->get();

        if (is_null($checkId) || count($checkId) == 0) {
            return redirect('berita');
        }

        $dataBerita = News::join('ref_countries','ref_countries.country_id','=','ref_news.news_country_id')
        ->join('ref_province','ref_province.province_id','=','ref_news.news_province_id')
        ->where([
            ['news_id','=',$id],
            ['news_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['news_status','=',1],
            ['news_log_id','=',null],
        ])
        ->get()
        ->first();

        $waktuPembuatanBerita = $this->time_elapsed_string(date('d-m-Y h:i:s',strtotime($dataBerita->news_created_at)));

        return view('berita.view',[
            'dataBerita' => $dataBerita,
            'id' => $id,
            'waktuPembuatanBerita' => $waktuPembuatanBerita,
        ]);
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'tahun',
            'm' => 'bulan',
            'w' => 'minggu',
            'd' => 'hari',
            'h' => 'jam',
            'i' => 'menit',
            's' => 'detik',
        );
        
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' yang lalu' : 'just now';
    }

    public function word_limit($text,$limit=10){
        if(strlen($text)>$limit){
            $word = mb_substr($text,0,$limit-3)."...";
        }
        else{
            $word = $text;
        }
        return $word;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
