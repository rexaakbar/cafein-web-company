<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Branch;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderOffTable;
use App\Models\OrderPayment;
use App\Models\OrderTable;
use App\Models\ProductCategories;
use App\Models\ProductItem;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Exceptions\Handler;
use DB;
use Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function view($id)
    {
        //
    }

    public function create(Request $req)
    {
        if($req->isMethod('POST')){

            $getIdOrder = Order::create([
                "order_branch_id" => $req->nama_cabang,
                "order_date_start" => now(),
                "order_date_end" => null,
                "order_customer_name" => $req->nama_pemesan,
                "order_pic_user_id" => 0,
                "order_status" => 1,
                "order_confirm_status" => 0,
            ]);

            OrderOffTable::create([
                "orderofftable_order_id" => $getIdOrder->order_id,
                "orderofftable_name" => $req->nama_pemesan,
                "orderofftable_address" => $req->alamat_pemesan,
                "orderofftable_email" => $req->email_pemesan,
                "orderofftable_phone" => $req->no_hp_pemesan,
                "orderofftable_latitude" => 0,
                "orderofftable_longtitude" => 0,
                "orderofftable_description" => $req->catatan_pemesan,
                "orderofftable_created_at" => now(),
                "orderofftable_deleted_at" => null,
                "orderofftable_status" => 1,
                "orderofftable_log_id" => null,
            ]);

            OrderTable::create([
                "ordertable_order_id" => $getIdOrder->order_id,
                "ordertable_branchtable_id" => 0,
                "ordertable_status" => 1,
            ]);

            for ($i = 0; $i < count($req->id_menu_pemesan) ; $i++) {
                if (!is_null($req->id_menu_pemesan[$i])) {

                    OrderItem::create([
                        "orderitem_order_id" => $getIdOrder->order_id,
                        "orderitem_package_id" => 0,
                        "orderitem_productitem_id" => $req->id_menu_pemesan[$i],
                        "orderitem_quantity" => 0,
                        "orderitem_quantity_takeaway" => $req->jumlah_dibeli_pemesan[$i],
                        "orderitem_note" => $req->catatan_pesanan[$i],
                        "orderitem_normal_price" => $req->harga_menu_pemesan[$i],
                        "orderitem_discount_percentage" => 0,
                        "orderitem_discount_total" => 0,
                        "orderitem_grand_total" => $req->total_harga_menu[$i],
                        "orderitem_cooking_date_start" => null,
                        "orderitem_cooking_date_end" => null,
                        "orderitem_status" => 1,
                        "orderitem_itemcooked" => 0,
                        "orderitem_itemserved" => 0,
                    ]);

                }
            }

            Session::flash('statusAlert','true');
            Session::flash('title','Berhasil.');
            Session::flash('message','Pesanan anda berhasil dikirim, silahkan tunggu konfirmasi.');
            Session::flash('type','success');

            return redirect('/');

        }

        $dataCabang = $this->dataCabang();
        $cekDataCabang = Branch::where([
            ['branch_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['branch_info','=',null],
            ['branch_status','=',1],
            ['branch_log_id','=',null],
        ])
        ->get();

        return view('order.create',[
            'dataCabang' => $dataCabang,
            'cekDataCabang' => $cekDataCabang,
        ]);
    }

    public function dataCabang(){
        $dataCabang = Branch::where([
            ['branch_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['branch_info','=',null],
            ['branch_status','=',1],
            ['branch_log_id','=',null],
        ])
        ->get();

        $resultData[null] = '-- Pilih Restoran --';

        foreach ($dataCabang as $value) {
            $resultData[$value->branch_id] = $value->branch_name;
        }

        $encodedData = json_encode($resultData);

        return json_decode($encodedData);
    }

    public function cariMenuPemesan(Request $req){
        if ($req->isMethod('POST')) {
            $resultData = ProductItem::join('ref_product_categories','ref_product_categories.productcate_id','=','ref_product_items.productitem_productcate_id')
            ->join('ref_product_images','ref_product_images.productimage_productitem_id','=','ref_product_items.productitem_id')
            ->where([
                ['ref_product_items.productitem_status','=',1],
                ['ref_product_items.productitem_log_id','=',null],
                ['ref_product_categories.productcate_branch_id','=',$req->idCabang],
                ['ref_product_categories.productcate_status','=',1],
                ['ref_product_categories.productcate_log_id','=',null],
            ])
            ->get();

            $encodedData = json_encode($resultData);

            return json_decode($encodedData);
        }
    }

    public function getMenuPemesan(Request $req){
        if ($req->isMethod('POST')) {
            $resultData = ProductItem::join('ref_product_images','ref_product_images.productimage_productitem_id','=','ref_product_items.productitem_id')
            ->where([
                ['ref_product_items.productitem_id','=',$req->idMenuPemesan],
                ['ref_product_items.productitem_status','=',1],
                ['ref_product_items.productitem_log_id','=',null],
                ['ref_product_images.productimage_status','=',1],
                ['ref_product_images.productimage_log_id','=',null],
            ])
            ->get()
            ->first();

            return $resultData;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
