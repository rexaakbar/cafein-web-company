<?php

namespace App\Http\Controllers\Cabang;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Branch;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderOffTable;
use App\Models\OrderPayment;
use App\Models\OrderTable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Exceptions\Handler;
use DB;

class CabangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $dataCabang = Branch::join('ref_countries','ref_countries.country_id','=','ref_branches.branch_country_id')
        ->join('ref_province','ref_province.province_id','=','ref_branches.branch_province_id')
        ->where([
            ['ref_branches.branch_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['ref_branches.branch_info','=',null],
            ['ref_branches.branch_status','=',1],
            ['ref_branches.branch_log_id','=',null],
        ])
        ->paginate(9);

        // dd($dataCabang);

        return view('cabang.index',[
            'dataCabang' => $dataCabang
        ]);
    }

    public function view($id)
    {
        $checkId = Branch::where([
            ['branch_id','=',$id],
            ['branch_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['branch_info','=',null],
            ['branch_status','=',1],
            ['branch_log_id','=',null],
        ])
        ->get();

        if (is_null($checkId) || count($checkId) == 0) {
            return redirect('restoran');
        }

        $dataCabang = Branch::join('ref_countries','ref_countries.country_id','=','ref_branches.branch_country_id')
        ->join('ref_province','ref_province.province_id','=','ref_branches.branch_province_id')
        ->where([
            ['branch_id','=',$id],
            ['branch_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['branch_info','=',null],
            ['branch_status','=',1],
            ['branch_log_id','=',null],
        ])
        ->get()
        ->first();

        return view('cabang.view',[
            'dataCabang' => $dataCabang,
            'id' => $id,
        ]);
    }

    public function order(Request $req,$id)
    {
        if ($req->isMethod('GET')) {
         


        }else if($req->isMethod('POST')){

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
