<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Branch;
use App\Models\News;
use App\Models\OrderItem;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Exceptions\Handler;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class WelcomeController extends Controller
{

    public function test(){
        $name = 'Rexa AKbar Malik';
       Mail::to('akbarrexa@gmail.com')->send(new SendMailable($name));
       
       return 'Email was sent';
    }

    public function index()
    {

        $dataCabangCompany = Branch::join('ref_countries','ref_countries.country_id','=','ref_branches.branch_country_id')
        ->join('ref_province','ref_province.province_id','=','ref_branches.branch_province_id')
        ->where([
            ['ref_branches.branch_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['ref_branches.branch_info','=',null],
            ['ref_branches.branch_status','=',1],
            ['ref_branches.branch_log_id','=',null],
        ])
        ->get();

        $dataBeritaCompany = News::where([
            ['news_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['news_status','=',1],
            ['news_log_id','=',null],
        ])
        ->orderBy('news_id','desc')
        ->limit(4)
        ->orderBy('news_id','asc')
        ->get();

        $dataMenuFavorit = OrderItem::selectRaw('SUM(trx_order_items.orderitem_quantity) as quantity_item, ref_product_items.productitem_id')
        ->join('ref_product_items','ref_product_items.productitem_id','=','trx_order_items.orderitem_productitem_id')
        ->join('ref_product_categories','ref_product_categories.productcate_id','=','ref_product_items.productitem_productcate_id')
        ->join('ref_branches','ref_branches.branch_id','=','ref_product_categories.productcate_branch_id')
        ->join('ref_product_images','ref_product_images.productimage_productitem_id','=','ref_product_items.productitem_id')
        ->where([
            ['trx_order_items.orderitem_status','=',3],
            ['ref_product_items.productitem_status','=',1],
            ['ref_product_items.productitem_log_id','=',null],
            ['ref_product_categories.productcate_status','=',1],
            ['ref_product_categories.productcate_log_id','=',null],
            ['ref_branches.branch_company_id','=',env('COMPANY_CAFE_ID',null)],
            ['ref_branches.branch_info','=',null],
            ['ref_branches.branch_status','=',1],
            ['ref_branches.branch_log_id','=',null],
        ])
        ->groupBy('ref_product_items.productitem_id')
        ->orderBy('quantity_item','desc')
        ->limit(3)
        ->get();

        // dd($dataMenuFavorit);

        foreach ($dataBeritaCompany as $value) {
            $value->description_news = $this->word_limit($value->news_body,37);
        }

        // dd($dataBeritaCompany);

        return view('welcome',[
            'dataCabangCompany' => $dataCabangCompany,
            'dataBeritaCompany' => $dataBeritaCompany,
            'dataMenuFavorit' => $dataMenuFavorit,
        ]);
    }

    public function word_limit($text,$limit=10){
        if(strlen($text)>$limit){
            $word = mb_substr($text,0,$limit-3)."...";
        }
        else{
            $word = $text;
        }
        return $word;
    }
    
}