<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Company;
use Session;
class CreateSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $dataCompany = Company::join('ref_countries','ref_countries.country_id','=','ref_companies.company_country_id')
        ->join('ref_province','ref_province.province_id','=','ref_companies.company_province_id')
        ->where([
            ['ref_companies.company_id','=',env('COMPANY_CAFE_ID',null)]
        ])
        ->get()
        ->first();
        
        session()->flush();
        Session::forget([
            'CompanyCode',
            'CompanyName',
            'CompanyDescription',
            'CompanyAddress',
            'CompanyCity',
            'CompanyDistrict',
            'CompanyProvince',
            'CompanyCountry',
            'CompanyEmail',
            'CompanyCp',
            'CompanyPhone',
            'CompanyFax',
            'CompanyLatitude',
            'CompanyLongitude',            
        ]);
        Session::put([
            'CompanyCode' => is_null($dataCompany->company_code)?'-':$dataCompany->company_code,
            'CompanyName' => is_null($dataCompany->company_name)?'-':$dataCompany->company_name,
            'CompanyDescription' => is_null($dataCompany->company_description)?'-':$dataCompany->company_description,
            'CompanyAddress' => is_null($dataCompany->company_address)?'-':$dataCompany->company_address,
            'CompanyCity' => is_null($dataCompany->company_city)?'-':$dataCompany->company_city,
            'CompanyDistrict' => is_null($dataCompany->company_district)?'-':$dataCompany->company_district,
            'CompanyProvince' => is_null($dataCompany->province_name)?'-':$dataCompany->province_name,
            'CompanyCountry' => is_null($dataCompany->country_name)?'-':$dataCompany->country_name,
            'CompanyEmail' => is_null($dataCompany->company_email)?'-':$dataCompany->company_email,
            'CompanyCp' => is_null($dataCompany->company_cp)?'-':$dataCompany->company_cp,
            'CompanyPhone' => is_null($dataCompany->company_phone)?'-':$dataCompany->company_phone,
            'CompanyFax' => is_null($dataCompany->company_fax)?'-':$dataCompany->company_fax,
            'CompanyLatitude' => is_null($dataCompany->company_latitude)?'-':$dataCompany->company_latitude,
            'CompanyLongitude' => is_null($dataCompany->company_longitude)?'-':$dataCompany->company_longitude,
        ]);

        Session::save();

        // dd(session()->get('CompanyName'));

        return $next($request);
    }
}
