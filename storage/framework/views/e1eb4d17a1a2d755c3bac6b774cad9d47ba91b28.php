<script src="<?php echo e(asset('storage/js/vendor/jquery-2.2.4.min.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="<?php echo e(asset('storage/js/vendor/bootstrap.min.js')); ?>"></script>      
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="<?php echo e(asset('storage/js/easing.min.js')); ?>"></script>      
<script src="<?php echo e(asset('storage/js/hoverIntent.js')); ?>"></script>
<script src="<?php echo e(asset('storage/js/superfish.min.js')); ?>"></script> 
<script src="<?php echo e(asset('storage/js/jquery.ajaxchimp.min.js')); ?>"></script>
<script src="<?php echo e(asset('storage/js/jquery.magnific-popup.min.js')); ?>"></script> 
<script src="<?php echo e(asset('storage/js/owl.carousel.min.js')); ?>"></script>      
<script src="<?php echo e(asset('storage/js/jquery.sticky.js')); ?>"></script>
<script src="<?php echo e(asset('storage/js/jquery.nice-select.min.js')); ?>"></script>      
<script src="<?php echo e(asset('storage/js/parallax.min.js')); ?>"></script>  
<script src="<?php echo e(asset('storage/js/mail-script.js')); ?>"></script> 
<script src="<?php echo e(asset('storage/js/main.js')); ?>"></script>
<!-- DataTables -->
<script src="<?php echo e(asset('storage/js/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('storage/js/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>