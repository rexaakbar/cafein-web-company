<header id="header" id="home">
  <div class="container">
    <div class="row align-items-center justify-content-between d-flex">
      <div id="logo">
        <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('storage/img/logo.png')); ?>" alt="" title="" /></a>
      </div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <?php if(Request::is('/')): ?>
            <li><a href="#home"><strong>Beranda</strong></a></li>
            <li><a href="#menu-favorit"><strong>Menu Favorit</strong></a></li>
            <li><a href="#restoran-kami"><strong>Restoran</strong></a></li>
            <li><a href="#berita-terkini"><strong>Berita</strong></a></li>
          <?php else: ?>
            <li><a href="<?php echo e(url('/')); ?>"><strong>Beranda</strong></a></li>
            <li><a href="<?php echo e(url('restoran')); ?>"><strong>Restoran</strong></a></li>
            <li><a href="<?php echo e(url('berita')); ?>"><strong>Berita</strong></a></li>
          <?php endif; ?>
          <li><a href="<?php echo e(url('order')); ?>"><strong>Pesan Makanan</strong></a></li>
          
          
          
          
        </ul>
      </nav><!-- #nav-menu-container -->            
    </div>
  </div>
</header><!-- #header -->