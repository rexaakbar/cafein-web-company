<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php echo Request::is('home*') ? 'active' : ''; ?>">
            
            <a href="<?php echo e(url('home')); ?>">
              <i class="fa fa-book"></i> <span>Home</span>
            </a>
          </li>
        @role(['superadmin','sub_leader','supervisor','assist_manager','pga_manager'])
        <li class="treeview <?php echo Request::is('absensi/*') ? 'active' : ''; ?>">
          <a href="#">
            <i class="fa fa-book"></i> <span>Rekap Absensi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            @role(['sub_leader','supervisor','assist_manager','pga_manager','superadmin'])
            <li class="<?php echo Request::is('absensi/pengajuan_lembur*') ? 'active' : ''; ?>">
              <a href="<?php echo e(url('absensi/pengajuan_lembur')); ?>"><i class="fa fa-circle-o"></i> Pengajuan Lembur</a>
            </li>
            @endrole
          </ul>
        </li>
        @endrole
        @role('superadmin')
          <li class="treeview <?php echo Request::is('master/*') ? 'active' : ''; ?>">
            <a href="#">
              <i class="fa fa-book"></i> <span>Data Master</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="<?php echo Request::is('master/departemen*') ? 'active' : ''; ?>">
                <a href="<?php echo e(url('master/departemen')); ?>"><i class="fa fa-circle-o"></i> Departemen</a>
              </li>
              <li class="<?php echo Request::is('master/grup*') ? 'active' : ''; ?>">
                <a href="<?php echo e(url('master/grup')); ?>"><i class="fa fa-circle-o"></i> Grup</a>
              </li>
              <li class="<?php echo Request::is('master/unit*') ? 'active' : ''; ?>">
                <a href="<?php echo e(url('master/unit')); ?>"><i class="fa fa-circle-o"></i> Unit</a>
              </li>
              <li class="<?php echo Request::is('master/pegawai*') ? 'active' : ''; ?>">
                <a href="<?php echo e(url('master/pegawai')); ?>"><i class="fa fa-circle-o"></i> Pegawai</a>
              </li>
              <li class="<?php echo Request::is('master/aktivitas*') ? 'active' : ''; ?>">
                <a href="<?php echo e(url('master/aktivitas')); ?>"><i class="fa fa-circle-o"></i> Aktivitas</a>
              </li>
              <li class="<?php echo Request::is('master/role*') ? 'active' : ''; ?>">
                <a href="<?php echo e(url('master/role')); ?>"><i class="fa fa-circle-o"></i> Role</a>
              </li>
            </ul>
          </li>
          <li class="<?php echo Request::is('konfirmasi_profile*') ? 'active' : ''; ?>">
            <a href="<?php echo e(url('konfirmasi_profile')); ?>"><i class="fa fa-book"></i> <span>Konfirmasi Profile</span></a>
          </li>
        @endrole
        @role(['sub_leader','supervisor','assist_manager'])
        <li class="<?php echo Request::is('mengetuai*') ? 'active' : ''; ?>">
            
            <a href="<?php echo e(url('mengetuai')); ?>">
              <i class="fa fa-book"></i> <span>Mengetuai</span>
            </a>
          </li>
        @endrole
       <li class="header">MENU LAINNYA</li>
        <li class="<?php echo Request::is('profile*') ? 'active' : ''; ?>">
          <a href="<?php echo e(url('profile')); ?>"><i class="fa fa-circle-o text-green"></i> <span>Biodata</span></a>
        </li>
        <li>
          
          <a href="#"
             onclick="callAlertLogout();">
              <i class="fa fa-circle-o text-red"></i> <span>Sign Out</span>
          </a>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>