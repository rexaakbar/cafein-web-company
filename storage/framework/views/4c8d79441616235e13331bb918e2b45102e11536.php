<?php $__env->startSection('title', Session::get('CompanyName').' | Order Makanan'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- About Generic Start -->
		<div class="main-wrapper">

			<section class="team-area pt-150" id="team">

				<div class="container">
					<div class="panel panel-default pb-40">
						<div class="panel-heading">
							<div class="panel-title">
								<h1 class="about-title text-center"><i class="fa fa-fw fa-cutlery"></i> Order Makanan <i class="fa fa-fw fa-cutlery"></i></h1>
							</div>
						</div>
					</div>

					<div class="panel-body">

						<div class="row justify-content-center d-flex align-items-center">

							<div class="col-md-10 col-md-offset-2">
							
								<?php echo Form::model('' , ['files' => true,'class' => 'form-horizontal ']); ?>

								<div class="form-group">
									<label class="control-label col-md-4">Nama Restoran</label>
									<div class="col-md-12">
										<?php if(count($cekDataCabang) == 1): ?>
											<?php echo Form::text('nama_cabang1',$cekDataCabang[0]->branch_name,[
												'class' => 'form-control',
												'disabled' => 'true',
												'id' => 'namaCabang1',
											]); ?>

											<?php echo Form::hidden('nama_cabang',$cekDataCabang[0]->branch_id,[
												'class' => 'form-control',
												'id' => 'namaCabang',
											]); ?>

										<?php else: ?>
											<?php echo Form::select('nama_cabang',$dataCabang,'',[
												'class' => 'form-control',
												'id' => 'namaCabang'
											]); ?>

										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-12 text-right">
									<p>
										<button class="btn btn-success add-more-menu-pemesan" type="button">
				                            <i class="fa fa-fw fa-plus"></i> Tambah Menu
				                        </button>
									</p>
								</div>
								<div class="col-md-12 text-center">
				                <div class="table-responsive">
				                  <table id="tableMenuPemesan" class="table table-hover table-striped" datatables>
				                    <thead>
				                      <tr>
				                        <th></th>
				                        <th></th>
				                        <th class="text-center">Nama Menu</th>
				                        <th class="text-center">Harga Satuan</th>
				                        <th class="text-center">Jumlah Dibeli</th>
				                        <th class="text-center">Total Harga</th>
				                        <th class="text-center">Catatan Pesanan</th>
				                        <th class="text-center">
				                          
				                        </th>
				                      </tr>
				                    </thead>
				                    <tbody class="after-add-more-menu-pemesan" id="bodyListMenuPemesan">

				                    </tbody>
				                  </table>
				                </div>
				              </div>
				              
				              <div class="col-md-12">
				              	<div class="row">
				              		<div class="col-md-6">
				              			<div class="text-right">
				              				<?php echo Form::label('label_total_harga_semua_menu','Total Harga Keseluruhan :',[
				                            'class' => 'control-label',
				                            'id' => 'label_total_harga_semua_menu'
				                          ]); ?>

				              			</div>
				              		</div>
				              		<div class="col-md-6">
				              			<?php echo Form::label('label_total_harga_rupiah_semua_menu','Rp. 0',[
				                            'class' => 'control-label',
				                            'id' => 'label_total_harga_rupiah_semua_menu'
				                          ]); ?>

				              			<?php echo Form::hidden('total_harga_rupiah_semua_menu',0,[
				                            'class' => 'form-control'
				                            ]); ?>

				              		</div>
				              	</div>	
				              </div>

				              <table>
				                <thead>
				                  <tr>
				                      <th></th>
				                      <th></th>
				                      <th></th>
				                      <th></th>
				                      <th></th>
				                      <th></th>
				                      <th></th>
				                      <th></th>
				                    </tr>
				                </thead>
				                <tbody class="copy-menu-pemesan" style="display: none;">
				                    <tr class="control-group">
				                    	<td style="vertical-align: middle;">
				                          <?php echo Form::button('<i class="fa fa-search"></i> Cari',[
				                            'class' => 'btn btn-primary',
				                            'onclick' => 'pilihMenuPemesan()',
				                            'id' => 'btnMenuPemesan'
				                            ]); ?>

				                          <?php echo Form::hidden('id_menu_pemesan[]','',[
				                            'class' => 'form-control'
				                            ]); ?>

				                          <?php echo Form::hidden('harga_menu_pemesan[]',0,[
				                            'class' => 'form-control'
				                            ]); ?>

				                        </td>
				                      <td style="vertical-align: middle;">
				                      	<div class="col-md-6 text-center mx-auto">
		                      				<a id="aImgMenu" href="<?php echo e(asset('storage/img/f1.png')); ?>" rel="lightbox" class="thumbnail">
			                      				<img id="imgMenu" rel="lightbox" style="width:80px;margin-left:-20px;" src="<?php echo e(asset('storage/img/f1.png')); ?>" alt="">
			                      			</a>
			                      		</div>
				                      </td>
				                        <td style="vertical-align: middle;">
				                          <?php echo Form::label('label_nama_menu','-',[
				                            'class' => 'control-label',
				                            'id' => 'label_nama_menu'
				                          ]); ?>

				                        </td>
				                        <td style="vertical-align: middle;">
				                          <?php echo Form::label('label_harga_satuan','Rp. 0',[
				                            'class' => 'control-label',
				                            'id' => 'label_harga_satuan'
				                          ]); ?>

				                        </td>
				                        <td style="vertical-align: middle;">
				                        	<?php echo Form::select('jumlah_dibeli_pemesan[]',[
				                        			1 => 1,
				                        			2 => 2,
				                        			3 => 3,
				                        			4 => 4,
				                        			5 => 5,
				                        			6 => 6,
				                        			7 => 7,
				                        			8 => 8,
				                        			9 => 9,
				                        			10 => 10,
				                        		],'',[
				                        		'class' => 'form-control',
				                        		'disabled' => true,
				                        		'min' => 1,
				                        		'onchange' => 'hitungTotalHargaMenu()',
				                        		'id' => 'totalHargaMenu',
				                        		]); ?>

				                        </td>
				                        <td style="vertical-align: middle;">
				                          <?php echo Form::label('label_total_harga','Rp. 0',[
				                            'class' => 'control-label',
				                            'id' => 'label_total_harga'
				                          ]); ?>

				                          <?php echo Form::hidden('total_harga_menu[]',0,[
				                            'class' => 'form-control'
				                            ]); ?>

				                        </td>
				                        <td style="vertical-align: middle;">
				                          <?php echo Form::text('catatan_pesanan[]','',[
				                            'class' => 'form-control',
				                            'id' => 'catatan_pesanan',
				                            'disabled' => true
				                            ]); ?>

				                        </td>
				                      <td class="text-center" style="vertical-align: middle;">
				                        <button class="btn btn-danger remove-menu-pemesan" type="button"><i class="fa fa-fw fa-times"></i></button>
				                      </td>
				                    </tr>
				                </tbody>
				              </table>

								<div class="form-group">
									<div class="col-md-12">
										<label class="control-label">Nama Pemesan</label>
									</div>
									<div class="col-md-12">
										<?php echo Form::text('nama_pemesan','',[
											'class' => 'form-control',
											'id' => 'namaPemesan',
											'placeholder' => 'Nama Pemesan'
										]); ?>

									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label class="control-label">E-mail Pemesan</label>
									</div>
									<div class="col-md-12">
										<?php echo Form::text('email_pemesan','',[
											'class' => 'form-control',
											'id' => 'emailPemesan',
											'placeholder' => 'example@example.com'
										]); ?>

									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label class="control-label">No. Handphone / Telephone Pemesan</label>
									</div>
									<div class="col-md-12">
										<?php echo Form::text('no_hp_pemesan','',[
											'class' => 'form-control',
											'id' => 'noHpPemesan',
											'placeholder' => '08XXXXXXXXXX'
										]); ?>

									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label class="control-label">Alamat Pemesan</label>
									</div>
									<div class="col-md-12">
										<?php echo Form::textarea('alamat_pemesan','',[
											'class' => 'form-control',
											'id' => 'alamatPemesan',
											'rows' => '3'
										]); ?>

									</div>
								</div>
								
								

								<div class="col-md-12 text-right pb-20">
				                    <?php echo Form::button('<i class="fa fa-fw fa-cutlery"></i> Pesan',[
				                        'class' => 'btn btn-success',
				                        'type' => 'submit'
				                        ]); ?>

				                  </div>
								
								<?php echo Form::close(); ?>


							</div>
						</div>
						
					</div>
					
				</div>

			</section>

		</div>

		<div id="modal_pilih_menu_pemesan" class="modal fade animated" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
              		<div class="modal-title">
              			<h4>
              				Silahkan pilih menu yang ada di bawah ini
              			</h4>
              		</div>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                  <div class="panel-body">
                      <div class="row" style="width:105%;">
                      	<div class="col-md-12" style="width:100%;">
                      		<div class="table-responsive" style="overflow: hidden;width:100%;">
	                          <table class="table table-striped table-hover" id="tableMenuPemesan1">
	                              <thead>
	                                  <tr>
	                                      <th class="text-center" style="vertical-align: middle;">Gambar Menu</th>
	                                      <th class="text-center" style="vertical-align: middle;">Nama Menu</th>
	                                      <th class="text-center" style="vertical-align: middle;">Harga Satuan</th>
	                                      <th></th>
	                                  </tr>
	                              </thead>
	                              <tbody id="listMenuPemesan">
	                                
	                              </tbody>
	                          </table>
	                      </div>
                      	</div>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascripts'); ?>

	<script type="text/javascript">
		$(document).ready(function() {
			if(document.getElementById("mapPemesan")){
		            
	            google.maps.event.addDomListener(window, 'load', init);
	        
	            function init() {
	                // Basic options for a simple Google Map
	                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
	                var mapOptions = {
	                    // How zoomed in you want the map to start at (always required)
	                    zoom: 15,

	                    // The latitude and longitude to center the map (always required)
	                    center: new google.maps.LatLng(-6.914744,107.609810), // New York

	                    // How you would like to style the map. 
	                    // This is where you would paste any style found on Snazzy Maps.
	                    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
	                };

	                // Get the HTML DOM element that will contain your map 
	                // We are using a div with id="map" seen below in the <body>
	                var mapElement = document.getElementById('mapPemesan');

	                // Create the Google Map using our element and options defined above
	                var map = new google.maps.Map(mapElement, mapOptions);

	                // Let's also add a marker while we're at it
	                var marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(-6.914744,107.609810),
	                    map: map
	                });
	            }
		    }
		});
	</script>

	<script type="text/javascript">
        $(document).ready(function() {
          $(".add-more-menu-pemesan").click(function(){ 
              var html = $(".copy-menu-pemesan").html();
              $(".after-add-more-menu-pemesan").append(html);
              for(i = 0;i < $('button[id="btnMenuPemesan"]').length;i++){
                $('button[id="btnMenuPemesan"]:eq('+i+')').attr('onclick','pilihMenuPemesan('+i+')');
              }
              for(i = 0;i < $('select[id="totalHargaMenu"]').length;i++){
                $('select[id="totalHargaMenu"]:eq('+i+')').attr('onchange','hitungTotalHargaMenu('+i+')');
              }
          });
          $("body").on("click",".remove-menu-pemesan",function(){ 
              var test = $(this).parents(".control-group").remove();
              console.log(test);
              for(i = 0;i < $('button[id="btnMenuPemesan"]').length;i++){
                $('button[id="btnMenuPemesan"]:eq('+i+')').attr('onclick','pilihMenuPemesan('+i+')');
              }
              for(i = 0;i < $('select[id="totalHargaMenu"]').length;i++){
                $('select[id="totalHargaMenu"]:eq('+i+')').attr('onchange','hitungTotalHargaMenu('+i+')');
              }

              var resultTotalRupiahHargaMenu = 0;

    		for(i = 0;i < $('select[id="totalHargaMenu"]').length;i++){
	            resultTotalRupiahHargaMenu += parseInt($('input[name="total_harga_menu[]"]:eq('+i+')').val());
	          }

	        $('label[id="label_total_harga_rupiah_semua_menu"]').text('Rp. '+number_format(parseInt(resultTotalRupiahHargaMenu),0,',','.'));
	        $('input[name="total_harga_rupiah_semua_menu"]').val(parseInt(resultTotalRupiahHargaMenu));
          });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#tableMenuPemesan1').DataTable(function(){
                responsive : true 
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#namaCabang').change(function(){
                 $('#bodyListMenuPemesan').html('');
                 $('label[id="label_total_harga_rupiah_semua_menu"]').text('Rp. '+parseInt(0));
				 $('input[name="total_harga_rupiah_semua_menu"]').val(parseInt(0));
            });
        });
    </script>

    <script type="text/javascript">
	      function pilihMenuPemesan(urutanMenuPemesan){
	        if ( $.fn.DataTable.isDataTable('#tableMenuPemesan1') ) {
	          $('#tableMenuPemesan1').DataTable().destroy();
	        }

	        $.ajax({
	            type: "POST",
	            url: "order/cariMenuPemesan",
	            data: { idCabang : $('#namaCabang').val() , _token : '<?php echo e(csrf_token()); ?>' },
	            dataType: "json",
	            error: function(e){
	              alert('Pilin restoran terlebih dahulu');
	            },
	            success: function (respond) {
	                var menu_pemesan = "";
	                for(var i=0; i<respond.length; i++){
	                    menu_pemesan += '<tr>'+
	                                '<td style="vertical-align: middle;"><div class="col-md-3 mx-auto"><a id="aImgMenu" href="<?php echo e(asset('storage/img/')); ?>/'+ respond[i].productimage_path +'" rel="lightbox" class="thumbnail"><img id="imgMenu" rel="lightbox" style="width:50px;margin-left:-25px;" src="<?php echo e(asset('storage/img/')); ?>/'+ respond[i].productimage_path +'" alt=""></a></div></td>'+
	                                '<td style="vertical-align: middle;">'+respond[i].productitem_name+'</td>'+
	                                '<td style="vertical-align: middle;">Rp. '+number_format(parseInt(respond[i].productitem_normal_price),0,',','.')+'</td>'+
	                                '<td class="vertical-align:middle;"><input type="button" class="btn btn-primary btn-sm pull-right" value="pilih" onclick=\"replaceListDataMenuPemesan(\''+urutanMenuPemesan+'\' , \''+respond[i].productitem_id+'\')\"/></td>'+
	                            '</tr>';
	                }
	                $("#listMenuPemesan").html(menu_pemesan);

	                $('#tableMenuPemesan1').DataTable({
	                    responsive : true,
	                    "bFilter": true,
						"bInfo": false,
						"bLengthChange": true,
						oLanguage: {
						    sLengthMenu: "_MENU_",
						}
	                });

	                // $('#tableMenuPemesan1_wrapper').removeClass('form-inline');
	                
	                $("#modal_pilih_menu_pemesan").modal('toggle');
	            }
	        });

	    }

	    function replaceListDataMenuPemesan(urutanMenuPemesan,idMenuPemesan){
	      var cekData = false;
	      for(i = 0;i < $('button[id="btnMenuPemesan"]').length;i++){
	          if($('input[name="id_menu_pemesan[]"]:eq('+i+')').val() == idMenuPemesan){
	              cekData = true;
	          }
	      }
	      
	      $("#modal_pilih_menu_pemesan").modal('toggle');

	      if(cekData){
	          alert('Menu tersebut sudah dipilih sebelumnya')
	      }else{
	          $.ajax({
	              type : "POST",
	              url : "order/getMenuPemesan",
	              data: { idMenuPemesan : idMenuPemesan , _token : '<?php echo e(csrf_token()); ?>' },
	              dataType : "json",
	              success: function(respond){

	                  $('a[id="aImgMenu"]:eq('+urutanMenuPemesan+')').attr('href','<?php echo e(asset('storage/img')); ?>/' + respond.productimage_path);

	                  $('img[id="imgMenu"]:eq('+urutanMenuPemesan+')').attr('src','<?php echo e(asset('storage/img')); ?>/' + respond.productimage_path);

	                  $('input[name="id_menu_pemesan[]"]:eq('+urutanMenuPemesan+')').val(respond.productitem_id);

	                  $('input[name="harga_menu_pemesan[]"]:eq('+urutanMenuPemesan+')').val(parseInt(respond.productitem_normal_price));

	                  $('select[name="jumlah_dibeli_pemesan[]"]:eq('+urutanMenuPemesan+')').removeAttr('disabled');

	                  $('label[id="label_nama_menu"]:eq('+urutanMenuPemesan+')').text(respond.productitem_name);

	                  $('label[id="label_harga_satuan"]:eq('+urutanMenuPemesan+')').text('Rp. '+number_format(parseInt(respond.productitem_normal_price),0,',','.'));

	                  var qtyMenuPemesan = $('select[name="jumlah_dibeli_pemesan[]"]:eq('+urutanMenuPemesan+')').val();

			    		var hargaMenuPemesan = $('input[name="harga_menu_pemesan[]"]:eq('+urutanMenuPemesan+')').val();

			    		var resultTotalHargaMenu = qtyMenuPemesan * hargaMenuPemesan;

			    		$('label[id="label_total_harga"]:eq('+urutanMenuPemesan+')').text('Rp. '+number_format(parseInt(resultTotalHargaMenu),0,',','.'));

			    		$('input[name="catatan_pesanan[]"]:eq('+urutanMenuPemesan+')').removeAttr('disabled');
			    		
			    		$('input[name="total_harga_menu[]"]:eq('+urutanMenuPemesan+')').val(parseInt(resultTotalHargaMenu));

			    		var resultTotalRupiahHargaMenu = 0;

			    		for(i = 0;i < $('select[id="totalHargaMenu"]').length;i++){
				            resultTotalRupiahHargaMenu += parseInt($('input[name="total_harga_menu[]"]:eq('+i+')').val());
				          }

				        $('label[id="label_total_harga_rupiah_semua_menu"]').text('Rp. '+number_format(parseInt(resultTotalRupiahHargaMenu),0,',','.'));
				        $('input[name="total_harga_rupiah_semua_menu"]').val(parseInt(resultTotalRupiahHargaMenu));
	                 
	              }
	          });
	      }
	      
	  }
    </script>

    <script type="text/javascript">
    	function hitungTotalHargaMenu(urutanMenuPemesan){
    		var qtyMenuPemesan = $('select[name="jumlah_dibeli_pemesan[]"]:eq('+urutanMenuPemesan+')').val();

    		var hargaMenuPemesan = $('input[name="harga_menu_pemesan[]"]:eq('+urutanMenuPemesan+')').val();

    		var resultTotalHargaMenu = qtyMenuPemesan * hargaMenuPemesan;

    		$('label[id="label_total_harga"]:eq('+urutanMenuPemesan+')').text('Rp. '+number_format(parseInt(resultTotalHargaMenu),0,',','.'));

    		$('input[name="total_harga_menu[]"]:eq('+urutanMenuPemesan+')').val(parseInt(resultTotalHargaMenu));

    		var resultTotalRupiahHargaMenu = 0;

    		for(i = 0;i < $('select[id="totalHargaMenu"]').length;i++){
	            resultTotalRupiahHargaMenu += parseInt($('input[name="total_harga_menu[]"]:eq('+i+')').val());
	          }

	        $('label[id="label_total_harga_rupiah_semua_menu"]').text('Rp. '+number_format(parseInt(resultTotalRupiahHargaMenu),0,',','.'));
	        $('input[name="total_harga_rupiah_semua_menu"]').val(parseInt(resultTotalRupiahHargaMenu));

    	}
    </script>

	

	

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>