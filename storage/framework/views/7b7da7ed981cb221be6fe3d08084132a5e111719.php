<?php $__env->startSection('title', ' | Restoran - Detail'); ?>

<?php $__env->startSection('content'); ?>
	
<!-- About Generic Start -->
	<div class="main-wrapper">
		<!-- Start Generic Area -->
		<section class="about-generic-area pt-60 mt-30">
			<div class="container border-top-generic">
				<h3 class="about-title mb-30">Detail Restoran - <?php echo e($dataCabang->branch_name); ?></h3>
				<div class="row">
					<div class="col-md-12">
						<div class="img-text">
							<div class="col-md-5">
								<a href="<?php echo e(asset('storage/img/branch/'.$dataCabang->branch_photo)); ?>" rel="lightbox" class="thumbnail">
									<img src="<?php echo e(asset('storage/img/branch/'.$dataCabang->branch_photo)); ?>" alt="" class="img-fluid float-left mr-20 mb-20">
								</a>
							</div>
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-1">
										<h1><i class="fa fa-fw fa-cutlery"></i></h1>
									</div>
									<div class="col-md-8">
										<h4>Nama Restoran</h4>
										<strong>
											<?php echo e($dataCabang->branch_name); ?>

										</strong>
									</div>
								</div>
								<div class="row">
									<div class="col-md-1">
										<p>
											<h1><i class="fa fa-fw fa-map-marker"></i></h1>
										</p>
									</div>
									<div class="col-md-8">
										<p>
											<h4>Lokasi & Alamat Restoran</h4>
											<strong>
												<?php echo e($dataCabang->branch_city.', '.$dataCabang->province_name.', '.$dataCabang->country_name); ?>

												<?php echo e(' | '); ?>

												<?php echo e($dataCabang->branch_address); ?>

											</strong>
										</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-1">
										<h1><i class="fa fa-fw fa-envelope"></i></h1>
									</div>
									<div class="col-md-8">
										<h4>E-mail Restoran</h4>
										<strong>
											<?php echo e((is_null($dataCabang->branch_email))?' - ':$dataCabang->branch_email); ?>

										</strong>
									</div>
								</div>
								<div class="row">
									<div class="col-md-1">
										<p>
											<h1><i class="fa fa-fw fa-phone"></i></h1>
										</p>
									</div>
									<div class="col-md-8">
										<p>
											<h4>CP & No. Telp Restoran</h4>
											<strong>
												<?php echo e((is_null($dataCabang->branch_cp))?' - ':$dataCabang->branch_cp); ?>

												<?php echo e('&'); ?>

												<?php echo e((is_null($dataCabang->branch_phone))?' - ':$dataCabang->branch_phone); ?>

											</strong>
										</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-1">
										<h1><i class="fa fa-fw fa-fax"></i></h1>
									</div>
									<div class="col-md-6">
										<h4>No. Fax Restoran</h4>
										<strong>
											<?php echo e((is_null($dataCabang->branch_fax))?' - ':$dataCabang->branch_fax); ?>

										</strong>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</section>
		<!-- End Generic Start -->	

		<!-- Start Contact Area -->
		<section class="contact-area pb-30 pt-50" id="contact">
			<div class="container-fluid">
				<div class="row align-items-center d-flex justify-content-start">
					<div class="col-md-12 pb-30">
						<div class="form-group">
							<div class="text-center">
								<h3>Letak Restoran</h3>
							</div>
						</div>
					</div>
					<div class="col-md-offset-1 col-md-10 contact-center">
      					<div class="iframe-container">
      						<div class="text-center display-block">
      							<div style=" width:100%; height: 545px;" class="" id="map"></div>
      						</div>
      					</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Contact Area -->
		
	</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascripts'); ?>
	<script type="text/javascript">
		$(document).ready(function() {
			if(document.getElementById("map")){
		            
	            google.maps.event.addDomListener(window, 'load', init);
	        
	            function init() {
	                // Basic options for a simple Google Map
	                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
	                var mapOptions = {
	                    // How zoomed in you want the map to start at (always required)
	                    zoom: 11,

	                    // The latitude and longitude to center the map (always required)
	                    center: new google.maps.LatLng(<?php echo e($dataCabang->branch_latitude); ?>, <?php echo e($dataCabang->branch_longitude); ?>), // New York

	                    // How you would like to style the map. 
	                    // This is where you would paste any style found on Snazzy Maps.
	                    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
	                };

	                // Get the HTML DOM element that will contain your map 
	                // We are using a div with id="map" seen below in the <body>
	                var mapElement = document.getElementById('map');

	                // Create the Google Map using our element and options defined above
	                var map = new google.maps.Map(mapElement, mapOptions);

	                // Let's also add a marker while we're at it
	                var marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(<?php echo e($dataCabang->branch_latitude); ?>, <?php echo e($dataCabang->branch_longitude); ?>),
	                    map: map,
	                    title: 'Snazzy!'
	                });
	            }
		    }
		});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>