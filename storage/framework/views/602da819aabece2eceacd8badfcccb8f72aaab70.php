<?php $__env->startSection('title', ' | Cabang - Detail'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- About Generic Start -->
		<div class="main-wrapper">
			<!-- Start Generic Area -->
			<section class="about-generic-area section-gap">
				<div class="container border-top-generic">
					<h3 class="about-title mb-30">Detail Cabang - <?php echo e($dataCabang->branch_name); ?></h3>
					<div class="row">
						<div class="col-md-12">
							<div class="img-text">
								<img src="<?php echo e(asset('img/t1.jpg')); ?>" alt="" class="img-fluid float-left mr-20 mb-20">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-4">
											<h4>Nama Cabang</h4>
											<p><?php echo e($dataCabang->branch_name); ?></p>
										</div>
										<div class="col-md-4">
											<h4>Lokasi Cabang</h4>
											<p><?php echo e($dataCabang->branch_city.', '.$dataCabang->province_name.', '.$dataCabang->country_name); ?></p>
										</div>
										<div class="col-md-4">
											<h4>Alamat Cabang</h4>
											<p><?php echo e($dataCabang->branch_address); ?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="text-center">
												<h3>Kontak Cabang</h3>
											</div>
											<div class="row">
												<div class="col-md-4">
													<div class="meta-text mt-30">
														<h4><i class="fa fa-fw fa-envelope"></i> E-mail Cabang</h4>
														<p><?php echo e((is_null($dataCabang->branch_email))?' - ':$dataCabang->branch_email); ?></p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="meta-text mt-30">
														<h4><i class="fa fa-fw fa-mobile"></i> CP Cabang</h4>
														<p><?php echo e((is_null($dataCabang->branch_cp))?' - ':$dataCabang->branch_cp); ?></p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="meta-text mt-30">
														<h4><i class="fa fa-fw fa-phone"></i> No. Telp Cabang</h4>
														<p><?php echo e((is_null($dataCabang->branch_phone))?' - ':$dataCabang->branch_phone); ?></p>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4 col-md-offset-4">
													<div class="meta-text">
														<h4><i class="fa fa-fw fa-fax"></i> No. Fax Cabang</h4>
													</div>
														<p><?php echo e((is_null($dataCabang->branch_fax))?' - ':$dataCabang->branch_fax); ?></p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="text-center">
												<h3>Keterangan Mengenai Cabang</h3>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="meta-text mt-30">
														<p><?php echo e((is_null($dataCabang->branch_description))?' - ':$dataCabang->branch_description); ?></p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<a href="<?php echo e(url('order/'.$id)); ?>" class="primary-btn text-uppercase">
								                <i class="fa fa-fw fa-coffee"></i> Pesan Makanan
								            </a>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Generic Start -->	

			<!-- Start Contact Area -->
			<section class="contact-area section-gap" id="contact">
				<div class="container-fluid">
					<div class="row align-items-center d-flex justify-content-start">
						<div class="col-md-12">
							<div class="text-center">
								<h3>Image Gallery</h3>
							</div>
							<div class="row gallery-item">
								<div class="col-md-4">
									<a href="<?php echo e(asset('img/elements/g1.jpg')); ?>" class="img-pop-up"><div class="single-gallery-image" style="background: url(<?php echo e(asset('img/elements/g1.jpg')); ?>);"></div></a>
								</div>
								<div class="col-md-4">
									<a href="<?php echo e(asset('img/elements/g2.jpg')); ?>" class="img-pop-up"><div class="single-gallery-image" style="background: url(<?php echo e(asset('img/elements/g2.jpg')); ?>);"></div></a>
								</div>
								<div class="col-md-4">
									<a href="<?php echo e(asset('img/elements/g3.jpg')); ?>" class="img-pop-up"><div class="single-gallery-image" style="background: url(<?php echo e(asset('img/elements/g3.jpg')); ?>);"></div></a>
								</div>
								<div class="col-md-6">
									<a href="<?php echo e(asset('img/elements/g4.jpg')); ?>" class="img-pop-up"><div class="single-gallery-image" style="background: url(<?php echo e(asset('img/elements/g4.jpg')); ?>);"></div></a>
								</div>
								<div class="col-md-6">
									<a href="<?php echo e(asset('img/elements/g5.jpg')); ?>" class="img-pop-up"><div class="single-gallery-image" style="background: url(<?php echo e(asset('img/elements/g5.jpg')); ?>);"></div></a>
								</div>
								<div class="col-md-4">
									<a href="<?php echo e(asset('img/elements/g6.jpg')); ?>" class="img-pop-up"><div class="single-gallery-image" style="background: url(<?php echo e(asset('img/elements/g6.jpg')); ?>);"></div></a>
								</div>
								<div class="col-md-4">
									<a href="<?php echo e(asset('img/elements/g7.jpg')); ?>" class="img-pop-up"><div class="single-gallery-image" style="background: url(<?php echo e(asset('img/elements/g7.jpg')); ?>);"></div></a>
								</div>
								<div class="col-md-4">
									<a href="<?php echo e(asset('img/elements/g8.jpg')); ?>" class="img-pop-up"><div class="single-gallery-image" style="background: url(<?php echo e(asset('img/elements/g8.jpg')); ?>);"></div></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Contact Area -->

			<!-- Start Contact Area -->
			<section class="contact-area section-gap" id="contact">
				<div class="container-fluid">
					<div class="row align-items-center d-flex justify-content-start">
						<div class="col-md-12">
							<div class="form-group">
								<div class="text-center">
									<h3>Letak Cabang</h3>
								</div>
							</div>
						</div>
						<div class="col-md-offset-1 col-md-10 contact-center">
	      					<div class="form-group">
	      						<div style=" width:100%;
	                height: 545px;" id="map"></div>
	      					</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Contact Area -->
			
		</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascripts'); ?>
	<script type="text/javascript">
		$(document).ready(function() {
			if(document.getElementById("map")){
		            
	            google.maps.event.addDomListener(window, 'load', init);
	        
	            function init() {
	                // Basic options for a simple Google Map
	                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
	                var mapOptions = {
	                    // How zoomed in you want the map to start at (always required)
	                    zoom: 11,

	                    // The latitude and longitude to center the map (always required)
	                    center: new google.maps.LatLng(<?php echo e($dataCabang->branch_latitude); ?>, <?php echo e($dataCabang->branch_longitude); ?>), // New York

	                    // How you would like to style the map. 
	                    // This is where you would paste any style found on Snazzy Maps.
	                    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
	                };

	                // Get the HTML DOM element that will contain your map 
	                // We are using a div with id="map" seen below in the <body>
	                var mapElement = document.getElementById('map');

	                // Create the Google Map using our element and options defined above
	                var map = new google.maps.Map(mapElement, mapOptions);

	                // Let's also add a marker while we're at it
	                var marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(<?php echo e($dataCabang->branch_latitude); ?>, <?php echo e($dataCabang->branch_longitude); ?>),
	                    map: map,
	                    title: 'Snazzy!'
	                });
	            }
		    }
		});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>