<?php $__env->startSection('title', ' | Restoran'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- About Generic Start -->
		<div class="main-wrapper">

			<!-- Start team Area -->
			<section class="team-area pt-100" id="team">
				<div class="container">
					<h3 class="about-title mb-30 text-center">Restoran Kami</h3>
					<div class="row justify-content-center d-flex align-items-center">
						<?php $__currentLoopData = $dataCabang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="col-md-4  single-team">
							    <div class="thumb">
							        <img class="img-fluid" src="<?php echo e(asset('img/t1.jpg')); ?>" alt="">
							        <div class="align-items-center justify-content-center d-flex">
										<a href="<?php echo e(url('restoran/detail/'.$value->branch_id)); ?>" data-toggle="tooltip" title="Lihat Restoran"><i class="fa fa-eye"></i></a>
										<a href="<?php echo e(url('order/'.$value->branch_id)); ?>" data-toggle="tooltip" title="Pesan Makanan"><i class="fa fa-coffee"></i></a>
										<a href="#" data-toggle="tooltip" title="Perbesar Gambar"><i class="fa fa-search-plus"></i></a>
							        </div>
							    </div>
							    <div class="meta-text mt-30 text-center">
								    <h4><?php echo e($value->branch_name); ?></h4>
								    <p><?php echo e($value->branch_city.', '.$value->province_name.', '.$value->country_name); ?></p>
							    </div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>						
					</div>

					<div class="text-center">
						<div class="form-group">
							<?php echo e($dataCabang->links()); ?>

						</div>
					</div>

				</div>

			</section>
			<!-- End team Area -->
			
		</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>