<?php $__env->startSection('title', Session::get('CompanyName').' | Welcome'); ?>

<?php $__env->startSection('content'); ?>
    <!-- start banner Area -->
      <section id="bannerArea" class="banner-area relative" style="background: url('<?php echo e(asset('storage/img/header-bg.jpg')); ?>') no-repeat; background-size:100%;"  id="home">
        <div class="container">
          <div class="row fullscreen d-flex align-items-center justify-content-start">
                                  
          </div>
        </div>
      </section>
      <!-- End banner Area -->  

      <!-- Start top-dish Area -->
      <section class="top-dish-area pb-10 mb-0 mt-30 pt-30" id="menu-favorit">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-8">
              <div class="title text-center">
                <h1 class="mb-10">Menu Favorit</h1>
                <p>Menu paling banyak dipesan pelanggan.</p>
              </div>
            </div>
          </div>            
          <div class="row">
            <?php if(count($dataMenuFavorit) == 0): ?>
              <div class="col-md-12 mb-20">
                <div class="text-center">
                  <h2>
                    Mohon maaf, belum ada menu favorit apapun yang bisa ditampilkan.
                  </h2>
                </div>
              </div>
            <?php else: ?>
              <?php $__currentLoopData = $dataMenuFavorit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="single-dish col-lg-4">
                  <div class="thumb">
                    <img class="img-fluid"  src="<?php echo e(asset('storage/img/'.$value->productimage_path)); ?>" alt="">
                  </div>
                  <h4 class="text-uppercase pt-20 pb-20"><?php echo e($value->productitem_name); ?></h4>
                  <p>
                    <?php echo e(strip_tags($value->productitem_description)); ?>

                  </p>
                </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
            <?php endif; ?>              
          </div>
        </div>  
      </section>
      <!-- End top-dish Area -->
      
      <!-- Start video Area -->
      
      <!-- End video Area -->
      

      <!-- Start features Area -->
      
      <!-- End features Area -->


      <!-- Start related Area -->
      <section class="related-area pb-0 mb-0 pt-30 mt-10" id="restoran-kami">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-8">
              <div class="title text-center">
                <h1 class="mb-10">Restoran Kami</h1>
                <p>Kini Kami Hadir Di Berbagai Daerah.</p>
              </div>
            </div>
          </div>            
          <div class="row justify-content-center">
            <?php if(count($dataCabangCompany) > 1): ?>
            <div class="active-realated-carusel">
            <?php endif; ?>
                <?php $__currentLoopData = $dataCabangCompany; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item row align-items-center">
                        <div class="col-lg-6 rel-left">
                           <h3>
                              <?php echo e($value->branch_name); ?>

                           </h3>
                           <h4>
                              <?php echo e($value->branch_city.', '.$value->province_name.', '.$value->country_name); ?>

                           </h4>
                           <p class="pt-30 pb-30">
                              <?php echo e(strip_tags($value->branch_description)); ?>

                           </p>
                          <a href="<?php echo e(url('restoran/detail/'.$value->branch_id)); ?>" class="primary-btn header-btn text-uppercase"><i class="fa fa-fw fa-eye"></i> Lihat Detail Restoran</a>
                        </div>
                        <div class="col-lg-6">
                          <img class="img-fluid" src="<?php echo e(asset('storage/img/branch/'.$value->branch_photo)); ?>" alt="">
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>             
            <?php if(count($dataCabangCompany) > 1): ?>
            </div>
            <?php endif; ?>
          </div>
          <?php if(count($dataCabangCompany) > 1): ?>
          <div class="row">
            <div class="col-md-12 text-center mt-80">
              <a href="<?php echo e(url('restoran')); ?>" class="primary-btn header-btn text-uppercase">
                    <i class="fa fa-fw fa-list-ul"></i> Lihat Lebih Banyak
                </a>
            </div>
          </div>
          <?php endif; ?>
        </div>  
      </section>
      <!-- End related Area --> 


      <!-- Start team Area -->
      
      <!-- End team Area -->      

      <!-- start blog Area -->    
      <section class="related-area pt-40 mt-20 mb-0 pb-30" id="berita-terkini">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-8">
              <div class="title text-center">
                <h1 class="mb-10">Berita Terkini</h1>
                <p>Jangan pernah lewatkan satupun berita dari kami.</p>
              </div>
            </div>
          </div>          
          <div class="row">

            <?php if(count($dataBeritaCompany) == 0): ?>
              
              <div class="col-md-12 mb--20">
                <div class="text-center">
                  <h2>
                    Mohon maaf, belum ada berita apapun yang bisa ditampilkan.
                  </h2>
                </div>
              </div>

            <?php else: ?>

              <?php $__currentLoopData = $dataBeritaCompany; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-3 col-md-6 single-blog">
                  <div class="thumb">
                    <img class="img-fluid" src="<?php echo e(asset('storage/img/news/'.$value->news_photo)); ?>" alt="">
                  </div>
                  <p class="date"><?php echo e(date('d-M-Y',strtotime($value->news_created_at))); ?></p>
                  <a href="<?php echo e(url('berita/detail/'.$value->news_id)); ?>"><h4><?php echo e($value->news_title); ?></h4></a>
                  <p>
                    <?php echo e($value->description_news); ?>

                  </p>
                  <a href="<?php echo e(url('berita/detail/'.$value->news_id)); ?>">
                    <p>
                      <u>Lihat Selengkapnya</u>
                    </p>
                  </a>
                </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <?php endif; ?>
          	          
          </div>
          <div class="row">

        	  <?php if(count($dataBeritaCompany) > 4): ?>
            
              <div class="col-md-12 text-center mt-40 ">
                <a href="<?php echo e(url('berita')); ?>" class="primary-btn header-btn text-uppercase">
                      <i class="fa fa-fw fa-list-ul"></i> Lihat Lebih Banyak
                  </a>
              </div>

            <?php endif; ?>
          </div>
        </div>  
      </section>
      <!-- end blog Area -->  

      <!-- Start Contact Area -->
      
      <!-- End Contact Area -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>