<?php $__env->startSection('title', ' | Welcome'); ?>

<?php $__env->startSection('content'); ?>
    <!-- start banner Area -->
      <section class="banner-area relative" id="home">
        <div class="container">
          <div class="row fullscreen d-flex align-items-center justify-content-start">
            <div class="banner-content col-lg-8 col-md-12">
              <h4 class="text-white text-uppercase">Wide Options of Choice</h4>
              <h1>
                Delicious Receipes          
              </h1>
              <p class="text-white">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp <br> or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
              </p>
              <a href="#" class="primary-btn header-btn text-uppercase">Check Our Menu</a>
            </div>                        
          </div>
        </div>
      </section>
      <!-- End banner Area -->  

      <!-- Start top-dish Area -->
      <section class="top-dish-area section-gap" id="dish">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-8">
              <div class="title text-center">
                <h1 class="mb-10">Our Top Rated Dishes</h1>
                <p>Who are in extremely love with eco friendly system.</p>
              </div>
            </div>
          </div>            
          <div class="row">
            <div class="single-dish col-lg-4">
              <div class="thumb">
                <img class="img-fluid"  src="img/d1.jpg" alt="">
              </div>
              <h4 class="text-uppercase pt-20 pb-20">Bread Fruit Cheese Sandwich</h4>
              <p>
                inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct women face higher conduct.
              </p>
            </div>
            <div class="single-dish col-lg-4">
              <div class="thumb">
                <img class="img-fluid"  src="img/d2.jpg" alt="">
              </div>
              <h4 class="text-uppercase pt-20 pb-20">Beef Cutlet with Spring Onion</h4>
              <p>
                inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct women face higher conduct.
              </p>
            </div>
            <div class="single-dish col-lg-4">
              <div class="thumb">
                <img class="img-fluid"  src="img/d3.jpg" alt="">
              </div>
              <h4 class="text-uppercase pt-20 pb-20">Meat with sauce & Vegetables</h4>
              <p>
                inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct women face higher conduct.
              </p>
            </div>                        
          </div>
        </div>  
      </section>
      <!-- End top-dish Area -->
      
      <!-- Start video Area -->
      
      <!-- End video Area -->
      

      <!-- Start features Area -->
      
      <!-- End features Area -->


      <!-- Start related Area -->
      <section class="related-area section-gap" id="cabang-kami">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-8">
              <div class="title text-center">
                <h1 class="mb-10">Restoran Kami</h1>
                <p>Kini Kami Hadir Di Berbagai Daerah.</p>
              </div>
            </div>
          </div>            
          <div class="row justify-content-center">
            <?php if(count($dataCabangCompany) > 1): ?>
            <div class="active-realated-carusel">
            <?php endif; ?>
                <?php $__currentLoopData = $dataCabangCompany; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item row align-items-center">
                        <div class="col-lg-6 rel-left">
                           <h3>
                              <?php echo e($value->branch_name); ?>

                           </h3>
                           <h4>
                              <?php echo e($value->branch_city.', '.$value->province_name.', '.$value->country_name); ?>

                           </h4>
                           <p class="pt-30 pb-30">
                              <?php echo e(strip_tags($value->branch_description)); ?>

                           </p>
                          <a href="<?php echo e(url('restoran/detail/'.$value->branch_id)); ?>" class="primary-btn header-btn text-uppercase"><i class="fa fa-fw fa-eye"></i> Lihat Detail Restoran</a>
                          <a href="<?php echo e(url('order/'.$value->branch_id)); ?>" class="primary-btn header-btn text-uppercase"><i class="fa fa-fw fa-coffee"></i> Pesan Makanan / Minuman</a>
                        </div>
                        <div class="col-lg-6">
                          <img class="img-fluid" src="<?php echo e(asset('storage/img/slider1.jpg')); ?>" alt="">
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>             
            <?php if(count($dataCabangCompany) > 1): ?>
            </div>
            <?php endif; ?>
          </div>
          <div class="row">
        	<div class="col-md-12 text-center mt-80">
        		<a href="<?php echo e(url('cabang')); ?>" class="primary-btn header-btn text-uppercase">
	                <i class="fa fa-fw fa-list-ul"></i> Lihat Lebih Banyak
	            </a>
        	</div>
          </div>
        </div>  
      </section>
      <!-- End related Area --> 


      <!-- Start team Area -->
      
      <!-- End team Area -->      

      <!-- start blog Area -->    
      <section class="related-area section-gap" id="blog">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-8">
              <div class="title text-center">
                <h1 class="mb-10">Berita Terkini</h1>
                <p>Jangan pernah lewatkan satupun berita dari kami.</p>
              </div>
            </div>
          </div>          
          <div class="row">

            <?php if(count($dataBeritaCompany) == 0): ?>
              
              <div class="col-md-12">
                <div class="text-center">
                  <h2>
                    Mohon maaf, belum ada berita apapun yang bisa ditampilkan.
                  </h2>
                </div>
              </div>

            <?php else: ?>

              <?php $__currentLoopData = $dataBeritaCompany; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-3 col-md-6 single-blog">
                  <div class="thumb">
                    <img class="img-fluid" src="<?php echo e(asset('img/b1.jpg')); ?>" alt="">
                  </div>
                  <p class="date"><?php echo e(date('d-M-Y',strtotime($value->news_created_at))); ?></p>
                  <a href="<?php echo e(url('berita/detail/'.$value->news_id)); ?>"><h4><?php echo e($value->news_title); ?></h4></a>
                  <p>
                    <?php echo e($value->description_news); ?>

                  </p>
                  <a href="<?php echo e(url('berita/detail/'.$value->news_id)); ?>">
                    <p>
                      <u>Lihat Selengkapnya</u>
                    </p>
                  </a>
                </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <?php endif; ?>
          	          
          </div>
          <div class="row">

        	  <?php if(count($dataBeritaCompany) > 1): ?>
            
              <div class="col-md-12 text-center mt-80">
                <a href="<?php echo e(url('berita')); ?>" class="primary-btn header-btn text-uppercase">
                      <i class="fa fa-fw fa-list-ul"></i> Lihat Lebih Banyak
                  </a>
              </div>

            <?php endif; ?>
          </div>
        </div>  
      </section>
      <!-- end blog Area -->  

      <!-- Start Contact Area -->
      <section class="contact-area" id="contact">
        <div class="container-fluid">
          <div class="row align-items-center d-flex justify-content-start">
            <div class="col-lg-6 col-md-12 contact-left no-padding">
                  <div style=" width:100%;
                  height: 545px;" id="map"></div>
            </div>
            <div class="col-lg-4 col-md-12 pt-100 pb-100">
              <form class="form-area" id="myForm" action="mail.php" method="post" class="contact-form text-right">
                <input name="fname" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" class="common-input mt-10" required="" type="text">
                <input name="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" class="common-input mt-10" required="" type="email">
                <textarea class="common-textarea mt-10" name="message" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
                <button class="primary-btn mt-20">Send Message<span class="lnr lnr-arrow-right"></span></button>
                <div class="mt-10 alert-msg">
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      <!-- End Contact Area -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>