<?php $__env->startSection('title', ' | Berita - Lihat'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- About Generic Start -->
		<div class="main-wrapper">
			
			<!-- Start Generic Area -->
			<section class="about-generic-area section-gap">
				<div class="container border-top-generic">
					<h3 class="about-title"><?php echo e($dataBerita->news_title); ?></h3>
					<p><?php echo e($waktuPembuatanBerita); ?> | <?php echo e(date('d-m-Y',strtotime($dataBerita->news_created_at))); ?> | <?php echo e($dataBerita->news_city.', '.$dataBerita->province_name.', '.$dataBerita->country_name); ?></p>
					<div class="mb-30"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="img-text">
								<img src="<?php echo e(asset('img/t1.jpg')); ?>" alt="" class="img-fluid float-left mr-20 mb-20">
								<p><?php echo e($dataBerita->news_body); ?></p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Generic Start -->	
			
		</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascripts'); ?>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>