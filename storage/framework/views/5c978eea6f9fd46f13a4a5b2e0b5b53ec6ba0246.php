<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>" class="no-js">
<head>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
  
  <?php echo $__env->make('layouts.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <title><?php echo $__env->yieldContent('title'); ?></title>
</head>
<body>
  
  <?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  

    <!-- Content Header (Page header) -->
    

    <?php echo $__env->yieldContent('content'); ?>

    <?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('layouts.javascript', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('javascripts'); ?>
</body>
</html>
