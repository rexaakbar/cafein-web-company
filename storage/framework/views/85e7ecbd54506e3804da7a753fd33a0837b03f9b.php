<?php $__env->startSection('title', ' | Restoran'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- About Generic Start -->
		<div class="main-wrapper">

			<!-- Start team Area -->
			<section class="team-area mt-60 pt-100 <?php echo e((count($dataCabang) == 0 || is_null($dataCabang))?'pb-100 mb-100':'pb-50'); ?> " id="team">
				<div class="container">
					<h3 class="about-title mb-30 text-center">Restoran Kami</h3>
					<div class="row justify-content-center d-flex align-items-center">

					<?php if(count($dataCabang) == 0 || is_null($dataCabang)): ?>
						<div class="col-md-12 singel-blog mt-30">
							<div class="text-center">
			                  <h2>
			                    Mohon maaf, kami belum membuka cabang dimanapun.
			                  </h2>
			                </div>
						</div>
					<?php else: ?>
						<?php $__currentLoopData = $dataCabang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="col-md-4  single-team">
							    <div class="thumb">
							        <img class="img-fluid" src="<?php echo e(asset('storage/img/branch/'.$value->branch_photo)); ?>" alt="">
							        <div class="align-items-center justify-content-center d-flex">
										<a href="<?php echo e(url('restoran/detail/'.$value->branch_id)); ?>" data-toggle="tooltip" title="Lihat Restoran"><i class="fa fa-eye"></i></a>
										<a rel="lightbox" href="<?php echo e(asset('storage/img/branch/'.$value->branch_photo)); ?>" data-toggle="tooltip" title="Perbesar Gambar"><i class="fa fa-search-plus"></i></a>
							        </div>
							    </div>
							    <div class="meta-text mt-30 text-center">
								    <h4><?php echo e($value->branch_name); ?></h4>
								    <p><?php echo e($value->branch_city.', '.$value->province_name.', '.$value->country_name); ?></p>
							    </div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
					<?php endif; ?>					
					</div>

					<div class="text-center">
						<div class="form-group">
							<?php echo e($dataCabang->links()); ?>

						</div>
					</div>

				</div>

			</section>
			<!-- End team Area -->
			
		</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>