<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->

<!-- Author Meta -->
<meta name="author" content="codepixer">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
<!--
CSS
============================================= -->
<link rel="stylesheet" href="<?php echo e(asset('storage/css/linearicons.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('storage/css/font-awesome.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('storage/css/bootstrap.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('storage/css/magnific-popup.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('storage/css/nice-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('storage/css/animate.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('storage/css/owl.carousel.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('storage/css/main.css')); ?>">
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(asset('storage/css/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>">