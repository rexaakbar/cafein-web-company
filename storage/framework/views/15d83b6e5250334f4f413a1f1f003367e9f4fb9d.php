<script src="<?php echo e(asset('storage/js/vendor/jquery-2.2.4.min.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="<?php echo e(asset('storage/js/vendor/bootstrap.min.js')); ?>"></script>      
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="<?php echo e(asset('storage/js/easing.min.js')); ?>"></script>      
<script src="<?php echo e(asset('storage/js/hoverIntent.js')); ?>"></script>
<script src="<?php echo e(asset('storage/js/superfish.min.js')); ?>"></script> 
<script src="<?php echo e(asset('storage/js/jquery.ajaxchimp.min.js')); ?>"></script>
<script src="<?php echo e(asset('storage/js/jquery.magnific-popup.min.js')); ?>"></script> 
<script src="<?php echo e(asset('storage/js/owl.carousel.min.js')); ?>"></script>      
<script src="<?php echo e(asset('storage/js/jquery.sticky.js')); ?>"></script>
<script src="<?php echo e(asset('storage/js/jquery.nice-select.min.js')); ?>"></script>      
<script src="<?php echo e(asset('storage/js/parallax.min.js')); ?>"></script>  
<script src="<?php echo e(asset('storage/js/mail-script.js')); ?>"></script> 
<script src="<?php echo e(asset('storage/js/main.js')); ?>"></script>
<!-- DataTables -->
<script src="<?php echo e(asset('storage/js/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('storage/js/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('storage/lightbox/js/lightbox.js')); ?>"></script>

<script type="text/javascript">
function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}  
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<?php if(Session::has('statusAlert')): ?>
  <script type="text/javascript"> 
  $(document).ready(function(){
    callAlertSession("<?php echo e(session('type')); ?>","<?php echo e(session('message')); ?>","<?php echo e(session('title')); ?>"); 
  });
</script>
<?php endif; ?>

<script type="text/javascript">
  function callAlertSession(type,message = '',title = ''){
    swal({
      title: title,
      text: message,
      type: type
    }); 
  }
</script>

<?php if(Session::has('statusAlert')): ?>
  <?php echo e(session()->forget('statusAlert')); ?>

  <?php echo e(session()->forget('title')); ?>

  <?php echo e(session()->forget('message')); ?>

  <?php echo e(session()->forget('type')); ?>

<?php endif; ?>