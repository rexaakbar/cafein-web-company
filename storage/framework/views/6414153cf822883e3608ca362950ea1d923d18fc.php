<?php $__env->startSection('title', ' | Berita'); ?>

<?php $__env->startSection('content'); ?>
	
	<!-- About Generic Start -->
		<div class="main-wrapper">

			<!-- Start team Area -->
			<section class="team-area pt-100" id="team">
				<div class="container">
					<h3 class="about-title mb-30 text-center">Berita</h3>
					<div class="row justify-content-center d-flex align-items-center">
			          	<?php $__currentLoopData = $dataBerita; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			          		<div class="col-lg-4 col-md-6 single-blog">
				              <div class="thumb">
				                <img class="img-fluid" src="<?php echo e(asset('img/b1.jpg')); ?>" alt="">
				              </div>
				              <p class="date"><?php echo e(date('d-M-Y',strtotime($value->news_created_at))); ?></p>
				              <a href="<?php echo e(url('berita/detail/'.$value->news_id)); ?>"><h4><?php echo e($value->news_title); ?></h4></a>
				              <p>
				                <?php echo e($value->description_news); ?>

				              </p>
				              <a href="<?php echo e(url('berita/detail/'.$value->news_id)); ?>">
					              <p>
					              	<u>Lihat Selengkapnya</u>
					              </p>
				              </a>
				            </div>
			          	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>          
			          </div>
					

					<div class="text-center">
						<div class="form-group">
							<?php echo e($dataBerita->links()); ?>

						</div>
					</div>

				</div>

			</section>
			<!-- End team Area -->
			
		</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>