/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 100133
Source Host           : localhost:3306
Source Database       : mki_cafein_3

Target Server Type    : MYSQL
Target Server Version : 100133
File Encoding         : 65001

Date: 2018-07-11 20:09:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api_devices
-- ----------------------------
DROP TABLE IF EXISTS `api_devices`;
CREATE TABLE `api_devices` (
  `apidevice_id` int(11) NOT NULL AUTO_INCREMENT,
  `apidevice_name` varchar(128) DEFAULT NULL,
  `apidevice_code` varchar(32) DEFAULT NULL,
  `apidevice_last_access_date` datetime DEFAULT NULL COMMENT 'Waktu terakhir akses',
  `apidevice_status` int(1) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  PRIMARY KEY (`apidevice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of api_devices
-- ----------------------------

-- ----------------------------
-- Table structure for api_gadgets
-- ----------------------------
DROP TABLE IF EXISTS `api_gadgets`;
CREATE TABLE `api_gadgets` (
  `gadget_id` int(11) NOT NULL AUTO_INCREMENT,
  `gadget_device_code` varchar(960) DEFAULT NULL,
  `gedget_user_id` int(11) DEFAULT NULL COMMENT 'user_id (sys_users)',
  `gadget_device_type` int(1) DEFAULT NULL COMMENT '1.Android, 2.iOS',
  PRIMARY KEY (`gadget_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of api_gadgets
-- ----------------------------

-- ----------------------------
-- Table structure for api_tokens
-- ----------------------------
DROP TABLE IF EXISTS `api_tokens`;
CREATE TABLE `api_tokens` (
  `apitoken_id` int(11) NOT NULL AUTO_INCREMENT,
  `apitoken_device_id` int(11) DEFAULT NULL COMMENT 'apidevice_id (api_devices)',
  `apitoken_code` varchar(255) DEFAULT NULL,
  `apitoken_valid_until` datetime DEFAULT NULL,
  `apitoken_ip` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`apitoken_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of api_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for ref_branches
-- ----------------------------
DROP TABLE IF EXISTS `ref_branches`;
CREATE TABLE `ref_branches` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_company_id` int(11) DEFAULT NULL,
  `branch_code` varchar(32) DEFAULT NULL,
  `branch_photo` text,
  `branch_name` varchar(96) DEFAULT NULL,
  `branch_info` varchar(255) DEFAULT NULL,
  `branch_description` varchar(960) DEFAULT NULL,
  `branch_rating` int(11) DEFAULT NULL,
  `branch_address` varchar(255) DEFAULT NULL,
  `branch_city` varchar(64) DEFAULT NULL,
  `branch_district` varchar(64) DEFAULT NULL,
  `branch_province_id` int(11) DEFAULT NULL,
  `branch_country_id` int(11) DEFAULT NULL,
  `branch_email` varchar(255) DEFAULT NULL,
  `branch_cp` varchar(255) DEFAULT NULL,
  `branch_phone` varchar(255) DEFAULT NULL,
  `branch_fax` varchar(255) DEFAULT NULL,
  `branch_latitude` varchar(32) DEFAULT NULL,
  `branch_longitude` varchar(32) DEFAULT NULL,
  `branch_create_user_id` int(11) DEFAULT NULL,
  `branch_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `branch_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `branch_log_id` int(11) DEFAULT NULL COMMENT 'ID Parent',
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_branches
-- ----------------------------
INSERT INTO `ref_branches` VALUES ('1', '1', '000', null, 'Z-COST SOERAPTI', null, '<p>Cabang Utama</p>\r\n', null, 'Cijerokaso Green Residence No. 1', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, null, '2018-03-20 11:29:29', '1', null);
INSERT INTO `ref_branches` VALUES ('23', '1', '001', null, 'Cabang baru', null, '<p>Deskripsinya</p>\r\n', null, 'Cijerokaso', 'Bandung', 'Bandung', '8', '101', null, null, null, null, null, null, '1', '2017-09-20 10:48:45', '1', null);
INSERT INTO `ref_branches` VALUES ('33', '1', '003', null, 'cabang 3', null, '<p>dsadasad</p>\r\n', null, 'dasdasda', 'dsadas', 'dasdas', '8', '101', null, null, null, null, null, null, '1', '2017-09-20 10:54:20', '1', null);
INSERT INTO `ref_branches` VALUES ('34', '1', '004', null, 'cabang 4', null, '<p>dsadasad</p>\r\n', null, 'dasdasda', 'dsadas', 'dasdas', '8', '101', null, null, null, null, null, null, '1', '2017-09-20 13:58:58', '1', null);
INSERT INTO `ref_branches` VALUES ('35', '1', '000', null, 'Z-COST SOERAPTI', null, '<p>Cabang Utama</p>\r\n', null, 'Cijerokaso Green Residence No. 1', 'Bandung', 'Bandung', '8', '101', null, null, null, null, null, null, null, '2017-09-14 17:40:28', '1', '1');
INSERT INTO `ref_branches` VALUES ('36', '1', '007', null, 'Cabang Cabangan', null, '<p>Huehuehue</p>\r\n', null, 'Jln. Jamika Gg. Siti Mariah', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', '2018-03-20 11:31:32', '0', null);
INSERT INTO `ref_branches` VALUES ('37', '1', '007', null, 'Cabang Cabangan', null, '<p>Huehuehue</p>\r\n', null, 'Jln. Jamika Gg. Siti Mariah', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', '2018-03-20 11:31:09', '1', '36');
INSERT INTO `ref_branches` VALUES ('38', '1', '212', null, 'Caabangan', null, '<p>adasdasd</p>\r\n', null, 'Asadasdas', 'asdasdas', 'asdasdasd', '8', '101', null, null, null, null, null, null, '14', '2018-03-20 16:27:17', '1', null);
INSERT INTO `ref_branches` VALUES ('39', '6', '333', null, 'Cabang Bandung', null, 'asdasdasdasd', null, 'Jln. Jamika Gg. Siti Mariah', 'Bandung', 'Bojongloa Kaler', '8', '101', 'cabangbandung@app.com', '081234567892', '081234567893', '022-7457814', '40.6700', '-73.9400', '1', '2018-06-28 14:30:54', '1', null);
INSERT INTO `ref_branches` VALUES ('40', '9', 'BZLTE', null, 'Pusat', 'Branch Pusat', '-', null, null, null, null, null, null, null, null, null, null, null, null, '1', null, '1', null);
INSERT INTO `ref_branches` VALUES ('41', '9', 'ASCSW', null, 'Cabang Bandung', null, '<p>asdasdasdadsa</p>\r\n', null, 'Jl. Jamika Gg. Siti Mariah', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '26', '2018-04-03 14:27:57', '1', null);
INSERT INTO `ref_branches` VALUES ('42', '10', 'GTODV', null, 'PusatPT. Sido Muncul', 'Branch Pusat', '-', null, 'Di jalan', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', null, '1', null);
INSERT INTO `ref_branches` VALUES ('43', '10', '009', null, 'Cabang Jakarta', null, '<p>asdasdasd</p>\r\n', null, 'Jakarta', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '27', '2018-04-09 09:59:34', '1', null);
INSERT INTO `ref_branches` VALUES ('44', '10', '002', null, 'Cabang Bandung', null, '<p>asasasas</p>\r\n', null, 'Jl. Sample', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '27', '2018-04-12 20:28:48', '1', null);
INSERT INTO `ref_branches` VALUES ('45', '13', 'AZYGL', null, 'PusatPT. Test Company', 'Branch Pusat', '-', null, 'Jalan Test 123', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', null, '1', null);
INSERT INTO `ref_branches` VALUES ('46', '14', 'ESDTU', null, 'PusatPT. Test 123', 'Branch Pusat', '-', null, 'Jalan Sample123', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', null, '1', null);
INSERT INTO `ref_branches` VALUES ('47', '15', 'UPVAE', null, 'PusatPt. Sample 123', 'Branch Pusat', '-', null, 'Jalan test 123 sample', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', null, '1', null);
INSERT INTO `ref_branches` VALUES ('48', '15', '001', null, 'Cabang Bandung', null, '<p>Test deskripsi</p>\r\n', null, 'Jalan Cijerokaso', 'Bandung', 'Bojongloa Kaler', '8', '101', 'sample123@gmail.com1', '081234567892', '081234567893', '022-7457814', null, null, '32', '2018-04-27 10:04:57', '1', null);
INSERT INTO `ref_branches` VALUES ('49', '15', '001', null, 'Cabang Bandung', null, '<p>Test deskripsi</p>\r\n', null, 'Jalan Cijerokaso', 'Bandung', 'Bojongloa Kaler', '8', '101', 'sample123@gmail.com', '08123456789', '08123456789', '022-745781', null, null, '32', '2018-04-19 16:55:46', '1', '48');
INSERT INTO `ref_branches` VALUES ('50', '15', '002', null, 'Cabang Surabaya', null, '<p>test 123</p>\r\n', null, 'Jalan test 123 sample', 'Bandung', 'Bojongloa Kaler', '8', '101', 'sample123@gmail.com', '08123456789', '8123456789', '022-745871', null, null, '32', '2018-04-20 10:00:57', '1', null);
INSERT INTO `ref_branches` VALUES ('51', '18', 'YZOTE', null, 'Pusat PT. Test Sample 123', 'Branch Pusat', '-', null, 'Jalan test 123', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', null, '1', null);
INSERT INTO `ref_branches` VALUES ('52', '15', '001', null, 'Cabang Bandung', null, '<p>Test deskripsi</p>\r\n', null, 'Jalan Cijerokaso', 'Bandung', 'Bojongloa Kaler', '8', '101', 'sample123@gmail.com1', '081234567892', '081234567893', '022-7457814', null, null, '32', '2018-04-19 17:02:57', '1', '48');
INSERT INTO `ref_branches` VALUES ('53', '15', '001', null, 'Cabang Bandung', null, '<p>Test deskripsi</p>\r\n', null, 'Jalan Cijerokaso', 'Bandung', 'Bojongloa Kaler', '8', '101', 'sample123@gmail.com1', '081234567892', '081234567893', '022-7457814', null, null, '32', '2018-04-27 10:04:33', '0', '48');
INSERT INTO `ref_branches` VALUES ('54', '19', 'PYDBR', null, 'Pusat Kedai YangTie', 'Branch Pusat', '-', null, 'Jalan Test 123', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', null, '1', null);
INSERT INTO `ref_branches` VALUES ('55', '19', '001', 'kedai-nongkrong-s.jpg', 'Kedai YangTie', null, '<p>Ini kedai yangtie</p>\r\n', null, 'Jalan Test 123', 'Bandung', 'Bojongloa Kaler', '8', '101', 'kedaiyangtierest@gmail.com', '083827163641', '083827163641', '022-7201923', null, null, '38', '2018-07-11 09:10:33', '1', null);

-- ----------------------------
-- Table structure for ref_branch_images
-- ----------------------------
DROP TABLE IF EXISTS `ref_branch_images`;
CREATE TABLE `ref_branch_images` (
  `branchimage_id` int(11) NOT NULL AUTO_INCREMENT,
  `branchimage_branch_id` int(11) DEFAULT NULL,
  `branchimage_image_path` varchar(255) DEFAULT NULL,
  `branchimage_create_user_id` int(11) DEFAULT NULL,
  `branchimage_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `branchimage_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  PRIMARY KEY (`branchimage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_branch_images
-- ----------------------------
INSERT INTO `ref_branch_images` VALUES ('33', '23', 'upload/branches/juice_manggo.jpg', '1', '2017-09-14 17:05:38', '1');
INSERT INTO `ref_branch_images` VALUES ('34', '23', 'upload/branches/juice_manggo1.jpg', '1', '2017-09-14 17:05:38', '1');
INSERT INTO `ref_branch_images` VALUES ('35', '27', 'upload/branches/juice_manggo2.jpg', '1', '2017-09-20 10:45:59', '1');
INSERT INTO `ref_branch_images` VALUES ('36', '28', 'upload/branches/juice_manggo3.jpg', '1', '2017-09-20 10:46:12', '1');
INSERT INTO `ref_branch_images` VALUES ('37', '31', 'upload/branches/nasgor1.jpg', '1', '2017-09-20 10:50:03', '1');
INSERT INTO `ref_branch_images` VALUES ('38', '32', 'upload/branches/nasgor11.jpg', '1', '2017-09-20 10:50:34', '1');
INSERT INTO `ref_branch_images` VALUES ('39', '33', 'upload/branches/nasgor.jpg', '1', '2017-09-20 10:54:20', '1');
INSERT INTO `ref_branch_images` VALUES ('40', '34', 'upload/branches/nasgor2.jpg', '1', '2017-09-20 10:55:01', '1');
INSERT INTO `ref_branch_images` VALUES ('41', '36', 'upload/branches/2-2-pengelolaan-penambahan-koreksi-0205010307-container-000103020000000.jpg', '1', '2018-03-20 11:31:09', '1');
INSERT INTO `ref_branch_images` VALUES ('42', '38', 'upload/branches/2-2-pengelolaan-penambahan-koreksi-0205010307-container-0001030200000001.jpg', '14', '2018-03-20 16:27:17', '1');
INSERT INTO `ref_branch_images` VALUES ('43', '41', 'upload/branches/2-1-pengelolaan-penambahan-koreksi-0413010101-jalan-negara-nasional-kelas-i-000103020000000.jpg', '26', '2018-04-03 14:27:58', '1');
INSERT INTO `ref_branch_images` VALUES ('44', '43', 'upload/branches/1349x51 (1).jpg', '27', '2018-04-09 09:59:34', '1');
INSERT INTO `ref_branch_images` VALUES ('45', '44', 'upload/branches/2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000_(2).jpg', '27', '2018-04-12 20:28:48', '1');
INSERT INTO `ref_branch_images` VALUES ('46', '48', 'upload/branches/5-perolehan-pembelian-0518020102-lapang-kuda-000103020000000_(2).jpg', '32', '2018-04-19 16:55:47', '1');
INSERT INTO `ref_branch_images` VALUES ('47', '50', 'upload/branches/2-1-pengelolaan-penambahan-koreksi-0413010101-jalan-negara-nasional-kelas-i-0001030200000001.jpg', '32', '2018-04-20 10:00:57', '1');
INSERT INTO `ref_branch_images` VALUES ('48', '55', 'upload/branches/kedai-nongkrong-s.jpg', '38', '2018-07-11 08:38:22', '1');

-- ----------------------------
-- Table structure for ref_branch_rooms
-- ----------------------------
DROP TABLE IF EXISTS `ref_branch_rooms`;
CREATE TABLE `ref_branch_rooms` (
  `branchroom_id` int(11) NOT NULL AUTO_INCREMENT,
  `branchroom_branch_id` int(11) DEFAULT NULL,
  `branchroom_code` varchar(32) DEFAULT NULL,
  `branchroom_name` varchar(96) DEFAULT NULL,
  `branchroom_description` varchar(960) DEFAULT NULL,
  `branchroom_map` blob,
  `branchroom_create_user_id` int(11) DEFAULT NULL,
  `branchroom_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `branchroom_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `branchroom_log_id` int(11) DEFAULT NULL COMMENT 'ID Parent',
  PRIMARY KEY (`branchroom_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_branch_rooms
-- ----------------------------
INSERT INTO `ref_branch_rooms` VALUES ('1', '1', '01', 'Ruang Utama', '<p>1</p>\r\n', null, null, '2017-09-07 17:50:42', '1', null);
INSERT INTO `ref_branch_rooms` VALUES ('2', '2', '02', 'Outdoor', 'di luar', null, null, '2017-09-07 14:54:19', '1', null);
INSERT INTO `ref_branch_rooms` VALUES ('3', '1', '03', 'ruang baru', '<p>das</p>\r\n', null, '1', '2017-09-07 17:08:55', '1', null);
INSERT INTO `ref_branch_rooms` VALUES ('4', '1', '01', 'Ruang Utama', null, null, null, '2016-12-22 14:06:08', '1', '1');
INSERT INTO `ref_branch_rooms` VALUES ('5', '1', '01', 'Ruang Utama', null, null, null, '2017-09-07 17:35:49', '0', '1');
INSERT INTO `ref_branch_rooms` VALUES ('6', '1', '01', 'Ruang Utama', null, null, null, '2017-09-07 17:35:55', '1', '1');
INSERT INTO `ref_branch_rooms` VALUES ('7', '1', '01', 'Ruang Utama', null, null, null, '2017-09-07 17:35:55', '1', '1');
INSERT INTO `ref_branch_rooms` VALUES ('8', '1', '01', 'Ruang Utama', null, null, null, '2017-09-07 17:35:55', '1', '1');
INSERT INTO `ref_branch_rooms` VALUES ('9', '1', '01', 'Ruang Utama', '<p>1</p>\r\n', null, null, '2017-09-07 17:45:29', '1', '1');
INSERT INTO `ref_branch_rooms` VALUES ('10', '1', '01', 'Ruang Utama', '', null, null, '2017-09-07 17:45:41', '1', '1');
INSERT INTO `ref_branch_rooms` VALUES ('11', '1', '007', 'Ruangan Makan Makanan', '<p>Huehuehue</p>\r\n', null, '1', '2018-03-20 11:38:26', '0', null);
INSERT INTO `ref_branch_rooms` VALUES ('12', '1', '007', 'Ruangan Makan', '<p>Huehuehue</p>\r\n', null, '1', '2018-03-20 11:35:31', '1', '1');
INSERT INTO `ref_branch_rooms` VALUES ('13', '1', '007', 'Ruangan Makan Makanan', '<p>Huehuehue</p>\r\n', null, '1', '2018-03-20 11:36:02', '1', '1');
INSERT INTO `ref_branch_rooms` VALUES ('14', '39', '000', 'SDDADA', '<p>asdasdasd</p>\r\n', null, '21', '2018-04-04 11:44:09', '1', null);
INSERT INTO `ref_branch_rooms` VALUES ('15', '41', '001', 'Ruangan Cabang Bandung 1', '<p>asdasdasdasda</p>\r\n', null, '26', '2018-04-04 15:14:23', '1', null);
INSERT INTO `ref_branch_rooms` VALUES ('16', '0', '', '', '', null, '27', '2018-04-12 17:03:53', '1', null);
INSERT INTO `ref_branch_rooms` VALUES ('17', '0', '', '', '', null, '27', '2018-04-12 17:04:08', '1', null);
INSERT INTO `ref_branch_rooms` VALUES ('18', '43', '001', 'Ruangan 001', '<p>asdasdasdas</p>\r\n', null, '27', '2018-04-12 20:41:22', '1', null);
INSERT INTO `ref_branch_rooms` VALUES ('19', '44', '001', 'Ruangan 001', '<p>asdasdasdas</p>\r\n', null, '27', '2018-04-12 20:41:22', '1', null);
INSERT INTO `ref_branch_rooms` VALUES ('20', '48', '001', 'Ruangan 001', '<p>Test ruangan 123</p>\r\n', null, '32', '2018-04-27 10:14:49', '1', null);
INSERT INTO `ref_branch_rooms` VALUES ('21', '48', '001', 'Ruangan 001', '<p>Test ruangan 1</p>\r\n', null, '32', '2018-04-19 17:27:52', '1', '20');
INSERT INTO `ref_branch_rooms` VALUES ('22', '48', '001', 'Ruangan 001', '<p>Test ruangan 123</p>\r\n', null, '32', '2018-04-19 17:28:55', '1', '1');
INSERT INTO `ref_branch_rooms` VALUES ('23', '48', '001', 'Ruangan 001', '<p>Test ruangan 123</p>\r\n', null, '32', '2018-04-27 10:14:22', '0', '1');

-- ----------------------------
-- Table structure for ref_branch_tables
-- ----------------------------
DROP TABLE IF EXISTS `ref_branch_tables`;
CREATE TABLE `ref_branch_tables` (
  `branchtable_id` int(11) NOT NULL AUTO_INCREMENT,
  `branchtable_room_id` int(11) DEFAULT NULL,
  `branchtable_code` varchar(32) DEFAULT NULL,
  `branchtable_name` varchar(96) DEFAULT NULL,
  `branchtable_description` varchar(960) DEFAULT NULL,
  `branchtable_map` blob,
  `branchtable_create_user_id` int(11) DEFAULT NULL,
  `branchtable_create_date` datetime DEFAULT NULL,
  `branchtable_status` int(11) DEFAULT '1' COMMENT '0.Tidak Aktif, 1.Opened, 2.Reserved, 3. Request Bill',
  `branchtable_log_id` int(11) DEFAULT NULL COMMENT 'ID Parent',
  PRIMARY KEY (`branchtable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_branch_tables
-- ----------------------------
INSERT INTO `ref_branch_tables` VALUES ('1', '1', '1', 'Meja 1', '', null, null, '2017-01-10 15:25:39', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('2', '1', '2', 'Meja 2', null, null, null, '2017-01-10 15:25:39', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('3', '1', '3', 'Meja 3', null, null, null, '2017-01-10 15:25:39', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('4', '1', '4', 'Meja 4', null, null, null, '2017-01-10 15:25:39', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('5', '1', '5', 'Meja 5', null, null, null, '2017-01-10 15:25:39', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('6', '1', '6', 'Meja 6', null, null, null, '2017-01-10 15:25:39', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('7', '1', '7', 'Meja 7', null, null, null, '2017-01-10 15:25:39', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('8', '1', '8', 'Meja 8', null, null, null, '2017-01-10 15:25:39', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('9', '1', '9', 'Meja 9', null, null, null, '2017-01-10 15:25:39', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('10', '1', '10', 'Meja 10', null, null, null, '2017-01-10 15:25:39', '3', null);
INSERT INTO `ref_branch_tables` VALUES ('11', '1', '11', 'Meja 11', '<p>meja ini baru</p>\r\n', null, '1', '2017-08-29 11:48:12', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('12', '1', '12', 'Meja 12', '<p>ini meja 12 masih baru</p>\r\n', null, '1', '2017-08-29 11:55:31', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('13', '1', '13', 'Meja 13', '<p>dsadasdas</p>\r\n', null, '1', '2017-08-29 11:58:53', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('15', '1', '14', 'Meja 14', '<p>14</p>\r\n', null, '1', '2017-09-05 11:34:29', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('16', '1', '15', 'Meja 15', '<p>dasdadas</p>\r\n', null, '1', '2017-09-05 11:38:07', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('17', '1', '16', 'Meja 16', '<p>dasdasdas</p>\r\n', null, '1', '2017-09-05 11:39:43', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('18', '1', '17', 'Meja 17', '<p>dsadsasd</p>\r\n', null, '1', '2017-09-05 11:42:26', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('19', '2', '18', 'Meja 18', '<p>dsdas</p>\r\n', null, '1', '2017-09-05 11:43:17', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('20', '2', '19', 'Meja 19', '<p>dadasd</p>\r\n', null, '1', '2017-09-05 11:45:37', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('21', '11', '007', 'Meja Mejaan', '<p>asdasdasdasd</p>\r\n', null, '1', '2018-03-20 11:39:59', '0', null);
INSERT INTO `ref_branch_tables` VALUES ('22', '11', '007', 'Meja Mejaan', '<p>asdasdasdasd</p>\r\n', null, '1', '2018-03-20 11:39:59', '1', '1');
INSERT INTO `ref_branch_tables` VALUES ('23', '15', '001', 'Meja Makan Cabang Bandung 123', '<p>asdasdasd</p>\r\n', null, '26', '2018-04-05 11:09:22', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('24', '15', '001', 'Meja Makan Cabang Bandung 1', '<p>asdasdasd</p>\r\n', null, '26', '2018-04-05 11:09:22', '1', '1');
INSERT INTO `ref_branch_tables` VALUES ('25', '18', '123', 'Meja 01', '<p>Meja 01</p>\r\n', null, '27', '2018-04-18 15:49:02', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('26', '20', '001', 'Meja 001', '<p>Test meja 123</p>\r\n', null, '32', '2018-04-19 17:44:43', '1', null);
INSERT INTO `ref_branch_tables` VALUES ('27', '20', '001', 'Meja 001', '<p>Test meja 123</p>\r\n', null, '32', '2018-04-19 17:44:43', '1', '1');
INSERT INTO `ref_branch_tables` VALUES ('28', '20', '001', 'Meja 001', '<p>Test meja 123</p>\r\n', null, '32', '2018-04-19 17:44:43', '0', '1');

-- ----------------------------
-- Table structure for ref_companies
-- ----------------------------
DROP TABLE IF EXISTS `ref_companies`;
CREATE TABLE `ref_companies` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `company_code` varchar(32) DEFAULT NULL,
  `company_name` varchar(96) DEFAULT NULL,
  `company_description` varchar(960) DEFAULT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `company_city` varchar(64) DEFAULT NULL,
  `company_district` varchar(64) DEFAULT NULL,
  `company_province_id` int(11) DEFAULT NULL,
  `company_country_id` int(11) DEFAULT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `company_cp` varchar(255) DEFAULT NULL,
  `company_phone` varchar(255) DEFAULT NULL,
  `company_fax` varchar(255) DEFAULT NULL,
  `company_latitude` varchar(32) DEFAULT NULL,
  `company_longitude` varchar(32) DEFAULT NULL,
  `company_create_user_id` int(11) DEFAULT NULL,
  `company_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `company_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `company_log_id` int(11) DEFAULT NULL COMMENT 'ID Parent',
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_companies
-- ----------------------------
INSERT INTO `ref_companies` VALUES ('1', null, '000', 'PT. Mediatama Kreasi Informatika', '<p>asdasdasdasd</p>\r\n', 'Jl. Cijerokaso Komp. Green Residence No. 1', 'Bandung', 'Bojongloa Kaler', '8', '101', 'sample123@gmail.com', '08123456789', '0812345678911', '022-78451', null, null, '1', '2018-07-01 12:38:51', '1', null);
INSERT INTO `ref_companies` VALUES ('2', null, '000', 'Z-COST', null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-03-27 14:41:38', '1', '1');
INSERT INTO `ref_companies` VALUES ('3', null, '000', 'Z-COST', null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-03-27 14:59:13', '0', '1');
INSERT INTO `ref_companies` VALUES ('4', null, '000', 'Z-COST', null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-03-27 15:02:07', '1', '1');
INSERT INTO `ref_companies` VALUES ('5', null, '000', 'Z-COST', null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-03-27 15:08:48', '0', '1');
INSERT INTO `ref_companies` VALUES ('6', '41', '001ZN', 'PT. Contoh', '<p>asdasdasdsad</p>\r\n', 'Jln. Jamika Gg. Siti Mariah', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', '2018-04-13 16:47:41', '1', null);
INSERT INTO `ref_companies` VALUES ('7', null, '001ZN', 'PT. Sejahtera Abadi', '<p>asdasdasdsad</p>\r\n', 'Jln. Jamika Gg. Siti Mariah', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', '2018-03-28 15:05:45', '1', '6');
INSERT INTO `ref_companies` VALUES ('8', null, '000', 'Z-COST', null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-03-27 15:08:53', '1', '1');
INSERT INTO `ref_companies` VALUES ('9', '46', '002DK', 'PT. Sample', '<p>asdasdasdasdsad</p>\r\n', 'Jl. Sample', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', '2018-04-03 11:18:55', '1', null);
INSERT INTO `ref_companies` VALUES ('10', '47', '003', 'PT. Sample Test 123', '<p>asdsadasdasd</p>\r\n', 'Di jalan', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', '2018-04-17 21:18:41', '1', null);
INSERT INTO `ref_companies` VALUES ('11', null, '001ZN', 'PT. Sejahtera Abadis', '<p>asdasdasdsad</p>\r\n', 'Jln. Jamika Gg. Siti Mariah', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', '2018-03-29 14:47:25', '1', '6');
INSERT INTO `ref_companies` VALUES ('12', null, '003', 'PT. Sido Muncul', '<p>asdsadasdasd</p>\r\n', 'Di jalan', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', '2018-04-17 21:18:37', '1', '10');
INSERT INTO `ref_companies` VALUES ('13', '48', '004', 'PT. Test Company', '<p>test 123</p>\r\n', 'Jalan Test 123', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', '2018-04-17 09:48:47', '1', null);
INSERT INTO `ref_companies` VALUES ('14', '53', '006', 'PT. Test 123', '<p>test123</p>\r\n', 'Jalan Sample123', 'Bandung', 'Bojongloa Kaler', '8', '101', null, null, null, null, null, null, '1', '2018-04-19 16:02:09', '1', null);
INSERT INTO `ref_companies` VALUES ('15', '55', '005', 'Pt. Sample 123', '<p>Test 123</p>\r\n', 'Jalan test 123 sample', 'Bandung', 'Bojongloa Kaler', '8', '101', 'sample123@gmail.com', '08123456789', '0812345678911', '022-78451', null, null, '1', '2018-04-19 16:26:09', '1', null);
INSERT INTO `ref_companies` VALUES ('16', null, '005', 'Pt. Sample 123', '<p>Test 123</p>\r\n', 'Jalan test 123 sample', 'Bandung', 'Bojongloa Kaler', '8', '101', 'sample123@gmail.com', '08123456789', '08123456789', '022-78451', null, null, '1', '2018-04-19 15:37:45', '1', '15');
INSERT INTO `ref_companies` VALUES ('17', null, '005', 'Pt. Sample 123', '<p>Test 123</p>\r\n', 'Jalan test 123 sample', 'Bandung', 'Bojongloa Kaler', '0', '101', 'sample123@gmail.com', '08123456789', '0812345678911', '022-78451', null, null, '1', '2018-04-19 16:19:31', '1', '15');
INSERT INTO `ref_companies` VALUES ('18', '56', '007', 'PT. Test Sample 123', '<p>Test 123456</p>\r\n', 'Jalan test 123', 'Bandung', 'Bojongloa Kaler', '8', '101', 'sample123@gmail.com', '08123456789', '8123456789', '022-72684', null, null, '1', '2018-04-20 17:07:50', '1', null);
INSERT INTO `ref_companies` VALUES ('19', '61', '008', 'Kedai YangTie', '<p>Ini adalah kedai YangTie</p>\r\n', 'Jalan Test 123', 'Bandung', 'Bojongloa Kaler', '8', '101', 'kedaiyangtie@gmail.com', '083827163641', '083827163641', '022-720381', null, null, '1', '2018-07-10 23:42:13', '1', null);

-- ----------------------------
-- Table structure for ref_countries
-- ----------------------------
DROP TABLE IF EXISTS `ref_countries`;
CREATE TABLE `ref_countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(16) DEFAULT NULL,
  `country_name` varchar(64) DEFAULT NULL,
  `country_flag` varchar(32) DEFAULT NULL,
  `country_latitude` varchar(32) DEFAULT NULL,
  `country_longitude` varchar(32) DEFAULT NULL,
  `country_polyline` text,
  `country_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `country_log_id` int(11) DEFAULT NULL COMMENT 'null=Data parent, not null=Histori untuk ID tercantum',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_countries
-- ----------------------------
INSERT INTO `ref_countries` VALUES ('1', 'AF', 'Afghanistan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('2', 'AL', 'Albania', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('3', 'DZ', 'Algeria', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('4', 'DS', 'American Samoa', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('5', 'AD', 'Andorra', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('6', 'AO', 'Angola', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('7', 'AI', 'Anguilla', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('8', 'AQ', 'Antarctica', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('9', 'AG', 'Antigua and Barbuda', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('10', 'AR', 'Argentina', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('11', 'AM', 'Armenia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('12', 'AW', 'Aruba', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('13', 'AU', 'Australia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('14', 'AT', 'Austria', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('15', 'AZ', 'Azerbaijan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('16', 'BS', 'Bahamas', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('17', 'BH', 'Bahrain', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('18', 'BD', 'Bangladesh', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('19', 'BB', 'Barbados', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('20', 'BY', 'Belarus', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('21', 'BE', 'Belgium', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('22', 'BZ', 'Belize', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('23', 'BJ', 'Benin', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('24', 'BM', 'Bermuda', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('25', 'BT', 'Bhutan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('26', 'BO', 'Bolivia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('27', 'BA', 'Bosnia and Herzegovina', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('28', 'BW', 'Botswana', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('29', 'BV', 'Bouvet Island', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('30', 'BR', 'Brazil', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('31', 'IO', 'British Indian Ocean Territory', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('32', 'BN', 'Brunei Darussalam', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('33', 'BG', 'Bulgaria', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('34', 'BF', 'Burkina Faso', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('35', 'BI', 'Burundi', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('36', 'KH', 'Cambodia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('37', 'CM', 'Cameroon', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('38', 'CA', 'Canada', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('39', 'CV', 'Cape Verde', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('40', 'KY', 'Cayman Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('41', 'CF', 'Central African Republic', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('42', 'TD', 'Chad', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('43', 'CL', 'Chile', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('44', 'CN', 'China', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('45', 'CX', 'Christmas Island', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('46', 'CC', 'Cocos (Keeling) Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('47', 'CO', 'Colombia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('48', 'KM', 'Comoros', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('49', 'CG', 'Congo', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('50', 'CK', 'Cook Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('51', 'CR', 'Costa Rica', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('52', 'HR', 'Croatia (Hrvatska)', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('53', 'CU', 'Cuba', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('54', 'CY', 'Cyprus', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('55', 'CZ', 'Czech Republic', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('56', 'DK', 'Denmark', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('57', 'DJ', 'Djibouti', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('58', 'DM', 'Dominica', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('59', 'DO', 'Dominican Republic', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('60', 'TP', 'East Timor', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('61', 'EC', 'Ecuador', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('62', 'EG', 'Egypt', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('63', 'SV', 'El Salvador', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('64', 'GQ', 'Equatorial Guinea', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('65', 'ER', 'Eritrea', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('66', 'EE', 'Estonia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('67', 'ET', 'Ethiopia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('68', 'FK', 'Falkland Islands (Malvinas)', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('69', 'FO', 'Faroe Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('70', 'FJ', 'Fiji', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('71', 'FI', 'Finland', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('72', 'FR', 'France', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('73', 'FX', 'France, Metropolitan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('74', 'GF', 'French Guiana', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('75', 'PF', 'French Polynesia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('76', 'TF', 'French Southern Territories', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('77', 'GA', 'Gabon', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('78', 'GM', 'Gambia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('79', 'GE', 'Georgia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('80', 'DE', 'Germany', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('81', 'GH', 'Ghana', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('82', 'GI', 'Gibraltar', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('83', 'GK', 'Guernsey', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('84', 'GR', 'Greece', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('85', 'GL', 'Greenland', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('86', 'GD', 'Grenada', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('87', 'GP', 'Guadeloupe', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('88', 'GU', 'Guam', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('89', 'GT', 'Guatemala', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('90', 'GN', 'Guinea', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('91', 'GW', 'Guinea-Bissau', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('92', 'GY', 'Guyana', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('93', 'HT', 'Haiti', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('94', 'HM', 'Heard and Mc Donald Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('95', 'HN', 'Honduras', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('96', 'HK', 'Hong Kong', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('97', 'HU', 'Hungary', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('98', 'IS', 'Iceland', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('99', 'IN', 'India', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('100', 'IM', 'Isle of Man', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('101', 'ID', 'Indonesia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('102', 'IR', 'Iran (Islamic Republic of)', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('103', 'IQ', 'Iraq', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('104', 'IE', 'Ireland', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('105', 'IL', 'Israel', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('106', 'IT', 'Italy', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('107', 'CI', 'Ivory Coast', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('108', 'JE', 'Jersey', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('109', 'JM', 'Jamaica', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('110', 'JP', 'Japan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('111', 'JO', 'Jordan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('112', 'KZ', 'Kazakhstan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('113', 'KE', 'Kenya', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('114', 'KI', 'Kiribati', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('115', 'KP', 'Korea, Democratic People\'\'s Republic of', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('116', 'KR', 'Korea, Republic of', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('117', 'XK', 'Kosovo', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('118', 'KW', 'Kuwait', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('119', 'KG', 'Kyrgyzstan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('120', 'LA', 'Lao People\'\'s Democratic Republic', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('121', 'LV', 'Latvia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('122', 'LB', 'Lebanon', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('123', 'LS', 'Lesotho', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('124', 'LR', 'Liberia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('125', 'LY', 'Libyan Arab Jamahiriya', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('126', 'LI', 'Liechtenstein', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('127', 'LT', 'Lithuania', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('128', 'LU', 'Luxembourg', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('129', 'MO', 'Macau', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('130', 'MK', 'Macedonia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('131', 'MG', 'Madagascar', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('132', 'MW', 'Malawi', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('133', 'MY', 'Malaysia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('134', 'MV', 'Maldives', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('135', 'ML', 'Mali', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('136', 'MT', 'Malta', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('137', 'MH', 'Marshall Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('138', 'MQ', 'Martinique', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('139', 'MR', 'Mauritania', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('140', 'MU', 'Mauritius', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('141', 'TY', 'Mayotte', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('142', 'MX', 'Mexico', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('143', 'FM', 'Micronesia, Federated States of', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('144', 'MD', 'Moldova, Republic of', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('145', 'MC', 'Monaco', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('146', 'MN', 'Mongolia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('147', 'ME', 'Montenegro', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('148', 'MS', 'Montserrat', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('149', 'MA', 'Morocco', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('150', 'MZ', 'Mozambique', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('151', 'MM', 'Myanmar', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('152', 'NA', 'Namibia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('153', 'NR', 'Nauru', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('154', 'NP', 'Nepal', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('155', 'NL', 'Netherlands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('156', 'AN', 'Netherlands Antilles', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('157', 'NC', 'New Caledonia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('158', 'NZ', 'New Zealand', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('159', 'NI', 'Nicaragua', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('160', 'NE', 'Niger', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('161', 'NG', 'Nigeria', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('162', 'NU', 'Niue', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('163', 'NF', 'Norfolk Island', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('164', 'MP', 'Northern Mariana Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('165', 'NO', 'Norway', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('166', 'OM', 'Oman', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('167', 'PK', 'Pakistan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('168', 'PW', 'Palau', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('169', 'PS', 'Palestine', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('170', 'PA', 'Panama', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('171', 'PG', 'Papua New Guinea', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('172', 'PY', 'Paraguay', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('173', 'PE', 'Peru', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('174', 'PH', 'Philippines', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('175', 'PN', 'Pitcairn', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('176', 'PL', 'Poland', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('177', 'PT', 'Portugal', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('178', 'PR', 'Puerto Rico', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('179', 'QA', 'Qatar', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('180', 'RE', 'Reunion', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('181', 'RO', 'Romania', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('182', 'RU', 'Russian Federation', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('183', 'RW', 'Rwanda', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('184', 'KN', 'Saint Kitts and Nevis', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('185', 'LC', 'Saint Lucia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('186', 'VC', 'Saint Vincent and the Grenadines', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('187', 'WS', 'Samoa', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('188', 'SM', 'San Marino', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('189', 'ST', 'Sao Tome and Principe', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('190', 'SA', 'Saudi Arabia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('191', 'SN', 'Senegal', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('192', 'RS', 'Serbia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('193', 'SC', 'Seychelles', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('194', 'SL', 'Sierra Leone', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('195', 'SG', 'Singapore', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('196', 'SK', 'Slovakia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('197', 'SI', 'Slovenia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('198', 'SB', 'Solomon Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('199', 'SO', 'Somalia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('200', 'ZA', 'South Africa', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('201', 'GS', 'South Georgia South Sandwich Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('202', 'ES', 'Spain', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('203', 'LK', 'Sri Lanka', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('204', 'SH', 'St. Helena', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('205', 'PM', 'St. Pierre and Miquelon', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('206', 'SD', 'Sudan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('207', 'SR', 'Suriname', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('208', 'SJ', 'Svalbard and Jan Mayen Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('209', 'SZ', 'Swaziland', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('210', 'SE', 'Sweden', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('211', 'CH', 'Switzerland', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('212', 'SY', 'Syrian Arab Republic', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('213', 'TW', 'Taiwan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('214', 'TJ', 'Tajikistan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('215', 'TZ', 'Tanzania, United Republic of', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('216', 'TH', 'Thailand', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('217', 'TG', 'Togo', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('218', 'TK', 'Tokelau', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('219', 'TO', 'Tonga', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('220', 'TT', 'Trinidad and Tobago', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('221', 'TN', 'Tunisia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('222', 'TR', 'Turkey', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('223', 'TM', 'Turkmenistan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('224', 'TC', 'Turks and Caicos Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('225', 'TV', 'Tuvalu', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('226', 'UG', 'Uganda', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('227', 'UA', 'Ukraine', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('228', 'AE', 'United Arab Emirates', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('229', 'GB', 'United Kingdom', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('230', 'US', 'United States', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('231', 'UM', 'United States minor outlying islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('232', 'UY', 'Uruguay', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('233', 'UZ', 'Uzbekistan', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('234', 'VU', 'Vanuatu', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('235', 'VA', 'Vatican City State', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('236', 'VE', 'Venezuela', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('237', 'VN', 'Vietnam', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('238', 'VG', 'Virgin Islands (British)', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('239', 'VI', 'Virgin Islands (U.S.)', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('240', 'WF', 'Wallis and Futuna Islands', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('241', 'EH', 'Western Sahara', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('242', 'YE', 'Yemen', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('243', 'YU', 'Yugoslavia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('244', 'ZR', 'Zaire', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('245', 'ZM', 'Zambia', null, null, null, null, '1', null);
INSERT INTO `ref_countries` VALUES ('246', 'ZW', 'Zimbabwe', null, null, null, null, '1', null);

-- ----------------------------
-- Table structure for ref_currencies
-- ----------------------------
DROP TABLE IF EXISTS `ref_currencies`;
CREATE TABLE `ref_currencies` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(32) DEFAULT NULL,
  `currency_name` varchar(96) DEFAULT NULL,
  `currency_value` decimal(18,2) DEFAULT NULL COMMENT 'Satuan Rupiah',
  `currency_create_user_id` int(11) DEFAULT NULL,
  `currency_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `currency_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `currency_log_id` int(11) DEFAULT NULL COMMENT 'ID Parent',
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_currencies
-- ----------------------------
INSERT INTO `ref_currencies` VALUES ('1', 'Rp.', 'Rupiah', '1.00', '1', '2016-12-21 11:58:01', '1', null);

-- ----------------------------
-- Table structure for ref_employees
-- ----------------------------
DROP TABLE IF EXISTS `ref_employees`;
CREATE TABLE `ref_employees` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_fullname` varchar(64) DEFAULT NULL,
  `employee_nip` varchar(255) DEFAULT NULL,
  `employee_address` varchar(255) DEFAULT NULL,
  `employee_is_owner` int(11) DEFAULT NULL,
  `employee_phone` varchar(255) DEFAULT NULL,
  `employee_email` varchar(255) DEFAULT NULL,
  `employee_create_user_id` int(11) DEFAULT NULL,
  `employee_create_date` datetime DEFAULT NULL,
  `employee_status` int(1) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `employee_log_id` int(11) DEFAULT NULL COMMENT 'null=Data parent, not null=Histori untuk ID tercantum',
  `employee_branch_id` int(11) NOT NULL,
  `employee_profession_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_employees
-- ----------------------------
INSERT INTO `ref_employees` VALUES ('1', 'Administrator', '0610231556416', '', '1', '14125123124124', 'asdasdad@gmail.com', null, null, '1', null, '1', null);
INSERT INTO `ref_employees` VALUES ('15', 'dio', '32131232', 'Sebelah Tetangganya', null, '213123', 'dio@example.com', null, null, '1', null, '1', null);
INSERT INTO `ref_employees` VALUES ('6', 'Novianto', '0321312', '<p>sebelah rumah tetangga</p>\r\n', null, '089089089089', 'novianto@example.com', '1', '2017-09-11 13:25:29', '1', null, '1', null);
INSERT INTO `ref_employees` VALUES ('30', 'dio', '32131232', 'Sebelah Tetangganya', null, '213123', 'dio@example.com', null, null, '1', '15', '1', null);
INSERT INTO `ref_employees` VALUES ('29', 'dio', '32131232', 'Sebelah Tetangganya', null, '213123', 'dio@example.com', null, null, '1', '15', '1', null);
INSERT INTO `ref_employees` VALUES ('28', 'dio', '32131232', 'Sebelah Tetangganya', null, '213123', 'dio@example.com', null, null, '1', '15', '1', null);
INSERT INTO `ref_employees` VALUES ('14', 'dio', '23102909909', 'Sebelah Rumah Tetangganya', null, '3213231312', 'dio@example.com', null, null, '1', null, '2', null);
INSERT INTO `ref_employees` VALUES ('33', 'dio', '32131232', 'Sebelah Tetangganya', null, '213123', 'dio@example.com', null, null, '0', '15', '1', null);
INSERT INTO `ref_employees` VALUES ('32', 'dio', '32131232', 'Sebelah Tetangganya', null, '213123', 'dio@example.com', null, null, '1', '15', '1', null);
INSERT INTO `ref_employees` VALUES ('31', 'dio', '32131232', 'Sebelah Tetangganya', null, '213123', 'dio@example.com', null, null, '0', '15', '1', null);
INSERT INTO `ref_employees` VALUES ('34', 'Rrrrrr', '123123123123', 'asdasdasd', null, '080824985412545', 'asdasdad@gmail.com', null, null, '1', null, '1', null);
INSERT INTO `ref_employees` VALUES ('35', 'Administrator', null, null, null, null, null, null, null, '1', '1', '0', null);
INSERT INTO `ref_employees` VALUES ('36', 'dio', '32131232', 'Sebelah Tetangganya', null, '213123', 'dio@example.com', null, null, '1', '15', '1', null);
INSERT INTO `ref_employees` VALUES ('37', 'asdasdasdas', '2321312124124', 'asdasdadas', null, '21312412412', 'asdasdad@gmail.com', null, null, '1', null, '1', null);
INSERT INTO `ref_employees` VALUES ('38', 'dio', '32131232', 'Sebelah Tetangganya', null, '213123', 'dio@example.com', null, null, '0', '15', '1', null);
INSERT INTO `ref_employees` VALUES ('41', 'Rexa Akbar Malik', '2321312124124', 'jalan jalan', null, '083821857496', 'akbarrexa@gmail.com', null, null, '1', null, '39', null);
INSERT INTO `ref_employees` VALUES ('42', 'Rexa Akbar Malik', '2321312124124', 'jalan jalan', null, '083821857496', 'akbarrexa@gmail.com', null, null, '1', '41', '39', null);
INSERT INTO `ref_employees` VALUES ('43', 'Rexa Akbar Maliks', '2321312124124', 'jalan jalan', null, '083821857496', 'akbarrexa@gmail.com', null, null, '1', '41', '39', null);
INSERT INTO `ref_employees` VALUES ('44', 'Rexa Akbar Malik', '2321312124124', 'jalan jalan', null, '083821857496', 'akbarrexa@gmail.com', null, null, '1', '41', '39', null);
INSERT INTO `ref_employees` VALUES ('45', 'Rexa Akbar Malik', '2321312124124', 'jalan jalanss', null, '083821857496', 'akbarrexa@gmail.com', null, null, '1', '41', '39', null);
INSERT INTO `ref_employees` VALUES ('46', 'Sample Sample Sample', '45346346345345', 'SampleSampleSampleSampleSampleSample', null, '08231231241', 'Sample@gmail.com', null, null, '1', null, '40', null);
INSERT INTO `ref_employees` VALUES ('47', 'Rexa Akbar Malik', '012312321', 'asdasdasdas', null, '083821857496', 'akbarrexa@gmail.com', null, null, '1', null, '42', null);
INSERT INTO `ref_employees` VALUES ('48', 'Test Test  123', '123456', 'Jalan. Test', null, '123456', 'test123@gmail.com', null, null, '1', null, '45', null);
INSERT INTO `ref_employees` VALUES ('49', 'Chef test 123', '123456789123', 'jalan sample', null, '123456789', 'test123@gmail.com', null, null, '1', null, '44', null);
INSERT INTO `ref_employees` VALUES ('50', 'Chef test 123', '123456789123', 'jalan sample', null, '123456789', 'test123@gmail.com', null, null, '1', null, '44', null);
INSERT INTO `ref_employees` VALUES ('51', 'Chef test 123', '123456789123', 'jalan sample123', null, '123456', 'chef123@gmail.com', null, null, '1', null, '43', null);
INSERT INTO `ref_employees` VALUES ('52', 'Chef test 123', '123456789123', 'jalan sample123', null, '123456', 'chef123@gmail.com', null, null, '1', null, '43', null);
INSERT INTO `ref_employees` VALUES ('53', 'Test 123', '123456', 'test jalan', null, '083821857496', 'test123@gmail.com', null, null, '1', null, '46', null);
INSERT INTO `ref_employees` VALUES ('54', 'Test Waitress', '123456', 'jalan test', null, '123456', 'waitress@gmail.com', null, null, '1', null, '44', null);
INSERT INTO `ref_employees` VALUES ('55', 'Admin 123', '123456789', 'Test 123', null, '08123456789', 'sample123@gmail.com', null, null, '1', null, '47', null);
INSERT INTO `ref_employees` VALUES ('56', 'Owner 123456', '132456789', 'jalan test 123456', null, '123456789', 'owner@gmail.com', null, null, '1', null, '51', null);
INSERT INTO `ref_employees` VALUES ('57', 'Owner 123456', '132456789', 'jalan test 123', '1', '123456789', 'owner@gmail.com', null, null, '1', null, '51', null);
INSERT INTO `ref_employees` VALUES ('58', 'Admin 123456', '123456', 'Jalan sample123', null, '08123456789', 'sample123@gmail.com', null, null, '1', '56', '0', null);
INSERT INTO `ref_employees` VALUES ('59', 'Owner 123456', '132456789', 'jalan test 12345', null, '123456789', 'owner@gmail.com', null, null, '1', '56', '0', null);
INSERT INTO `ref_employees` VALUES ('60', 'Admin 123', '123456789', 'Test 123', null, '08123456789', 'sample123@gmail.com', null, null, '1', '55', '47', null);
INSERT INTO `ref_employees` VALUES ('61', 'Pemilik Kedai', '11445230294851', 'jalan test 123', null, '08127346581', 'pemilikkedai@gmail.com', null, null, '1', null, '54', null);

-- ----------------------------
-- Table structure for ref_materials
-- ----------------------------
DROP TABLE IF EXISTS `ref_materials`;
CREATE TABLE `ref_materials` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_unit_id` int(11) DEFAULT NULL,
  `material_code` varchar(16) DEFAULT NULL,
  `material_name` varchar(64) DEFAULT NULL,
  `material_quantity_current` int(11) DEFAULT NULL,
  `material_quantity_buffer` int(11) DEFAULT NULL,
  `material_create_user_id` int(11) DEFAULT NULL,
  `material_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `material_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `material_log_id` int(11) DEFAULT NULL COMMENT 'ID parent',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_materials
-- ----------------------------

-- ----------------------------
-- Table structure for ref_material_stocks
-- ----------------------------
DROP TABLE IF EXISTS `ref_material_stocks`;
CREATE TABLE `ref_material_stocks` (
  `materialstock_id` int(11) NOT NULL AUTO_INCREMENT,
  `materialstock_material_id` int(11) DEFAULT NULL,
  `materialstock_type` int(11) DEFAULT NULL COMMENT '1.Pembelian, 2.Penjualan, 3.StockOpnme',
  `materialstock_data_id` int(11) DEFAULT NULL COMMENT 'ID Penjualan atau ID Pembelian',
  `materialstock_quantity` int(11) DEFAULT NULL,
  `materialstock_create_user_id` int(11) DEFAULT NULL,
  `materialstock_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`materialstock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_material_stocks
-- ----------------------------

-- ----------------------------
-- Table structure for ref_news
-- ----------------------------
DROP TABLE IF EXISTS `ref_news`;
CREATE TABLE `ref_news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_company_id` int(11) DEFAULT NULL,
  `news_photo` text,
  `news_title` varchar(255) DEFAULT NULL,
  `news_body` text,
  `news_description` text,
  `news_city` varchar(255) DEFAULT NULL,
  `news_district` varchar(255) DEFAULT NULL,
  `news_province_id` int(11) DEFAULT NULL,
  `news_country_id` int(11) DEFAULT NULL,
  `news_created_at` timestamp NULL DEFAULT NULL,
  `news_updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `news_deleted_at` timestamp NULL DEFAULT NULL,
  `news_create_user_id` int(11) DEFAULT NULL,
  `news_status` int(11) DEFAULT NULL,
  `news_log_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_news
-- ----------------------------
INSERT INTO `ref_news` VALUES ('1', '6', null, 'Diskon Diseluruh Cabang', 'Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.', 'It won’t be a bigger problem to find one video game lover in your \r\nneighbor. Since the introduction of Virtual Game.', 'Bandung', 'Bojongloa Kaler', '8', '101', '2018-06-21 16:06:49', '2018-06-28 21:54:43', null, '1', '1', null);
INSERT INTO `ref_news` VALUES ('2', '6', null, 'Pembukaan Cabang Baru Di Kota X', 'Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.', 'It won’t be a bigger problem to find one video game lover in your \r\nneighbor. Since the introduction of Virtual Game.', 'Bandung', 'Bojongloa Kaler', '8', '101', '2018-06-28 16:12:14', '2018-06-28 21:54:43', null, '1', '1', null);
INSERT INTO `ref_news` VALUES ('3', '6', null, 'Diskon Pembukaan Cabang Baru', 'Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.', 'It won’t be a bigger problem to find one video game lover in your \r\nneighbor. Since the introduction of Virtual Game.', 'Bandung', 'Bojongloa Wetan', '8', '101', '2018-06-28 16:13:43', '2018-06-28 21:54:44', null, '1', '1', null);
INSERT INTO `ref_news` VALUES ('4', '6', null, 'Cabang Baru Di Kota Y', 'Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.', 'It won’t be a bigger problem to find one video game lover in your \r\nneighbor. Since the introduction of Virtual Game.', 'Bandung', 'Bojongloa Kaler', '8', '101', '2018-06-28 16:15:20', '2018-06-28 21:54:45', null, '1', '1', null);
INSERT INTO `ref_news` VALUES ('5', '6', null, 'Penutupan Cabang Di Kota Z', 'Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.\r\n\r\n\r\nRecently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems that would accept this new clarification and start doing business with me. Listed in this article are the electronic banking systems that accept players from the United States that wish to play in online casinos.', 'It won’t be a bigger problem to find one video game lover in your \r\nneighbor. Since the introduction of Virtual Game.', 'Bandung', 'Bojongloa Wetan', '8', '101', '2018-06-28 16:21:53', '2018-06-28 21:54:46', null, '1', '1', null);

-- ----------------------------
-- Table structure for ref_packages
-- ----------------------------
DROP TABLE IF EXISTS `ref_packages`;
CREATE TABLE `ref_packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_branch_id` int(11) DEFAULT NULL,
  `package_code` varchar(16) DEFAULT NULL,
  `package_name` varchar(64) DEFAULT NULL,
  `package_image` varchar(255) DEFAULT NULL,
  `package_price` int(11) NOT NULL,
  `package_date_start` date DEFAULT NULL,
  `package_date_end` date DEFAULT NULL,
  `package_time_start` time DEFAULT NULL,
  `package_time_end` time DEFAULT NULL,
  `package_create_user_id` int(11) DEFAULT NULL,
  `package_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `package_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `package_log_id` int(11) DEFAULT NULL COMMENT 'ID parent',
  `package_desc` text,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_packages
-- ----------------------------
INSERT INTO `ref_packages` VALUES ('1', '1', '1', 'Paket Hemat Oke', 'upload/packages/paket2.jpg', '123123123', '2016-12-22', '2018-02-22', '06:00:00', '19:00:00', '1', '2017-09-06 15:53:33', '1', null, '<p>2</p>\r\n');
INSERT INTO `ref_packages` VALUES ('2', '1', '2', 'Paket Hemat Malam', 'upload/packages/paket1.jpg', '11111111', '2016-12-22', '2018-02-23', '04:00:00', '22:00:00', '1', '2017-09-05 15:51:56', '1', null, '2');
INSERT INTO `ref_packages` VALUES ('59', '1', '3', 'dsadasas', 'upload/packages/juice_manggo.jpg', '321312312', '2017-09-05', '2017-09-05', '18:10:00', '19:00:00', '1', '2017-09-05 17:24:52', '1', null, '<p>gjhgjgghjg</p>\r\n');
INSERT INTO `ref_packages` VALUES ('60', '1', '21', 'paket baru', 'upload/packages/nasgor.jpg', '50000', '2017-09-15', '2017-09-23', '04:30:00', '21:30:00', '1', '2017-09-15 17:00:36', '1', null, '<p>paket baru</p>\r\n');
INSERT INTO `ref_packages` VALUES ('64', '1', '22', 'qwertyuiop', 'upload/packages/nasgor.jpg', '50000', '2017-09-15', '2017-09-30', '10:00:00', '19:00:00', '1', '2017-09-15 17:12:52', '1', null, '<p>dsdasdaasdsa</p>\r\n');
INSERT INTO `ref_packages` VALUES ('65', '1', '8', 'Paket Hematssss', 'upload/packages/2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000_(1).jpg', '30000', '2018-03-20', '2018-03-30', '24:00:00', '24:00:00', '1', '2018-03-20 14:04:10', '0', null, '<p>asdasdasdasdasdsa</p>\r\n');
INSERT INTO `ref_packages` VALUES ('66', '36', '8', 'Paket Hemat', 'upload/packages/2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000_(1).jpg', '30000', '2018-03-20', '2018-03-30', '24:00:00', '24:00:00', '1', '2018-03-20 13:25:55', '1', '1', '<p>asdasdasdasdasdsa</p>\r\n');
INSERT INTO `ref_packages` VALUES ('67', '1', '8', 'Paket Hematssss', 'upload/packages/2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000_(1).jpg', '30000', '2018-03-20', '2018-03-30', '24:00:00', '24:00:00', '1', '2018-03-20 14:03:45', '1', '1', '<p>asdasdasdasdasdsa</p>\r\n');
INSERT INTO `ref_packages` VALUES ('68', '1', '9', 'Paket Paketansss', 'upload/packages/2-1-pengelolaan-penambahan-koreksi-0413010101-jalan-negara-nasional-kelas-i-000103020000000.jpg', '400000', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '1', '2018-03-23 10:59:28', '0', null, '<p>asdsadasdasd</p>\r\n');
INSERT INTO `ref_packages` VALUES ('69', '1', '9', 'Paket Paketan', 'upload/packages/2-1-pengelolaan-penambahan-koreksi-0413010101-jalan-negara-nasional-kelas-i-000103020000000.jpg', '400000', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '1', '2018-03-20 14:17:37', '1', '1', '<p>asdsadasdasd</p>\r\n');
INSERT INTO `ref_packages` VALUES ('70', '1', '9', 'Paket Paketan', 'upload/packages/2-1-pengelolaan-penambahan-koreksi-0413010101-jalan-negara-nasional-kelas-i-000103020000000.jpg', '400000', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '1', '2018-03-20 14:18:07', '0', '1', '<p>asdsadasdasd</p>\r\n');
INSERT INTO `ref_packages` VALUES ('71', '1', '9', 'Paket Paketansss', 'upload/packages/2-1-pengelolaan-penambahan-koreksi-0413010101-jalan-negara-nasional-kelas-i-000103020000000.jpg', '400000', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '1', '2018-03-20 14:18:35', '1', '1', '<p>asdsadasdasd</p>\r\n');
INSERT INTO `ref_packages` VALUES ('72', '48', '001', 'Paket 01', 'upload/packages/2-2-pengelolaan-penambahan-koreksi-0205010307-container-000103020000000.jpg', '100000', '2018-04-19', '2018-04-30', '24:00:00', '24:00:00', '32', '2018-04-27 10:32:38', '1', null, '<p>paket 1 test</p>\r\n');
INSERT INTO `ref_packages` VALUES ('73', '48', '001', 'Paket 01', 'upload/packages/2-2-pengelolaan-penambahan-koreksi-0205010307-container-000103020000000.jpg', '80000', '2018-04-19', '2018-04-30', '24:00:00', '24:00:00', '32', '2018-04-23 11:37:17', '1', '1', '<p>paket 1 test</p>\r\n');
INSERT INTO `ref_packages` VALUES ('74', '48', '001', 'Paket 01', 'upload/packages/2-2-pengelolaan-penambahan-koreksi-0205010307-container-000103020000000.jpg', '100000', '2018-04-19', '2018-04-30', '24:00:00', '24:00:00', '32', '2018-04-24 09:43:45', '1', '1', '<p>paket 1 test</p>\r\n');
INSERT INTO `ref_packages` VALUES ('75', '48', '001', 'Paket 01', 'upload/packages/2-2-pengelolaan-penambahan-koreksi-0205010307-container-000103020000000.jpg', '100000', '2018-04-19', '2018-04-30', '24:00:00', '24:00:00', '32', '2018-04-27 10:31:16', '0', '1', '<p>paket 1 test</p>\r\n');

-- ----------------------------
-- Table structure for ref_package_items
-- ----------------------------
DROP TABLE IF EXISTS `ref_package_items`;
CREATE TABLE `ref_package_items` (
  `packageitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `packageitem_package_id` int(11) DEFAULT NULL,
  `packageitem_productitem_id` int(11) DEFAULT NULL,
  `packageitem_price` decimal(18,2) DEFAULT NULL,
  `packageitem_create_user_id` int(11) DEFAULT NULL,
  `packageitem_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `packageitem_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `packageitem_log_id` int(11) DEFAULT NULL COMMENT 'ID parent',
  PRIMARY KEY (`packageitem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_package_items
-- ----------------------------
INSERT INTO `ref_package_items` VALUES ('3', '2', '2', '20000.00', null, null, '1', null);
INSERT INTO `ref_package_items` VALUES ('4', '2', '11', '10000.00', null, null, '1', null);
INSERT INTO `ref_package_items` VALUES ('70', '59', '2', '25000.00', '1', '2017-09-15 17:28:53', '1', null);
INSERT INTO `ref_package_items` VALUES ('71', '59', '7', '15000.00', '1', '2017-09-15 17:28:53', '1', null);
INSERT INTO `ref_package_items` VALUES ('106', '64', '9', '12000.00', '1', '2017-09-18 10:28:20', '1', null);
INSERT INTO `ref_package_items` VALUES ('107', '64', '4', '10000.00', '1', '2017-09-18 10:28:20', '1', null);
INSERT INTO `ref_package_items` VALUES ('130', '60', '6', '20000.00', '1', '2017-09-18 10:59:54', '1', null);
INSERT INTO `ref_package_items` VALUES ('131', '60', '9', '12000.00', '1', '2017-09-18 10:59:54', '1', null);
INSERT INTO `ref_package_items` VALUES ('132', '60', '13', '30000.00', '1', '2017-09-18 10:59:55', '1', null);
INSERT INTO `ref_package_items` VALUES ('133', '65', '1', '15000.00', '1', '2018-03-20 14:03:45', '1', '1');
INSERT INTO `ref_package_items` VALUES ('134', '65', '2', '25000.00', '1', '2018-03-20 14:03:45', '1', '1');
INSERT INTO `ref_package_items` VALUES ('135', '65', '1', '15000.00', '1', '2018-03-20 14:03:45', '1', null);
INSERT INTO `ref_package_items` VALUES ('136', '65', '2', '25000.00', '1', '2018-03-20 14:03:45', '1', null);
INSERT INTO `ref_package_items` VALUES ('137', '65', '0', null, '1', '2018-03-20 14:03:45', '1', null);
INSERT INTO `ref_package_items` VALUES ('138', '68', '8', '12000.00', '1', '2018-03-20 14:18:35', '1', '1');
INSERT INTO `ref_package_items` VALUES ('139', '68', '8', '12000.00', '1', '2018-03-20 14:18:35', '1', null);
INSERT INTO `ref_package_items` VALUES ('140', '68', '0', null, '1', '2018-03-20 14:18:35', '1', null);
INSERT INTO `ref_package_items` VALUES ('141', '72', '26', '30000.00', '32', '2018-04-24 09:43:45', '1', '1');
INSERT INTO `ref_package_items` VALUES ('142', '72', '28', '25000.00', '32', '2018-04-24 09:43:45', '1', '1');
INSERT INTO `ref_package_items` VALUES ('143', '72', '26', '30000.00', '32', '2018-04-24 09:43:45', '1', '1');
INSERT INTO `ref_package_items` VALUES ('144', '72', '28', '25000.00', '32', '2018-04-24 09:43:45', '1', '1');
INSERT INTO `ref_package_items` VALUES ('145', '72', '26', '30000.00', '32', '2018-04-24 09:43:45', '1', null);
INSERT INTO `ref_package_items` VALUES ('146', '72', '28', '25000.00', '32', '2018-04-24 09:43:45', '1', null);
INSERT INTO `ref_package_items` VALUES ('147', '72', '26', '30000.00', '32', '2018-04-24 09:43:45', '1', null);
INSERT INTO `ref_package_items` VALUES ('148', '72', '28', '25000.00', '32', '2018-04-24 09:43:45', '1', null);
INSERT INTO `ref_package_items` VALUES ('149', '72', '26', '30000.00', '32', '2018-04-24 09:43:45', '1', null);

-- ----------------------------
-- Table structure for ref_product_categories
-- ----------------------------
DROP TABLE IF EXISTS `ref_product_categories`;
CREATE TABLE `ref_product_categories` (
  `productcate_id` int(11) NOT NULL AUTO_INCREMENT,
  `productcate_branch_id` int(11) DEFAULT NULL,
  `productcate_code` varchar(16) DEFAULT NULL,
  `productcate_image` varchar(255) DEFAULT NULL,
  `productcate_name` varchar(64) DEFAULT NULL,
  `productcate_create_user_id` int(11) DEFAULT NULL,
  `productcate_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `productcate_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `productcate_log_id` int(11) DEFAULT NULL COMMENT 'ID parent',
  PRIMARY KEY (`productcate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_product_categories
-- ----------------------------
INSERT INTO `ref_product_categories` VALUES ('1', '1', '1', 'menu_meal.png', 'Food', '1', '2017-09-07 14:36:04', '1', null);
INSERT INTO `ref_product_categories` VALUES ('2', '1', '2', 'menu_snack.png', 'Snack', '1', '2017-07-20 14:35:01', '1', null);
INSERT INTO `ref_product_categories` VALUES ('3', '1', '3', 'menu_dessert.png', 'Dessert', '1', '2017-07-20 14:35:01', '1', null);
INSERT INTO `ref_product_categories` VALUES ('4', '1', '4', 'menu_fruit.png', 'Fruits', '1', '2017-07-20 14:35:01', '1', null);
INSERT INTO `ref_product_categories` VALUES ('5', '1', '5', 'menu_juice.png', 'Juice', '1', '2017-07-20 14:35:01', '1', null);
INSERT INTO `ref_product_categories` VALUES ('6', '1', '6', 'menu_coffee.png', 'Coffe', '1', '2017-07-20 14:35:01', '1', null);
INSERT INTO `ref_product_categories` VALUES ('27', '1', '7', 'juice_manggo2.jpg', 'kategori 7', '1', '2017-09-20 15:42:42', '1', null);
INSERT INTO `ref_product_categories` VALUES ('28', '23', '7', 'juice_manggo2.jpg', 'kategori 7', '1', '2017-09-20 14:48:14', '1', '1');
INSERT INTO `ref_product_categories` VALUES ('29', '23', '7', 'juice_manggo2.jpg', 'kategori 7', '1', '2017-09-20 14:48:14', '1', '1');
INSERT INTO `ref_product_categories` VALUES ('30', '36', '007', '2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000_(1).jpg', 'Dessertsss', '1', '2018-03-20 13:14:54', '0', null);
INSERT INTO `ref_product_categories` VALUES ('31', '36', '007', '2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000_(1).jpg', 'Dessert', '1', '2018-03-20 13:13:34', '1', '1');
INSERT INTO `ref_product_categories` VALUES ('32', '36', '007', '2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000_(1).jpg', 'Dessertsss', '1', '2018-03-20 13:14:39', '1', '1');
INSERT INTO `ref_product_categories` VALUES ('33', '41', '123123123', '2-1-pengelolaan-penambahan-koreksi-0413010101-jalan-negara-nasional-kelas-i-000103020000000.jpg', 'Foodss', '26', '2018-04-05 13:28:49', '1', null);
INSERT INTO `ref_product_categories` VALUES ('34', '41', '123123123', '2-1-pengelolaan-penambahan-koreksi-0413010101-jalan-negara-nasional-kelas-i-000103020000000.jpg', 'Food', '26', '2018-04-05 13:22:28', '1', '1');
INSERT INTO `ref_product_categories` VALUES ('35', '44', '001', '5-perolehan-pembelian-0518020102-lapang-kuda-000103020000000.jpg', 'Food', '27', '2018-04-12 22:47:09', '1', null);
INSERT INTO `ref_product_categories` VALUES ('36', '43', '005', '14-tahun-sejak-kehadirannya--harga-mobil-avanza-tetap-kompetitif-7a24ce.jpg', 'Food', '27', '2018-04-13 17:16:52', '1', null);
INSERT INTO `ref_product_categories` VALUES ('37', '43', '999', 'Artboard_8.png', 'Dessert', '27', '2018-04-13 17:17:41', '1', null);
INSERT INTO `ref_product_categories` VALUES ('38', '48', '001', '14-tahun-sejak-kehadirannya--harga-mobil-avanza-tetap-kompetitif-7a24ce.jpg', 'Food', '32', '2018-04-27 10:26:44', '1', null);
INSERT INTO `ref_product_categories` VALUES ('39', '50', '002', '14-tahun-sejak-kehadirannya--harga-mobil-avanza-tetap-kompetitif-7a24ce.jpg', 'Food', '32', '2018-04-20 10:02:58', '1', null);
INSERT INTO `ref_product_categories` VALUES ('40', '48', '003', '5-perolehan-pembelian-0518020102-lapang-kuda-000103020000000_(1).jpg', 'Drink', '32', '2018-04-20 10:03:40', '1', null);
INSERT INTO `ref_product_categories` VALUES ('41', '50', '004', '5-perolehan-pembelian-0518020102-lapang-kuda-000103020000000_(1).jpg', 'Drink', '32', '2018-04-20 10:03:40', '1', null);
INSERT INTO `ref_product_categories` VALUES ('42', '48', '001', '14-tahun-sejak-kehadirannya--harga-mobil-avanza-tetap-kompetitif-7a24ce.jpg', 'Food', '32', '2018-04-20 10:02:55', '1', '1');
INSERT INTO `ref_product_categories` VALUES ('43', '48', '001', '14-tahun-sejak-kehadirannya--harga-mobil-avanza-tetap-kompetitif-7a24ce.jpg', 'Food', '32', '2018-04-27 10:26:26', '0', '1');
INSERT INTO `ref_product_categories` VALUES ('44', '55', '001', 'restaurant-cutlery-circular-symbol-of-a-spoon-and-a-fork-in-a-circle_318-61086.jpg', 'Food', '38', '2018-07-11 09:14:04', '1', null);
INSERT INTO `ref_product_categories` VALUES ('45', '55', '002', 'download.png', 'Drink', '38', '2018-07-11 09:14:23', '1', null);

-- ----------------------------
-- Table structure for ref_product_images
-- ----------------------------
DROP TABLE IF EXISTS `ref_product_images`;
CREATE TABLE `ref_product_images` (
  `productimage_id` int(11) NOT NULL AUTO_INCREMENT,
  `productimage_productitem_id` int(11) DEFAULT NULL,
  `productimage_path` varchar(255) DEFAULT NULL,
  `productimage_create_user_id` int(11) DEFAULT NULL,
  `productimage_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `productimage_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `productimage_log_id` int(11) DEFAULT NULL COMMENT 'ID parent',
  PRIMARY KEY (`productimage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_product_images
-- ----------------------------
INSERT INTO `ref_product_images` VALUES ('1', '1', 'upload/products/nasgor.jpg', '1', '2016-12-22 15:30:31', '1', null);
INSERT INTO `ref_product_images` VALUES ('2', '2', 'upload/products/steak.jpg', '1', '2016-12-22 15:30:31', '1', null);
INSERT INTO `ref_product_images` VALUES ('3', '3', 'upload/products/sate.jpg', '1', '2016-12-22 15:30:31', '1', null);
INSERT INTO `ref_product_images` VALUES ('4', '8', 'upload/products/juice_manggo.jpg', '1', '2016-12-22 15:30:31', '1', null);
INSERT INTO `ref_product_images` VALUES ('5', '14', 'upload/products/juice_manggo1.jpg', '1', '2017-09-05 13:31:35', '1', null);
INSERT INTO `ref_product_images` VALUES ('6', '14', 'upload/products/nasgor1.jpg', '1', '2017-09-05 13:31:39', '1', null);
INSERT INTO `ref_product_images` VALUES ('7', '14', 'upload/products/sate1.jpg', '1', '2017-09-05 13:31:44', '1', null);
INSERT INTO `ref_product_images` VALUES ('8', '14', 'upload/products/steak1.jpg', '1', '2017-09-05 13:31:50', '1', null);
INSERT INTO `ref_product_images` VALUES ('9', '15', 'upload/products/juice_manggo2.jpg', '1', '2017-09-05 13:35:27', '1', null);
INSERT INTO `ref_product_images` VALUES ('10', '15', 'upload/products/nasgor2.jpg', '1', '2017-09-05 13:35:31', '1', null);
INSERT INTO `ref_product_images` VALUES ('11', '15', 'upload/products/sate2.jpg', '1', '2017-09-05 13:35:35', '1', null);
INSERT INTO `ref_product_images` VALUES ('12', '15', 'upload/products/steak2.jpg', '1', '2017-09-05 13:35:38', '1', null);
INSERT INTO `ref_product_images` VALUES ('18', '17', 'upload/products/juice_manggo3.jpg', '1', '2017-09-14 15:05:28', '1', null);
INSERT INTO `ref_product_images` VALUES ('19', '17', 'upload/products/juice_manggo11.jpg', '1', '2017-09-14 15:05:31', '1', null);
INSERT INTO `ref_product_images` VALUES ('20', '17', 'upload/products/juice_manggo21.jpg', '1', '2017-09-14 15:05:33', '1', null);
INSERT INTO `ref_product_images` VALUES ('21', '18', 'upload/products/nasgor11.jpg', '1', '2017-09-14 15:36:42', '1', null);
INSERT INTO `ref_product_images` VALUES ('22', '18', 'upload/products/nasgor21.jpg', '1', '2017-09-14 15:36:50', '1', null);
INSERT INTO `ref_product_images` VALUES ('23', '19', 'upload/products/2-2-pengelolaan-penambahan-koreksi-0205010307-container-000103020000000.jpg', '1', '2018-03-20 14:25:54', '1', null);
INSERT INTO `ref_product_images` VALUES ('24', '22', 'upload/products/com.png', '27', '2018-04-16 21:33:23', '1', null);
INSERT INTO `ref_product_images` VALUES ('25', '23', 'upload/products/com.png', '27', '2018-04-16 21:33:23', '1', null);
INSERT INTO `ref_product_images` VALUES ('26', '22', 'upload/products/2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000_(2).jpg', '27', '2018-04-16 21:33:24', '1', null);
INSERT INTO `ref_product_images` VALUES ('27', '23', 'upload/products/2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000_(2).jpg', '27', '2018-04-16 21:33:24', '1', null);
INSERT INTO `ref_product_images` VALUES ('28', '24', 'upload/products/2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000.jpg', '27', '2018-04-18 15:58:33', '1', null);
INSERT INTO `ref_product_images` VALUES ('29', '25', 'upload/products/2-2-pengelolaan-penambahan-penambahan-nilai-0205010307-container-000103020000000.jpg', '27', '2018-04-18 15:58:33', '1', null);
INSERT INTO `ref_product_images` VALUES ('30', '26', 'upload/products/2-pengelolaan-perolehan-pembelian-0205010307-container-000103020000000_(4).jpg', '32', '2018-04-20 11:08:25', '1', null);
INSERT INTO `ref_product_images` VALUES ('31', '27', 'upload/products/2-pengelolaan-perolehan-pembelian-0205010307-container-000103020000000_(4).jpg', '32', '2018-04-20 11:08:25', '1', null);
INSERT INTO `ref_product_images` VALUES ('32', '28', 'upload/products/2-2-pengelolaan-pengurangan-pengurangan-nilai-0517010101-ilmu-pengetahuan-umum-000103020000000.jpg', '32', '2018-04-20 11:15:57', '1', null);
INSERT INTO `ref_product_images` VALUES ('33', '29', 'upload/products/2-2-pengelolaan-pengurangan-pengurangan-nilai-0517010101-ilmu-pengetahuan-umum-000103020000000.jpg', '32', '2018-04-20 11:15:57', '1', null);
INSERT INTO `ref_product_images` VALUES ('34', '33', 'upload/products/ayam-goreng.jpg', '38', '2018-07-11 09:19:12', '1', null);
INSERT INTO `ref_product_images` VALUES ('35', '34', 'upload/products/Ayam-Bakar-Asam-Manis.jpg', '38', '2018-07-11 09:20:25', '1', null);
INSERT INTO `ref_product_images` VALUES ('36', '35', 'upload/products/ayam-goreng-mentega.jpg', '38', '2018-07-11 09:21:27', '1', null);
INSERT INTO `ref_product_images` VALUES ('37', '36', 'upload/products/Ayam_Goreng_Manis.jpg', '38', '2018-07-11 09:22:51', '1', null);
INSERT INTO `ref_product_images` VALUES ('38', '37', 'upload/products/Ayam_Nanking.jpg', '38', '2018-07-11 09:24:06', '1', null);
INSERT INTO `ref_product_images` VALUES ('39', '38', 'upload/products/brungkol-ayam.jpg', '38', '2018-07-11 09:25:59', '1', null);
INSERT INTO `ref_product_images` VALUES ('40', '39', 'upload/products/brungkol-sapi.jpg', '38', '2018-07-11 09:27:10', '1', null);
INSERT INTO `ref_product_images` VALUES ('41', '40', 'upload/products/brungkol-udang.jpg', '38', '2018-07-11 09:28:21', '1', null);
INSERT INTO `ref_product_images` VALUES ('42', '41', 'upload/products/caisim-cha-ayam.jpg', '38', '2018-07-11 09:35:03', '1', null);
INSERT INTO `ref_product_images` VALUES ('43', '42', 'upload/products/caisim-cha-sapi.jpg', '38', '2018-07-11 09:36:21', '1', null);
INSERT INTO `ref_product_images` VALUES ('44', '43', 'upload/products/caisim-cha-udang.jpg', '38', '2018-07-11 09:37:15', '1', null);
INSERT INTO `ref_product_images` VALUES ('45', '44', 'upload/products/capcay-goreng.jpg', '38', '2018-07-11 09:38:12', '1', null);
INSERT INTO `ref_product_images` VALUES ('46', '45', 'upload/products/capcay-kuah-kental.jpg', '38', '2018-07-11 09:39:04', '1', null);
INSERT INTO `ref_product_images` VALUES ('47', '46', 'upload/products/gurame-asam-manis.jpg', '38', '2018-07-11 09:39:38', '1', null);
INSERT INTO `ref_product_images` VALUES ('48', '47', 'upload/products/gurame-goreng-tepung.jpg', '38', '2018-07-11 09:46:49', '1', null);
INSERT INTO `ref_product_images` VALUES ('49', '48', 'upload/products/i-fu-mie.jpg', '38', '2018-07-11 09:48:32', '1', null);
INSERT INTO `ref_product_images` VALUES ('50', '49', 'upload/products/Jamur_Cha_Ayam.jpg', '38', '2018-07-11 09:49:05', '1', null);
INSERT INTO `ref_product_images` VALUES ('51', '50', 'upload/products/jamur-cah-sapi.jpg', '38', '2018-07-11 09:50:10', '1', null);
INSERT INTO `ref_product_images` VALUES ('52', '51', 'upload/products/jamur-cach-udang.jpg', '38', '2018-07-11 09:53:00', '1', null);
INSERT INTO `ref_product_images` VALUES ('53', '52', 'upload/products/kangkung-cha-ayam.jpg', '38', '2018-07-11 09:54:11', '1', null);
INSERT INTO `ref_product_images` VALUES ('54', '53', 'upload/products/kangkung-cha-sapi.jpg', '38', '2018-07-11 09:55:23', '1', null);
INSERT INTO `ref_product_images` VALUES ('55', '54', 'upload/products/kangkung-cha-udang.jpg', '38', '2018-07-11 10:00:25', '1', null);
INSERT INTO `ref_product_images` VALUES ('56', '55', 'upload/products/nugget-kentang.jpg', '38', '2018-07-11 10:02:03', '1', null);
INSERT INTO `ref_product_images` VALUES ('57', '56', 'upload/products/kwetiaw-goreng.jpg', '38', '2018-07-11 10:03:36', '1', null);
INSERT INTO `ref_product_images` VALUES ('58', '57', 'upload/products/mie-goreng-kucay.jpg', '38', '2018-07-11 10:07:41', '1', null);
INSERT INTO `ref_product_images` VALUES ('59', '58', 'upload/products/mie-bihun-goreng.jpg', '38', '2018-07-11 10:11:49', '1', null);
INSERT INTO `ref_product_images` VALUES ('60', '59', 'upload/products/mie-goreng-spesial.jpg', '38', '2018-07-11 10:13:54', '1', null);
INSERT INTO `ref_product_images` VALUES ('61', '60', 'upload/products/mun-tahu-ayam.jpg', '38', '2018-07-11 10:14:59', '1', null);
INSERT INTO `ref_product_images` VALUES ('62', '61', 'upload/products/mun-tahu-ikan.jpg', '38', '2018-07-11 10:16:23', '1', null);
INSERT INTO `ref_product_images` VALUES ('63', '62', 'upload/products/mun-tahu-sapi.jpg', '38', '2018-07-11 10:17:24', '1', null);
INSERT INTO `ref_product_images` VALUES ('64', '63', 'upload/products/mun-tahu-udang.jpg', '38', '2018-07-11 10:18:19', '1', null);
INSERT INTO `ref_product_images` VALUES ('65', '64', 'upload/products/nasi-ayam-jamur.Jpeg', '38', '2018-07-11 10:21:22', '1', null);
INSERT INTO `ref_product_images` VALUES ('66', '65', 'upload/products/nasi-ayam-komplit.jpg', '38', '2018-07-11 10:22:31', '1', null);
INSERT INTO `ref_product_images` VALUES ('67', '66', 'upload/products/nasi-capcay.jpg', '38', '2018-07-11 10:23:17', '1', null);
INSERT INTO `ref_product_images` VALUES ('68', '67', 'upload/products/nasi-goreng-ayam.jpg', '38', '2018-07-11 10:28:05', '1', null);
INSERT INTO `ref_product_images` VALUES ('69', '68', 'upload/products/nasi-goreng-biasa.jpg', '38', '2018-07-11 10:30:15', '1', null);
INSERT INTO `ref_product_images` VALUES ('70', '69', 'upload/products/', '38', '2018-07-11 11:17:49', '1', null);
INSERT INTO `ref_product_images` VALUES ('71', '70', 'upload/products/nasi-goreng-sapi.jpg', '38', '2018-07-11 11:20:53', '1', null);
INSERT INTO `ref_product_images` VALUES ('72', '71', 'upload/products/nasi-goreng-seafood.jpg', '38', '2018-07-11 11:23:41', '1', null);
INSERT INTO `ref_product_images` VALUES ('73', '72', 'upload/products/nasi-goreng-spesial.jpg', '38', '2018-07-11 11:26:42', '1', null);
INSERT INTO `ref_product_images` VALUES ('74', '73', 'upload/products/nasi-sapi-jamur.jpg', '38', '2018-07-11 11:27:41', '1', null);
INSERT INTO `ref_product_images` VALUES ('75', '74', 'upload/products/nasi-putih.jpg', '38', '2018-07-11 11:28:33', '1', null);
INSERT INTO `ref_product_images` VALUES ('76', '75', 'upload/products/Puyung hay.JPG', '38', '2018-07-11 11:28:59', '1', null);
INSERT INTO `ref_product_images` VALUES ('77', '76', 'upload/products/sambal-dadakan-lalab.jpg', '38', '2018-07-11 11:29:55', '1', null);
INSERT INTO `ref_product_images` VALUES ('78', '77', 'upload/products/sapo-tahu-ayam.jpg', '38', '2018-07-11 11:30:55', '1', null);
INSERT INTO `ref_product_images` VALUES ('79', '78', 'upload/products/sapo-tahu-ikan.jpg', '38', '2018-07-11 11:35:45', '1', null);
INSERT INTO `ref_product_images` VALUES ('80', '79', 'upload/products/sapo-tahu-udang.jpg', '38', '2018-07-11 11:38:20', '1', null);
INSERT INTO `ref_product_images` VALUES ('81', '80', 'upload/products/tahu-tempe.jpg', '38', '2018-07-11 11:39:17', '1', null);
INSERT INTO `ref_product_images` VALUES ('82', '81', 'upload/products/tahu-tausi-ayam.JPG', '38', '2018-07-11 11:40:11', '1', null);
INSERT INTO `ref_product_images` VALUES ('83', '82', 'upload/products/', '38', '2018-07-11 11:42:30', '1', null);
INSERT INTO `ref_product_images` VALUES ('84', '83', 'upload/products/telur-dadar-mata-sapi.png', '38', '2018-07-11 11:43:15', '1', null);
INSERT INTO `ref_product_images` VALUES ('85', '84', 'upload/products/tumis-kangkung.jpg', '38', '2018-07-11 11:44:16', '1', null);
INSERT INTO `ref_product_images` VALUES ('86', '85', 'upload/products/udang-goreng-mentega.jpg', '38', '2018-07-11 11:45:01', '1', null);
INSERT INTO `ref_product_images` VALUES ('87', '86', 'upload/products/udang-goreng-tepung.jpg', '38', '2018-07-11 11:45:57', '1', null);
INSERT INTO `ref_product_images` VALUES ('88', '87', 'upload/products/bandrek-bajigur.jpg', '38', '2018-07-11 11:49:03', '1', null);
INSERT INTO `ref_product_images` VALUES ('89', '88', 'upload/products/capuccino.jpg', '38', '2018-07-11 11:56:41', '1', null);
INSERT INTO `ref_product_images` VALUES ('90', '89', 'upload/products/coca-cola-sprite-fanta.jpg', '38', '2018-07-11 11:51:56', '1', null);
INSERT INTO `ref_product_images` VALUES ('91', '90', 'upload/products/es-jeruk-panas.jpg', '38', '2018-07-11 11:52:56', '1', null);
INSERT INTO `ref_product_images` VALUES ('92', '92', 'upload/products/es-lemon-tea.jpg', '38', '2018-07-11 11:58:23', '1', null);
INSERT INTO `ref_product_images` VALUES ('93', '93', 'upload/products/es-teh-manis.png', '38', '2018-07-11 11:59:44', '1', null);
INSERT INTO `ref_product_images` VALUES ('94', '94', 'upload/products/es-teh-tarik.jpg', '38', '2018-07-11 12:00:48', '1', null);
INSERT INTO `ref_product_images` VALUES ('95', '95', 'upload/products/es-teh-tawar.jpg', '38', '2018-07-11 12:01:27', '1', null);
INSERT INTO `ref_product_images` VALUES ('96', '96', 'upload/products/jus-alpukat.jpg', '38', '2018-07-11 12:02:14', '1', null);
INSERT INTO `ref_product_images` VALUES ('97', '97', 'upload/products/jus-buah-naga.jpg', '38', '2018-07-11 12:02:57', '1', null);
INSERT INTO `ref_product_images` VALUES ('98', '98', 'upload/products/just-jambu.jpg', '38', '2018-07-11 12:03:43', '1', null);
INSERT INTO `ref_product_images` VALUES ('99', '99', 'upload/products/jus-mangga.jpg', '38', '2018-07-11 12:04:34', '1', null);
INSERT INTO `ref_product_images` VALUES ('100', '100', 'upload/products/jus-melon.png', '38', '2018-07-11 12:05:09', '1', null);
INSERT INTO `ref_product_images` VALUES ('101', '101', 'upload/products/jus-mix.jpg', '38', '2018-07-11 12:05:58', '1', null);
INSERT INTO `ref_product_images` VALUES ('102', '102', 'upload/products/jus-sirsak.png', '38', '2018-07-11 12:11:16', '1', null);
INSERT INTO `ref_product_images` VALUES ('103', '103', 'upload/products/jus-strawberry.jpg', '38', '2018-07-11 13:02:50', '1', null);
INSERT INTO `ref_product_images` VALUES ('104', '104', 'upload/products/jus-tomat.jpg', '38', '2018-07-11 13:03:36', '1', null);
INSERT INTO `ref_product_images` VALUES ('105', '105', 'upload/products/jus-wortel.jpg', '38', '2018-07-11 13:04:30', '1', null);
INSERT INTO `ref_product_images` VALUES ('106', '106', 'upload/products/kopi-hitam.jpg', '38', '2018-07-11 13:05:21', '1', null);
INSERT INTO `ref_product_images` VALUES ('107', '107', 'upload/products/kopi-susu.png', '38', '2018-07-11 13:06:00', '1', null);
INSERT INTO `ref_product_images` VALUES ('108', '108', 'upload/products/mineral-water.jpg', '38', '2018-07-11 13:07:11', '1', null);
INSERT INTO `ref_product_images` VALUES ('109', '109', 'upload/products/soup-buah.jpg', '38', '2018-07-11 13:07:59', '1', null);
INSERT INTO `ref_product_images` VALUES ('110', '110', 'upload/products/susu-soda.jpg', '38', '2018-07-11 13:11:39', '1', null);
INSERT INTO `ref_product_images` VALUES ('111', '111', 'upload/products/teh-botol.jpg', '38', '2018-07-11 13:13:17', '1', null);
INSERT INTO `ref_product_images` VALUES ('112', '112', 'upload/products/teh-kayu-manis-madu.jpg', '38', '2018-07-11 13:14:02', '1', null);
INSERT INTO `ref_product_images` VALUES ('113', '113', 'upload/products/teh-kayu-manis-panas.jpg', '38', '2018-07-11 13:14:43', '1', null);

-- ----------------------------
-- Table structure for ref_product_items
-- ----------------------------
DROP TABLE IF EXISTS `ref_product_items`;
CREATE TABLE `ref_product_items` (
  `productitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `productitem_productcate_id` int(11) DEFAULT NULL,
  `productitem_unit_id` int(11) DEFAULT NULL,
  `productitem_code` varchar(16) DEFAULT NULL,
  `productitem_name` varchar(64) DEFAULT NULL,
  `productitem_availability` int(11) DEFAULT NULL,
  `productitem_auto_count` int(11) DEFAULT NULL,
  `productitem_default_availability` int(11) DEFAULT NULL,
  `productitem_currency_id` int(11) DEFAULT NULL,
  `productitem_normal_price` decimal(18,2) DEFAULT NULL,
  `productitem_description` varchar(4000) DEFAULT NULL,
  `productitem_create_user_id` int(11) DEFAULT NULL,
  `productitem_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `productitem_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `productitem_log_id` int(11) DEFAULT NULL COMMENT 'ID parent',
  PRIMARY KEY (`productitem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_product_items
-- ----------------------------
INSERT INTO `ref_product_items` VALUES ('1', '1', '1', '1.1', 'Nasi Goreng', '13', '1', '10', '1', '15000.00', '', '1', '2017-08-31 11:59:36', '1', null);
INSERT INTO `ref_product_items` VALUES ('2', '1', '1', '1.2', 'Steak Sapi', '10', '1', '10', '1', '25000.00', null, '1', '2017-08-04 09:30:53', '1', null);
INSERT INTO `ref_product_items` VALUES ('3', '1', '1', '1.3', 'Sate Ayam', '0', '1', '10', '1', '20000.00', null, '1', '2017-08-09 15:09:49', '1', null);
INSERT INTO `ref_product_items` VALUES ('4', '2', '1', '2.1', 'Lumpia', '1', '0', '1', '1', '10000.00', null, '1', '2016-12-22 13:54:38', '1', null);
INSERT INTO `ref_product_items` VALUES ('5', '2', '1', '2.2', 'Kentang Goreng', '1', '0', '1', '1', '8000.00', null, '1', '2016-12-22 13:54:32', '1', null);
INSERT INTO `ref_product_items` VALUES ('6', '3', '1', '3.1', 'Brownies', '0', '0', '1', '1', '20000.00', null, '1', '2017-08-04 11:52:24', '1', null);
INSERT INTO `ref_product_items` VALUES ('7', '4', '1', '4.1', 'Salad Buah', '1', '0', '1', '1', '15000.00', null, '1', '2016-12-22 13:55:40', '1', null);
INSERT INTO `ref_product_items` VALUES ('8', '5', '1', '5.1', 'Juice Mangga', '1', '0', '1', '1', '12000.00', null, '1', '2016-12-22 13:57:54', '1', null);
INSERT INTO `ref_product_items` VALUES ('9', '5', '1', '5.2', 'Juice Strawberi', '1', '0', '1', '1', '12000.00', null, '1', '2016-12-22 13:57:54', '1', null);
INSERT INTO `ref_product_items` VALUES ('10', '5', '1', '5.3', 'Juice Melon', '1', '0', '1', '1', '12000.00', null, '1', '2016-12-22 13:57:54', '1', null);
INSERT INTO `ref_product_items` VALUES ('11', '6', '1', '6.1', 'Cappucino Ice', '1', '0', '1', '1', '12000.00', null, '1', '2016-12-22 13:57:54', '1', null);
INSERT INTO `ref_product_items` VALUES ('12', '6', '1', '6.2', 'Cappucino Hot', '1', '0', '1', '1', '10000.00', null, '1', '2016-12-22 13:57:54', '1', null);
INSERT INTO `ref_product_items` VALUES ('13', '1', '1', '1.4', 'PEPESH', '12', '1', '12', '1', '30000.00', '', '1', '2017-09-05 16:56:12', '1', null);
INSERT INTO `ref_product_items` VALUES ('14', '1', '1', '1.6', 'Seblak', null, null, '10', '1', '11000.00', '<p>ini seblak</p>\r\n', '1', '2017-09-04 09:20:59', '1', null);
INSERT INTO `ref_product_items` VALUES ('15', '1', '1', '1.7', 'Kwetiaw', '0', '1', '10', '1', '10000.00', '', '1', '2017-09-05 13:33:44', '1', null);
INSERT INTO `ref_product_items` VALUES ('16', '1', '1', '1.4', 'PEPESH', '0', '1', '12', '1', '2222.00', null, '1', '2017-08-09 16:43:32', '1', '1');
INSERT INTO `ref_product_items` VALUES ('17', '1', '1', '1.8', 'mie goreng', '0', '1', '10', '1', '100000.00', '<p>bjhgghjghjhjg</p>\r\n', '1', '2017-09-14 15:04:45', '1', null);
INSERT INTO `ref_product_items` VALUES ('18', '1', '1', '1.9', 'nama', '0', '1', '10', '1', '100000.00', '<p>dsaasdsa</p>\r\n', '1', '2017-09-14 15:35:58', '1', null);
INSERT INTO `ref_product_items` VALUES ('19', '1', '1', '007', 'Ayam Ngebakar Jakarta', '500', '1', '50', '1', '500000.00', '<p>asdasdasdasd</p>\r\n', '1', '2018-03-20 14:30:50', '0', null);
INSERT INTO `ref_product_items` VALUES ('20', '1', '1', '007', 'Ayam Ngebakar Jakarta', '0', '1', '50', '1', '500000.00', null, '1', '2018-03-20 14:25:54', '1', '1');
INSERT INTO `ref_product_items` VALUES ('21', '1', '1', '007', 'Ayam Ngebakar Jakarta', '500', '1', '50', '1', '500000.00', null, '1', '2018-03-20 14:30:24', '1', '1');
INSERT INTO `ref_product_items` VALUES ('22', '35', null, '001', 'Nasi Goreng Baso', '0', '1', '0', '1', '30000.00', '<p>tidak ada</p>\r\n', '27', '2018-04-16 21:40:40', '1', null);
INSERT INTO `ref_product_items` VALUES ('23', '36', null, '001', 'Nasi Goreng Baso', '0', '1', '0', '1', '50000.00', '<p>tidak ada</p>\r\n', '27', '2018-04-16 21:40:43', '1', null);
INSERT INTO `ref_product_items` VALUES ('24', '35', null, '123', 'Nasi Goreng', '0', '1', '20', '1', '25000.00', '<p>nasi goreng</p>\r\n', '27', '2018-04-18 15:58:33', '1', null);
INSERT INTO `ref_product_items` VALUES ('25', '36', null, '123', 'Nasi Goreng', '0', '1', '20', '1', '30000.00', '<p>nasi goreng</p>\r\n', '27', '2018-04-18 15:58:33', '1', null);
INSERT INTO `ref_product_items` VALUES ('26', '40', '0', '001', 'Es Teh Manis', '20', '1', '20', '1', '30000.00', '<p>Es teh manis dengan gula aren</p>\r\n', '32', '2018-04-27 10:40:30', '1', null);
INSERT INTO `ref_product_items` VALUES ('27', '41', null, '002', 'Es Teh Manis', '0', '1', '30', '1', '35000.00', '<p>Es teh manis dengan gula aren</p>\r\n', '32', '2018-04-20 11:08:25', '1', null);
INSERT INTO `ref_product_items` VALUES ('28', '38', null, '003', 'Nasi Goreng Ayam', '0', '1', '20', '1', '25000.00', '<p>Nasi goreng ayam</p>\r\n', '32', '2018-04-20 11:15:57', '1', null);
INSERT INTO `ref_product_items` VALUES ('29', '39', null, '004', 'Nasi Goreng Ayam', '0', '1', '25', '1', '30000.00', '<p>Nasi goreng ayam</p>\r\n', '32', '2018-04-20 11:15:57', '1', null);
INSERT INTO `ref_product_items` VALUES ('30', '40', null, '001', 'Es Teh Manis', '0', '1', '20', '1', '25000.00', null, '32', '2018-04-20 11:08:25', '1', '1');
INSERT INTO `ref_product_items` VALUES ('31', '40', '0', '001', 'Es Teh Manis', '20', '1', '20', '1', '30000.00', null, '32', '2018-04-20 11:28:39', '1', '1');
INSERT INTO `ref_product_items` VALUES ('32', '40', '0', '001', 'Es Teh Manis', '20', '1', '20', '1', '30000.00', null, '32', '2018-04-27 10:39:48', '0', '1');
INSERT INTO `ref_product_items` VALUES ('33', '44', null, '001', 'Ayam Goreng ', '0', '1', '40', '1', '10000.00', '<p>Ini ayam goreng</p>\r\n', '38', '2018-07-11 09:19:12', '1', null);
INSERT INTO `ref_product_items` VALUES ('34', '44', null, '002', 'Ayam Goreng Asam Manis', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Ayam Goreng Asam Manis</p>\r\n', '38', '2018-07-11 09:20:25', '1', null);
INSERT INTO `ref_product_items` VALUES ('35', '44', null, '003', 'Ayam Goreng Mentega', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Ayam Goreng Mentega</p>\r\n', '38', '2018-07-11 09:21:27', '1', null);
INSERT INTO `ref_product_items` VALUES ('36', '44', null, '004', 'Ayam Kecap Manis', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Ayam Kecap Manis</p>\r\n', '38', '2018-07-11 09:22:51', '1', null);
INSERT INTO `ref_product_items` VALUES ('37', '44', null, '005', 'Ayam Nangking', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Ayam Nangking</p>\r\n', '38', '2018-07-11 09:24:06', '1', null);
INSERT INTO `ref_product_items` VALUES ('38', '44', null, '006', 'Brungkol Cha Ayam', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Brungkol Cha Ayam</p>\r\n', '38', '2018-07-11 09:25:59', '1', null);
INSERT INTO `ref_product_items` VALUES ('39', '44', null, '007', 'Brungkol Cha Sapi', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Brungkol Cha Sapi</p>\r\n', '38', '2018-07-11 09:27:10', '1', null);
INSERT INTO `ref_product_items` VALUES ('40', '44', null, '008', 'Brungkol Cha Udang', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Brungkol Cha Udang</p>\r\n', '38', '2018-07-11 09:28:21', '1', null);
INSERT INTO `ref_product_items` VALUES ('41', '44', null, '009', 'Caisim Cha Ayam', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Caisim Cha Ayam</p>\r\n', '38', '2018-07-11 09:35:03', '1', null);
INSERT INTO `ref_product_items` VALUES ('42', '44', null, '010', 'Caisim Cha Sapi', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Caisim Cha Sapi</p>\r\n', '38', '2018-07-11 09:36:21', '1', null);
INSERT INTO `ref_product_items` VALUES ('43', '44', null, '011', 'Caisim Cha Udang', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Caisim Cha Udang</p>\r\n', '38', '2018-07-11 09:37:15', '1', null);
INSERT INTO `ref_product_items` VALUES ('44', '44', null, '012', 'Capcay Goreng', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Capcay Goreng</p>\r\n', '38', '2018-07-11 09:38:12', '1', null);
INSERT INTO `ref_product_items` VALUES ('45', '44', null, '013', 'Capcay Kuah', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Capcay Kuah</p>\r\n', '38', '2018-07-11 09:39:03', '1', null);
INSERT INTO `ref_product_items` VALUES ('46', '44', null, '014', 'Gurame Asam Manis', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Gurame Asam Manis</p>\r\n', '38', '2018-07-11 09:39:38', '1', null);
INSERT INTO `ref_product_items` VALUES ('47', '44', null, '015', 'Gurame Goreng Tepung', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Gurame Goreng Tepung</p>\r\n', '38', '2018-07-11 09:46:49', '1', null);
INSERT INTO `ref_product_items` VALUES ('48', '44', null, '016', 'I Fu Mie ', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;I Fu Mie&nbsp;</p>\r\n', '38', '2018-07-11 09:48:32', '1', null);
INSERT INTO `ref_product_items` VALUES ('49', '44', null, '017', 'Jamur Cha Ayam', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Jamur Cha Ayam</p>\r\n', '38', '2018-07-11 09:49:05', '1', null);
INSERT INTO `ref_product_items` VALUES ('50', '44', null, '018', 'Jamur Cha Sapi ', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Jamur Cha Sapi&nbsp;</p>\r\n', '38', '2018-07-11 09:50:10', '1', null);
INSERT INTO `ref_product_items` VALUES ('51', '44', null, '019', 'Jamur Cha Udang', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Jamur Cha Udang</p>\r\n', '38', '2018-07-11 09:53:00', '1', null);
INSERT INTO `ref_product_items` VALUES ('52', '44', null, '020', 'Kangkung Cha Ayam', '0', '1', '40', '1', '20000.00', '<p>ini&nbsp;Kangkung Cha Ayam</p>\r\n', '38', '2018-07-11 09:54:11', '1', null);
INSERT INTO `ref_product_items` VALUES ('53', '44', null, '021', 'Kangkung Cha Sapi', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Kangkung Cha Sapi</p>\r\n', '38', '2018-07-11 09:55:23', '1', null);
INSERT INTO `ref_product_items` VALUES ('54', '44', null, '022', 'Kangkung Cha Udang', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Kangkung Cha Udang</p>\r\n', '38', '2018-07-11 10:00:25', '1', null);
INSERT INTO `ref_product_items` VALUES ('55', '44', null, '023', 'Kentang Nugget / Sosis ', '0', '1', '40', '1', '15000.00', '<p>ini&nbsp;Kentang Nugget / Sosis&nbsp;</p>\r\n', '38', '2018-07-11 10:02:03', '1', null);
INSERT INTO `ref_product_items` VALUES ('56', '44', null, '024', 'Kwetiaw Goreng', '0', '1', '40', '1', '20000.00', '<p>ini&nbsp;Kwetiaw Goreng</p>\r\n', '38', '2018-07-11 10:03:36', '1', null);
INSERT INTO `ref_product_items` VALUES ('57', '44', null, '025', 'Mie Goreng Kucay', '0', '1', '40', '1', '20000.00', '<p>ini&nbsp;Mie Goreng Kucay</p>\r\n', '38', '2018-07-11 10:07:41', '1', null);
INSERT INTO `ref_product_items` VALUES ('58', '44', null, '026', 'Mie Goreng/Bihun Biasa', '0', '1', '40', '1', '15000.00', '<p>ini&nbsp;Mie Goreng/Bihun Biasa</p>\r\n', '38', '2018-07-11 10:11:49', '1', null);
INSERT INTO `ref_product_items` VALUES ('59', '44', null, '027', 'Mie/Bihun Goreng Special', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Mie/Bihun Goreng Special</p>\r\n', '38', '2018-07-11 10:13:54', '1', null);
INSERT INTO `ref_product_items` VALUES ('60', '44', null, '028', 'Mun Tahu Ayam', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Mun Tahu Ayam</p>\r\n', '38', '2018-07-11 10:14:59', '1', null);
INSERT INTO `ref_product_items` VALUES ('61', '44', null, '029', 'Mun Tahu Ikan', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Mun Tahu Ikan</p>\r\n', '38', '2018-07-11 10:16:22', '1', null);
INSERT INTO `ref_product_items` VALUES ('62', '44', null, '030', 'Mun Tahu Sapi', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Mun Tahu Sapi</p>\r\n', '38', '2018-07-11 10:17:24', '1', null);
INSERT INTO `ref_product_items` VALUES ('63', '44', null, '031', 'Mun Tahu Udang', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Mun Tahu Udang</p>\r\n', '38', '2018-07-11 10:18:19', '1', null);
INSERT INTO `ref_product_items` VALUES ('64', '44', null, '032', 'Nasi Ayam Jamur', '0', '1', '40', '1', '20000.00', '<p>ini&nbsp;Nasi Ayam Jamur</p>\r\n', '38', '2018-07-11 10:21:22', '1', null);
INSERT INTO `ref_product_items` VALUES ('65', '44', null, '033', 'Nasi Ayam Komplit ', '0', '1', '40', '1', '12000.00', '<p>ini&nbsp;Nasi Ayam Komplit&nbsp;</p>\r\n', '38', '2018-07-11 10:22:31', '1', null);
INSERT INTO `ref_product_items` VALUES ('66', '44', null, '034', 'Nasi Capcay', '0', '1', '40', '1', '20000.00', '<p>ini&nbsp;Nasi Capcay</p>\r\n', '38', '2018-07-11 10:23:17', '1', null);
INSERT INTO `ref_product_items` VALUES ('67', '44', null, '035', 'Nasi Goreng Ayam/Sosis', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Nasi Goreng Ayam/Sosis</p>\r\n', '38', '2018-07-11 10:28:05', '1', null);
INSERT INTO `ref_product_items` VALUES ('68', '44', null, '036', 'Nasi Goreng Biasa', '0', '1', '40', '1', '15000.00', '<p>ini&nbsp;Nasi Goreng Biasa</p>\r\n', '38', '2018-07-11 10:30:15', '1', null);
INSERT INTO `ref_product_items` VALUES ('69', '44', null, '037', 'Nasi Goreng Bistik', '0', '1', '40', '1', '20000.00', '<p>ini&nbsp;Nasi Goreng Bistik</p>\r\n', '38', '2018-07-11 11:17:49', '1', null);
INSERT INTO `ref_product_items` VALUES ('70', '44', null, '038', 'Nasi Goreng Sapi', '0', '1', '40', '1', '20000.00', '<p>ini&nbsp;Nasi Goreng Sapi</p>\r\n', '38', '2018-07-11 11:20:53', '1', null);
INSERT INTO `ref_product_items` VALUES ('71', '44', null, '039', 'Nasi Goreng Seafood', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Nasi Goreng Seafood</p>\r\n', '38', '2018-07-11 11:23:41', '1', null);
INSERT INTO `ref_product_items` VALUES ('72', '44', null, '040', 'Nasi Goreng Special', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Nasi Goreng Special</p>\r\n', '38', '2018-07-11 11:26:42', '1', null);
INSERT INTO `ref_product_items` VALUES ('73', '44', null, '041', 'Nasi Sapi Jamur', '0', '1', '40', '1', '4000.00', '<p>ini&nbsp;Nasi Sapi Jamur</p>\r\n', '38', '2018-07-11 11:27:41', '1', null);
INSERT INTO `ref_product_items` VALUES ('74', '44', null, '042', 'Nasi Putih', '0', '1', '40', '1', '0.00', '<p>ini&nbsp;Nasi Putih</p>\r\n', '38', '2018-07-11 11:28:33', '1', null);
INSERT INTO `ref_product_items` VALUES ('75', '44', null, '043', 'Puyunghay', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Puyunghay</p>\r\n', '38', '2018-07-11 11:28:59', '1', null);
INSERT INTO `ref_product_items` VALUES ('76', '44', null, '044', 'Sambal Dadakan & Lalab', '0', '1', '40', '1', '3000.00', '<p>ini&nbsp;Sambal Dadakan &amp; Lalab</p>\r\n', '38', '2018-07-11 11:29:55', '1', null);
INSERT INTO `ref_product_items` VALUES ('77', '44', null, '045', 'Sapo Tahu Ayam', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Sapo Tahu Ayam</p>\r\n', '38', '2018-07-11 11:30:54', '1', null);
INSERT INTO `ref_product_items` VALUES ('78', '44', null, '046', 'Sapo Tahu Ikan', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Sapo Tahu Ikan</p>\r\n', '38', '2018-07-11 11:35:45', '1', null);
INSERT INTO `ref_product_items` VALUES ('79', '44', null, '047', 'Sapo Tahu Udang', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Sapo Tahu Udang</p>\r\n', '38', '2018-07-11 11:38:20', '1', null);
INSERT INTO `ref_product_items` VALUES ('80', '44', null, '048', 'Tahu / Tempe', '0', '1', '40', '1', '20000.00', '<p>ini&nbsp;Tahu / Tempe</p>\r\n', '38', '2018-07-11 11:39:17', '1', null);
INSERT INTO `ref_product_items` VALUES ('81', '44', null, '049', 'Tahu Tausi Ayam', '0', '1', '40', '1', '25000.00', '<p>ini&nbsp;Tahu Tausi Ayam</p>\r\n', '38', '2018-07-11 11:40:11', '1', null);
INSERT INTO `ref_product_items` VALUES ('82', '44', null, '050', 'Tahu Tausi Ikan', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Tahu Tausi Ikan</p>\r\n', '38', '2018-07-11 11:42:30', '1', null);
INSERT INTO `ref_product_items` VALUES ('83', '44', null, '051', 'Telur Dadar/Mata Sapi', '0', '1', '40', '1', '4000.00', '<p>ini&nbsp;Telur Dadar/Mata Sapi</p>\r\n', '38', '2018-07-11 11:43:15', '1', null);
INSERT INTO `ref_product_items` VALUES ('84', '44', null, '052', 'Tumis Kangkung', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Tumis Kangkung</p>\r\n', '38', '2018-07-11 11:44:16', '1', null);
INSERT INTO `ref_product_items` VALUES ('85', '44', null, '053', 'Udang Goreng Mentega', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Udang Goreng Mentega</p>\r\n', '38', '2018-07-11 11:45:01', '1', null);
INSERT INTO `ref_product_items` VALUES ('86', '44', null, '054', 'Udang Goreng Tepung', '0', '1', '40', '1', '30000.00', '<p>ini&nbsp;Udang Goreng Tepung</p>\r\n', '38', '2018-07-11 11:45:57', '1', null);
INSERT INTO `ref_product_items` VALUES ('87', '45', null, '055', 'Bandrek / Bajigur', '0', '1', '40', '1', '5000.00', '<p>ini&nbsp;Bandrek / Bajigur</p>\r\n', '38', '2018-07-11 11:49:03', '1', null);
INSERT INTO `ref_product_items` VALUES ('88', '45', null, '056', 'Capuccino', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Capuccino</p>\r\n', '38', '2018-07-11 11:50:19', '1', null);
INSERT INTO `ref_product_items` VALUES ('89', '45', '0', '057', 'Coca-cola/Sprite/fanta', '40', '1', '40', '1', '10000.00', '<p>ini&nbsp;Coca-cola/Sprite/fanta</p>\r\n', '38', '2018-07-11 11:54:08', '1', null);
INSERT INTO `ref_product_items` VALUES ('90', '45', null, '058', 'Es Jeruk  / Panas ', '0', '1', '40', '1', '8000.00', '<p>ini&nbsp;Es Jeruk&nbsp; / Panas&nbsp;</p>\r\n', '38', '2018-07-11 11:52:56', '1', null);
INSERT INTO `ref_product_items` VALUES ('91', '44', null, '057', 'Coca-cola/Sprite/fanta', '0', '1', '40', '1', '10000.00', null, '38', '2018-07-11 11:51:56', '1', '1');
INSERT INTO `ref_product_items` VALUES ('92', '45', null, '060', 'Es Lemon Tea / Panas ', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Es Lemon Tea / Panas&nbsp;</p>\r\n', '38', '2018-07-11 11:58:23', '1', null);
INSERT INTO `ref_product_items` VALUES ('93', '45', null, '061', 'Es Teh Manis / Panas', '0', '1', '40', '1', '5000.00', '<p>ini&nbsp;Es Teh Manis / Panas&nbsp;</p>\r\n', '38', '2018-07-11 11:59:44', '1', null);
INSERT INTO `ref_product_items` VALUES ('94', '45', null, '062', 'Es Teh Tarik / Panas ', '0', '1', '40', '1', '12000.00', '<p>ini&nbsp;Es Teh Tarik / Panas&nbsp;</p>\r\n', '38', '2018-07-11 12:00:48', '1', null);
INSERT INTO `ref_product_items` VALUES ('95', '45', null, '063', 'Es Teh Tawar ', '0', '1', '40', '1', '3000.00', '<p>ini&nbsp;Es Teh Tawar&nbsp;</p>\r\n', '38', '2018-07-11 12:01:26', '1', null);
INSERT INTO `ref_product_items` VALUES ('96', '45', null, '064', 'Jus Alpukat', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Jus Alpukat</p>\r\n', '38', '2018-07-11 12:02:14', '1', null);
INSERT INTO `ref_product_items` VALUES ('97', '45', null, '065', 'Jus Buah Naga', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Jus Buah Naga</p>\r\n', '38', '2018-07-11 12:02:57', '1', null);
INSERT INTO `ref_product_items` VALUES ('98', '45', null, '066', 'Jus Jambu', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Jus Jambu</p>\r\n', '38', '2018-07-11 12:03:43', '1', null);
INSERT INTO `ref_product_items` VALUES ('99', '45', null, '067', 'Jus Mangga', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Jus Mangga</p>\r\n', '38', '2018-07-11 12:04:34', '1', null);
INSERT INTO `ref_product_items` VALUES ('100', '45', null, '068', 'Jus Melon', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Jus Melon</p>\r\n', '38', '2018-07-11 12:05:09', '1', null);
INSERT INTO `ref_product_items` VALUES ('101', '45', null, '069', 'Jus Mix', '0', '1', '40', '1', '15000.00', '<p>ini&nbsp;Jus Mix</p>\r\n', '38', '2018-07-11 12:05:58', '1', null);
INSERT INTO `ref_product_items` VALUES ('102', '45', null, '070', 'Jus Sirsak', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Jus Sirsak</p>\r\n', '38', '2018-07-11 12:11:16', '1', null);
INSERT INTO `ref_product_items` VALUES ('103', '45', null, '071', 'Jus Strawberry', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Jus Strawberry</p>\r\n', '38', '2018-07-11 13:02:49', '1', null);
INSERT INTO `ref_product_items` VALUES ('104', '45', null, '072', 'Jus Tomat', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Jus Tomat</p>\r\n', '38', '2018-07-11 13:03:36', '1', null);
INSERT INTO `ref_product_items` VALUES ('105', '45', null, '073', 'Jus Wortel', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Jus Wortel</p>\r\n', '38', '2018-07-11 13:04:30', '1', null);
INSERT INTO `ref_product_items` VALUES ('106', '45', null, '074', 'Kopi Hitam', '0', '1', '40', '1', '6000.00', '<p>ini&nbsp;Kopi Hitam</p>\r\n', '38', '2018-07-11 13:05:21', '1', null);
INSERT INTO `ref_product_items` VALUES ('107', '45', null, '075', 'Kopi Susu', '0', '1', '40', '1', '8000.00', '<p>ini&nbsp;Kopi Susu</p>\r\n', '38', '2018-07-11 13:06:00', '1', null);
INSERT INTO `ref_product_items` VALUES ('108', '45', null, '076', 'Mineral Water', '0', '1', '40', '1', '5000.00', '<p>ini&nbsp;Mineral Water</p>\r\n', '38', '2018-07-11 13:07:11', '1', null);
INSERT INTO `ref_product_items` VALUES ('109', '45', null, '077', 'Soup Buah/Buah Potong', '0', '1', '40', '1', '15000.00', '<p>ini&nbsp;Soup Buah/Buah Potong</p>\r\n', '38', '2018-07-11 13:07:59', '1', null);
INSERT INTO `ref_product_items` VALUES ('110', '45', null, '078', 'Susu Soda', '0', '1', '40', '1', '12000.00', '<p>ini&nbsp;Susu Soda</p>\r\n', '38', '2018-07-11 13:11:39', '1', null);
INSERT INTO `ref_product_items` VALUES ('111', '45', null, '079', 'Teh Botol', '0', '1', '40', '1', '5000.00', '<p>ini&nbsp;Teh Botol</p>\r\n', '38', '2018-07-11 13:13:17', '1', null);
INSERT INTO `ref_product_items` VALUES ('112', '45', null, '080', 'Teh Kayu Manis + Madu', '0', '1', '40', '1', '10000.00', '<p>ini&nbsp;Teh Kayu Manis + Madu</p>\r\n', '38', '2018-07-11 13:14:01', '1', null);
INSERT INTO `ref_product_items` VALUES ('113', '45', null, '081', 'Teh Kayu Manis Panas', '0', '1', '40', '1', '7000.00', '<p>ini&nbsp;Teh Kayu Manis Panas</p>\r\n', '38', '2018-07-11 13:14:42', '1', null);

-- ----------------------------
-- Table structure for ref_product_materials
-- ----------------------------
DROP TABLE IF EXISTS `ref_product_materials`;
CREATE TABLE `ref_product_materials` (
  `productmaterial_id` int(11) NOT NULL AUTO_INCREMENT,
  `productmaterial_productitem_id` int(11) DEFAULT NULL,
  `productmaterial_quantity` int(11) DEFAULT NULL,
  `productmaterial_create_user_id` int(11) DEFAULT NULL,
  `productmaterial_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `productmaterial_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `productmaterial_log_id` int(11) DEFAULT NULL COMMENT 'ID parent',
  PRIMARY KEY (`productmaterial_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_product_materials
-- ----------------------------

-- ----------------------------
-- Table structure for ref_product_prices
-- ----------------------------
DROP TABLE IF EXISTS `ref_product_prices`;
CREATE TABLE `ref_product_prices` (
  `productprice_id` int(11) NOT NULL AUTO_INCREMENT,
  `productprice_productitem_id` int(11) DEFAULT NULL,
  `productprice_date_start` date DEFAULT NULL,
  `productprice_date_end` date DEFAULT NULL,
  `productprice_time_start` time DEFAULT NULL,
  `productprice_time_end` time DEFAULT NULL,
  `productprice_percentage` decimal(18,2) DEFAULT NULL,
  `productprice_price` decimal(18,2) DEFAULT NULL,
  `productprice_for_member` int(11) DEFAULT NULL,
  `productprice_create_user_id` int(11) DEFAULT NULL,
  `productprice_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `productprice_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `productprice_log_id` int(11) DEFAULT NULL COMMENT 'ID parent',
  PRIMARY KEY (`productprice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_product_prices
-- ----------------------------
INSERT INTO `ref_product_prices` VALUES ('1', '1', '2016-12-22', '2016-12-31', null, null, '10.00', '13500.00', null, null, '2016-12-22 16:17:58', '1', null);
INSERT INTO `ref_product_prices` VALUES ('2', '2', '2016-12-22', '2016-12-31', '18:00:00', '20:00:00', '10.00', '22500.00', null, null, '2016-12-22 16:20:08', '1', null);

-- ----------------------------
-- Table structure for ref_product_recomendeds
-- ----------------------------
DROP TABLE IF EXISTS `ref_product_recomendeds`;
CREATE TABLE `ref_product_recomendeds` (
  `recomended_id` int(11) NOT NULL AUTO_INCREMENT,
  `recomended_branch_id` int(11) DEFAULT NULL,
  `recomended_type` varchar(16) DEFAULT NULL,
  `recomended_productitem_id` int(11) DEFAULT NULL,
  `recomended_date_start` date DEFAULT NULL,
  `recomended_date_end` date DEFAULT NULL,
  `recomended_time_start` time DEFAULT NULL,
  `recomended_time_end` time DEFAULT NULL,
  `recomended_create_user_id` int(11) DEFAULT NULL,
  `recomended_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `recomended_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `recomended_log_id` int(11) DEFAULT NULL COMMENT 'ID parent',
  PRIMARY KEY (`recomended_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_product_recomendeds
-- ----------------------------
INSERT INTO `ref_product_recomendeds` VALUES ('1', '1', '1', '1', '2016-12-22', '2017-11-30', null, null, '1', '2017-09-18 16:14:56', '1', null);
INSERT INTO `ref_product_recomendeds` VALUES ('2', '1', '2', '2', '2016-12-22', '2018-01-31', '05:00:00', '20:00:00', '1', '2017-07-20 13:49:02', '1', null);
INSERT INTO `ref_product_recomendeds` VALUES ('3', '1', '2', '4', '2016-12-22', '2018-01-31', null, null, '1', '2017-09-19 10:02:11', '1', null);
INSERT INTO `ref_product_recomendeds` VALUES ('4', '1', '1', '14', '2017-09-19', '2017-09-30', '07:00:00', '20:30:00', '1', '2017-09-19 13:26:03', '1', null);
INSERT INTO `ref_product_recomendeds` VALUES ('25', '1', '1', '1', '2017-09-15', '2017-09-30', '07:00:00', '20:30:00', '1', '2017-09-18 16:46:15', '1', '4');
INSERT INTO `ref_product_recomendeds` VALUES ('26', '1', '2', '1', '2017-09-30', '0000-00-00', '07:00:00', '20:30:00', '1', '2017-09-19 11:37:27', '1', '4');
INSERT INTO `ref_product_recomendeds` VALUES ('27', '1', '2', '2', '2017-09-30', '0000-00-00', '07:00:00', '20:30:00', '1', '2017-09-19 11:37:56', '1', '4');
INSERT INTO `ref_product_recomendeds` VALUES ('28', '1', '2', '2', '2017-09-19', '2017-09-30', '07:00:00', '20:30:00', '1', '2017-09-19 11:44:46', '1', '4');
INSERT INTO `ref_product_recomendeds` VALUES ('29', '1', '2', '2', '2017-09-19', '2017-09-30', '07:00:00', '20:30:00', '1', '2017-09-19 11:44:46', '1', '4');
INSERT INTO `ref_product_recomendeds` VALUES ('30', '1', '1', '12', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '1', '2018-03-20 14:04:53', '1', null);
INSERT INTO `ref_product_recomendeds` VALUES ('31', '1', '1', '15', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '1', '2018-03-20 14:23:53', '0', null);
INSERT INTO `ref_product_recomendeds` VALUES ('32', '1', '1', '1', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '1', '2018-03-20 14:20:46', '1', '31');
INSERT INTO `ref_product_recomendeds` VALUES ('33', '1', '1', '15', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '1', '2018-03-20 14:23:35', '1', '31');
INSERT INTO `ref_product_recomendeds` VALUES ('34', '48', '1', '28', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '32', '2018-04-27 10:36:32', '1', null);
INSERT INTO `ref_product_recomendeds` VALUES ('35', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `ref_product_recomendeds` VALUES ('36', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `ref_product_recomendeds` VALUES ('37', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `ref_product_recomendeds` VALUES ('38', '48', '1', '26', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '32', '2018-04-20 15:29:46', '1', '34');
INSERT INTO `ref_product_recomendeds` VALUES ('39', '48', '1', '28', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '32', '2018-04-20 16:06:20', '1', '34');
INSERT INTO `ref_product_recomendeds` VALUES ('40', '48', '1', '28', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '32', '2018-04-27 10:35:39', '0', '34');
INSERT INTO `ref_product_recomendeds` VALUES ('41', '48', '1', '28', '2017-03-20', '2017-03-30', '24:00:00', '24:00:00', '32', '2018-04-27 10:36:32', '1', '34');

-- ----------------------------
-- Table structure for ref_province
-- ----------------------------
DROP TABLE IF EXISTS `ref_province`;
CREATE TABLE `ref_province` (
  `province_id` int(11) NOT NULL AUTO_INCREMENT,
  `province_country_id` int(11) NOT NULL,
  `province_code` varchar(16) DEFAULT NULL,
  `province_name` varchar(64) DEFAULT NULL,
  `province_latitude` varchar(32) DEFAULT NULL,
  `province_langitude` varchar(32) DEFAULT NULL,
  `province_status` int(11) DEFAULT NULL,
  `province_log_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_province
-- ----------------------------
INSERT INTO `ref_province` VALUES ('1', '101', 'AC', 'Aceh', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('2', '101', 'BA', 'Bali', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('3', '101', 'BT', 'Banten', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('4', '101', 'BE', 'Bengkulu', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('5', '101', 'GO', 'Gorontalo', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('6', '101', 'JK', 'Jakarta', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('7', '101', 'JA', 'Jambi', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('8', '101', 'JB', 'Jawa Barat', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('9', '101', 'JT', 'Jawa Tengah', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('10', '101', 'Jl', 'Jawa Timur', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('11', '101', 'KB', 'Kalimantan Barat', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('12', '101', 'KS', 'Kaimantan Selatan', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('13', '101', 'KT', 'Kalimantan Tengah', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('14', '101', 'Kl', 'Kalimantan Timur', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('15', '101', 'KU', 'Kalimantan Utara', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('16', '101', 'BB', 'Kepulauan Bangka Belitung', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('17', '101', 'KR', 'Kepulauan Riau', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('18', '101', 'LA', 'Lampung', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('19', '101', 'MA', 'Maluku', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('20', '101', 'MU', 'Maluku Utara', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('21', '101', 'NB', 'Nusa Tenggara Barat', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('22', '101', 'NT', 'Nusa Tenggara Timur', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('23', '101', 'PA', 'Papua', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('24', '101', 'PB', 'Papua Barat', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('25', '101', 'RI', 'Riau', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('26', '101', 'SR', 'Sulawesi Barat', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('27', '101', 'SN', 'Sulawesi Selatan', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('28', '101', 'ST', 'Sulawesi Tengah', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('29', '101', 'SG', 'Sualawesi Tenggara', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('30', '101', 'SA', 'Sulawesi Utara', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('31', '101', 'SB', 'Sumatra Barat', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('32', '101', 'SS', 'Sumatra Selatan', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('33', '101', 'SU', 'Sumatra Utara', null, null, '1', null);
INSERT INTO `ref_province` VALUES ('34', '101', 'YO', 'Yogyakarta', null, null, '1', null);

-- ----------------------------
-- Table structure for ref_service_charge
-- ----------------------------
DROP TABLE IF EXISTS `ref_service_charge`;
CREATE TABLE `ref_service_charge` (
  `servicecharge_id` int(11) NOT NULL,
  `servicecharge_name` varchar(255) NOT NULL,
  `servicecharge_value` int(11) NOT NULL,
  `servicecharge_percentage` int(11) DEFAULT NULL,
  `servicecharge_create_user_id` varchar(255) DEFAULT NULL,
  `servicecharge_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `servicecharge_log_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`servicecharge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_service_charge
-- ----------------------------

-- ----------------------------
-- Table structure for ref_tax
-- ----------------------------
DROP TABLE IF EXISTS `ref_tax`;
CREATE TABLE `ref_tax` (
  `tax_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_company_id` int(11) DEFAULT NULL,
  `tax_name` varchar(255) DEFAULT NULL,
  `tax_percentage` int(11) NOT NULL,
  `tax_status` int(11) NOT NULL,
  `tax_create_user_id` varchar(255) DEFAULT NULL,
  `tax_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `tax_log_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`tax_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_tax
-- ----------------------------
INSERT INTO `ref_tax` VALUES ('1', null, 'PPN', '10', '1', '1', '2017-09-07 09:56:06', null);
INSERT INTO `ref_tax` VALUES ('15', null, 'pajak baru', '11', '1', '1', '2017-09-07 09:36:11', null);
INSERT INTO `ref_tax` VALUES ('20', null, 'PPN', '12', '1', '1', '2017-09-07 09:55:26', '1');
INSERT INTO `ref_tax` VALUES ('21', null, 'Pajak Pajakan', '50', '0', '1', '2018-03-20 14:42:53', null);
INSERT INTO `ref_tax` VALUES ('22', null, 'Pajak Pajakan', '120', '1', '1', '2018-03-20 14:42:08', '1');
INSERT INTO `ref_tax` VALUES ('23', null, 'Pajak Pajakan', '50', '1', '1', '2018-03-20 14:42:37', '1');
INSERT INTO `ref_tax` VALUES ('24', '10', 'PPN', '10', '1', '1', null, null);
INSERT INTO `ref_tax` VALUES ('25', '13', 'PPN', '10', '1', '1', null, null);
INSERT INTO `ref_tax` VALUES ('26', '14', 'PPN', '10', '1', '1', null, null);
INSERT INTO `ref_tax` VALUES ('27', '15', 'PPN', '10', '1', '1', '2018-04-27 10:50:02', null);
INSERT INTO `ref_tax` VALUES ('28', '15', 'Ekstra Fee', '2', '1', '32', '2018-04-20 07:08:38', null);
INSERT INTO `ref_tax` VALUES ('29', '18', 'PPN', '10', '1', '1', null, null);
INSERT INTO `ref_tax` VALUES ('30', '15', 'PPN', '10', '1', '1', null, '1');
INSERT INTO `ref_tax` VALUES ('31', '15', 'PPN', '10', '0', '1', '2018-04-27 10:49:43', '1');
INSERT INTO `ref_tax` VALUES ('32', '19', 'PPN', '10', '1', '1', null, null);

-- ----------------------------
-- Table structure for ref_units
-- ----------------------------
DROP TABLE IF EXISTS `ref_units`;
CREATE TABLE `ref_units` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_branch_id` int(11) DEFAULT NULL,
  `unit_type` int(11) DEFAULT NULL COMMENT '1.Product Item, 2.Material',
  `unit_code` varchar(16) DEFAULT NULL,
  `unit_name` varchar(64) DEFAULT NULL,
  `unit_create_user_id` int(11) DEFAULT NULL,
  `unit_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `unit_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `unit_log_id` int(11) DEFAULT NULL COMMENT 'ID parent',
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_units
-- ----------------------------
INSERT INTO `ref_units` VALUES ('1', '1', '1', 'PO', 'Porsi', '1', '2017-09-13 11:05:23', '1', null);
INSERT INTO `ref_units` VALUES ('2', '1', '1', 'GLS', 'Gelas', '1', '2017-09-13 11:07:45', '1', null);
INSERT INTO `ref_units` VALUES ('4', '1', '1', 'PO', 'Porsi', '1', '2016-12-22 16:27:01', '1', '1');
INSERT INTO `ref_units` VALUES ('5', '1', '2', 'PO', 'Porsi', '1', '2017-09-13 11:05:08', '1', '1');
INSERT INTO `ref_units` VALUES ('6', '1', '1', 'GLS', 'Gelas', '1', '2017-09-11 17:40:40', '1', '1');
INSERT INTO `ref_units` VALUES ('7', '1', '1', 'GLS', 'Gelas', '1', '2017-09-13 11:07:40', '0', '1');
INSERT INTO `ref_units` VALUES ('8', '1', '2', '007', 'Unit Unitansss', '1', '2018-03-20 14:32:09', '0', null);
INSERT INTO `ref_units` VALUES ('9', '1', '1', '007', 'Unit Unitan', '1', '2018-03-20 14:31:23', '1', '1');
INSERT INTO `ref_units` VALUES ('10', '1', '2', '007', 'Unit Unitansss', '1', '2018-03-20 14:31:53', '1', '1');
INSERT INTO `ref_units` VALUES ('11', '41', '1', '001', 'Porsi', '26', '2018-04-06 10:49:02', '1', null);
INSERT INTO `ref_units` VALUES ('12', '41', '1', '001234', 'Porsi', '26', '2018-04-06 10:47:51', '1', '1');
INSERT INTO `ref_units` VALUES ('13', '44', '2', '001', 'Sendok', '27', '2018-04-12 22:49:01', '1', null);
INSERT INTO `ref_units` VALUES ('14', '43', '2', '002', 'Garpu', '27', '2018-04-12 22:49:43', '1', null);

-- ----------------------------
-- Table structure for ref_voucher
-- ----------------------------
DROP TABLE IF EXISTS `ref_voucher`;
CREATE TABLE `ref_voucher` (
  `id_voucher` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_item_id` int(11) DEFAULT NULL,
  `voucher_name` varchar(255) NOT NULL,
  `voucher_value` int(11) DEFAULT NULL,
  `voucher_create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `voucher_expired_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `voucher_valid_time` time NOT NULL,
  `voucher_valid_until_time` time NOT NULL,
  `voucher_status` int(11) NOT NULL,
  `voucher_log_id` int(11) DEFAULT NULL,
  `voucher_jumlah` int(11) NOT NULL,
  PRIMARY KEY (`id_voucher`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_voucher
-- ----------------------------
INSERT INTO `ref_voucher` VALUES ('16', '2', 'voucher nasi goreng', '100000', '2018-03-20 14:40:19', '2017-09-20 00:00:00', '06:00:00', '20:30:00', '1', null, '1');
INSERT INTO `ref_voucher` VALUES ('17', '2', 'voucher steak sapi', '11000', '2017-09-08 00:00:00', '2017-09-30 00:00:00', '06:30:00', '19:30:00', '1', null, '3');
INSERT INTO `ref_voucher` VALUES ('24', '2', 'voucher nasi goreng', '10000', '2017-09-08 16:46:25', '2017-09-08 16:46:25', '06:00:00', '20:30:00', '1', '1', '0');
INSERT INTO `ref_voucher` VALUES ('25', '2', 'voucher nasi goreng', '10000', '2017-09-20 11:52:11', '2017-09-20 11:52:11', '06:00:00', '20:30:00', '0', '1', '0');
INSERT INTO `ref_voucher` VALUES ('26', '1', 'Voucher Voucheran', '200000', '2018-03-20 14:40:49', '2018-03-20 14:40:49', '08:00:00', '24:00:00', '0', null, '10');
INSERT INTO `ref_voucher` VALUES ('27', '2', 'voucher nasi goreng', '10000', '2017-09-20 11:52:17', '2017-09-20 11:52:17', '06:00:00', '20:30:00', '1', '1', '0');
INSERT INTO `ref_voucher` VALUES ('28', '2', 'voucher nasi goreng', '10000', '2017-09-20 11:52:17', '2017-09-20 11:52:17', '06:00:00', '20:30:00', '1', '1', '0');
INSERT INTO `ref_voucher` VALUES ('29', '1', 'Voucher Voucheran', '200000', '2018-03-20 00:00:00', '2017-03-30 00:00:00', '08:00:00', '24:00:00', '1', '1', '0');
INSERT INTO `ref_voucher` VALUES ('30', '28', 'Voucher Hemat', '20000', '2018-04-27 10:44:38', '2018-04-27 10:44:38', '24:00:00', '24:00:00', '1', null, '5');
INSERT INTO `ref_voucher` VALUES ('31', '28', 'Voucher Hemat', '20000', '2018-04-20 00:00:00', '2017-03-30 00:00:00', '24:00:00', '24:00:00', '1', '1', '0');
INSERT INTO `ref_voucher` VALUES ('32', '28', 'Voucher Hemat', '20000', '2018-04-27 10:44:12', '2018-04-27 10:44:12', '24:00:00', '24:00:00', '0', '1', '0');

-- ----------------------------
-- Table structure for ref_voucher_detail
-- ----------------------------
DROP TABLE IF EXISTS `ref_voucher_detail`;
CREATE TABLE `ref_voucher_detail` (
  `voucherdetail_id` int(11) NOT NULL AUTO_INCREMENT,
  `voucherdetail_code` varchar(255) NOT NULL,
  `voucherdetail_voucher_id` int(11) NOT NULL,
  `voucherdetail_status` int(11) NOT NULL,
  PRIMARY KEY (`voucherdetail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_voucher_detail
-- ----------------------------
INSERT INTO `ref_voucher_detail` VALUES ('1', '80mzut3vy1', '16', '1');
INSERT INTO `ref_voucher_detail` VALUES ('2', 'ti2gd1jfiy', '17', '1');
INSERT INTO `ref_voucher_detail` VALUES ('3', 'zxce8xh6nv', '17', '1');
INSERT INTO `ref_voucher_detail` VALUES ('4', 'zg1sgyr30x', '17', '1');
INSERT INTO `ref_voucher_detail` VALUES ('5', 'hqxxyvnx3p', '26', '1');
INSERT INTO `ref_voucher_detail` VALUES ('6', 'mld2rigf6g', '26', '1');
INSERT INTO `ref_voucher_detail` VALUES ('7', 'cu643xnwlg', '26', '1');
INSERT INTO `ref_voucher_detail` VALUES ('8', 'zmw85o0u1d', '26', '1');
INSERT INTO `ref_voucher_detail` VALUES ('9', 'x0wnwh3442', '26', '1');
INSERT INTO `ref_voucher_detail` VALUES ('10', '3h9iv2igok', '26', '1');
INSERT INTO `ref_voucher_detail` VALUES ('11', 'r8eql1camw', '26', '1');
INSERT INTO `ref_voucher_detail` VALUES ('12', 'i3e6joq4yg', '26', '1');
INSERT INTO `ref_voucher_detail` VALUES ('13', '6kqg0r2dnl', '26', '1');
INSERT INTO `ref_voucher_detail` VALUES ('14', 'q5lkx1xsfm', '26', '1');
INSERT INTO `ref_voucher_detail` VALUES ('15', '0hcsx34frh', '30', '1');
INSERT INTO `ref_voucher_detail` VALUES ('16', 'dupvw1ekor', '30', '1');
INSERT INTO `ref_voucher_detail` VALUES ('17', '0680tsrmgs', '30', '1');
INSERT INTO `ref_voucher_detail` VALUES ('18', '5s4e51v8g2', '30', '1');
INSERT INTO `ref_voucher_detail` VALUES ('19', '6rbnnk3kqa', '30', '1');

-- ----------------------------
-- Table structure for sys_access
-- ----------------------------
DROP TABLE IF EXISTS `sys_access`;
CREATE TABLE `sys_access` (
  `access_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(255) DEFAULT NULL,
  `access_default_menu_id` int(11) DEFAULT NULL,
  `access_create_user_id` int(11) DEFAULT NULL,
  `access_create_date` datetime DEFAULT NULL,
  `access_update_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `access_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `access_log_id` int(11) DEFAULT NULL COMMENT 'ID Parent',
  PRIMARY KEY (`access_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_access
-- ----------------------------
INSERT INTO `sys_access` VALUES ('1', 'Admin', null, '1', null, '2018-04-20 06:51:08', '1', null);
INSERT INTO `sys_access` VALUES ('2', 'Waitress', null, '1', null, '2017-09-20 13:39:08', '1', null);
INSERT INTO `sys_access` VALUES ('3', 'Chef', null, '1', null, '2017-09-20 13:38:06', '1', null);
INSERT INTO `sys_access` VALUES ('4', 'Cashier', null, '1', null, '2017-09-20 13:37:18', '1', null);
INSERT INTO `sys_access` VALUES ('5', 'Super Admin', null, '1', null, '2018-04-02 13:56:42', '1', null);
INSERT INTO `sys_access` VALUES ('7', 'Admin', null, null, null, null, '1', '1');
INSERT INTO `sys_access` VALUES ('8', 'Cashier', null, null, null, null, '1', '4');
INSERT INTO `sys_access` VALUES ('9', 'Chef', null, null, null, null, '1', '3');
INSERT INTO `sys_access` VALUES ('10', 'Waitress', null, null, null, null, '1', '2');
INSERT INTO `sys_access` VALUES ('21', 'Super Admin', null, null, null, null, '1', '5');
INSERT INTO `sys_access` VALUES ('24', 'Admin', null, '1', null, '2017-09-20 13:36:16', '1', '1');
INSERT INTO `sys_access` VALUES ('25', 'Super Admin', null, '1', null, '2018-03-22 10:04:17', '1', '5');
INSERT INTO `sys_access` VALUES ('26', 'Super Admin', null, '1', null, '2018-03-27 11:47:34', '1', '5');
INSERT INTO `sys_access` VALUES ('27', 'Admin', null, '1', null, '2018-03-27 11:39:22', '1', '1');
INSERT INTO `sys_access` VALUES ('28', 'Super Admin', null, '1', null, '2018-04-02 10:07:00', '1', '5');
INSERT INTO `sys_access` VALUES ('29', 'Owner', null, '1', '2018-04-19 13:15:01', null, '1', null);
INSERT INTO `sys_access` VALUES ('30', 'Admin', null, '1', null, '2018-04-02 10:07:37', '1', '1');

-- ----------------------------
-- Table structure for sys_accesses
-- ----------------------------
DROP TABLE IF EXISTS `sys_accesses`;
CREATE TABLE `sys_accesses` (
  `access_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(255) DEFAULT NULL,
  `access_create_user_id` int(11) DEFAULT NULL COMMENT 'user_id (sys_users)',
  `access_create_date` datetime DEFAULT NULL,
  `access_status` int(1) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `access_log_id` int(11) DEFAULT NULL COMMENT 'null=Data parent, not null=Histori untuk ID tercantum',
  `access_update_date` datetime DEFAULT NULL,
  `access_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`access_id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_accesses
-- ----------------------------
INSERT INTO `sys_accesses` VALUES ('1', 'administrator', '1', null, '1', null, '2016-10-24 21:35:30', null);
INSERT INTO `sys_accesses` VALUES ('12', 'Operator Kantor', '1', '2016-06-06 11:32:11', '1', null, '2016-09-08 14:50:35', null);
INSERT INTO `sys_accesses` VALUES ('32', 'Hanya Home', '1', '2016-11-03 16:37:41', '1', null, null, null);
INSERT INTO `sys_accesses` VALUES ('2', 'Pemilik', '1', null, '1', null, null, null);

-- ----------------------------
-- Table structure for sys_access_details
-- ----------------------------
DROP TABLE IF EXISTS `sys_access_details`;
CREATE TABLE `sys_access_details` (
  `accessdetail_id` int(11) NOT NULL AUTO_INCREMENT,
  `accessdetail_access_id` int(11) DEFAULT NULL COMMENT 'access_id (sys_accesses)',
  `accessdetail_menu_id` int(11) DEFAULT NULL COMMENT 'menu_id (sys_menus)',
  `accessdetail_can_create` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `accessdetail_can_read` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `accessdetail_can_update` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `accessdetail_can_delete` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `accessdetail_can_approve` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `accessdetail_can_print` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `accessdetail_create_user_id` int(11) DEFAULT NULL COMMENT 'user_id (sys_users)',
  `accessdetail_create_date` datetime DEFAULT NULL,
  `accessdetail_status` int(1) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `accessdetail_log_id` int(11) DEFAULT NULL COMMENT 'null=Data parent, not null=Histori untuk ID tercantum',
  PRIMARY KEY (`accessdetail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=340 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_access_details
-- ----------------------------
INSERT INTO `sys_access_details` VALUES ('140', '12', '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('141', '12', '2', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('142', '12', '3', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('143', '12', '4', '1', '1', '1', '1', '1', '0', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('144', '12', '5', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('145', '12', '6', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('146', '12', '7', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('147', '12', '8', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('339', '1', '15', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('326', '29', '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('325', '5', '16', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('324', '5', '14', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('323', '5', '22', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('322', '5', '2', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('196', '32', '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('321', '5', '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('338', '1', '14', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('337', '1', '17', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('336', '1', '19', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('335', '1', '18', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('334', '1', '8', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('333', '1', '3', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('332', '1', '10', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('331', '1', '9', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('330', '1', '6', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('329', '1', '13', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('328', '1', '2', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('327', '1', '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('211', '4', '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('212', '4', '2', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('213', '4', '6', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('214', '4', '11', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('215', '4', '13', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('216', '3', '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('217', '3', '2', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('218', '3', '8', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('219', '3', '18', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('220', '3', '19', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('221', '3', '17', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('222', '2', '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('223', '2', '2', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('224', '2', '10', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('225', '2', '3', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('226', '2', '19', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('255', '11', '16', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('254', '11', '15', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('253', '11', '14', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('252', '11', '13', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('251', '11', '11', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('250', '11', '17', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('249', '11', '19', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('248', '11', '18', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('247', '11', '8', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('246', '11', '3', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('245', '11', '10', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('244', '11', '6', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('243', '11', '9', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_access_details` VALUES ('242', '11', '2', '1', '1', '1', '1', '1', '1', null, null, '1', null);

-- ----------------------------
-- Table structure for sys_menus
-- ----------------------------
DROP TABLE IF EXISTS `sys_menus`;
CREATE TABLE `sys_menus` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_parent_id` int(11) DEFAULT '0' COMMENT 'menu_id (sys_menus)',
  `menu_url` varchar(255) DEFAULT '',
  `menu_icon` varchar(32) DEFAULT NULL,
  `menu_name` varchar(64) DEFAULT NULL,
  `menu_description` varchar(255) DEFAULT NULL,
  `menu_sort` int(11) DEFAULT NULL COMMENT 'Urutan',
  `menu_have_create` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `menu_have_read` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `menu_have_update` int(1) NOT NULL DEFAULT '0' COMMENT '0.False, 1.True',
  `menu_have_delete` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `menu_have_approve` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `menu_have_print` int(1) DEFAULT '0' COMMENT '0.False, 1.True',
  `menu_create_user_id` int(11) DEFAULT NULL COMMENT 'user_id (sys_users)',
  `menu_create_date` datetime DEFAULT NULL,
  `menu_status` int(1) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `menu_log_id` int(11) DEFAULT NULL COMMENT 'null=Data parent, not null=Histori untuk ID tercantum',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menus
-- ----------------------------
INSERT INTO `sys_menus` VALUES ('1', '0', 'home/dashboard', 'zmdi zmdi-home', 'Home', 'Halaman Utama Backend', '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('2', '0', 'javascript:void()', 'zmdi zmdi-folder-outline zmdi-hc', 'Master Data', null, '2', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('3', '2', 'master/meja', null, 'Meja', null, '4', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('6', '2', 'master/pajak', null, 'Pajak', null, '11', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('8', '2', 'master/kategori', null, 'Kategori Makanan', null, '5', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('9', '2', 'master/branches', null, 'Branches', null, '2', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('10', '2', 'master/room', null, 'Branches Room', null, '3', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('11', '2', 'master/unit', null, 'Unit', null, '9', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('19', '2', 'master/recommend', '', 'Rekomendasi', null, '7', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('13', '2', 'master/voucher', null, 'Voucher', null, '10', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('14', '0', 'javascript:void()', 'zmdi zmdi-settings', 'Setting', null, '5', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('15', '14', 'setting/users', '', 'User', null, '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('16', '14', 'setting/roles', null, 'Akses Role', null, '2', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('17', '2', 'master/menu', '', 'Menu', '', '8', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('18', '2', 'master/paket', null, 'Paket', null, '6', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('21', '0', 'home/viewprofile', 'zmdi zmdi-account', 'View Profile', null, '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);
INSERT INTO `sys_menus` VALUES ('22', '2', 'master/companies', null, 'Companies', null, '1', '1', '1', '1', '1', '1', '1', null, null, '1', null);

-- ----------------------------
-- Table structure for sys_settings
-- ----------------------------
DROP TABLE IF EXISTS `sys_settings`;
CREATE TABLE `sys_settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_initial` varchar(16) DEFAULT NULL,
  `setting_name` varchar(64) DEFAULT NULL,
  `setting_value` varchar(255) DEFAULT NULL,
  `setting_description` varchar(255) DEFAULT NULL,
  `setting_create_user_id` int(11) DEFAULT NULL COMMENT 'user_id (sys_users)',
  `setting_create_date` datetime DEFAULT NULL,
  `setting_status` int(1) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `setting_log_id` int(11) DEFAULT NULL COMMENT 'null=Data parent, not null=Histori untuk ID tercantum',
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_settings
-- ----------------------------
INSERT INTO `sys_settings` VALUES ('1', 'DEFAULT', 'Nama Agen', 'Nama Agen', 'Booking, reviews and advices on hotels, resorts, and flights!', '1', null, '1', null);
INSERT INTO `sys_settings` VALUES ('3', 'CONTACT_PHONE', 'No. Telepon/Fax', '+62 22-8200 2068', null, null, null, '1', null);
INSERT INTO `sys_settings` VALUES ('4', 'CONTACT_EMAIL', 'Alamat Email', ' support@travelagent.co.id', null, null, null, '1', null);
INSERT INTO `sys_settings` VALUES ('5', 'SOCIAL_FB', 'Facebook', '#', '', '1', null, '1', null);
INSERT INTO `sys_settings` VALUES ('6', 'SOCIAL_TWITTER', 'Twitter', '#', null, null, null, '1', null);
INSERT INTO `sys_settings` VALUES ('7', 'SOCIAL_GP', 'Google Plus', '#', null, null, null, '1', null);
INSERT INTO `sys_settings` VALUES ('8', 'SOCIAL_LINKID', 'LinkedIn', '#', null, null, null, '1', null);
INSERT INTO `sys_settings` VALUES ('9', 'SOCIAL_INSTAGRAM', 'Instagram', '#', '', '1', null, '1', null);

-- ----------------------------
-- Table structure for sys_users
-- ----------------------------
DROP TABLE IF EXISTS `sys_users`;
CREATE TABLE `sys_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_data_type` int(255) DEFAULT NULL COMMENT '0. System, 1.Admin Web',
  `user_data_id` int(11) DEFAULT NULL COMMENT '1. peopleapp_id (trx_people_apps), \r\n2. peoplemobile_id (trx_people_mobiles)',
  `user_name` varchar(32) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_access_id` int(11) DEFAULT NULL COMMENT 'access_id (sys_accesses)',
  `user_last_login` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_online` int(1) DEFAULT '0' COMMENT '0.Offline, 1.Online',
  `user_last_online` datetime DEFAULT NULL,
  `user_last_login_ip` varchar(11) DEFAULT NULL,
  `user_last_login_date` datetime DEFAULT NULL,
  `user_create_user_id` int(11) DEFAULT NULL COMMENT 'user_id (sys_users)',
  `user_create_date` datetime DEFAULT NULL,
  `user_status` int(1) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `user_log_id` int(11) DEFAULT NULL COMMENT 'null=Data parent, not null=Histori untuk ID tercantum',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_users
-- ----------------------------
INSERT INTO `sys_users` VALUES ('1', '1', '1', '000admincms', '5f4dcc3b5aa765d61d8327deb882cf99', '5', '2018-07-10 23:36:34', '1', null, '::1', '2018-07-10 23:36:34', '1', null, '1', null);
INSERT INTO `sys_users` VALUES ('2', '1', '6', 'admin123', 'dbe58bc635038849fe55e3b7b8b0481e', '5', '2017-09-15 11:07:08', null, null, null, null, '1', null, '1', null);
INSERT INTO `sys_users` VALUES ('4', '1', '14', 'username', '5f4dcc3b5aa765d61d8327deb882cf99', '1', null, '0', null, null, null, '1', '2017-09-19 16:05:17', '1', null);
INSERT INTO `sys_users` VALUES ('5', '1', '15', 'username2', '5f4dcc3b5aa765d61d8327deb882cf99', '1', '2018-03-21 10:01:04', '0', null, null, null, '1', '2017-09-19 17:39:48', '1', null);
INSERT INTO `sys_users` VALUES ('13', '1', '15', 'username2', '5f4dcc3b5aa765d61d8327deb882cf99', '1', null, '0', null, null, null, '1', '2017-09-19 17:39:48', '0', '5');
INSERT INTO `sys_users` VALUES ('12', '1', '15', 'username2', '5f4dcc3b5aa765d61d8327deb882cf99', '1', null, '0', null, null, null, '1', '2017-09-19 17:39:48', '1', '5');
INSERT INTO `sys_users` VALUES ('11', '1', '15', 'username2', '5f4dcc3b5aa765d61d8327deb882cf99', '1', null, '0', null, null, null, '1', '2017-09-19 17:39:48', '0', '5');
INSERT INTO `sys_users` VALUES ('10', '1', '15', 'username2', '5f4dcc3b5aa765d61d8327deb882cf99', '1', null, '0', null, null, null, '1', '2017-09-19 17:39:48', '1', '5');
INSERT INTO `sys_users` VALUES ('14', '1', '34', 'rexaakbar1', 'f54dbca7c35255d2296c40d6766e63ad', '1', '2018-03-20 16:26:01', '1', null, '::1', '2018-03-20 16:26:01', '1', '2018-03-20 14:43:59', '1', null);
INSERT INTO `sys_users` VALUES ('15', '1', '1', 'admincms', '5f4dcc3b5aa765d61d8327deb882cf99', '5', null, '1', null, '::1', '2018-03-20 10:46:42', '1', null, '1', '1');
INSERT INTO `sys_users` VALUES ('16', '1', '15', 'username2', '5f4dcc3b5aa765d61d8327deb882cf99', '1', null, '0', null, null, null, '1', '2017-09-19 17:39:48', '1', '5');
INSERT INTO `sys_users` VALUES ('17', '1', '37', 'rexaakbar2', 'f54dbca7c35255d2296c40d6766e63ad', '3', '2018-03-20 16:31:26', '1', null, '::1', '2018-03-20 16:31:26', '1', '2018-03-20 16:31:01', '1', null);
INSERT INTO `sys_users` VALUES ('18', '1', '15', 'username2', '5f4dcc3b5aa765d61d8327deb882cf99', '1', null, '0', null, null, null, '1', '2017-09-19 17:39:48', '0', '5');
INSERT INTO `sys_users` VALUES ('21', '1', '41', 'rexaakbar', 'f54dbca7c35255d2296c40d6766e63ad', '1', '2018-04-05 13:25:08', '1', null, '::1', '2018-04-05 13:25:08', '1', '2018-03-29 14:47:25', '1', null);
INSERT INTO `sys_users` VALUES ('22', '1', '41', 'rexaakbar', 'f54dbca7c35255d2296c40d6766e63ad', '1', null, '0', null, null, null, '1', '2018-03-29 14:47:25', '1', '21');
INSERT INTO `sys_users` VALUES ('23', '1', '41', 'rexaakbar', 'f54dbca7c35255d2296c40d6766e63ad', '1', null, '0', null, null, null, '1', '2018-03-29 14:47:25', '1', '21');
INSERT INTO `sys_users` VALUES ('24', '1', '41', 'rexaakbar', 'f54dbca7c35255d2296c40d6766e63ad', '1', null, '0', null, null, null, '1', '2018-03-29 14:47:25', '1', '21');
INSERT INTO `sys_users` VALUES ('25', '1', '41', 'rexaakbar', 'f54dbca7c35255d2296c40d6766e63ad', '1', null, '0', null, null, null, '1', '2018-03-29 14:47:25', '1', '21');
INSERT INTO `sys_users` VALUES ('26', '1', '46', 'sample123', 'f54dbca7c35255d2296c40d6766e63ad', '1', '2018-04-06 09:19:40', '1', null, '::1', '2018-04-06 09:19:40', '1', '2018-04-03 11:18:55', '1', null);
INSERT INTO `sys_users` VALUES ('27', '1', '47', '003rexaakbar123', 'f54dbca7c35255d2296c40d6766e63ad', '1', '2018-04-18 15:43:51', '1', null, '::1', '2018-04-18 15:43:51', '1', '2018-04-06 13:49:30', '1', null);
INSERT INTO `sys_users` VALUES ('28', '1', '48', '004test123456', 'f54dbca7c35255d2296c40d6766e63ad', '1', '2018-04-17 21:05:11', '1', null, '::1', '2018-04-17 21:05:11', '1', '2018-04-17 09:48:47', '1', null);
INSERT INTO `sys_users` VALUES ('29', '1', '52', '003chef1234', 'f54dbca7c35255d2296c40d6766e63ad', '3', null, '0', null, null, null, '27', '2018-04-18 07:03:32', '1', null);
INSERT INTO `sys_users` VALUES ('30', '1', '53', '006testuser123', '5f4dcc3b5aa765d61d8327deb882cf99', '1', '2018-04-19 16:02:44', '0', null, null, null, '1', '2018-04-18 15:42:35', '1', null);
INSERT INTO `sys_users` VALUES ('31', '1', '54', '003waitress123', '5f4dcc3b5aa765d61d8327deb882cf99', '2', null, '0', null, null, null, '27', '2018-04-18 16:03:37', '1', null);
INSERT INTO `sys_users` VALUES ('32', '1', '55', '005admin123', 'f54dbca7c35255d2296c40d6766e63ad', '1', '2018-06-05 09:49:52', '1', null, '::1', '2018-06-05 09:49:52', '1', '2018-04-19 16:26:09', '1', null);
INSERT INTO `sys_users` VALUES ('33', '1', '56', '007owner123', 'f54dbca7c35255d2296c40d6766e63ad', '29', '2018-04-27 11:00:46', '1', null, '::1', '2018-04-27 11:00:46', '1', '2018-04-20 17:07:51', '1', null);
INSERT INTO `sys_users` VALUES ('34', '1', '57', '007owner123', 'f54dbca7c35255d2296c40d6766e63ad', '29', null, '0', null, null, null, '1', '2018-04-20 17:57:33', '1', null);
INSERT INTO `sys_users` VALUES ('35', '1', '56', '007admin123', 'f54dbca7c35255d2296c40d6766e63ad', '1', null, '0', null, null, null, '1', '2018-04-20 17:07:51', '1', '33');
INSERT INTO `sys_users` VALUES ('36', '1', '56', '007owner123', 'f54dbca7c35255d2296c40d6766e63ad', '29', null, '0', null, null, null, '1', '2018-04-20 17:07:51', '1', '33');
INSERT INTO `sys_users` VALUES ('37', '1', '55', '005admin123', 'f54dbca7c35255d2296c40d6766e63ad', '1', '2018-04-27 11:05:20', '1', null, '::1', '2018-04-27 09:45:46', '1', '2018-04-19 16:26:09', '1', '32');
INSERT INTO `sys_users` VALUES ('38', '1', '61', '008kedaiyangtie', '5f4dcc3b5aa765d61d8327deb882cf99', '1', '2018-07-11 08:33:59', '1', null, '::1', '2018-07-11 08:33:59', '1', '2018-07-10 23:42:13', '1', null);

-- ----------------------------
-- Table structure for ta_master_countries
-- ----------------------------
DROP TABLE IF EXISTS `ta_master_countries`;
CREATE TABLE `ta_master_countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(16) DEFAULT NULL,
  `country_name` varchar(64) DEFAULT NULL,
  `country_flag` varchar(32) DEFAULT NULL,
  `country_latitude` varchar(32) DEFAULT NULL,
  `country_longitude` varchar(32) DEFAULT NULL,
  `country_polyline` text,
  `country_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `country_log_id` int(11) DEFAULT NULL COMMENT 'null=Data parent, not null=Histori untuk ID tercantum',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ta_master_countries
-- ----------------------------

-- ----------------------------
-- Table structure for trx_branch_comments
-- ----------------------------
DROP TABLE IF EXISTS `trx_branch_comments`;
CREATE TABLE `trx_branch_comments` (
  `branchcomment_id` int(11) NOT NULL AUTO_INCREMENT,
  `branchcomment_branch_id` int(11) DEFAULT NULL,
  `branchcomment_rating` int(11) DEFAULT NULL,
  `branchcomment_text` varchar(960) DEFAULT NULL,
  `branchcomment_create_user_id` int(11) DEFAULT NULL,
  `branchcomment_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `branchcomment_status` int(11) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  PRIMARY KEY (`branchcomment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_branch_comments
-- ----------------------------

-- ----------------------------
-- Table structure for trx_clients
-- ----------------------------
DROP TABLE IF EXISTS `trx_clients`;
CREATE TABLE `trx_clients` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id_type` int(1) DEFAULT NULL COMMENT '1. KTP, 2.SIM, 3.Paspor',
  `client_id_number` varchar(32) DEFAULT NULL,
  `client_referral_id` int(11) DEFAULT NULL,
  `client_fullname` varchar(96) DEFAULT NULL,
  `client_phone` varchar(32) DEFAULT NULL,
  `client_email` varchar(64) DEFAULT NULL,
  `client_address` varchar(960) DEFAULT NULL,
  `client_city` varchar(64) DEFAULT NULL,
  `client_nationaly_country_id` int(11) DEFAULT NULL,
  `client_passport_number` varchar(32) DEFAULT NULL,
  `client_passport_expire` datetime DEFAULT NULL,
  `client_create_user_id` int(11) DEFAULT NULL,
  `client_create_date` datetime DEFAULT NULL,
  `client_status` int(255) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `client_log_id` int(11) DEFAULT NULL COMMENT 'null=Data parent, not null=Histori untuk ID tercantum',
  PRIMARY KEY (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_clients
-- ----------------------------

-- ----------------------------
-- Table structure for trx_contacts
-- ----------------------------
DROP TABLE IF EXISTS `trx_contacts`;
CREATE TABLE `trx_contacts` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_content` longtext,
  `contact_create_user_id` int(11) DEFAULT NULL,
  `contact_create_date` datetime DEFAULT NULL,
  `contact_status` int(11) DEFAULT NULL,
  `contact_log_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_contacts
-- ----------------------------
INSERT INTO `trx_contacts` VALUES ('1', '<p>Jl.Kalijati Indah (alamat lengkap langsung hubungi 081220055562) Antapani Kulon. Bandung, Jawa Barat, Indonesia - 40123</p>\n\n<p><img alt=\"\" border=\"0\" hspace=\"0\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAlaklEQVR42u2dB5wURfbHf909M7vLAhJWQBFPxXAGRPHuzJjF41CRKCoqoKCioCeIhL8iQUBAgmICVDgJnopgIKlgQEQ8z3icoCIqghkRF3Zmurv+9ap6hoXTXXUZuqf7ffksMzub3nR3/fpV1QtGzc5HCTAME0kMFgCGiS4sAAwTYVgAGCbCsAAwTIRhAWCYCMMCwDARhgWAYSIMCwDDRBgWAIaJMCwADBNhWAAYJsKwADBMhGEBYJgIwwLAMBGGBYBhIgwLAMNEGBYAhokwLAAME2FYABgmwrAAMEyEYQFgmAjDAsAwEYYFgGEiDAsAw0QYFgCGiTAsAAwTYVgAGCbCsAAwTIRhAWCYCMMCwDARhgWAYSIMCwDDRBgWAIaJMCwADBNhWAAYJsKwADBMhGEBYJgIwwLAMBGGBYBhIgwLAMNEGBYAhokwLAAME2FYABgmwrAAMEyEYQFgmAjDAsAwEYYFgGEiDAsAw0QYFgCGiTAsAAwTYVgAGCbCsAAwTIRhAWCYCMMCwDARhgWAYSIMCwDDRBgWAIaJMCwADBNhWAAYJsKwADBMhGEBYJgIwwLAMBGGBYBhIgwLAMNEGBYAhokwLABRR579uBVDyR61sfee9bFfw32xT8295BeM7Le4QiDpJLH2q8/w6Qb58c0GpFKp8t/C5CksABHDkGe7sKAIf25yDE446Bj86cAmOP6gI5CQX4vD9L5LP9L4psFvGPqRngjXhTAFvi39ES+99Tre3vghFr75ItZt/FxqCV9K+QYLQEQoihfgbyeehRZHnYx2R50G07AghCEFQehHiwa5HOBGZvjrgU+4Qn2qFUF+GG65Xyw/ScPBFz9+izmvLMDcFYvx7mdr/H67zK+EBSDMyDO7b929cW2rLriweStUjyVgysFrxMyq/+4K+HD9R5j4/GzMevkppO2030eBqQAWgDAiz+g+JQ3Qq3U3XNL8XBTRbVtYsOlB3t8LjNxO3ktFSk4nDGz6cRPGPP0gHl4yBykWgkDCAhAyiuKFGNjuanQ+pwOqy0Fo2jR1j6uvCcsADcNcC4ArXG+aIP+TLsenP32HITMmYs7yRbxOEDBYAEICrdGdcfRJmHblENQoLJZzeW+QmwbJAFw5kTfpNflhmLkVAOEI2HKWkRQOiukFW6i1gmWfvo/rpwzHRxs+8ftwMR4sACEgZloY3u0mdDupNWJC3u1pUY+m+eTyq9V7ZD5Rjzl2ANR4Nw1Bf0mtIBoO1M4BLIFtro1bZo3DlEWPsi8QAFgA8px96jbArOvHosnejWl1D6WWiYShV/dj9A3C8Ma/gCNflzdn+fXc2qR0Rk0BhNpSsGn7UP6zoDRAfj2FZ1etRPeJ/VFaVur3IYw0LAB5Cg2ypgcehjl9JqLErA1Dzu+TlgvLlO6+oQeaJYe+8gAyW3g0GOWTeK5ts+lvCbWlKNSfpT+uPZGUfBZPSjFKuFj17XpcNbEfVq1bw0FFPsECkKecesSxmHXjWBTKf44c9BTgY2U37KHm+jQIbfXUkk+F+h54e/65RMjbv+2ZEdPG6GACaZMrRSglbUikHGmTi61WGS4a3wcvvruSRcAHWADyDoFWh56K+/uNQVyOZBpgmV19I9eT+131DlR04XZbk3YSvR+4DbNeW8AisJthAcgzTj38T3jkpnsQk3PsuGkp9zozZvJHAFxkViINNUNxsEWk0Ov+2/Dk8sUsArsRFoA84tB9D8Iz/SZhj+p14ToOLMMkLx+GnAKoEN08EgDX0AsEplowFBCWg1LDRrcxfbHwnWV5817yHRaAPKFmQXW8OHwm9itpqAYHJeWYUgBoMKnHPBIA2iKgTULaKDCzOwaAYzhIiTTOG9kdb6x+328jIwELQB5AA/uxPhNw5iEnIhk3UZBZ6PO2+MsP/syCf6DxMgt3fE2/D2LT1i9xYv/LsGHT135bGnpYAAIOnZzr/tYZw9peB9rYK5PefuFOgyd/7vzee3JFVqWUN0MZieW2CpG2sfyLVTh/8BWwXdtvc0MNC0DAOXCv/fHqiFlI2HE4MSFdZgOWldtsvlyjLrhy8QnecgCy0YqOQMpy8eCLT+DmB0fmgUuTv7AABBhTmHhxyHQ02fdQlFlATFAQD22h7SgAeeH2l8OhdQvavVAaQGHL5QoMuAa2kZeTFvJpGhdP6IMF7yzz2+TQwgIQYLqe0gZ3XtofImYhKUd4obdibuS5B1AZaSUQUgClCHxd9gOO6XMBtpRt8dusUMICEFCKC6th2ZjHsX9xPTkSTKSh7/5READa4aD3TI+uIacCLz2JvlNvzy83J09gAQgi8oz0bXcl+p53BWKgYB/TWzUnh1lH14cZ4YU0UwETinQUTgrNb7sc73262m/TQgcLQACpUVANq+9dgGpmkXT9TRQIldYDR9Xr+981gNDh0PxfC0DckZ+7NpauewsXDO2BnOcyRwwWgADSp213DDqvq7z1xZCSrnCC1sgEJdKIaAgAXFWglNB+jys1IYWukwbhqTeW+G1cqGABCBhxM4Z37noWexfXgbrfm4aXxYcd9s7DDA14XbEYqrJRmeGiKO3iv199jOP6X8hOwC6EBSBgnHfCWZjWfbh09001+zdUmJ/wcvx1YY3QC4Agv1+XNKIahpTKbEkvKBVz0GZUTyz7z0q/TQwNLAABgu54U/qORttDT8dPMR3xF0sbNBOAfJBTAZFXST+/GyE9AIfetxaARIqShYBtpoPlby1Hu/G9/bYwNLAABIg6xbWw9p7F2Sq+zM/TtNdfse6HL/02IxSwAASIbn+9BGM79oJhsgBUxMin7sOIx+/324xQwAIQFORZmHbjOJzb5CRYVsxvawLN2k2foVnv1txjYBfAAhAQErEE1k5diuqiUBf4YH4RIZI46ZbL8D4HBlUZFoCA0Oygpnhu0GRY1FQr5Gt8VUXYDsYumoahj97ltyl5DwtAQOjX9mr0O7craPMv15178h3KEVi18SOccHNHzg+oIiwAAeGRXqPRstmpqm136Lf5qoirkqJsHHDNmdi09Ue/zclrWAACANXFW3P/YtQsrI0EewCVkhQCCSkAl00dhHnLFvttTl7DAhAAaiaq45MHlqisP0MVyOBFwIpQJcTkx6T50zHonxP8NievYQEIAIc2PBCvDp+luviqUtkhz/evKrT9R5XDlvx3BdqOvMZvc/IaFoAAcN7x5+ChHrfJ4R9TufBWLMe9u/IcXUQU2OSUonGX5hAGX8K/FxaAAHBd6ysw9IIe8sI21RZg+NN9qwaJJC0Epg0Xh15zFr4v/cFvk/IWFoAAcH2bq3BLq64wYjFV8svkNYAK0aXCqLGIg7MGX463PvmP3yblLSwAAWBgpxvQt8VFEKalMgJ5G7BiSABseYws4aDb5Fsw59UFfpuUt7AABIBBJADnXCzvat4iIAtAhdAUIOWlRw+eew/Gz5vqt0l5CwtAACAB6NPiElX9hua2MV4DqBAlAPIQFdjAqGen4vYnJvltUt7CAhAAlABID0CVABO8BlAZqrWoKpPm4O5FMzFo1ji/TcpbWAACwMALr1dTACr5SfN/XgOoGBaAXQcLQADo37EX+rW8FOwB/DqE11fQMFxMWDATt8y+02+T8hYWgADQq/WVGHJBDxiwIlH0s6rsKAAzpACwB/B7YQEIAJ3ObIN7L+mvG2VSMJDFAlARSgBcKZUkAItmYPCs8X6blLewAASAvxxyNBYOeEDf1VwTZoynABWhBEDh4o5nHsbtj93tt0l5CwtAAGhYuwHeG/8MXEfAMkxOBqqEjABQ/4ChTz6AO+dN9tukvIUFIAAUGAlsePhVUPF7S4S/+29VySQDUdREzweHYsZL8/w2KW9hAQgC8gy8MPwhNGvURF7SJmK8CFghSUFRgC5gp3HcmG74YDXnAvxeWAACwqjOfdDjzAvlGbG4zF0lJOVHwqb2YTb27d0CP27Z7LdJeQsLQEDo1qI9xl50k3zGNQErw6FYCQoHFmWo3/Uk7g9QBVgAAsK+9fbGO6PnwhAxrglYCToOwMWrH72Jvw3r4bc5eQ0LQECgLsAf3P886hfU5jiASqB0YGLMc9MxbAbXBKwKLAABYthl/XDtaR24KGglUDagnAig05SBWMBVgasEC0CAaH7MyXiq1wReA6gEipcwTAeNrj0TW3gBsEqwAASImBXH+vteQlFBkd+mBBryANZt+ARNB7TlHZMqwgIQJOSZGHnFTbj6Tx1RWgyV8lrkGLQxAFUtVAjQp7GQX/Y2JUZTZSRh6BLgJgX9amhyZCYdDHnuQYz9571+m5r3sAAEjGP/2BQL+05BKibkQI/Boivf1HUCMyGwYZ8i6Cg/fadX79sQ0IfB0K8LG3+5uQPWbPzEb1PzHhaAgEG7Aa/c8U8cXn9ftSUozEyZcN0MgwZA2AXgl8ikAa/+dh1O+HtbOCZfulWFBSCAdDztAtzbuY90gQthx8nlN7X7L78Wo7MV8jgBIVx9589MdTKeD41/+dItT9yNiU896LeZoYAFIIDErTjeHT8PexWXwLYoN8BSdz+a/8cdebrCniwkXXyoKY92+kkQ6M7vyH8pqQLUDOQH7gq8S2ABCCg9WnTEkE43IGHE5Q3flJe+dn8pWxAhjxOgNF8tAIae9Tu0EOggKQf/o688g96ThyDk66C7DRaAgFIQS+DVcU/iwBoNQFe7Y6osAekGC70jEGpU2U+9GEgiIB0ARwrAZsPB2X074MOvPvXbwNDAAhBg2h/fEpOvvA2GacE2af5Pg8KR4z/czUNd4e0DGEJvBUoPwI6lMfffL+HKcTepdQBm18ACEGAseaUvGTwNTfc9DCk55hNqMdCh/uHeIlk5MmWyyEHI80VCW5X9NmA6+vO0Rb0A0zj5hnZY8916v80LFSwAAadxg/2wYuSjiLsx2FIEKC4gM8DLi0B+D/kdUem+md0OVf/fxb0vPor+D90RrjcaAFgAAg6dnBtad8Hg86+WZyuGrfKVasZ2Ach8T5jGhbogXR0BSLsfX//0Hf789wuwJVnqt2mhgwUgD6BdgIW3TsVf9jkSybiBAuwYDLTzCcx3MRC2gGvJKYAc/K6cB3SZNBDzXuesv1zAApAn7FuyN5aNmI3qsWrqc+oe5Lo6YCbTSSgs0wFa9FNVfgwbc956Ed3G98v/NxVQWADyiOZHHod5N06Ai7gKjrFo4Ls7thILw3QgLd9bLGXjg81f4PSBF2NrcpvfJoUWFoB8Qp6pK1u0w7CLB+gEQUF9BIzQJQrRImCZkULL27rh7Y+54m8uYQHIQ+6+5BZ0OPt8lRb8c/kw+S4CwnFw8aSb8ey/nvfblNDDApCHUIT86O79cdkJrVUZcYqaU0lCIqXbi0tp0GmD+vtFNrBGhxT7XU9AuGkIg96FCZXhJAw4lou0IZBwHdw4bzweenJmBCIe/YcFIE+hYT6qx0B0OeF8xB0vbJaWAqheXiymvsf1CmZbShx0/EAQ1giSco6foHA+14Ar5zIp6Bp/ZP7MF+bg+ukjfLYwOrAA5DE0kIdffAOuOqO9fB5XyTO0ZYiYka2VT4U0VKsR4RXXoCIbfkcKemm9lNjoUrivY8OSc5kJi2dh8IwJXOd/N8ICkO/Is3fZWW0xqnNfFDoJr3oQsoMsu0DovUZRdZRb4KvJNlTFIzKngFRApDHs8Xsw5tnp/rsnEYMFIATQ+D6pyV/wcO+xqBsv1luEan7t6toBFrZXEyKfYDcnE6lmnuV2KVRhH2mbaVKNAwddJ/bHvDeX8uD3ARaAELFXrXqYet3tOPrAIxGnvXQR13OAmCkHmtBbh+q+u3vrCTiOA8vSRU0oeMmktQg3hdVff44u9wzAqnUf8XqfT7AAhAzaFrzwjAsw6KLeqBcvQoxW2aTL76cAlPcAiKSRxLzXl+C6+25Dyk37fcgiDQtASNmrZgnGXNYfLf90WqaiuK9TgEzY8ifrP8U1M4fitf/8O+/jFcIAC0CYkWe26R8Px03trkHzg49GdekNmK6Bslhc+gC65KaOItB+gfB2CjJV+NXwzKwoZtm+o5D5XA1w77fQY5n8vJDaGdjQK35xgc82f4Vxz/wD0xc/ocubMYGABSAKyDN8/GHNcNXZnXBGs5NRPR3Xrxt6u9BUtfehZgauCi/2kouyC3fbPQj6P+XNIOiBwpHV6+RYGN6llKTtBwd2gYtV327AlMUz8egL85C0U34fCWYnWACihDzT9WuX4NrTO+Fvx7fAvvUaeNMCQ0UXCs8byGBkrwyv7oAXUIRyTgENetp1UFuOKtrQlR5AGZ5+/RU88vozeOWtFVIb3F9vI7NbYQGIKHTSG+21D8499mwc1/hInNXkWFSjtQEHsOUUIevgCz3IjXLSIGxdtZdcAFsO7pT8+HTzBixduRwrP3wH8996Cclkklf28wAWAEaXG7cs1CvZE4cdfCiO2/MQlNSsjZrFNdCg7p6oVa2Guv3TVGH911/iy22b8fUP3+HbrT/gzbXv479r12BL6U+8j5+HsAAwTIRhAWCYCMMCwDARhgVgdyKPdCKRQP06e6L2HrVQS34QW7dtxRdfbsT3P3yPZDrFc2lmt8ECkEvkkT1gn/1wzjHN0azRoTjlqGNRM1ENCUN3+zWxva6/rTbLTHz+1Qa89O5reHntW1j0xsvYlipjQWByBgvArkYezT1r1sGFZ7RB+5NbYb+SBihEDAld5F4VwRAxL/ZOqOxcVciDAnCy2+y0w+Ztwi98exlmrpyPxStfQjKVjLYYCC/WgCIWonwcdiEsALsIGtt/KGmIgW2uQofjzpGD2pQXqW5s6YXMYXudru1tvLJXspENtZM/o5uBUkUfHapro9Quwz9efhpTn38Ca774JDo6QMd1z4a4pkUntD3pHNSoVozNWzZj8stPYfKiWfhh8ya/LcxrWAB2AUWJQvRq2w3XtrgY1d04DMvaXogjc0svlw23U2xtuYg7jW4ESt6BoYJwkoYKs5OvyUmCFJRFH72BaS88joUrXlAiEVZM+a/rOR1xa6frUEMU6HJnKpnRhuO62OQm0WfqCMxdwU1Dfi8sAFWkeZNjcXfXAWhUoz5F0yDtNe5UiTbZKDpRLtGGauBheygtvE8yobT0c47+YVfokt+G4ymFKUDJsyQOKeFg3fcbcN/CmZi5ZA6STrjSauNmDA/3GoGWTZtLbyquaoe6KmRZekUkBK4+gLYUg8ffXorek27lXIPfAQvA7yRmWOh7YU95178Exa68LOUH3Z5TlotCEZeevb5bKU/AECoaXhXpFJQc5w34X4iVTQoTCfphx5sumIaqned4vTJjjlBTC/Xj8u9t3PI9pkoRuO/ZR7ClrDTvQ3CL4oV4sM8YtDjkWHlI5bG1DKS8xCVLHjsqgqqqHEGXEKfKQv/6bBUuH9cP6zd95bf5eQULwO+gRmExpt90J5rv10zejS2kTGpkKe9OLlXhUXG1enS6otyMP1OX738LcmQGrCjnFdDSQBqGl2QjqOSnyrxTnoS8+l2RuR+66tfRmNiaTmHa/Ecx/Nn7sW3btrzMty8pKcGzve/C/o32hyvv/NQiXeUuOjpN2ZC+QJmlcxXo3cdcOm6Oqi78wXefo8vov2PNxnV+v428gQXgN1JSUg9LbpyCRns1zL5WvjVXELDldGDKkicweu4UfPPjd3njEZx8xPF4pOdo1Cou/k0/V77aUGlpKZqPvBwff/ax328nL2AB+A00rNsAjw6chMNq/yG7yEeDP2h32lKRllMIea+Uc+JpLz2FMfOmqOSdoG4d0NrH9a0uw6D21yEtj2XRbzQ0U3Iscx4+++krtB7aA2u//MzvtxZ4WAB+JQ1q18MTg+7H4bUaqsYburil7tBLmXRBgmyCV6OHHn+SgjBt6VzcM/8RfPHtRr/N24G6Nepgcu8ROOXAZjBppU8eU9P6bR5VeQ9AkU7jvR/Wo82w7viGhI/5RVgAfgV1q9fG/KEP4ZBajfSE3soUyNCHLmhTACq57ZoGbFo0dG0UGLRI6cq5MzDn5UUYv2AKPlz/qb8egTx0rY9tgXGX90Otglpq1yMNag0mParYbzesvAiIlKt+3+ufr8L5Q65E0uHdgV+CBaAS4kYccwdPwQmNDoMhL85U3JQDym+rKsYWugKwijQ09WIiVQum3QPYgJOwsejd5bhr8Qy89v4bu92+urXqqoF//hGnqk6F8CqUkSDZtOhXxeNb5goUOPoAPPbeEnQffxN3G/oFWAAqgFzo+64bjo5HnSUHkqW28Wzpnhb6bdivoPwdUZfqQrZb0DbpDRTJ+bLjpvHuFx/h7gUz8PTrzyFl5zaWoMCKo9PZ7TC0XQ8UW0UqFwLCUt6KS/EOnnBVdU1lG3kSUgBMKXjCcjFg9njcu2jGbjjq+QcLQAVcenobjOvcT9455UUq3f60qYN7YgFz+XdGqI5Ahg4lli6AcFRAggovLpVjq4YcaPSSpWp36hCbLc42TF76DB5cMhvrN67fpdMDGuhtTmyFm1tfgcYl9WGk4nALdVPQAi/YiRb/yEuxKMbhd0wBdnj/8v2l6Tx527DbzDTOG3EV3lj9tq/nJYiwAPwCR+93CJ67daocG9JFjSX0vrOrA/tNK9hzABuOagGuTqwcDLb3PEaDjJKRXBr0hq4KrOr66S7dBUKHJS/bsApPvjofT722CN9s+n2LaDQIqxcVo/2JLXF1y4txYN19dJNS6WSk5TQq7uVIuPTXaeHP3V5mvKrNS4WrXR0hFSWltM/Bl5u/xCl9O2FT8ie/T0+gYAH4GYrMBJ4Z8Q8c2WA/xGxTxfZTNJ6qh6/ieIItAFWFPAahIg8NvP3Zaix+dxkWvvcKVn+8GmXp5C87B/L4FBUW4eymp6JF01Nw7olnoNiI6VwoL6hJlSHP9eoj9Snwoia1sAiUmSnMXrEYN0y6hTMJy8ECsDPyaAy/9EZ0ObM9iuX8VLgWvGK52Vy+oO3772rSQi3N6dBElabsyGMgpxRyuvDtpu/xwgdv4Pstm7GlbCvSjo1qBYWov0cdNDngEDTe5w9y0BXK6YWl1vZ03wB9V6cLTWc45vb4CR0fqfIoKTKTUq5d6QVsNVxcfVd/PP2vJX4f4sDAArATx+x/GBbe+pCa65uClqQoNc9Q3WyyjTCMYK8BVBWHApxUNqKrUpPJlTZcoaYLygOKWdk1dZ3wKHQNA6Hdb+3Cm7rYAWHqeb6ud5B7D4BCgykVmyIhLKU6FJbtwIk5WP/91zixXwf8lNrm92EOBCwA5YjLe9PiYdNw1N5/9BbNvTuXqQdFzMisjYVbANSiYSYJCbpRWFoV4TCVd1CLBpdw1QBTF49awS93t3dFOXdJ/86M17RdIHJov9B9C1S2hDDKpRHrDKqJC6bhllkTAxsZuTthAShHlzM6YkznGxEz49nX1A1E6MaWhucSG2awIv92NWoL0UtpVgtq2aZg5Y9KBT/vZTpm7viU1pz1GWjVP8f6SbsghsoWNNXYN43ttgu1KJrCiQMuwuov1vpyfIMEC4DHHgU1sGLiPOxVsAeccneoTNYZPMd1d9zB/Mb25uk6eVFkS3FlW4NZFV8y7k6Hxyz3u3bHIqrIeiB6K9TrcJjtY0gTurnvLcPlo2+IvBfAAkDIIzDsouvR8+zO6oKg0N7MQVGXkNAhdZmFrLBfM5nEGu396Pp76hB4bnwl418VQcnM87MNRrH953N9/LLtCz1PRkBkXyQBcuUUJ2U56DCqJ15etfsjIYMEC4CkTo3a+M/dz6KoLAG7wFSr13qryLvlCX3foAfaM0+EfBcAYsfA2Z3f7c4XjLHTvlp2wHmvG15NREdNx6se6lsZaeiIQtMzYweDhS7QQvs6r657B+cPvjLS7cpZACQjOvfH1c3bw03QRWOrElRMeKFuxmptQM5Vuk66GXPejG5NwcgLQF15919513zUFRThL1TBScNgAQgzrpuWV35MlVV7Zd3bOPe2rn6b5BuRF4Dhl/wdV5x1kdriKkxJxzDuqnp/THghD4DSniyXioo6aDWiB1Z+8JbfZvlCpAWgRkEx1ty7EAVGAVJmDEVSAOyEjplnwgttE1IdRxIA2p6c997LuHzsjZEMEY60APRudTlubXM1TFVk0lDbV0YEtvmijm7QJNT2ruGYSJopHNunPT75JnolxCIrAKZ0+d8ePx+NapbowB5XuoWWgbib+0AVxl+2b02qIGaVHXn3c//E/80c7bdpu53ICsBfmzXH9F7jEM8Uy6DEVOn6x1XAu9/WMblEJTupbQAhz7mJmAN8k96KI64+PXLlwyIrAI/2HoPTmp0KSlalEHFHTggtFcOa82RVxmeS8oovsNUCAMpMU5UPo2Iv3R8YqOogRIlICkDDPeph+V3zsAd1k6CCHxQxYuruvYbxv407mHCRlB8JW4cl23EdImw6abyweiXajrrOb/N2K5EUgBvO64a+bbujGrn7UgAMS0epmZl+nbwLEGpSqr0YPTOQjlFos6tKkpdaKRx5bUt8tyU6HYcjJwCk+q+NmYPG9RoiQZtAJAKmzvenKYChG/r5bSaTQ2zhwHJ1eDct/KraD7Yt7wUC//fYJEx6ZprfJu42IicAB+29P1aMeFTO+4UO+ZWPVPBTJbnBK3jBAhBqHPlPlQrzipGqasS27iXw1vqPcPrATpFZCI6cAPTv2BP9WmZCP3XWm5Mt8+E19OQ4gFDjSg/ANHSit+N1aja94kXU5PmP152FrzdHo6NQpARAuf93zsGhdffLRn3pRT9CL/xFId2X2Y4rdDmzTOInnfybZt+BBxbM9tu03UKkBODgvQ/A6yNnw1TNtj2ELvdpqDLShrogLJ4ChJpsu3Zv0GfqPBAUCbroPy+jwx29IzEVjJQA9GpzBYa06iHnerpAlapTp0rGeiWsDN1SM+Bl/5kqsr3AK7L1AbIeoXyyTZSi8ZVnYmu6zG9Tc06EBMDAikHTccDBR6DAb1OYQEOhwW3HXY8X3lnmtyk5JzICULOwOj67b6m8w5uwAt7ai/EXqio8YdEs3DpzrN+m5JzICEDbk1ti6hWD5cm1AtfOmwkWJADvb1yLk/p1CP2KcGQEYOzlN+Oy09oipop7sgAwvwxVFbaNNPa/6gxs2RbuXoKREABa2Png3gWoU1yCuGNWufssE27U1qBIo9WdvbHs3RV+m5NTIiEADfYowX8nPoM0YkiwADCVoOoFOA5GzH8Iox6/x29zckokBKDrme0wrnM/bJXvtEhYqtElw/wSru3KaaKL+auW4yKKBwgxkRCA0Zf2wZVnXKgKQcZdkwWAqRDXcWGaBr6zf0TjbqdBhLhvQCQE4N8jHkPjho0hbOneWfDiwBnm57FVNCitBdg4ste5WL/5K79NyhmhF4BEPIGNU15Rrb4NKgNpsAAwFZOpGSjgoOt9A/Hka+FtHBJ6AWhy0OFYOughxKjhl9fnjgWAqQgSAEoOpDzRUfOmYOSc+/02KWeEXgCuatUZw9r3hiW8Wn/G9l71DPNzUBwA5QdQitD0V+ah15ShfpuUM0IvABMu74/Op7dTBSBUnj/n+zKVQAJAJeIoZezdjR+ieb8LQ3vNhFsA5DtbNmQ6Dtv/cJhCV4CJRoNvpkpIAUjJS4Q6RJYhiUaXnYQ0HL+tygmhFgALJr5/eLlU8wJV8FN1g6EoLw4FZipA2AJpSwuACxuHXNMC35SGs1BoqAWgfp09sWb0U3BiBarwo0P1P10WAKZiXCkAdoyaxNBagIMzhnTBvz9632+zckKoBeDMQ4/DjJvvBm0Axh0XwjJ0OjAvAjIVYqtq0bRYnJRTxgEP3Y4pS5/w26icEGoB6N2qCwZ16Klm/DGv8D+1AefeP0zF2PBqRCMpXNz55P0YOfcBv43KCaEWgCEdr0fPlpeoBpAx2tg1qQCk5bdZTMAR8EJGJWn52eQFMzBg9p1+m5UTQi0A9/cYivYn/FXd9anuO2UBCr7/M5UghA4DopqB5DjOXbkQXScN8NusnBBqAVhwy4M4/oCmcMyMB8BDn6kcEgC1YUzNQywXr3/8Nlrc1s1vs3JCeAVAvqvV9z2P+kV11Oo/CQCV/WYJYCrDgasiR5UjYLr45LvPcfQNrUMZPhJaAaClvg0PLUehSMCRrr/lCg4DZn4VthQAKh1HLSMM6QEkkUK9S49nAcgnqhVUw6cPLEXcIQGgtm/CiwRkmIqxpfsYo8hRh7xGoMwowz5dT4bt2H6btssJrQDUr1EH7979LAocqgAU0/3/MpGABgcCMRWQEkgl9K0iLsd8ykyicffTsSW11W/LdjmhFYCDGx6AF2+fiSIp4aYR97rB6E5AHAnIVEhaDnqKAxZUQQpImmkc3rMFvv0pfOHAoRWAgxrsj5dGzdYCYMa2F3VypABYLABMBdiuCgUmYo70BkwXJ/bviDUb1vpt2S4ntAJw/p/PwuSew+RcLgZLDvhMDqDg9t9MJQh5k3ApeYSWkuVzx3DRddJAzHvjOb9N2+WEVgD6XdAdA1r3gFBpwMh2euWhz1SOq5J/DUFrRlRGzsWIuQ9g1JPhCwcOrQDcfH4P3Nz6KjnuvZqu5e76LAJMRVAgkOvtGJkUCyCfjZh7H0bODV9psNAKAMMwlcMCwDARhgWAYSIMCwDDRBgWAIaJMCwADBNhWAAYJsKwADBMhGEBYJgIwwLAMBGGBYBhIgwLAMNEGBYAhokwLAAME2FYABgmwrAAMEyEYQFgmAjDAsAwEYYFgGEiDAsAw0QYFgCGiTAsAAwTYVgAGCbCsAAwTIRhAWCYCMMCwDARhgWAYSIMCwDDRBgWAIaJMP8PRRzX07YcZyMAAAAASUVORK5CYII=\" style=\"border:0px solid black; height:256px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; width:256px\" vspace=\"0\" /></p>\n', null, null, null, null);

-- ----------------------------
-- Table structure for trx_logos
-- ----------------------------
DROP TABLE IF EXISTS `trx_logos`;
CREATE TABLE `trx_logos` (
  `logo_id` int(11) NOT NULL AUTO_INCREMENT,
  `logo_path_name` varchar(255) DEFAULT NULL,
  `logo_file_name` varchar(255) DEFAULT NULL,
  `logo_extension_name` varchar(255) DEFAULT NULL,
  `logo_status` int(11) DEFAULT NULL,
  `logo_create_by` int(11) DEFAULT NULL,
  `logo_create_date` datetime DEFAULT NULL,
  `logo_log_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`logo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_logos
-- ----------------------------
INSERT INTO `trx_logos` VALUES ('1', 'assets/upload/logos/', 'logo201611031620343841', '.png', '1', '1', '2016-11-03 16:20:34', null);

-- ----------------------------
-- Table structure for trx_ordermethods
-- ----------------------------
DROP TABLE IF EXISTS `trx_ordermethods`;
CREATE TABLE `trx_ordermethods` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_type` int(3) DEFAULT NULL COMMENT '1=pesawat, 2=hotel',
  `order_content` longtext,
  `order_update_by` int(11) DEFAULT NULL,
  `order_update_date` datetime DEFAULT NULL,
  `order_status` int(3) DEFAULT NULL,
  `order_log_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_ordermethods
-- ----------------------------
INSERT INTO `trx_ordermethods` VALUES ('1', '1', '<p>Cara pemesanan Pesawat pada perusahaan kami sebagai berikut:</p>\n\n<ul>\n	<li>Harus beli tiket</li>\n	<li>Harus punya uang</li>\n	<li>Harus pulang lagi</li>\n</ul>\n\n<p><img alt=\"\" border=\"0\" hspace=\"0\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAlaklEQVR42u2dB5wURfbHf909M7vLAhJWQBFPxXAGRPHuzJjF41CRKCoqoKCioCeIhL8iQUBAgmICVDgJnopgIKlgQEQ8z3icoCIqghkRF3Zmurv+9ap6hoXTXXUZuqf7ffksMzub3nR3/fpV1QtGzc5HCTAME0kMFgCGiS4sAAwTYVgAGCbCsAAwTIRhAWCYCMMCwDARhgWAYSIMCwDDRBgWAIaJMCwADBNhWAAYJsKwADBMhGEBYJgIwwLAMBGGBYBhIgwLAMNEGBYAhokwLAAME2FYABgmwrAAMEyEYQFgmAjDAsAwEYYFgGEiDAsAw0QYFgCGiTAsAAwTYVgAGCbCsAAwTIRhAWCYCMMCwDARhgWAYSIMCwDDRBgWAIaJMCwADBNhWAAYJsKwADBMhGEBYJgIwwLAMBGGBYBhIgwLAMNEGBYAhokwLAAME2FYABgmwrAAMEyEYQFgmAjDAsAwEYYFgGEiDAsAw0QYFgCGiTAsAAwTYVgAGCbCsAAwTIRhAWCYCMMCwDARhgWAYSIMCwDDRBgWAIaJMCwADBNhWAAYJsKwADBMhGEBYJgIwwLAMBGGBYBhIgwLAMNEGBYAhokwLABRR579uBVDyR61sfee9bFfw32xT8295BeM7Le4QiDpJLH2q8/w6Qb58c0GpFKp8t/C5CksABHDkGe7sKAIf25yDE446Bj86cAmOP6gI5CQX4vD9L5LP9L4psFvGPqRngjXhTAFvi39ES+99Tre3vghFr75ItZt/FxqCV9K+QYLQEQoihfgbyeehRZHnYx2R50G07AghCEFQehHiwa5HOBGZvjrgU+4Qn2qFUF+GG65Xyw/ScPBFz9+izmvLMDcFYvx7mdr/H67zK+EBSDMyDO7b929cW2rLriweStUjyVgysFrxMyq/+4K+HD9R5j4/GzMevkppO2030eBqQAWgDAiz+g+JQ3Qq3U3XNL8XBTRbVtYsOlB3t8LjNxO3ktFSk4nDGz6cRPGPP0gHl4yBykWgkDCAhAyiuKFGNjuanQ+pwOqy0Fo2jR1j6uvCcsADcNcC4ArXG+aIP+TLsenP32HITMmYs7yRbxOEDBYAEICrdGdcfRJmHblENQoLJZzeW+QmwbJAFw5kTfpNflhmLkVAOEI2HKWkRQOiukFW6i1gmWfvo/rpwzHRxs+8ftwMR4sACEgZloY3u0mdDupNWJC3u1pUY+m+eTyq9V7ZD5Rjzl2ANR4Nw1Bf0mtIBoO1M4BLIFtro1bZo3DlEWPsi8QAFgA8px96jbArOvHosnejWl1D6WWiYShV/dj9A3C8Ma/gCNflzdn+fXc2qR0Rk0BhNpSsGn7UP6zoDRAfj2FZ1etRPeJ/VFaVur3IYw0LAB5Cg2ypgcehjl9JqLErA1Dzu+TlgvLlO6+oQeaJYe+8gAyW3g0GOWTeK5ts+lvCbWlKNSfpT+uPZGUfBZPSjFKuFj17XpcNbEfVq1bw0FFPsECkKecesSxmHXjWBTKf44c9BTgY2U37KHm+jQIbfXUkk+F+h54e/65RMjbv+2ZEdPG6GACaZMrRSglbUikHGmTi61WGS4a3wcvvruSRcAHWADyDoFWh56K+/uNQVyOZBpgmV19I9eT+131DlR04XZbk3YSvR+4DbNeW8AisJthAcgzTj38T3jkpnsQk3PsuGkp9zozZvJHAFxkViINNUNxsEWk0Ov+2/Dk8sUsArsRFoA84tB9D8Iz/SZhj+p14ToOLMMkLx+GnAKoEN08EgDX0AsEplowFBCWg1LDRrcxfbHwnWV5817yHRaAPKFmQXW8OHwm9itpqAYHJeWYUgBoMKnHPBIA2iKgTULaKDCzOwaAYzhIiTTOG9kdb6x+328jIwELQB5AA/uxPhNw5iEnIhk3UZBZ6PO2+MsP/syCf6DxMgt3fE2/D2LT1i9xYv/LsGHT135bGnpYAAIOnZzr/tYZw9peB9rYK5PefuFOgyd/7vzee3JFVqWUN0MZieW2CpG2sfyLVTh/8BWwXdtvc0MNC0DAOXCv/fHqiFlI2HE4MSFdZgOWldtsvlyjLrhy8QnecgCy0YqOQMpy8eCLT+DmB0fmgUuTv7AABBhTmHhxyHQ02fdQlFlATFAQD22h7SgAeeH2l8OhdQvavVAaQGHL5QoMuAa2kZeTFvJpGhdP6IMF7yzz2+TQwgIQYLqe0gZ3XtofImYhKUd4obdibuS5B1AZaSUQUgClCHxd9gOO6XMBtpRt8dusUMICEFCKC6th2ZjHsX9xPTkSTKSh7/5READa4aD3TI+uIacCLz2JvlNvzy83J09gAQgi8oz0bXcl+p53BWKgYB/TWzUnh1lH14cZ4YU0UwETinQUTgrNb7sc73262m/TQgcLQACpUVANq+9dgGpmkXT9TRQIldYDR9Xr+981gNDh0PxfC0DckZ+7NpauewsXDO2BnOcyRwwWgADSp213DDqvq7z1xZCSrnCC1sgEJdKIaAgAXFWglNB+jys1IYWukwbhqTeW+G1cqGABCBhxM4Z37noWexfXgbrfm4aXxYcd9s7DDA14XbEYqrJRmeGiKO3iv199jOP6X8hOwC6EBSBgnHfCWZjWfbh09001+zdUmJ/wcvx1YY3QC4Agv1+XNKIahpTKbEkvKBVz0GZUTyz7z0q/TQwNLAABgu54U/qORttDT8dPMR3xF0sbNBOAfJBTAZFXST+/GyE9AIfetxaARIqShYBtpoPlby1Hu/G9/bYwNLAABIg6xbWw9p7F2Sq+zM/TtNdfse6HL/02IxSwAASIbn+9BGM79oJhsgBUxMin7sOIx+/324xQwAIQFORZmHbjOJzb5CRYVsxvawLN2k2foVnv1txjYBfAAhAQErEE1k5diuqiUBf4YH4RIZI46ZbL8D4HBlUZFoCA0Oygpnhu0GRY1FQr5Gt8VUXYDsYumoahj97ltyl5DwtAQOjX9mr0O7craPMv15178h3KEVi18SOccHNHzg+oIiwAAeGRXqPRstmpqm136Lf5qoirkqJsHHDNmdi09Ue/zclrWAACANXFW3P/YtQsrI0EewCVkhQCCSkAl00dhHnLFvttTl7DAhAAaiaq45MHlqisP0MVyOBFwIpQJcTkx6T50zHonxP8NievYQEIAIc2PBCvDp+luviqUtkhz/evKrT9R5XDlvx3BdqOvMZvc/IaFoAAcN7x5+ChHrfJ4R9TufBWLMe9u/IcXUQU2OSUonGX5hAGX8K/FxaAAHBd6ysw9IIe8sI21RZg+NN9qwaJJC0Epg0Xh15zFr4v/cFvk/IWFoAAcH2bq3BLq64wYjFV8svkNYAK0aXCqLGIg7MGX463PvmP3yblLSwAAWBgpxvQt8VFEKalMgJ5G7BiSABseYws4aDb5Fsw59UFfpuUt7AABIBBJADnXCzvat4iIAtAhdAUIOWlRw+eew/Gz5vqt0l5CwtAACAB6NPiElX9hua2MV4DqBAlAPIQFdjAqGen4vYnJvltUt7CAhAAlABID0CVABO8BlAZqrWoKpPm4O5FMzFo1ji/TcpbWAACwMALr1dTACr5SfN/XgOoGBaAXQcLQADo37EX+rW8FOwB/DqE11fQMFxMWDATt8y+02+T8hYWgADQq/WVGHJBDxiwIlH0s6rsKAAzpACwB/B7YQEIAJ3ObIN7L+mvG2VSMJDFAlARSgBcKZUkAItmYPCs8X6blLewAASAvxxyNBYOeEDf1VwTZoynABWhBEDh4o5nHsbtj93tt0l5CwtAAGhYuwHeG/8MXEfAMkxOBqqEjABQ/4ChTz6AO+dN9tukvIUFIAAUGAlsePhVUPF7S4S/+29VySQDUdREzweHYsZL8/w2KW9hAQgC8gy8MPwhNGvURF7SJmK8CFghSUFRgC5gp3HcmG74YDXnAvxeWAACwqjOfdDjzAvlGbG4zF0lJOVHwqb2YTb27d0CP27Z7LdJeQsLQEDo1qI9xl50k3zGNQErw6FYCQoHFmWo3/Uk7g9QBVgAAsK+9fbGO6PnwhAxrglYCToOwMWrH72Jvw3r4bc5eQ0LQECgLsAf3P886hfU5jiASqB0YGLMc9MxbAbXBKwKLAABYthl/XDtaR24KGglUDagnAig05SBWMBVgasEC0CAaH7MyXiq1wReA6gEipcwTAeNrj0TW3gBsEqwAASImBXH+vteQlFBkd+mBBryANZt+ARNB7TlHZMqwgIQJOSZGHnFTbj6Tx1RWgyV8lrkGLQxAFUtVAjQp7GQX/Y2JUZTZSRh6BLgJgX9amhyZCYdDHnuQYz9571+m5r3sAAEjGP/2BQL+05BKibkQI/Boivf1HUCMyGwYZ8i6Cg/fadX79sQ0IfB0K8LG3+5uQPWbPzEb1PzHhaAgEG7Aa/c8U8cXn9ftSUozEyZcN0MgwZA2AXgl8ikAa/+dh1O+HtbOCZfulWFBSCAdDztAtzbuY90gQthx8nlN7X7L78Wo7MV8jgBIVx9589MdTKeD41/+dItT9yNiU896LeZoYAFIIDErTjeHT8PexWXwLYoN8BSdz+a/8cdebrCniwkXXyoKY92+kkQ6M7vyH8pqQLUDOQH7gq8S2ABCCg9WnTEkE43IGHE5Q3flJe+dn8pWxAhjxOgNF8tAIae9Tu0EOggKQf/o688g96ThyDk66C7DRaAgFIQS+DVcU/iwBoNQFe7Y6osAekGC70jEGpU2U+9GEgiIB0ARwrAZsPB2X074MOvPvXbwNDAAhBg2h/fEpOvvA2GacE2af5Pg8KR4z/czUNd4e0DGEJvBUoPwI6lMfffL+HKcTepdQBm18ACEGAseaUvGTwNTfc9DCk55hNqMdCh/uHeIlk5MmWyyEHI80VCW5X9NmA6+vO0Rb0A0zj5hnZY8916v80LFSwAAadxg/2wYuSjiLsx2FIEKC4gM8DLi0B+D/kdUem+md0OVf/fxb0vPor+D90RrjcaAFgAAg6dnBtad8Hg86+WZyuGrfKVasZ2Ach8T5jGhbogXR0BSLsfX//0Hf789wuwJVnqt2mhgwUgD6BdgIW3TsVf9jkSybiBAuwYDLTzCcx3MRC2gGvJKYAc/K6cB3SZNBDzXuesv1zAApAn7FuyN5aNmI3qsWrqc+oe5Lo6YCbTSSgs0wFa9FNVfgwbc956Ed3G98v/NxVQWADyiOZHHod5N06Ai7gKjrFo4Ls7thILw3QgLd9bLGXjg81f4PSBF2NrcpvfJoUWFoB8Qp6pK1u0w7CLB+gEQUF9BIzQJQrRImCZkULL27rh7Y+54m8uYQHIQ+6+5BZ0OPt8lRb8c/kw+S4CwnFw8aSb8ey/nvfblNDDApCHUIT86O79cdkJrVUZcYqaU0lCIqXbi0tp0GmD+vtFNrBGhxT7XU9AuGkIg96FCZXhJAw4lou0IZBwHdw4bzweenJmBCIe/YcFIE+hYT6qx0B0OeF8xB0vbJaWAqheXiymvsf1CmZbShx0/EAQ1giSco6foHA+14Ar5zIp6Bp/ZP7MF+bg+ukjfLYwOrAA5DE0kIdffAOuOqO9fB5XyTO0ZYiYka2VT4U0VKsR4RXXoCIbfkcKemm9lNjoUrivY8OSc5kJi2dh8IwJXOd/N8ICkO/Is3fZWW0xqnNfFDoJr3oQsoMsu0DovUZRdZRb4KvJNlTFIzKngFRApDHs8Xsw5tnp/rsnEYMFIATQ+D6pyV/wcO+xqBsv1luEan7t6toBFrZXEyKfYDcnE6lmnuV2KVRhH2mbaVKNAwddJ/bHvDeX8uD3ARaAELFXrXqYet3tOPrAIxGnvXQR13OAmCkHmtBbh+q+u3vrCTiOA8vSRU0oeMmktQg3hdVff44u9wzAqnUf8XqfT7AAhAzaFrzwjAsw6KLeqBcvQoxW2aTL76cAlPcAiKSRxLzXl+C6+25Dyk37fcgiDQtASNmrZgnGXNYfLf90WqaiuK9TgEzY8ifrP8U1M4fitf/8O+/jFcIAC0CYkWe26R8Px03trkHzg49GdekNmK6Bslhc+gC65KaOItB+gfB2CjJV+NXwzKwoZtm+o5D5XA1w77fQY5n8vJDaGdjQK35xgc82f4Vxz/wD0xc/ocubMYGABSAKyDN8/GHNcNXZnXBGs5NRPR3Xrxt6u9BUtfehZgauCi/2kouyC3fbPQj6P+XNIOiBwpHV6+RYGN6llKTtBwd2gYtV327AlMUz8egL85C0U34fCWYnWACihDzT9WuX4NrTO+Fvx7fAvvUaeNMCQ0UXCs8byGBkrwyv7oAXUIRyTgENetp1UFuOKtrQlR5AGZ5+/RU88vozeOWtFVIb3F9vI7NbYQGIKHTSG+21D8499mwc1/hInNXkWFSjtQEHsOUUIevgCz3IjXLSIGxdtZdcAFsO7pT8+HTzBixduRwrP3wH8996Cclkklf28wAWAEaXG7cs1CvZE4cdfCiO2/MQlNSsjZrFNdCg7p6oVa2Guv3TVGH911/iy22b8fUP3+HbrT/gzbXv479r12BL6U+8j5+HsAAwTIRhAWCYCMMCwDARhgVgdyKPdCKRQP06e6L2HrVQS34QW7dtxRdfbsT3P3yPZDrFc2lmt8ECkEvkkT1gn/1wzjHN0azRoTjlqGNRM1ENCUN3+zWxva6/rTbLTHz+1Qa89O5reHntW1j0xsvYlipjQWByBgvArkYezT1r1sGFZ7RB+5NbYb+SBihEDAld5F4VwRAxL/ZOqOxcVciDAnCy2+y0w+Ztwi98exlmrpyPxStfQjKVjLYYCC/WgCIWonwcdiEsALsIGtt/KGmIgW2uQofjzpGD2pQXqW5s6YXMYXudru1tvLJXspENtZM/o5uBUkUfHapro9Quwz9efhpTn38Ca774JDo6QMd1z4a4pkUntD3pHNSoVozNWzZj8stPYfKiWfhh8ya/LcxrWAB2AUWJQvRq2w3XtrgY1d04DMvaXogjc0svlw23U2xtuYg7jW4ESt6BoYJwkoYKs5OvyUmCFJRFH72BaS88joUrXlAiEVZM+a/rOR1xa6frUEMU6HJnKpnRhuO62OQm0WfqCMxdwU1Dfi8sAFWkeZNjcXfXAWhUoz5F0yDtNe5UiTbZKDpRLtGGauBheygtvE8yobT0c47+YVfokt+G4ymFKUDJsyQOKeFg3fcbcN/CmZi5ZA6STrjSauNmDA/3GoGWTZtLbyquaoe6KmRZekUkBK4+gLYUg8ffXorek27lXIPfAQvA7yRmWOh7YU95178Exa68LOUH3Z5TlotCEZeevb5bKU/AECoaXhXpFJQc5w34X4iVTQoTCfphx5sumIaqned4vTJjjlBTC/Xj8u9t3PI9pkoRuO/ZR7ClrDTvQ3CL4oV4sM8YtDjkWHlI5bG1DKS8xCVLHjsqgqqqHEGXEKfKQv/6bBUuH9cP6zd95bf5eQULwO+gRmExpt90J5rv10zejS2kTGpkKe9OLlXhUXG1enS6otyMP1OX738LcmQGrCjnFdDSQBqGl2QjqOSnyrxTnoS8+l2RuR+66tfRmNiaTmHa/Ecx/Nn7sW3btrzMty8pKcGzve/C/o32hyvv/NQiXeUuOjpN2ZC+QJmlcxXo3cdcOm6Oqi78wXefo8vov2PNxnV+v428gQXgN1JSUg9LbpyCRns1zL5WvjVXELDldGDKkicweu4UfPPjd3njEZx8xPF4pOdo1Cou/k0/V77aUGlpKZqPvBwff/ax328nL2AB+A00rNsAjw6chMNq/yG7yEeDP2h32lKRllMIea+Uc+JpLz2FMfOmqOSdoG4d0NrH9a0uw6D21yEtj2XRbzQ0U3Iscx4+++krtB7aA2u//MzvtxZ4WAB+JQ1q18MTg+7H4bUaqsYburil7tBLmXRBgmyCV6OHHn+SgjBt6VzcM/8RfPHtRr/N24G6Nepgcu8ROOXAZjBppU8eU9P6bR5VeQ9AkU7jvR/Wo82w7viGhI/5RVgAfgV1q9fG/KEP4ZBajfSE3soUyNCHLmhTACq57ZoGbFo0dG0UGLRI6cq5MzDn5UUYv2AKPlz/qb8egTx0rY9tgXGX90Otglpq1yMNag0mParYbzesvAiIlKt+3+ufr8L5Q65E0uHdgV+CBaAS4kYccwdPwQmNDoMhL85U3JQDym+rKsYWugKwijQ09WIiVQum3QPYgJOwsejd5bhr8Qy89v4bu92+urXqqoF//hGnqk6F8CqUkSDZtOhXxeNb5goUOPoAPPbeEnQffxN3G/oFWAAqgFzo+64bjo5HnSUHkqW28Wzpnhb6bdivoPwdUZfqQrZb0DbpDRTJ+bLjpvHuFx/h7gUz8PTrzyFl5zaWoMCKo9PZ7TC0XQ8UW0UqFwLCUt6KS/EOnnBVdU1lG3kSUgBMKXjCcjFg9njcu2jGbjjq+QcLQAVcenobjOvcT9455UUq3f60qYN7YgFz+XdGqI5Ahg4lli6AcFRAggovLpVjq4YcaPSSpWp36hCbLc42TF76DB5cMhvrN67fpdMDGuhtTmyFm1tfgcYl9WGk4nALdVPQAi/YiRb/yEuxKMbhd0wBdnj/8v2l6Tx527DbzDTOG3EV3lj9tq/nJYiwAPwCR+93CJ67daocG9JFjSX0vrOrA/tNK9hzABuOagGuTqwcDLb3PEaDjJKRXBr0hq4KrOr66S7dBUKHJS/bsApPvjofT722CN9s+n2LaDQIqxcVo/2JLXF1y4txYN19dJNS6WSk5TQq7uVIuPTXaeHP3V5mvKrNS4WrXR0hFSWltM/Bl5u/xCl9O2FT8ie/T0+gYAH4GYrMBJ4Z8Q8c2WA/xGxTxfZTNJ6qh6/ieIItAFWFPAahIg8NvP3Zaix+dxkWvvcKVn+8GmXp5C87B/L4FBUW4eymp6JF01Nw7olnoNiI6VwoL6hJlSHP9eoj9Snwoia1sAiUmSnMXrEYN0y6hTMJy8ECsDPyaAy/9EZ0ObM9iuX8VLgWvGK52Vy+oO3772rSQi3N6dBElabsyGMgpxRyuvDtpu/xwgdv4Pstm7GlbCvSjo1qBYWov0cdNDngEDTe5w9y0BXK6YWl1vZ03wB9V6cLTWc45vb4CR0fqfIoKTKTUq5d6QVsNVxcfVd/PP2vJX4f4sDAArATx+x/GBbe+pCa65uClqQoNc9Q3WyyjTCMYK8BVBWHApxUNqKrUpPJlTZcoaYLygOKWdk1dZ3wKHQNA6Hdb+3Cm7rYAWHqeb6ud5B7D4BCgykVmyIhLKU6FJbtwIk5WP/91zixXwf8lNrm92EOBCwA5YjLe9PiYdNw1N5/9BbNvTuXqQdFzMisjYVbANSiYSYJCbpRWFoV4TCVd1CLBpdw1QBTF49awS93t3dFOXdJ/86M17RdIHJov9B9C1S2hDDKpRHrDKqJC6bhllkTAxsZuTthAShHlzM6YkznGxEz49nX1A1E6MaWhucSG2awIv92NWoL0UtpVgtq2aZg5Y9KBT/vZTpm7viU1pz1GWjVP8f6SbsghsoWNNXYN43ttgu1KJrCiQMuwuov1vpyfIMEC4DHHgU1sGLiPOxVsAeccneoTNYZPMd1d9zB/Mb25uk6eVFkS3FlW4NZFV8y7k6Hxyz3u3bHIqrIeiB6K9TrcJjtY0gTurnvLcPlo2+IvBfAAkDIIzDsouvR8+zO6oKg0N7MQVGXkNAhdZmFrLBfM5nEGu396Pp76hB4bnwl418VQcnM87MNRrH953N9/LLtCz1PRkBkXyQBcuUUJ2U56DCqJ15etfsjIYMEC4CkTo3a+M/dz6KoLAG7wFSr13qryLvlCX3foAfaM0+EfBcAYsfA2Z3f7c4XjLHTvlp2wHmvG15NREdNx6se6lsZaeiIQtMzYweDhS7QQvs6r657B+cPvjLS7cpZACQjOvfH1c3bw03QRWOrElRMeKFuxmptQM5Vuk66GXPejG5NwcgLQF15919513zUFRThL1TBScNgAQgzrpuWV35MlVV7Zd3bOPe2rn6b5BuRF4Dhl/wdV5x1kdriKkxJxzDuqnp/THghD4DSniyXioo6aDWiB1Z+8JbfZvlCpAWgRkEx1ty7EAVGAVJmDEVSAOyEjplnwgttE1IdRxIA2p6c997LuHzsjZEMEY60APRudTlubXM1TFVk0lDbV0YEtvmijm7QJNT2ruGYSJopHNunPT75JnolxCIrAKZ0+d8ePx+NapbowB5XuoWWgbib+0AVxl+2b02qIGaVHXn3c//E/80c7bdpu53ICsBfmzXH9F7jEM8Uy6DEVOn6x1XAu9/WMblEJTupbQAhz7mJmAN8k96KI64+PXLlwyIrAI/2HoPTmp0KSlalEHFHTggtFcOa82RVxmeS8oovsNUCAMpMU5UPo2Iv3R8YqOogRIlICkDDPeph+V3zsAd1k6CCHxQxYuruvYbxv407mHCRlB8JW4cl23EdImw6abyweiXajrrOb/N2K5EUgBvO64a+bbujGrn7UgAMS0epmZl+nbwLEGpSqr0YPTOQjlFos6tKkpdaKRx5bUt8tyU6HYcjJwCk+q+NmYPG9RoiQZtAJAKmzvenKYChG/r5bSaTQ2zhwHJ1eDct/KraD7Yt7wUC//fYJEx6ZprfJu42IicAB+29P1aMeFTO+4UO+ZWPVPBTJbnBK3jBAhBqHPlPlQrzipGqasS27iXw1vqPcPrATpFZCI6cAPTv2BP9WmZCP3XWm5Mt8+E19OQ4gFDjSg/ANHSit+N1aja94kXU5PmP152FrzdHo6NQpARAuf93zsGhdffLRn3pRT9CL/xFId2X2Y4rdDmzTOInnfybZt+BBxbM9tu03UKkBODgvQ/A6yNnw1TNtj2ELvdpqDLShrogLJ4ChJpsu3Zv0GfqPBAUCbroPy+jwx29IzEVjJQA9GpzBYa06iHnerpAlapTp0rGeiWsDN1SM+Bl/5kqsr3AK7L1AbIeoXyyTZSi8ZVnYmu6zG9Tc06EBMDAikHTccDBR6DAb1OYQEOhwW3HXY8X3lnmtyk5JzICULOwOj67b6m8w5uwAt7ai/EXqio8YdEs3DpzrN+m5JzICEDbk1ti6hWD5cm1AtfOmwkWJADvb1yLk/p1CP2KcGQEYOzlN+Oy09oipop7sgAwvwxVFbaNNPa/6gxs2RbuXoKREABa2Png3gWoU1yCuGNWufssE27U1qBIo9WdvbHs3RV+m5NTIiEADfYowX8nPoM0YkiwADCVoOoFOA5GzH8Iox6/x29zckokBKDrme0wrnM/bJXvtEhYqtElw/wSru3KaaKL+auW4yKKBwgxkRCA0Zf2wZVnXKgKQcZdkwWAqRDXcWGaBr6zf0TjbqdBhLhvQCQE4N8jHkPjho0hbOneWfDiwBnm57FVNCitBdg4ste5WL/5K79NyhmhF4BEPIGNU15Rrb4NKgNpsAAwFZOpGSjgoOt9A/Hka+FtHBJ6AWhy0OFYOughxKjhl9fnjgWAqQgSAEoOpDzRUfOmYOSc+/02KWeEXgCuatUZw9r3hiW8Wn/G9l71DPNzUBwA5QdQitD0V+ah15ShfpuUM0IvABMu74/Op7dTBSBUnj/n+zKVQAJAJeIoZezdjR+ieb8LQ3vNhFsA5DtbNmQ6Dtv/cJhCV4CJRoNvpkpIAUjJS4Q6RJYhiUaXnYQ0HL+tygmhFgALJr5/eLlU8wJV8FN1g6EoLw4FZipA2AJpSwuACxuHXNMC35SGs1BoqAWgfp09sWb0U3BiBarwo0P1P10WAKZiXCkAdoyaxNBagIMzhnTBvz9632+zckKoBeDMQ4/DjJvvBm0Axh0XwjJ0OjAvAjIVYqtq0bRYnJRTxgEP3Y4pS5/w26icEGoB6N2qCwZ16Klm/DGv8D+1AefeP0zF2PBqRCMpXNz55P0YOfcBv43KCaEWgCEdr0fPlpeoBpAx2tg1qQCk5bdZTMAR8EJGJWn52eQFMzBg9p1+m5UTQi0A9/cYivYn/FXd9anuO2UBCr7/M5UghA4DopqB5DjOXbkQXScN8NusnBBqAVhwy4M4/oCmcMyMB8BDn6kcEgC1YUzNQywXr3/8Nlrc1s1vs3JCeAVAvqvV9z2P+kV11Oo/CQCV/WYJYCrDgasiR5UjYLr45LvPcfQNrUMZPhJaAaClvg0PLUehSMCRrr/lCg4DZn4VthQAKh1HLSMM6QEkkUK9S49nAcgnqhVUw6cPLEXcIQGgtm/CiwRkmIqxpfsYo8hRh7xGoMwowz5dT4bt2H6btssJrQDUr1EH7979LAocqgAU0/3/MpGABgcCMRWQEkgl9K0iLsd8ykyicffTsSW11W/LdjmhFYCDGx6AF2+fiSIp4aYR97rB6E5AHAnIVEhaDnqKAxZUQQpImmkc3rMFvv0pfOHAoRWAgxrsj5dGzdYCYMa2F3VypABYLABMBdiuCgUmYo70BkwXJ/bviDUb1vpt2S4ntAJw/p/PwuSew+RcLgZLDvhMDqDg9t9MJQh5k3ApeYSWkuVzx3DRddJAzHvjOb9N2+WEVgD6XdAdA1r3gFBpwMh2euWhz1SOq5J/DUFrRlRGzsWIuQ9g1JPhCwcOrQDcfH4P3Nz6KjnuvZqu5e76LAJMRVAgkOvtGJkUCyCfjZh7H0bODV9psNAKAMMwlcMCwDARhgWAYSIMCwDDRBgWAIaJMCwADBNhWAAYJsKwADBMhGEBYJgIwwLAMBGGBYBhIgwLAMNEGBYAhokwLAAME2FYABgmwrAAMEyEYQFgmAjDAsAwEYYFgGEiDAsAw0QYFgCGiTAsAAwTYVgAGCbCsAAwTIRhAWCYCMMCwDARhgWAYSIMCwDDRBgWAIaJMP8PRRzX07YcZyMAAAAASUVORK5CYII=\" style=\"border:0px solid black; height:256px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; width:256px\" vspace=\"0\" /></p>\n', null, null, '1', null);
INSERT INTO `trx_ordermethods` VALUES ('2', '2', '<p>Cara pemesanan Hotel di perusahaan kami sebagai berikut:</p><ul><li>Order via online</li><li>Bayar via ATM</li></ul>', null, null, '1', null);

-- ----------------------------
-- Table structure for trx_orders
-- ----------------------------
DROP TABLE IF EXISTS `trx_orders`;
CREATE TABLE `trx_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_branch_id` int(11) DEFAULT NULL,
  `order_date_start` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `order_date_end` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `order_customer_name` varchar(255) DEFAULT NULL,
  `order_pic_user_id` int(11) DEFAULT NULL,
  `order_status` int(11) DEFAULT NULL COMMENT '0.Cancel, 1.Order, 2.Bill, 3.Closed',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_orders
-- ----------------------------
INSERT INTO `trx_orders` VALUES ('79', '1', '2017-08-21 11:54:05', '2017-08-24 17:31:52', '', '3', '2');
INSERT INTO `trx_orders` VALUES ('80', '1', '2017-08-22 14:58:44', null, '', '3', '1');
INSERT INTO `trx_orders` VALUES ('81', '1', '2017-08-23 17:30:21', '2017-08-24 17:30:41', null, '3', '2');
INSERT INTO `trx_orders` VALUES ('82', '1', '2017-08-24 17:30:21', '2017-08-24 17:30:21', null, '3', '1');
INSERT INTO `trx_orders` VALUES ('83', '1', '2017-07-20 17:31:31', '2018-04-18 10:17:47', null, '3', '2');
INSERT INTO `trx_orders` VALUES ('84', '1', '2017-08-19 17:31:32', '2017-08-24 17:32:00', null, '3', '1');
INSERT INTO `trx_orders` VALUES ('85', '43', '2018-04-05 16:08:00', '2018-04-11 16:10:40', null, '3', '2');
INSERT INTO `trx_orders` VALUES ('86', '43', '2018-04-08 16:07:35', '2018-04-11 16:33:32', null, '3', '2');
INSERT INTO `trx_orders` VALUES ('87', '1', '2018-07-01 09:53:34', '2018-07-01 09:53:34', 'Rexa Akbar Malik', '0', '1');

-- ----------------------------
-- Table structure for trx_order_items
-- ----------------------------
DROP TABLE IF EXISTS `trx_order_items`;
CREATE TABLE `trx_order_items` (
  `orderitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `orderitem_order_id` int(11) DEFAULT NULL,
  `orderitem_package_id` int(11) DEFAULT NULL,
  `orderitem_productitem_id` int(11) DEFAULT NULL,
  `orderitem_quantity` int(11) DEFAULT NULL,
  `orderitem_quantity_takeaway` int(11) DEFAULT NULL,
  `orderitem_note` varchar(960) DEFAULT NULL,
  `orderitem_normal_price` decimal(18,2) DEFAULT NULL,
  `orderitem_discount_percentage` decimal(18,2) DEFAULT NULL,
  `orderitem_discount_total` decimal(18,2) DEFAULT NULL,
  `orderitem_grand_total` decimal(18,2) DEFAULT NULL,
  `orderitem_cooking_date_start` datetime DEFAULT NULL,
  `orderitem_cooking_date_end` datetime DEFAULT NULL,
  `orderitem_status` int(11) DEFAULT NULL COMMENT '0.Cancel, 1.Order, 2.Cooking, 3.Delivered',
  `orderitem_itemcooked` int(11) DEFAULT '0',
  `orderitem_itemserved` int(11) DEFAULT '0',
  PRIMARY KEY (`orderitem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_order_items
-- ----------------------------
INSERT INTO `trx_order_items` VALUES ('177', '79', '0', '1', '2', '0', '', '15000.00', '0.00', '0.00', '30000.00', '2017-08-21 15:16:04', '2017-08-21 15:16:09', '3', '2', '2');
INSERT INTO `trx_order_items` VALUES ('178', '80', '0', '6', '2', '0', '', '20000.00', '0.00', '0.00', '40000.00', null, null, '1', '0', '0');
INSERT INTO `trx_order_items` VALUES ('179', '87', '0', '2', '5', '0', null, '25000.00', '0.00', '0.00', '125000.00', '2018-07-01 09:53:34', '2018-07-01 09:53:34', '1', '0', '0');
INSERT INTO `trx_order_items` VALUES ('180', '87', '0', '3', '10', '0', null, '20000.00', '0.00', '0.00', '200000.00', '2018-07-01 09:53:34', '2018-07-01 09:53:34', '1', '0', '0');

-- ----------------------------
-- Table structure for trx_order_offtables
-- ----------------------------
DROP TABLE IF EXISTS `trx_order_offtables`;
CREATE TABLE `trx_order_offtables` (
  `orderofftable_id` int(11) NOT NULL AUTO_INCREMENT,
  `orderofftable_order_id` int(11) DEFAULT NULL,
  `orderofftable_name` varchar(255) DEFAULT NULL,
  `orderofftable_address` text,
  `orderofftable_email` varchar(255) DEFAULT NULL,
  `orderofftable_phone` varchar(255) DEFAULT NULL,
  `orderofftable_latitude` varchar(255) DEFAULT NULL,
  `orderofftable_longtitude` varchar(255) DEFAULT NULL,
  `orderofftable_description` varchar(255) DEFAULT NULL,
  `orderofftable_created_at` timestamp NULL DEFAULT NULL,
  `orderofftable_updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `orderofftable_deleted_at` timestamp NULL DEFAULT NULL,
  `orderofftable_status` int(11) DEFAULT NULL,
  `orderofftable_log_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`orderofftable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trx_order_offtables
-- ----------------------------
INSERT INTO `trx_order_offtables` VALUES ('1', '87', 'Rexa Akbar Malik', 'Jalan jamika gang siti mariah no 416/86 004/002', 'akbarrexa@gmail.com', '083821857496', '0', '0', 'Tidak ada', '2018-07-01 09:53:34', null, null, '1', null);

-- ----------------------------
-- Table structure for trx_order_payments
-- ----------------------------
DROP TABLE IF EXISTS `trx_order_payments`;
CREATE TABLE `trx_order_payments` (
  `orderpayment_id` int(11) NOT NULL AUTO_INCREMENT,
  `orderpayment_code` varchar(255) DEFAULT NULL,
  `orderpayment_order_id` int(11) DEFAULT NULL,
  `orderpayment_total_price` decimal(18,2) DEFAULT NULL,
  `orderpayment_voucher_id` int(11) DEFAULT NULL,
  `orderpayment_voucher_paid` int(11) DEFAULT NULL,
  `orderpayment_discount` decimal(18,2) DEFAULT NULL,
  `orderpayment_tax_percentage` int(11) DEFAULT NULL,
  `orderpayment_tax` decimal(18,2) DEFAULT NULL,
  `orderpayment_service_charge` decimal(18,2) DEFAULT NULL,
  `orderpayment_additional_charge` decimal(18,2) DEFAULT NULL,
  `orderpayment_rounding` varchar(255) DEFAULT NULL,
  `orderpayment_grand_total` decimal(18,2) DEFAULT NULL,
  `orderpayment_method` int(11) DEFAULT NULL COMMENT '1.Cash, 2.Debit, 3.Credit',
  `orderpayment_number_card` varchar(32) DEFAULT NULL,
  `orderpayment_number_verification` varchar(32) DEFAULT NULL,
  `orderpayment_paid` decimal(18,2) DEFAULT NULL,
  `orderpayment_return_cash` decimal(18,2) DEFAULT NULL,
  `orderpayment_create_user_id` int(11) DEFAULT NULL,
  `orderpayment_create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `orderpayment_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`orderpayment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_order_payments
-- ----------------------------
INSERT INTO `trx_order_payments` VALUES ('2', 'PO.01.0817.79', '79', '30000.00', null, null, '0.00', null, null, null, null, null, '30000.00', null, null, null, null, null, null, null, '0');
INSERT INTO `trx_order_payments` VALUES ('3', 'PO.01.0817.80', '80', '30000.00', null, null, null, null, null, null, null, null, '30000.00', null, null, null, null, null, null, '2017-08-25 11:37:24', null);
INSERT INTO `trx_order_payments` VALUES ('4', 'PO.01.0817.81', '81', '30000.00', null, null, null, null, null, null, null, null, '30000.00', null, null, null, null, null, null, '2017-08-25 11:37:26', null);
INSERT INTO `trx_order_payments` VALUES ('5', 'PO.01.0817.82', '82', '30000.00', null, null, null, null, null, null, null, null, '30000.00', null, null, null, null, null, null, '2017-08-25 11:37:27', null);
INSERT INTO `trx_order_payments` VALUES ('6', 'PO.01.0817.83', '83', '30000.00', null, null, null, null, null, null, null, null, '30000.00', null, null, null, null, null, null, '2017-08-25 11:37:28', null);
INSERT INTO `trx_order_payments` VALUES ('7', 'PO.01.0817.84', '84', '30000.00', null, null, null, null, null, null, null, null, '30000.00', null, null, null, null, null, null, '2017-08-25 11:37:29', null);
INSERT INTO `trx_order_payments` VALUES ('8', 'PO.01.0817.85', '85', '30000.00', null, null, null, null, null, null, null, null, '30000.00', null, null, null, null, null, null, '2017-08-25 11:37:30', null);
INSERT INTO `trx_order_payments` VALUES ('9', 'PO.01.0817.86', '86', '30000.00', null, null, null, null, null, null, null, null, '30000.00', null, null, null, null, null, null, '2017-08-25 11:37:34', null);

-- ----------------------------
-- Table structure for trx_order_tables
-- ----------------------------
DROP TABLE IF EXISTS `trx_order_tables`;
CREATE TABLE `trx_order_tables` (
  `ordertable_id` int(11) NOT NULL AUTO_INCREMENT,
  `ordertable_order_id` int(11) DEFAULT NULL,
  `ordertable_branchtable_id` int(11) DEFAULT NULL,
  `ordertable_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`ordertable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_order_tables
-- ----------------------------
INSERT INTO `trx_order_tables` VALUES ('94', '79', '1', '1');
INSERT INTO `trx_order_tables` VALUES ('95', '80', '10', '1');

-- ----------------------------
-- Table structure for trx_profiles
-- ----------------------------
DROP TABLE IF EXISTS `trx_profiles`;
CREATE TABLE `trx_profiles` (
  `profile_agen_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_agen_content` longtext,
  `profile_agen_create_user_id` int(11) DEFAULT NULL,
  `profile_agen_create_date` datetime DEFAULT NULL,
  `profile_agen_status` int(11) DEFAULT NULL,
  `profile_agen_log_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`profile_agen_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_profiles
-- ----------------------------
INSERT INTO `trx_profiles` VALUES ('1', '<p>Tessss.... tes tes</p>\n\n<p><img alt=\"\" border=\"0\" hspace=\"0\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/2wBDAAUEBAQEBAUEBAUHBQQFBwkGBQUGCQoICAkICAoNCgsLCwsKDQwMDA0MDAwPDxERDw8XFhYWFxkZGRkZGRkZGRn/2wBDAQYGBgoJChMNDRMWEQ4RFhkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRn/wgARCAGyAooDAREAAhEBAxEB/8QAHAAAAQUBAQEAAAAAAAAAAAAAAAECAwQFBgcI/8QAGgEAAwEBAQEAAAAAAAAAAAAAAAECAwQFBv/aAAwDAQACEAMQAAABoe151HbO3nd3LSOpa5itTRVuLWSrpFTSVTbSgqUHdyubO42IhZpyb0TDUUVKGpY1V0lyLMXZh19cs3aK1JBAAIAAAAAAAMBAAAAAQBAAAAMEADAQQMAEDAAAEDBAAACAA1VT59mSrO2e9pFe4kl2YuGpY08depeiaamlxsa0AgR0kC5jpG0Bay0llqmrUdywI2NacnLLAg0zoazBUgAAIAAAAAACAAAAADQQAmMAEAAAIAAAAAAAAAMAAAQAADQAVDl6Oc8rtjTcHoP0XjgIyWW9CAxqOk+W8YhlJoNaUaCByw2saKVNrSjfI1k8UicdTLLkl19JqaSqGNIAAAACDBSZ6R6ZgwEAAAABAAAAAEwASADBAwAAAAAAAAAYJAABiM7k6Mbg6qOOioRnon0fjqJAcmo2NAkABQsRSpuRU0hGAAwGiAVMBQQAUib5cNJwRtNBAAAAAAAAAJqXm6IevlABA0AAAGgAAAgAYgBAAAAAABggAAAAAYCAAAA2WybzOPoz+bbP59fRfo/GdLvY7avL0SIzujDM6ueJiAoTxV3HW/hpR3zp65sacmqGUoaljJJYDGMaVEqcTTWAAICgAAgCAQMATj5Oh/ZzAAAIAAACAAAAAADAEAAIAAAAAAAAAMBAAAABggAbZcWd9blpby0elU1ycMRBoreWkNxX0ixndvLSRFbTNrTKVTXMCnrEbTAZQIAAs51BcxsAAAAAenazuvcNCVOvUoyTHReTpTv42tTxde4aCCBoAIGAAAADBAAwEEJgAAAAAAwQNBAAwAAAAABPuvN72E1tc6PRhXuLOd2s9JJbKlVSoa1BpnQ3zictY1kstAipMYAgA1B0jGASS2hNNWstLMW5KWaVVZzuG4yOrnipLxdUWNu9TgAQK1yjEAAQCQAAYAAAAAAACDAAEAAAAAAAICgDQAAAGp5fD1er4bU9sc3p56G+UdKfOlBycFxDaVACAo1QoRMRp6bGkBABgAAAACJoq5jpr8nUrBO7hrLLjaZaq6RQ3xxeLpXLST3PIgtNYgkGCABiAQCMEAAAwAAAAAEAAATBAwAAAAEAAAAHzvl92Vx9Hs/p8WJ2ccFoYACE3osZ6NaguWtAKhjHA0FBAYwABAAAAQFATAkl3cdNXk6r2GyocEs0go6VVnC8m88Xue35Ob188VJAQAAaCABgAACAQNAUEAAAAABAABghMAAAABAAAq464XndmbzbemfQ+MxyjEABAUJYpQiqWsAQEBAAAAQAAAEAGAAAAAqJ870ebp0OXdQemoSJyIh0jI8/s44LUVtd3NW9nyoqQCAAAAgAAAAwAAAT5qWWDgqEYgAAAIAAAAgGAAgAAVsteX8jvp5aet+95Fe4kmoqmKpRiCQAAYJAQAAEGACQAAAYCACBgAAT53qcnSs06KZUyzUssZFrlDS5vwfZblpq51W2yk+r+ehuAaCAABgAAAAASxUiq5lpvcfTZi2BQ1jne/ir3DQBAwSDATKoU0y1q+b0HPWh7nE7pyEYfB253Lvmc2vuvqcb5djO1Crcx1OX082f0YMaemqEYBE5BoAwAQgAgYxEAAAAKOSG+au83RNnaNOTj0zh0irtlax2i+a+g5q8tiNNXHSl9d81V3yAEDBAwQgKNBA5ZfU+Z36OO08UAgRtZu+WN18tHbKtpKCAAanned14XD1T4OPYjyu1nPQ+750vbjX59qnLtic/RWyv6Wmqty1NwomIFPbLB7+JBgR1NXXKKpiqWsEDEAABAAAAQABMBQVDk3JvlvmpIuO5rbZNabydOP4HssasD28NnfTeBHvkxra4ut81DcMah0maHPF4PdxzZ3o82+7x9drK1Q1gwClrHLep51a4RgCABHFYHl92HxdUkoHczBK1tGr6/JgeN3bLyze1Y/Jv9QBAyBoBGNahuczq56W+NTXKKphuQIbhgDEEAAgCBiADAEDBAAADk1BonKlTbSnw2b5vdicHY9OSXamtL3PJZ1c1rLa7hray1mzuSWoOZT2y530OGGpt47a/F1XMtI6VXXODXNwSxWN2cud0YoAACDAbLq8+3H+L6cQXc1rdmFjaIJuPzuir055mOj5PpPUjFBQCbSg0mtrnQ6OfJ7OVjmKkgkBAAGICAAAgAAAIAACgIAQAAAAFGJ3/ADvQw/H9GII06rNfO7/o8TvU8+t0c93Dbc8/vsZXLNKChHSzenHH7OO3y9FnHaxFDK+2VffCKpp7ZQXLWIAAAAACBV59059Ux2t1OPx1Q5em7zrD69ISWtfRmgxqG5h0ipviylDpnay2i0yxezjhuAbRIAACMQQNBAIAAAAAAAACAADABA3Y6ZnzX0VGVNGk0U4L0lDoy2fd8iDu4dDm6dHj6pJplzYzufO0ZR2yz+nnXPRc6lVOCLbHP6+avcI0jYCAAIUb5bWmsdLGUuXfH8H08jVavG4uhZewiGQ/oTZQa5waxNFKKtpFiLlTh0jJ7ON83IivcRXNLbBoMaRpAAQQACDBAAAAAwAAASDUJeHttfJfSYW2MVIHcmtTOmzVX0eJPqPnkFe5elRsuLmG1vDZU4bnG7+KTO5cNUuXNVOjGK4a0DQAABAAN8tlAJU0ZWw1xvF9Kz0YP2zw+broY3HLjD3zozfStRU0uO5huZIqWKkRdlzoGU9JQHofNV7jI6uflPV8yNyMQAAAAABCAAAAAMEAM8f1meH6+dUgoKW9hu0LeZP040Pqvnm7YvmmXKyTRcsW1oTg0gATj0zBgkYgAAIAAAMAQMQAETJcfPtX5taNVzfldz5YlBZ7t2ZOByL+dyggQaRNFWM6tS7aI2oxyIGQNVqUNLlvV8vG7ORGAwSAAAAAIGINUkGrBCBT+a+gocnXp6Zabnmp06fi6reVc53czs3d+q+ei6uZBKNAUBCMAEwEaAAAAQAAAAAaAoIBnoaQBzXl+hq9XNBhtkeB3w9arRU81W3zvZnq/bnEFiXbmtXOrCK9KINnKngoRhKDgjZUpUqmC5wfS87G7eNGAAIAAAAAACAIGACAKfz3vT8PZ2t5Y48C1oed3Xc3uejw8bGsnveLD6PAAAADQQMEADABAIAMEAg1BAAABgU/I9S36vm1MNea8L06edyuormXWaWd2ELTzhez9WLgml24vWzetFMazaWrFWkQtQA8HgoV2VqnM6ceT9nx2VKAMQAAQMBABAwAAAABFP5v6C3ydXSXnck4iy9wdujlWl6PDgq4PpPAg6uUAGACBoAAIAGAAAAgAAAAwQMGCr+N6tn0eLkvM7cTj6kDptJop4o2B1uufOZa0he0dGcqJk5k9PN6kOFpB2kSor0QAoSggoKWJ1YQXNDv4KXRzqCCRgAAACAAIAABjQMSofM/RwcnXc2wrhjRp13H024c3dyCed9N8/F08yAAAjBAAAAwQAMRAAAMAAGglGAgUfE9fT6cKXZycj4fsXrn0noyoY7eZ5V0lT1rOHiug3x6nPV6K1JydyHfh3EKETGCo0oWSA8GUmVNLpwp9XLBvzw6ZoxBADBACAMEAAAAAAPm8z5X6bJy1nlyp12tzn22+euX7sXudP6n5uPp50YgkAaAVNAAGAIAkDAABgAXefps4b1tsYriOXkeB7WkCfSeDmeT6lfHT0DSaud0bm3vlRislPm8dfY4cFTn2ogenYl25YKvRA1E01itI5Vk6d2WbY43pecypfF2sN6+2NXo5lTVUyoQAAAJ8ttPi7oNcczt4bfP0wfKfS5GekTUVJE7cm1hpdyvK2iX6/5h++MVxDpmIGAAAICggCAYEkXHcIEuetvm6rWGz4sar6Z5uO3Heb27vPrrfReJLy9fCZP0veEucXr5Oc59uS87umpVor3bSZ0U6VelGDAQQCNMYolYlSrTgsp2gb0c9Xp5oribPSxhvZw3o9fJDrm6XFpmyobUzZbSRdrn6WOaPZxNqdvzPU5/xfUx+fpz9JnkQOjwq9jtibToSm/a/JM6OdAAAQAStgCAAAblT4uTPSDbFjmXPVGnRdrn6JIvP7eKOpj+M+pp3Gx9L4r8dvLOHq9P9DiodHP594/qVE5GWKmnlftW0AoWRtNZEJWAI0rTmlae0xyVMu+K6ZPBKTXLLm1z9CMly0p9XK1yjLfP0zY6zZax3FPo54dsI9M7fL1Kxvz3tz+T6OLrFqXRubyfQ8umY6i3zofefGDkAABBKMAAAABAVMBVQDaSCBiHJz562MtbmG+L837eZpl0vq8cPVz+Red2+g+n52a1ynn9sEXEhg0D3TaJEkCNlG54jHS8n1fRjODgnQ2pbpnU7eFtpXKVKNAPjSfHafDdU6/Rzwb88dw6asYdFnHavrjFpnHpDKhAdNMqZPmPo9DxvTxNs7Wd0dJstdLzaOiqXXhifdfHAkGCUAAYAAAACADAQAAJM7lz2juN3y/Ruc/QoRM8+4OvoOe9f0uWhc+SS+k6OfsO7k5Hj6+L5dmjaDw+jgkQBIEVT5HNdWq6fXJwRtJSoAnVz0u7gSpki4dcUaBA5sd0aRpl5qU+KucvWqdHr42VKVKOUBVVni7rGWlHxvUueX34u2dmap1NgOm5dVap9WOL9z8cAAADAUJs9NHk7I6jP6uVrTWkEDQAHTeh53oqqgc2cdnxbQY1zXk93Vc+i9mGZvHnTXWQ3UefXFQEABQ9wyveqdAVejOa8kmu+VdHrlBU1NYupeScvR6brJ6XnSZ6KGb3+fFcqhmG79clqWtSRpLnos06ai1yg35xOSbmy1r7Yvw30vN9IDJ8j0bHF04+0SJ07myjoefS9nWd28+V9v8AIAAgap2MtZY01eHtsYbW02Usjr5a2meb18TaSAIWbAAFVPzPRuc3RO1nep558r9AwWdrOJrGTrnqZaZ9zm6ZoCoGNDrcdZA0A3KnlKVOXZZ6btnNU0Nc47jzfz+31D0uCt3+e2pQOX4e/oCbXN0wzTNcpunnz1vd9DyQAABk2Wt/j7Zefony0raZxNTRVbScnxfUsce9DVQtU6nRzvcwtQg68KX2vyAIHPnrqcPfrcXWxpjUYXJdqKQdalg9/Fk9/nIwB0trTpsT4z5n6HreXbWmuZ+n8HS+Y97ntZx982iyNs4UoqQCgA9DA6XPUBzQE8tyKdrrKnY3yj6MMPK8Dj6+197w7fRzoDPM9Di/M9P0f0eE1xWlXuDDazNc/UXvU80ATAucHoXOTrSXamnp098Y5qtU0fE9Sfl6MrSFClc7MPYx0qg3q56X2vyIxZrX8v1rnPtLLcEyb0KnBSll0dJqaTj+t5Doqnx9lXzu671YVOHq5Nu/jfc8XRqi53qybjfDdmVZywebrm1FapQFB4TQSJbhqxNrVK4jHtZWBIm4JKmgEVK91c3c+v5C8HdBOnH+d3dl9D4M2uQGThvneb6PVdfLqc2+B1c2ptlZz0o65rjtHz9DlU7nRw2mioGqGsZXi+po8e+FvnMqzbjZzrYxuVFPt56H2XygE3N1O5um5jrbzuxNXYoCIVS1WpRXKtMuGhjeR32+ToxNp0szmeiK1x2nHvsDsyZ7flvfzwNQizdYQUTTQUHoUGM6bK8uiw06ag1z18Na1J6FTg1h8uzFIzT1y17jnMtKc11Hr+X0HbxSZ3jcnXU59+g9Tzdby/Tj4+7Q2wupc763lVts0l2MOhai1hvR6eattisXV+T+mu8m+LtEyeZpOzlW3hciKXbzu+i8asyqJE5s9LWdvmpU1FX1zg6edwT8+5Sg1yh6ebJ+c96ljqiBVz/RlSqOw4erazpLWk58168oaiAMTfJyGtRhIhtE8EVGvFwUrKU81C08efc3Zc009rN6ctbl2agCdEyYJNo7LfHZT8+y0Qfp3TzWcNqUVmM6Wpe0o2VFrLSZOFrK3yr65yS+R8X1rXNtm6wrM2408tNrGrydfqwr/SeHWmohtztzUs08JWl6OdnTyo5dzdSbYpUNqanP0s+Y+gv8ur5pQ4nuxztM/Q/N69sVpENrx70edjjN0nL1zEAOByFCRNod1hpibTYSzSobzaUIstWJqhc2kE0jJkAUujLR59XzUyJ5IKVS16BrHYJU5q21I1ZkUcqAECBlS0go6XGeR6T4uncsZQuNrDTbyqtNN7+RfqvmkTQcPB3WN8LGes2el7PSG4zerlXTOhlc2ua6ZlTzPz30G9w9FnHVmdAcz054PTj6D5nZvubKJ6nxjvyjrONmXrnnXCA8EBjFAR2OVrNw65avL0ch18/Xc23KdGV+Kp0qtzbQJspApFTwcnYQ1OJprNLOpQ9M3yYFVG2K8NBCbGoKVSlIhGNDD8T1GTVG5RlDSN7n02cLgoqejxn1nzBz9FiLw/D9npe3lnGzK9AMzfPOlu6+TIx0sMhzp3Tjg+J6+7hehnUcU2a5vpjmurn9F8ru6Bw5q8HlHdjSqI2s3WcTXIYAAgKCB2krU5urmujm6bDfl9sehw6Od0i+TZz05jox6fK6VLI1zkmlaAcDBzCRNtK5Fzy71x13Rj0eOkOkSRTLiNjGQ1KjsQ2tSS8TxPUdNVrTBZWs73Ppu43Cih6fFY97x87zfQv9GXOeV6O1181rq587j6bvo8DOrly/H9bR9vxcbz++p53o0tIhuZOfXpOfTYzqpnYnzu8ch283pfj+hb1z5Toz6bN8rvFOooaRTtYm2YAACAAgdLJ0PJ18p18nWcvTzXRzdXz9FGpyNc97LXHuJxWYvJsLz08tKlKlpFG5uw67HBGxQtS+sud6Xnp9MKDWItc2NNY9NyFC7neL4vpxpq1EzK0newvVyrnu3GbTJvpcEHD2ydGFTk6rNSmudXHW3UwhFjrLrm0Ic9LGuVYeOlvY67eN4+k106zON7ubsfO7dGs93N41Ou1n0nVOLtHN9PPExAAQAAOozOn5+nj+jn6vn6OZ2z2cqYrwOnl6Pm6qN5z1lqRrE1n65TZbMHCFLSLRN2Lq1OfRA08LSfq8OCLuy3ir9nLmdnO5p0aTxViHdy04zy/RrXAyTO8ffLbw1qbZztVQstOzvN3ytTTYplysutcqF3DXK6Mo2tXDWlcYfRl0GF72F6EXJnVObqD4L1OHv+LeZaef9WWtWXW8m+BRrSY1PA6+TI1i7NYmkNYoKG/gX8tYry346MayWs9fHbkOrm3sOhqMPo597m3hbw98ehx0cDAjqYrm7FKNAhCOpuB6NDZLnTuy6vdxZ3qebXy6Ks6avNvrYaVIrieHqx9c3p9Dz9GD0Z9VzaUN8+X6Muq5d+e3ztSSqub6Meq5OjJ2ijplu83RzPThu4aNdc508/V8fRS0XLdfN1GF2c718Nxzy202JXQZnURXHPXk+3lq64+yeX159GDrM+kea9nNo53mXNZoYA1GzzAO1F9Aa1wo7Y62O/LdPP0+GqK8ffl67Das3l3nE3ai640CrrlflqqcEbTQ2ZrsBNl3EW0Rd3Fm+hwR57VaSp4fB2YeOudk3u5pcslPZ9xwdXP9OWH0Yd35/X556XJ2/DvSt8t183pvk93n3p8mllXQcvR5/wClxd353XT0XE9/H6N5PoYPXz8n14dZza2Yqfn6d6c+O0vRePawEVmU+Q3k0z73j1kpZOs+b9WXPb4AIDmANQBt8ziZoY1NT6Za8z083Q8/TxvRn0OZoZa8T1Y9JjaKqOkOU350mQ4nP0lWrSay4Rx1OnN61TuZ1qImRH04UezmVBplmOOB4e7Ly0lm7ca0NMp4Vla+j+b24fVjyfXz9/5nZwPqcfVcXREzkuzn9E83s5Dt5gOh5Ojje7l6nl3aPkO3l7bz+uG54Pv5uxwqSXrRouV4VzqCkBAqhGyIJQcjlenLmernEANYoIAAGvytGo29DJ6buGst+Onm9JsuNzLXmNY2Vnaz3ptYm2WxlqwcgVtMkqbEtw5E2BuTelBE1vIjl6HXzv0mzFVKjzYnBx1yKdrOnOppIiNLPX0Lh7Oa6s+Z6ufvvN7OB9Hk6rl1rl8t2c3ovm9fEd/PMp6Lj6uH9Dk6zk3iK4/u4+88zux+nLlevl7zl16Pm1vzeGLQSe1IMEjecONU9zoueP3nz3u5RENJAAAAcjV5xyHDQLMjiuijfndIc1pY6Ze2dyVdy1qVMNTdHCLTLoE1lM9uxSYD8657pz3MqdmSsnTfU9RpO1pPIw8VEIYkDKqJkkliBHXofn9uN1Z811Yei+V2+Yevwd35/Xnarlevl9M8j0OA9ThvZ30PH08H6XD2HD0xVXE9/F6N5PoY3Vhx/Zz9vyadfy7bCrzjZSChY1CMiZXqbEWqdTSMvSMTo53gxlepEDLUmvl0SZ5bTOcJfL1VT6inGjHVbTPWx3irOe5zkSzaWaDmtk2poxFEiUrqzZDT5zfPbw0uQSsAynOoN9NtziozyZ0WprN0iaSaXZh+gcHoYvZz8/vHa+f18N6HJ1fH0VdJ5bqw7vzezme3nYLf5ejk+3m6Ll1r2c11c3Z8HZV0jjezm7LCuy4+joZOIx3p0V9Z5bu5drn1rNQbRf5d9LK8Hrx5zs5KOmc0ONtwoGkYAgehc3R0Sz8xvOaD0XPpzLw5VFmdc7eL2WnVBM44dy4e+Vv5vinMVKGk1Reyvad3m+UJwevPZ5r0x9PF8VWTLHBC3XubMpyET3MOjluvC1nMs1E47/z+/N78KgdBw9XnvpcnY8e1Cq5fs5fRPM7eE7+bRk3+Tp4f0OTq+TbnN5y9+ftvP7cjfLm+rn7rk16vi6Ouc8jjtk6pk3xfpceftjBU2pfQcfTu82/M9eOR0c9S5hqN3m6IbWLtg0HAxnoWG2defN6S9HeY783WWWy5LhasqvQ8NcfXPhtYlhej4706njHMSVfRTQpVXfTpOT53cZuy1eeupVb5Xnryr6GlFMpY2yswdXlpiVnnJUdVp56sUMHq5PsLutN2+Pp839Xh7Xz+pNFxHXzel+T38h6XGyX0nH08j6PFo4b8x0YR3F3LXO1yRnbce3S8fT3Sjkctq7MHaeL9Liq6RUqdXDfouXXqOHr839jz3CngoaxIqoaxMjexvl982i9g5erjN+bm9E1Pscr4XbPYmpAmRoI7HHbErHm9Fry+7jTJR5ltlPm4bTQB+sYb4+mXne+b1Tszuo03keU9GcbVlNoqrTR+xcu3kXTjXaAllq0g3Je1c2/J0YRWB04dBzb2ajI0nb5uiPbHNDTx2o74YGubQaAAEgdBz7dHy79nByLuOTH2nm+zkr0qdzKjtfN9HQzOJ9HjUV7K6lqvc09M7aPQuLr8v7uRgvU+Tr4Xq5KD06XC8bbLHqdGdJk9FNTPs8Ojl9ufLs6LOvSB+bJchtMsEbUTFF6Pjvla5c1rFKLWZ9Gy21kvGOnJwIEgIAj3rk6PEuvCskjLWZHRNLUXteG/n9zyeudWyzNXU7M1SqdmKrMxdMwImkABwASp6uWmzzb9VDwRxDoaTW0yqXOHtlamrcX0vH05nRi0ULM3SI2orho9rDXb5tXLXi+3gh1kK7Tk6eG6+OFmrJM0g9eLqzeqlQ1noYOvK8q1nOhvcjyt8/fT6OOtZpTVG4il2dMt7DfRT491SeQy3G1jXCgGhNZ7SJuqSLmmqN5vD2Ti6fO+nHIpFy8azTUQimTr0kEA0FBwlVNaenbz0uZ6dRx9GNtMTTWlTrXOF1cm7y9VqKFSNQ0qG+NDTFwWAqj2ebomzuF1hdvA1ty0va4SkMy1kJSk6a25Cbki8vWOzmrDPODpjrGrfM4U2W1S85bmabrVL2rDJAki5eb0dmMrbxw+mM+8ctNU7VOuTO5ntbHJ0auOmZrF5HLbY0bGS3tXmr8mlhrS2yny2Y1C0wKrcLTFcki6Z9JltBFz56SkZu2WBotvn63Bib8ujj0TPK0tWyq9Z4m2efplMnZGifQ8u+Rtnq8vXyvo+apSJunWfTmki49s4U52axNjG3To5HVRrz+mmfryOHbwvs/L4+U7q4fr1YzViq1J7UtKSKVVTjfV1wrbYVMNKjGA5EiuOoeKZVfH0fPpXpcrtnXaYACg9qxa086vOem4eu3LmADiejOjeec6hc6YdbjtVisLXLt+bXiujLmdsuhw9G3l7uJ0fLsOizGdh4wK4Nsaw4HKABdGsVXudHl6Mrr5kAEFIKSWu+ckVZtO6+bV6uaTl2XyO5l752mDx7uHu5e3h9Nyadnx8HkHp7Z2l9hw9XMdnPrh2BHmrdcdZrTZWTnl1ammDgcra4QHgrLsV0GO3K9HPADQAAe1KEacrOr5t9HK3joaTzO+GjF5VF6VZm7pNqlSVdNz6850Z4F5dXhvzG+WbcKVdlyVnlg0AHjYIBzJERBbRUoBAwQAOTWCKsp2ezm0+7htVnj+D7aDhpShdbEX89PUuTPzK8+c6NdbDbF3xGdNLqo53SagnhZGwIhSJxNQg4GgoAWQsqoGoBKEQAKDQUJA1IuZVZRE09iortWppAmABqqFzl3NWpmTrOUCQLKqo1XEAA4FBRyCQAFCFgACgCQbmNSsjt9fNt+p5z0snzPRTk642mUWweF3LX0Xl054VCnTeeFrNpNBQsrVNVpAeAxwIADQQAEABQAAQEBAEIDWKiQFBWNBARCBIyYJkxoRINU64aE1VqJCoCYGkCSLy7hAAQJAeVKSwbWkD//EADIQAAICAgEDAgQGAgIDAQEAAAECAAMEERIFEyEQFCAiIzEVJDAyM0A0QUJQBiVDNUT/2gAIAQEAAQUCLQmVsAOXhbtRrZzi3zvzu7j/AHOoNRtbeCI08TfrqD1MMIhXc46gX0suCxrOQ/6xvsz8YcqsL3a4TDAZsxpoz/U5QPOazjzm+M7kL7m5ygaM0DQH13C83DN+uwIbI/3/AOtyHKRmM38IYzc5am/Qag4wieZv4BNwmbnOczOZnMzkZuMYGnKcgJY/jc3+iy8f+kyrmqDi5k+IAmab4l+xTz2vBGvUfDqa9dw+fTlN/p+I5U/9IQDLEFirhAG7Fav1A8108mVK0hVdWKFh8eujODSlC04iPVynYaGphK6+RegiBdQjzAZ4MPqf1vMVXUf9MRyHariY6CBFhIEJ8h9lvmi0LO2sOOs7WiEgUCfaM0GjGYTmJ3DG8kRvhA8H4wpM9u3E1sIFJgr8eieH8snGanEw/wDS8YRCsO4H1A5MUznO7A6Gc1ndAj5AhuYzmxhZjNwPGm/i36/eagrBgxtxcYLNIIFbS0mdtVBGxdWFi1+GUVpWAaSROQjWf9IfA77QxrVWNeI1haCc5zM5GNy9NmbPpxg1Neu9/pgxGG11pvIrQQKJqcZxj1gx00MnwuP5U1nR/wClysk8+40bIWOdn4f9/eFVmvXfpv01uH9QeIljRAYJuBvXUPHVr9yzH3H8kgf9JeHavLRefBv0AQIW/rKjGV1genOcpucpyjPOrWVrOSCU5SoK+VqMD/QCM0Wl2jUusII/o3W9pLL7LT5jVgTjs9loa2A1/ZrA3y8bnKbgM3NxrdTZc34F/d9nUsxKMeyqmy/AybCS/wCotZadl4uFYxqwwi9njDXuWUDVizXx7E7te3dFHuBxHdJHrmVljetiCPjbi0hYE3DTsezWNhgyzFZIVI9NH01CB/R+08xZ4E2JuF9Q2wtuYp/MdUb8yvgYdgXFp1dL1CXfpqvI11+FUev3jR2jum2YfCSFHu62N1yvFyGVbH9xGLCVF7YoC+ncXdmnL926zswoIapxmvGpqWDw1e4MZYa1WcA0cf09wNqctzkBN7jb0fQu9ce7KvZa73NGK1ltTviZFx7l/bIgQmVYiasxU2cPc9i09pZKMR+WRQOOoi7icRK/tNzlOUJj/a0fN8Loti34Y5fYsXtL9uBbKgzAsrXWx7LBWLLCMZSWyMg8JuGGb9DDohkrWNaumtnchO5yh/peJ49Nzc+5tpsplparG27B0ZlDhbLRziKtSqVJW5BBeIH3B6a9Ld6tBnLwn3RjA0Z4XM5GBzGfYsK/HxGmprB8sftO6ZTSmTTXT7eP88+WiNktwUcj8k3C03NwtC8ayWspHHZOv7Yhr4G663If5tBlRXsrCtYvdq4tQtFrVypSSAJubm/Qy0eAkA1A0DQwxxC6iGw6P6JpXVeNQkyq1vustrqSy1MiX+CyMarAAOWvTcJhM3LORg5R1fa0DVtLa4H01/Z3xmV1C7Js+u8rxLrSmGk7dNeO2NzpDssCNxiWBQj7jWandgaAwywbikTfpynKM0ewn9Tj6W2Kk79qonJm5i6ZXBE4HTgcpuO5E5lpqFfG/IE0YVeNTaXXCcl8SezbXtLYyFYFJ/qopaWa7VRJbFIDVhVouRnq3xrux0tlLXUA+lbxrBOUSycpyjmHwQdzc5Tl42T+u6CCvJEqx7DccULdkWc58qksSJuDzFUTjNTtwDUE47i1zhDUI1MWlYaKjFVEj1Uk24dZVlKH+jYWWt7Oac8dZ7kKHybGFq2qDj+2oS4FmatqWGjN+qNotZud0zucoTFnL+iSBPuAAQ2UvKzLq4aBY1eSgedtofvsCC2Iymfeamm2F3AsUwCFoBNTUMcxTuZ1JV/6OUCMZ6a+3hdN5v8AhqW05FT48ymCSxt4tmLdujJW1n/f/b6nVKGqqpuy0CYyOxSi2E/PdbsortbxrjtNwRTE9H8RDqJ5HHy24IPQyzxGM5hJmXB/6WZ/h1U90qiKt3UsS059SLVlhdZGlxqcfdWTjJXluOLf2GBORMk6pscmd3TNW2qBa91l91cBUlED35H07PQQGIZWDAI6CBeRrBHoxm4Gm5uNoxuMtCEN9/6Odr2OOawidi6t3oFfUbrCMwFpfvtUPk2Y1qO1tv8AJ/Y/+2tHPvVinzOOO+n1234vazMOixGrdTovamKmQdv66iytotnhuTRdJFsnOM85zlOcNmoX8WXklPJvrCN/R6gPyNthSynqlSvZ1NTT3XeZFivHvWyrpmXMuzmbP3/2HOrX1yspruBxuF2DRUM6sTIROzdUvt8Totd2N7dPbZHTzdf7Kie3InZhGoDFaK85+Dqb1Gs1HtnendgtENsNsYeOeo7l/wCiq8j1M8MaypbLa6AXqr5k16xrqAGysfmO1ZTZVez22D59f00oJnt0jU6PaMKaBTbM5auZDY1eVi/+wzB4mTQmTXwoFj3IobqOIFbPx9/i2VB4Lu+yTOUDwXQZAhyZ7gw3TnucpynKBoDuLLKw0NfElYKWM9vsPSU9ACZx+JK+RVK5eq8ZShJy1FidxOdeRj1lbq1Q3Hg9nUHTv30HVV6+2+dv3LqN9/1Au/VV3BWARZPvGEHiZ93DFx3yqZi33tiJtzkYmNYnSLHTIDRrPFrIhzcytqPLMHJr3OcI8MBCJr03NzfoJub9BFMD6jWbh8zzBZqK5M5DTBCQuiftGXU1O0TO1qLqNGO4FLSpeCWYlb5+TUMfIU5C1tj5XC7GsEw/KZtbsQfnTL1GIJ/V1PsD6cpswPFtjWCMSZrcxRvp3SmXta0zs3bx7WrNT86clru1bxg+/Dhat4rXcDCG6dycpyhdRN/DqBZqa9AYdCc594FJnAxQdsEhJE36L80WqcYwEPiFj6IeA7m4D+ZuVbKruQrzh8uYNHF8KRzluOrRKONX6+z8QBM7VkGK5lOL5xF1idHPnt8gU1Au3wnD09SGQ6+0yObV6b19vDiT2jR8RwGqZZnr+c6VSWs7YnBQErE7Q32l2VCj4gYNzcM3qMdmeJX5hthsJJM38Nf+TZ/HZ/Fm+a8/waB415lm/wCmEZp2SIEJNNS1zwYSJ3IllplGPVTYqkTqK/lKx+YwRxyANzq/KqbPr49eUEaqdR//AFOhVA0e3rBZV0OM4xsvp5nDa6mpw8fBzm4T6gRaknJVDEfEqQoDF+XKfyrea8z+HqP3xwNDW4/6FdfM+3Esx+K8GMII+IRX0Gca5wWzvTuTnqdO1ZSPLteivfmYVtae3XqNWR0+lrer6S6y25vgq65jtK8rFtnATWpl9Qqwanuquu6HfjnGLR9gK3kODAQVwKxTgvx2vGMmwV16aiMjmHfouhOQnOF/BPj0VC0FDkFSIqxR4htQZj3V7D1lMm3nV1JnC0d7Qt4EHw++Xw9lyFx3aJT25qIBDXs2UNzerwQR+guStjLyduFusl+GPVuui13quybFZ97h8ZQ8S1gYfHpoTWpueIJVmZNEr65eBk5mfkP7uwTFzkx76L6slWIafY2WcEQfl624IWm4WM/FvmNYyKzX2wtdEaqq17a7KFpyEt+JELkYyiKAvpyPLtmMNAkTkvunsHeFo09zvX1B7GTHrsnMoVVYTZv1WpmiY6QATxBasNolIgdY0OpdXv4OJHpxMKkDp7v3ahlO35tVzjvH4jebZvL8bPblx1azs/rv00JuD5GG6yXZT3Vn3n3jUAnFy6KKRfQ05TNvvnC5ZjXG+r02SmOOTEMYTUD3UMPDWzCiWrVbYW+CsETkPQFdDyG28CExhop/kWLvJp/juGqc8CUlZxM7UPd9QNxPlCkTe4FGhQkFKx13Fq1PtO4RCYy6IrJlz2Uzu57AWLVXxuyZmpYlvSv56v3S7AutlfLv2fUs+3pf+52HwajFePiX6KLq1FMsQgqTKt8KzyUN4m217ddlLUmBk3+5b5Fr265WQlNOPcaZXY1tfp1ElcTAV+6paBFtRbVY1Y72mzCdAOOuR1siBzPMrbwWWMyx23E/mb/IoB7dujVnzHB4p4HjbbmjrU4ankRGgaBpynKGwANfqd0mHzOMIBHyifV7+zMjEtyXx6vb1dV/m6adZCW1KwsRvS6lLbprcdlU2tyjDkx2IPTzN/LLq+VddNitkH5a7+8CQrV/sqPzb4Wj5WL31MMoTkHGuJrzchIvUjwy8izJsPZI6flV2JxgrYzq1TLi4CkTcoUR8VTZTfW45mX16s4n0Six4ce4T2tmrEsSeZoyr+Rx+ao/ZZrhnHZxdcFE1qFdzQCsQJzO+RMD6gsgtneM75jOTNxWgsnc3C05RLU3+IWGDMzGnd6kZnNkk4lRuyE6WncHSqTB73HFGf5uT51+1iV6cagfhDr0343FXm8ZM2kd41u9yXCjzLq+5ZSrJXW9yvkWeA6ZFanvIQQav4k2auY0bwkWyto6iYre0uq6lQ8GVjcc/MfMuwb7MfI4o08CfUYYAyR6cOUKCdldgamo40HAadkTsIRVbqy3I1k1sVHcssTLNhXGS4otr1yuxLIdbZofPpv0B+L5R8F9vbpoQqw8DlNzqVBSzB8ZifuWD7XY1WRMgnuLw3l636/70YU0qsor3OrN9ZKzZdnLj0ZWNie7uK3LEtsVe4qlmqcUqumpIsyA3Gm48KMhOQ+WzWoV02KfCt8gbi3yvGoQyyo8ej2qMVftj75llSb3B9p4m5yhca9NzHsZja59yjvEa0i/m4xKrDWQRDXW0VCD6agALrWWgoYz28FQ09XjgeRQiXtwQHkN79Mm0tXXx90yhfXqv7cT/JXcXcEEzEPvAk8EXlOH+odQevmdWdhmdMbu5eW4bK6UFSxlYuENPSPDnqWNXi3Y+NdfO68OQxCWVmcU29RJbn2wz2rjWputhzUEh/nCX2Tl4RzW+NlJfVj3KZlY1lmRSpRN6hsAnMGd6PdO5Oc5TlMbyMhfq1oDYFAlo+pht9LWya9xax6a23Hxhq3cqb5FMsfU5gC23cDfVmboilvlx7eYuuFcKs/Uk4DLcVgwzqv8eMdZAMUzu1dyZL8ssr5NXjKA7fx5r1Nl9I7PuLaxY+BWV6dxuU5wavp2IOeT1ZuWX04cMOtGZ+qArZRiVWUCsWNfU+PbyuCC9InAv2152i1Xewoz8RGcGmwgWYt5psxN9huKAWmBp4PpzjPuKfT7wLMT9mR96vGR/wArNCzC/wAdfs79taiO34hs+s9oSut9LRZ8vOZNjl+fjkJUx73JZmjuRWbhi15CC+rIL4erc8Mvun+Sa3OJnVB9Cr+YCZWSMdO1mgY/UQ2McO/keUyHtEu5+3+Mvt+ljVbp81INXSPe3OM+6sJgNjv1DP8AqZ1VfHo9FN/uOquwzavl6XgjnlZid7Ly17WFj1I1FGMmVeycXtoyqobXncqdeAK8X4MbO1g9SSunqFlN6V5/bVq8iacTnOc3NznNmI3y4Z+lla1Xo5afuP8AJhf4/JVFmWLahcVTvnYYtczsVPyVKWC7bbHbmedL+8/MDyEqZaquVby/xV0wMCuZSbXzqzLEyLLPbZUyKLUx6i62Wo+sfFaxjbUhyMB+deQ1lZOPA2OW6pT27+XyfE+DZqik0dK+qpyt19IxlXvdaO7ejV7ycohrvNfR8C/KyMzNy+OSllJ6XgphfiF+E3Hq69qVK9fSulf5NKpZdnfNm5KnuWUJXR7Wtcbs2dtWyBXjZdKNR7TJq7VPcey97DdkLMndg7bTg0C7naaJQ5jYzJVh5D9vIzGY1ZbtZXdeZZZcbMZcsU33ZDZDamTe1QS0FK/mai3I5vekyRd20uZa6rlybLa7xk2ZXYXH+rXTQaZdmtS71IcQUduZWVbYenVarwMGvfs7jZdStTcjMjVlFNLWvjY3dOblNUL0Zz0nPsqty8MXCu76hxa7a8qrMuxsHIxcfEXFyLKxgOuPo+mvQ5F+ve2HGbJaxer3csPpnYOV1jRzOjVBENFwnUEaqjo43k5RDZOX9PpfR01ci22v1c8b92Ho/SbFAw/aHLup7mX1Ku33eXzol7CvFu17Fgq4GNS7ordqljzycQDtjkJb/HHsIgbRW3Z7qLMzKFeM2atCX2WsuMWMrUkk7yKuFldlRh5V9QcVvM/j2Om2pXWbMdUfRzjlUOl+TjtX09u2bsmqZd9dgw2tprsyHUWut+R+brovuyK7C5Jw8u+nExepZ2Vk/iuYFHUb71uzsnlj9QtumDiTWplV3I+HdZ3cVe9mCZWHVlJXfbiHKe5sam3txup5vO3ItumvE36a8q6zHA31Y8T0ukNldTZDl9NHDA9zeZ1r9vQ7R3HsxWmXTRlLgUJQlGHfXkdXJOflhq+lYI44nSaqzl/JdkZ53lZ72BstyKchkfCvWt8HDqCjIVAlevd4mva6mQPkj1AzswVspzMirnk5YyZqObOGMzUpXe4bDd3zxiUGZeMiJ09e5ktiUNMpUE6bSr1ZdVSphg+4FczPKdJ+2b9KjLH5jp2nTLOhWAbKfnxOo/NfOkjfTun6/ELcddXYlQty8fWXi4XOBeImWvIJQ6uvcpypoTNuwqktzzW2vPxdgQIlRFksLXTpWMyWZWPlG4I1PRMZed/WSWy+mDjjIvNuufy4Cdvp2A+VkZfUrGGb1KxjhYlyfhfTHxvdYVNbZeVS75/UldczqW67M5uCZfEUUVVtKBZ3scnv42/aNqWn5SpMCtCjCZd3t6MvTNc6ym4l7XoGMHsUKzMcJOPUAPkykFswawmWbEUZtIVOmlBVkhLFxxXVmBwq5VNXZ6UU1lMLk6iiK+DdutvqC5Fqy67Wtxb62Nli8bOhVd3FwaOx1CzpvYN2WgneVrB1Oufign4nuP1Az3h4DNfdefa5bKyd9TTKuuIZZo/GCYWQjZBqYyi+xQM+1Ksp3/B+l2omX1d+edivWnSunVLZldXpazKSsp0Xplbe6yfmyesLwIXXR+lIta9NVbMtvq5fUO4cvqdlq3dRsJPUGqdaTi95nCZWNk190W1YvT2z8dQdWoiziN3WIB1PN5vdartxBcMglzbSviUUskxK2Ocg+TIuapcflkZNvdmZks6dLqcJl231CljkZSIVTKycjt9Mr5HN71VOZZc1vTahMlXWcme6ipPbZykXed/+PkoqYwF+bjnKoxsOpKzQrDtLrgkVeLW1pbDh0M1uCjrViV1Dis6xmW4stusuavny+HRgGhwBnDUCvGXyROoN+WxBprm/M8CMbGTjkZTm/JNgGNRfwdrW559pyLmyQ+Lg8FwsHtY9oHCz6mRl9RDHO6lyXMzBuzLuv7mCluRW1DpDm9wKEdUZ6Tj5d/YryUcdaySR7tL5kezWar0EDB25CvgIJjC/u0d5K85hrp381h+XLKzpZKpmnxh+chWLLln6XS2KzqBIozDvI6cGByC0G+9QH9vnO4uH36B9kY960uKW6rmVh8/OM/EOohvfdR5Ll9S3Zl9Ugt6gW/8AZkM2ewd+ppLayUtWpZQC16/s9ceztXe8olFbZD9Sxgi9wRXmJQbnzau1kF3J2wmi7OdU0crLc+h1sZW2plQe23qGK1cesLX3O2lfKxov7+Vt1ucbVty21Mzv97Cu7VeRf37L67a8ahudQyCGfO1b+IsImauRfmVpXb54jQl2PbXS/PhoGN96CaWFyFbGXJOFxozLL+IzzQV6fcezbdW4qK15hvdBlOj0dLsbWTcxmednp1tprtyLRMg6yaLrmx7LL+Vmxd/4/FYG/Xhq12wVFs2tR6hko9fUMQ1t1Wnn7ymtqMhHJAWZl1yS6++5OL8aPF/Bmd6nq+HoAXj1ytvw0ACH5D0lTXidbTgv/GvfAMwPzKOmgrX1kcsM+COYXpdNaHO7VuJb8tly2kKj9uvwV0Tiqnb6gi2UWFg13PuI268IF7LEqfHsB4sOD8Nv+2Cs2W/Jypp7zCpd35CpiNZyr4rqwHgjEpi2K9GMiML8feZkiyZV1rzpaAplG1JQTZf2eKZFtoTpdYeZytXXklu50ypSuW47/JnehO3i5pWqzyx6CdGuzd4jqCbOIOU3DGZa8pKLRXZe9Vl9rp3OnGjnjsrU5t6rflVLjWWO3BP5MEvXnX2d1joegUt6dMTjj9RybidzlKM7Lw8LP6sc484rNE5CHvGdOT8p1vKXe9w2eOiJyxut+zQZDJYd7AAsnllVuJ6bWt2F1cVYmBzG79lwdL0zi0KJh46ZHbllg44pXl1G2tra7dGngz4qGqvKCJaWr5Oqk10B6HJS1vpnHyClaY6PZmKKrsluIyuO+lDStUMiY/8AOhJrzMXWN0xuLZ+xXn0NWen5SKLP5CRybMtKMxYzohCmnQuEuB5Kg31U/laNdmvly/3k/LZ068923euof5fUvN2HiDMS7FNOWlpTMsDAhlXGbp1teAQQfM6ahTD6xbXbkb9Os6pxot7QXz3JIAr9ph4itR1OvtXbJdAjiqpun0dfoN4eri7Juv8A1ubnR/HT/wDyWpA02gHLx0HzV1ihX6d2RGr0BXaC1N6qKrNfUEpx9y7l3tmAkQF9HuE/MIjNyrorDdVsNWfmdQa+O/NcfqBxqqM23Zr1e/UGQnLd6cQnHe/qC2NdkNkUeR8KidOtNcXMsLjNMfMaxvdurZ9/dop8V1HjYf3aYn3m3fL+XMPO/LPOdOs7T5lnLMfQy31upwKOqdRR8YnbTDvr7WYQ2SPJTpF7VdQyLL8iDhwRVIqxjxI44uBngnqJPvAtjPi1k2XkLV1260ZPcbRsqafuOiJ5mD1XBxsTq/UKuotwbhapX06Ha1mN129a+nQaBmxN+JUAJlFTlcp8pm0ASz5rLliOIs66fz7nY3Njji2CmHIlrLYabKVpe6pY7Bjzbhv18egEw3VHV61K5uK07lYhsSW9uxVpxkhxK1ZgNU8RQAfetqWY1FjPj0uaFNbZ2/cWf5nBY4CDIt5U1Jj8fbCF9QuTDKeqPVVaeTgbKVQoVh57sRgrKz29QqEwsn2z4Ce4vbHW2rIyxXZSSz2rWGVKiMVRVLk4kPYsr20KDdqMpCB1w82vFxs/NTNxGr+n2rZwbSY3drfHpxjue9zSe8DNgnj4pq712TVWlsrHKz3DVnqOYuVfzWcqIGoZlNSxm8cvAbyTORE36/6KsADNiUcdoayy1Y84VThXONJnCmP2VX/QrQ4liJo1VwV1QrWHYokudWNn83/G39zVJ7PsLvi8JE36VtXD265xAbwJ30E5Cx7yOdDhXyr+5TQnzdN5Kes3P7e3F+r2nrg5ktzES7IELszhl0rMUDbjWbhdS30LoeVDdt7Hrylqlrq1eEyKuSw9wWi+WavHrYcePIcxbxbG6dqHEqtl+PXTf1a0IMjJOQxs3P8AmPEXVjW0Vqq9J0uTirUV6Lc8/Br5+E5RY9Jy5dg3463Y9lCPa70UhbK7MW6t3xStzIcden5HOD3EyORppvFZuf6tvYapqeISxXqe8Im1FaNW1bNX3cr+SM22JG3HlU/9fXkfU4D13B9lccK3JZ35TzKnKs13cZDNgxdF8Y104eTnWZTbQelF9mOU7WZVm9LWqtvB3KleLWzPdjXUPXRa1ddjq13fWJc5sutDw5aIS05GcjBtiVYLB9+diNX1K5Ji5ffPULON1t9lzkzYmxN79ASoXqVgXvr1ChAwjVOoopbi6duvqTm2i2pnwOP09sJS9qPg2vbfkKe5iJvKLXLOprZUYK7O7b81fcOqbCjZDcrrW3RiPqc/BPJvHoZy+n3X4LO/d8QOp94IAsEr0VIYReaJblvasDGF8b23f1OiZPLKtpNb5tftMnkpiKq0U0m6/qCuMqrspTcl2JdZdkvF8MLecFNgNnzIqJwghYmAcjORnAzFYpfb27EVOTnwfXyJ3G4/eI7VH3RtYXkQ5o3kZPdqeolq7S2BZyM7Yajvn2fTruwtmS+VZR+XnvVU9VvtcYYoa+/JstBdac37xDo2lWTBWmy/epszfw7HCbX9AxDP+VUYeMm6yxj9yYpm/l8GYH0updVvqx16lVXl3th8YMhAoptM52LeOqLw6lmY972XF1An/I6nicdwV6h5TRnkeplZ4TuGMWaDY9CD6FifVPv7gxcrjDlLO8pgVQ3bUAo3fJg1Dbudzc35cWtLt6I4zuT7n/SannRRh8A9NTXjUUTh+ikSf6/43f5DRoJ/qf8A1zvsf2GWftn/APQYY0M/3/bEHxWyz+LH/jMMT9jfcfGPtD6f/8QAOBEAAgIBAwIDBwIFBAMAAwAAAAECEQMQEiEEMRMgQSIwMlFhcYEzQAUUkaHBI1Cx0SRC4VLw8f/aAAgBAwEBPwFyLFIcxZB5DeLKzxTxBsbLGxlikWbiy9LLNxuNxfkslkNxelllllllllllllllllllll6WWWWWWWWWXrZZet6ruKN8CwSNrG/I/LZuK03F6WKQ5G4TLNxY5F6XrdDl/t+GF+5vVF+da2WXpeljEyzcSl7vJDa/9kwYlLuR2p0vOlZXno2/sL95Gr5Op2+n+yJtEJ7WPqTHlUvvqkQx33FSHRNeXayEShwNg4kY2Sh5L/Zx7nVJKXH+zp0eLL5ixJCihssUrJcixo2o8NGw26uRZKRuN37BI8Pg2lG3XCrmjrFyitK/2ahjQxSN1iZvPEFNG9HiEspvZet+8oSPCFio4FBixG0aJxFE6aHtHVR9myyxv/ZEjwV8mMllSJZRybEbjcWP9rZGQhkIla0OBKJghUfuZYXBm3/ZsOKlz30eaxv3Dr9tGTIoWl+TbbKEjLF7q/2XE0nyYZPbz7lOjd+2UWQjpZZZZY5DgvQoTM2NykqVjTT/AGCVig2ODRX7HHj3MjjUdHEqzw2ODX7qBYmbixMsslkI+00eEbUKKaJRoyfE/eqNnhsWBkcNGwoljGvcKDZ4cvkYoNiwws8NUZMbjr08uDG09JYbFChIcDwEPp0TwNDWlav9nGi0jcixzHkLOm/UQ3wj/wCEWPlMzqpv7+8SshEryMk6JSQ5eVKzH0v/AORCNKhxIx2lEjNuvnTwmzHFqIkoIvSiitZocRYEONG1Ml+yss3G43CdktekdZEb2WyI3R1K/wBVjjQotkMC9SWFH8vZ/LM/l2Y8Dvky4+ONIqyNEdbLLJGTv5ozcXaMWfj2vNNxqn2I4VupiRkhvIYqk2/cMaiiWVDym8s3ftrLO7M2F4+50eOo38x9xnqSRn9mXJFr1I5keMKV+WaZNMUiJHRscizcOZJrz7ndkcknxetE8ri6JZVk4Etti5RHHX281jZuJTMkkyh/u4q3QunlLJ7XZDHzpZf9TrMblHjuiGCUoOXy0xqxLW9chGBFCL0myY5o3e6jldpsn1DMea1yLG5GLDsEI+/lek0xWSi7FiRkxsca7/u8Pxr7nIsUiOL+54cR8WOO5FNdyXTzipfLSE6ITsc6PEE9ZojL0EyzcbiciUvfYYuX2REZ2FLgsWsnQpNlEkbijaSgx4ZNkelfqT6f5H8sfy8hxoUW/wBr02LfBr6o9CMu32I+glx+SQtOoi1Bj0xzJ5EbiGQ3G4kx9yMizebxu/f45viPpZuiZ8/oiHU+zz3IKrfkvRa7Na1o2iijatHFfIydOmuCUa/ZdFhvG79WbuDcjey2bRR4NyZ1OLxIbex1KrJJfXS9LIyolksWVniWNkWbyyy/fxdMWRrg/l3Ix4GpaV5Uy9GIr3fVYqd/P9l0eRzw2+6NqG6Nwp2N8HoTqxT9GdWv9WX3/cwhuv6a9HLhxJxlKV13MXTO/a0c0nWlknS5Nw/M/cvRyo6nLu4/ZdB+h+SbEdxdxrgXY7sxvg6/NuyOPov3P8PSJVfB08bn2sjCvuxiJcGxXfroxryoXvclND/ZdB+h+TIIciKJLg9CuSHc639aX3/c/wAO9WdVHbkaOlxbVz66r4ir/Gq9pfb/AJ//AIX+xZKYjPj2v9l0a/0EMcShMb4E+DIqZi7nWfrS+/7n+Hr2X+T+IR9tP5ox5ZQ7EZWk/mZHS/JFib5Ltv6ClyQ9kb4NzKK91ZZZY2ZLSPGoyZN37GEbdGKKjiVdjamxR/uL0H2/JJaSx2dlR1f6svuV+zx4Gz+WRLDTPBZHBJs6eCX9zqMHiRS+VkouLp9zDexfcm+NohMlNI32+BS/sO3SNq0etllllll62XpRlxJksdPkcPkR6eTP5XgydO46JNjhXmx4rI44nUxjWnT47dmF/wCmkWr/AAWv6aclPkp6bTrK8aVEWib97GF6442xYUhT0khcdzFkW4hF3aINs6hqeTgwvlL0MvYslkJbpHT4WpW9K595ZZZZZY50Ty2Sd9i2RzUQytm9EoxZGNMl2E/QnGhI8Fs8CiNE2ZJWyMHLsYobYnSPciSpo9r+jHF8jj3+xH/BQ0Pg6r9WX399QuESQjxPkb2LKRzjyonNsRg7EOLMsKyP7mN8oaJOjFJb/aIsZ6G23ret+5s3mTKLIOoniD5FBs8NkE75JKI5NG75m4h7XcjiNpKiXBLI3pjlsQ81n8Oa2fkY33JdmS/wIWmV7Yt/QlJt2/fWbmbvKuTwZC6WTMXTU+TF3ZHuzND22bHY+51EHZ0u1cvueIqvy0bTaOOmNE17OjEitMjpDmWNljExSItljfBaROVvTgxcks5LNbJSN2ren8M/S/qIf+SfZkv8C16n9OX2f7OONs/l2u5HE2zDhUStfnRRm+Igh9zqV7JGNmGCS9zRi7E1wih6UNGWFo7HLPD4t+RHim8lMTGyMSGCI5RiqJyXoMvWKtkMCJYU0dDHbjS++n/ZLsT/AMEdep/Tl9vcY8e5i6dGTp6RsY415okJJIlNMUkKZuNwpEPU9DLG3YlRJcmyxQivM4latkVSol20Yhy4ZFeyZXyTauyFGSFolGihqu5/Lz27q4Ixb7cjvSDSHlQsw8qSJTVaWRxuXYXTSaHBmDHzYlp0/wAJZ/2SJ+oteq/Tl9vN4MmrI9O2Y8SiVptJ4nZPFx2HFryKVDmzcxTZh6acu/BDpnXLH0s6+pim3kX3Ma9k28WPR99JP3LiJUWWNUPST40ll7kpm48Rn8jfqYOnlCVmbFHJ+BNKFWYMfht07M+Nzy9qs63+GPBjU/8ArzYsTmyHSJdyGNRWlChQ9On+EYj0J9mRWvVfpS+3khhlLsQ6aKEtLLF5M+G+fJtZQoNmxmPuUyjBH/Wf0s7RJOuB6SL9zZetEo2OLQzHivl6dRDZKtX0kdtnZDnxZLLG3bPGhwXGvuLI09r5SMuLxY2v6E+lgoX6+XBGkX5N1lDR0/wr7FWX2+5Iyeoter/Sl9tYR3Mxx2qhaUbSijbqzJBxZDC2dN0d8sfSxfcy9Il2MWGO3sTxpGPuPRYIW2lyzJ2JvnWQ/PP5ndHfVC1cUyijqcalB36EIbnR/K+G7ftEYuX9SzMqk1d69Kv9RHU+zAhLm4/CjJBZY8H8lLbZi6dzMnSNK1yQ21yQy3wKRY5UQlem5EpGH4V9hC9CTJeohadZ+lL7G11Y4sx4qV+orF5rNw9HFMSSMPbjRqxKjL6GP4hyRuWnyJd9G6Gx+diRIuy9Ii0baN+rwQfoPC/mRjWnV4ed3oxRI4ZPsdPgcch1z9lfcWSjCuPoY8m1O+UxxSfHbTNjqQkxTZGOSQ8ORCwTrhmVTj3NzOTB8C+yKF6D/wAkvUQtOqV45fY8NVR4aWq0s3G4lMlniYshvR4iZPKkb2zp47YpPvRye0bZGRMjHk8Ndzajw/kU0ZPiejGX55XHuKWjfA++kSQ3Z8SKPQ9NL0onj3KjwDbRGJPGpdx4ov0Eeg9NpR4UbvyTgpdxYIolgizD8JYvQ7k+35Etep+Biya17OtjmZM3yHJsRCrQ5G56Y8Sbin6kfQWuVepHuh62ZFzpPzrTPzIwq5JGT4mQx7tU9USL4IsXyPodiHbyUUTWr93j7D7iPkS/yRWvWcYpCmb2LLIqSwqRuk+yIRn6ixsWMliRPCyWFpHSY28h1MNs6Jqq+xFWzwFGK+a0WuX4SPcfky/Fq69xk5kzp4+1f0HyzHxGRFE/00RVsyqpcCg6b+Req0Y+UQIvRl6vgT87ZZesOwxd0P0+5/2J69b+jLTDickyWLih/p/g2beBEVekmV7FjyI6R3M6/E9275nVYF4Sa9DpsK4J90eotcvwkRrS9Jay7e4nyzEuHRHkbrH9xMzNbYoxfEifxMXGNkUdVW4glsbIJt0NU6GXp6l8l0x8DZ6k47kXT/ppuE1X11sb8lESR6/g+Q/8ke2vWfosailwdLjXhv6kWmxRtHUUqHJHTOzNLkcpl/8Aj/g8KXyOhjKLdmeO7HQuVT7EIUd5eSzJ8IhsvgdsgxrSR6e4aMXGNshZPjEJcnUKpGBOxK2S+BGPlmRpyZVYvyYF7Y+7M3ZL6EI/6bZijboV2S9l0M7jGNjgrsaHGzwh42vLekSJL1H3/Au6H/kj2LPEszLfDb8z+RXzMGPw40j+VjdiZlxqVWeDExxS7DgjahR4Ej1JrgXJHuY1yIY7NrNjZRKEUtLPqu5/Y2ofsrlGWPPuOxKSWOvX/wCiM3ZIgrZm+MweumT4EYF7RNcmReyjBdsvmjK/aH+n+TD6/YxL2kZO51EbkZFwhxqKfzGqVnoJjPXSzqZ1RvR4iE7KFEUSN0Tl9CEm2cuh2RTpE5NuhGQ9CJHuepkXAuxDknB7ifBDlEcdE3THG4kYUT7mMXAxFCQlY3ZGJl5IyoqxOvv/AMnxcElfFmJpcspsUPn280p2q+RFcnURW5GJcr5k73ci9mLEZnfHyMHfTN2ijBxZBGV+2T+BEeYv7GG9xHl/kyP2jK1a+iMnZE/gQ/gR/wCq0S5F20634V99IcIQpaIeWiUm1ZAXoPuvuJjR/wC2mTsYi0P4jcmhyRjdDkTZiscn8ibtkVJIbY3bICk26HBibRkkyGV2LjSJjm95dvXtw+w5ST03s3Pz4+51D9owqn+CXMmLjERXJn+Ix00xGdNMhfhsxSuRLmTMvZIj+mzDwyCuSMj9pmf4zLSr7GRdifZD9PsLvp6adb2RGNmzaeILKYrasWn/AAQ4FNp8kX7RGKJxog+TbwTZhRNUR7iXBkZiMvCsn8RhMvDX5EYkZe/4EYO7HxRP1Kszrs/JEUEv6k41/XVuu48ldu3ukSbbMLpG4m/YSMXxIyfEJ+wyC5Rn+IXGIwL2hvlmZU0NLZZhumYbsTuX5MkrkZfiM3p9jIuEvoSXC+xFeTMzbY42ThRDHuZBV9hMi+SVUJtaY+5EyKyCpl8EoOjEzJz/AHIqpCfBOBiZk5Rkj6mGRLkkqZjkSVklyYe441Q4t/8AIuBuyy0bjcWWW9Mqbor3ViIyqNFWZlwjD3JKpEl/pmJcmSXtckvgSMXBVs6h3Il+mjHxFmFe2RveS/UM3xmbiRl7mTv5c0LYkUSVmPHXJQtL4PTTH3ESdC5ek58GFE3QnbIrgnLgxmThE5WzCjJxRbZjRk7/AILMXcasXcei1totm4soyPahybF7yKM/JjH3ZLiBi4kS5l9BtOCIuji7Jr2hu0V7DMXBHgXxEuZmXmRkXJNu+RdvJIaOdWhCG1onRDv9SKpGX/sxq2ehP0MRkfP9SHcXYn/kxGX4SfcwmX0+56kEZO/4EYu4xdyVr0Kl8hqXyPa+R7XyHuEplS+hUyUZoeN17RJJdnZDuj5+XxV/+oSsyLnWKt2dhSYnpJ8Ee4+4u+ifJLuSVF0qIuyxMT5s9Sa5J3fInwMeslZFIa5NtHcocWl9PJGVMg7RkaZj4kSlwTijC+CVMXEiySVGKXoT+pkSMMjJIn3MciRLuYu41wLuXomSm7I5l6n8xGx5yOVS7F2ZeCTbWke6Jdxxa8uP4TL6a9kZO9aLlCZQuCS7DNxFcEkepI9NEiKJIsZZFkuxYpG4T1SFQ5eykxy8mGXB3K5GZJGFE+P7idsiuCcnRjRPhGR8mFGaVMbsxR9Sbr+h3MXcl20sk+PuTaSKU7ZZJpvgl6GDbu4MTtsz5OaJpR/I3xpH9QyO2/KuxJ3rKT9USdss3CZv0k+S9I9iRLl+S+Rdiej0iPtqiWsUPWMbRYyM60j3JdiTswoyvkj3F2J1/wAmIydvwS55MU0jJ8WnivsiTEYu5LsLRmb4CHZ/Yj340y1x9jDK5diJ1Hxsy9o/Yxw3cE4bXRdTsYuxGPs39f7DWj4Jd9EZe49eCQ0S0S5FKuCSGtHrHsZO+ka9dIEiijn3u6mZM1rSGajdZ6njcFkHRPMnwN+fH3G1QjcbkZX7JD1+xHhj76OT236XV/UUzK7kT7L7GB0zO/aJfGMXZ/YbWz+g9HzpjhulQ8W2XzobvT2eCMSMf7jO4++ke+k77aX5FJE5G9G199Idifb3/oT8iLH5L88O43wRLNxN2KCQ8YyHr9je7+lm4lBMcF2IKmZifxaMfwfgionhjlRYpOLtDkvy1reiE+S/aRMQiTIT455fBFmSaLZhhs9uXYb9rTEop89hs3to8QjlSRLLEW2S+TX9ytFiuNr013sRaPEgQW50ZIpPjnySkmWiX0Ex0vqWWJ+etEY1yKrFtKibYm2JsiSUUtIQWwcVsRsibIjgiSSJMloxQXh2LEmkeH9Tb8xJHhozQmla5ISbReli0gS7FETa5SSXc/k6+KUY/n/olgadRal9iUa7iR4sv6Ddlc6MSHyLgnDc+O5g/h0nxa+v0M+J4ZbWN8DbUavhjWiFaXPcZKPBjtGNWOKGkTfAkV5ENbVb7IxZllipJUbDYbGbGbSvUc24kWttVyP2UQdKiLr07imnfzs3k5c0Qy7TCt/45MuFxjzwx4+5D2o8LsuTxaijAt/H0LTVjkZOdHpRD4P6kcn9CtE6N7FzEp2Udyi9p4iLExmJWyfUKPs4+F6v1f8A0vpptrkc9/E+V/dGXC8c6f4fzIZXqkU9fQxupJj6rJdp0ZMjn3v8m1p8PyvE0r1Uhyt2KdkhvzUW6r0McdqUV5Zvj6D+EaaX3NzqvQ2uSF8b+hxuplVKtMn/ADp0E9uS26P4m/8Ax/yTk2dDneOf0Z1db+OwjE/PueniS8ik1oxCJRskSdGDquaYz4YfWX/H/wB1nkwPp4wk+a4+jLFDxYNese329f8AslBx7kHa0XZCJPk3LS9EUPyPJJ+WHclz7rcWWhy4KI/C0T7R+3+TbwRlUaMXcySTkiklemRkVbH2peg5u+/GkMjg7XclLdy+4uX5F5r8yGtcqoyvgxP2kKakrXmx9zMrMUGu5RZTLOBsb81e4v3W8WQ3m5FnA2WjgW04LGxvS9UduCvNXu3ojL3JdyPr9jovg80O6J/gXYQ/xp66P/ZnovLHyrzf/8QAPxEAAgIBAgMHAgMGBQIGAwAAAAECEQMSIQQQMRMgIjJBUXFhgRQwQAUjM1CRsUJygqHBJDRDUmLC4fCSotH/2gAIAQIBAT8BSESRQ4EYGkljOzNBEQxCGSXdsvvJll8oYxKv5ayeTTuPi4Je52sf+SuT5IvnQ0aS6OppNJRQ4iiaRo0mkoUSuVc6FAX8u4jLoG75X3GiiubNy+8yhIo0mk0mkoaEhxKNJGP5ePJrX3/knFZ3j6dSanJapd+y13pFmsT5vu33KK5UV+XK62OEc68Xv/JHFPqZIKapkeC9zNw7h8c2yeSuhKUmJshK+5ZqRklRZGdHaIU0SnRHJZfOiv0Uuhwbk4W/f+TuNo7GHsiWZjmxKythxojsSzM1sWZms1DfKMRpkYmk0IXJd1sXfbO13FJFmrnndQfwfs+XhfyXyv8Ak1iYmIcTQho0HZjgzQzs2Rwixo0ooocRFd6u5Y5nbjzNlsckPKjXYmY5WSmcZl8FHA5PE0UaRR/kjPxD911r15RxNkcIoJDNJpNIq5UVyvlfOvy6JQHYicjUWWaiMyMrOKyXOvY4fJpyI1fybieIeql0NTFgIqu/0E33KKK/PZKCJMfJorkjXpVlkn0MU042L+SZ1JxpHExipVE0v8ho0/ppTSMmR8tJRRRQonaN7Mse5wuRRi03Qmv0DdDmkLImJ/oc2XQrJ55T68lOyztEKSf6qb2NI0aShooojjsfhi37IeSjtDJJp2/QjP8AoY0tK+PzXOjtEPiIonns7SxSI5SMvyJ54QdNpM/EY7q0cRlUGh8Znq10snxjcrTa/scPxEci2d/7c+Lg2zNGUeUc9Dy2OQsh+JYuKIcQmJ3+omU2aWUKBHEKJxO2N/BpVs/+SRG4yTXucO7xxf0/MbonMci+aIxIxYo91tJbnEftDT5FZkkpu6pm+rZmTI2t92dSop7+pwXZaP3f/wA8nxEYumZc8HL6e/oScsr26Ggs1ll8rIMjMedim2amiP6KihxNNGkcaIi5cbG8TNCKRLoR636HCv8AdR+BTvoOSRk4h+hDiGLiqPxaFxMTLxCrYw5d9yycqJ2S50UUQMfTvTgpqmZ+F8Xg6evJJIV+paYo+vqcNjalcOpPPLRar/76GS27Zi4zsV5bbMnE3BKP5ESMpMjiYsR2ZRp/TaSjojBnjlWxx+bVLT/5f7iEz0IP2OHanBV0GmlSJYWzsWOND5WWQZjY472TJrkkKJQ4igQT7+lVRPDFXKubxnD8GpK23ZHhXilr9v8A7uPIslVWw3+9tO0ZMqm707+43Rv3KFEUCMDHFpli/VydKyfExhj8PWV/3ELYopmn+h+z8umVPoyfEQjNQb3fLK0kOXOuSMTJTJMY0IiiAoMUCvyZ4VpaWxDhImXh93XsSzrEcXxiyJKqPD0RdSI/QrmhIoxtDohJEs3sYsqNXt+rzPwP4HR2kUSzV9h5Xv8ARWKTbX1QslSaL9SHEY5uL9eU8bZOFEcdnZjiUIgyUX1GijSaSECMPykuV8uJyrGv/UzKtbt+hps06fEQxyW79f7GnYaXpzhFMcEiyMjSWaiM4kc0UiXFIhxPufiV9j8RETsbS/S8Vm7Oab9pf8HWX3JLzf5kTXn+w3cn9UR2a+B9SLaODmpZFzywIYmaSeM0mkghbolE0nZixlJC/OyY15vWiUJdH6nCcG49TP8As9Sf0ZmldRXoUt+ekqhsvkpj35WaizUKY8jFkl7jbYskl6mPipJ79CMk1a/Rcfm05Y16L+41uU2aDQat/uKeqQ0cDn7LJq67M4aWrFF/TlXOcbIY6HhR2dCRJGgoor86x7olgUjJx0cWzMnGQcPqxyZrE6NS5tDXJDocvyVy4TKmq9f0XHY4489Lo1ZddCvcpDiQjv8AcXnIJtbmhx3Rwb/cx+P1M5qNfXnx8OkiGSGOKTa2M/GR0eHczSXqLFKUbXQohEWPXsjsJe660LkxjEIf5CEKNnCYtO/6L9o/9wv8pHdjFGh9DG3f3I+cujIfs3DpxKVu2v1P7Tk1RG636nFT043vTMmXX8IWNMQsakKbjHSughC27rG+SGx/kIxNpi6fov2h/wBz9jH1GRiSZCSv7kPNyn0OB/gQ+P1P7WfRHCz14ov6HG5dcqXoPZDQo7G6+/Pyb+/eY+V8q71FEID26GDLqX6LjX/1T+EewmWNepCO4oeMhvHcy9Dgv4Mfj9T+0/Oj9lyvFXszJhjNbksemTXsYo+IkqGkONJfU0bEtxQ3OzRqL5vu0JGk0lGkUTHuzsbMePT+hlKkZZOWeV9TW0vvQ8mz+jJPzfQTuX+khLoRnTIZTrucF/Bh8Iv9HPOkfiJEc1nbInnikcVk1NP4OD4jspN+mxGSkrXQz12jsgt7GxojjbOz0rccV79SOlWztZclXOhxNJpNJpK5UUVysxZmiOS1sKXuSzxR+KMedS5N0KfenkSJZJHDzd8s80lRn/jNml01X+Kxwk79mOPXfqhR6bi0KvqNRl0N4nanA6uxjfsSTIL81yrnknQ8zY4HQiyW/QzY3oMkl5WzIkpfQ4ZOGGmZY+pi6lEMZFxicTni41FnUvauVFl92iuVFCXOhQshioiqNiWGyeJI0uyEpJEpWhdRr1ISsseZIWeyVkCEaJSS6mWeqRx147+zIS1Re49LfyhZI7P32I5Ft9HRl/5ZGVC/qjs76HC/wo/C/Ose7EM7P3NCJYiWAjiZGCQ0cR5ziEY53jXwT6MToxqzKpaPD1JR+tsRVPcU6W3KjSUUUUV36NBDEPGRtnZnQckjtETarYi5CimV7FE/D0JZjURZHcjjXLJHUxYaP2svFv7Ijs2RW8fgxdY/Ji/9xNdPv/cYpUYVrnH6uiMVFUun51GlFd1s7WI+KijLxV9DiPMZ/Q4fJ+7RrjXLhZrTRxmt7LoLFK6K7mo1msUxMysxvfm2aiyCtigUJclycSSQ0JMq0QjS5bmXYjgsjhSIxNPOuX7Y/iP4X/J6v4Irdf5TF1iYvT/MyT6fflRwn8WH+b9HLIkfiE+hLKoozZnIssomle45NnDP92Te3LhHUyUqOIyO/wAnUZGRe5rYiixOjFOmXZ0O03pd3sjQRgUIkyeeXoaZS3IRfryrm3RPORzNM/actWRv4F1Yuq/ymPrEw+nyyfoPlwX8WHz+RknpQ+IMee3uOaE770icbZGDQ4scDQaTSZ+vLh8tRoeS0J7CyNHayfqX3dRZfKiVtkeURo0knUjEtiClVE7ITpidlidn4mGrTe5KSXXYjymmxY2PELE2yMN+cpqPUfERToUkzPPahvlxi8X3NPVlb/6THGmjDW33JVa+CrGtzgv4sPnvPLFOifEJGTM5FjYpkMy0kMonfccTSjSaUZ+Lxx6bmTjEnsrFxuPVXp7mWKjB/Bk8zItEVy/w8l+TqG0zQij0FySG9xYuhGFGk7ND46vQz8TDJGtzBllj+5JNzujPl7SKtUcNNQxbO2cNx3ay097JkUFZLjH6GTI5Plew52J8uM833F0Y11+Cla+DDVonPp8HUbZwVdtD57k8sYmTipDfKjSSKFywZq27mpcnJI1oz1W5qiWjiZ/ufmj1IR2sXKPQqu9R9TqUaeeojlojkTEZs9bFo4fJ2kb5rjJ6qHu/uKHiohilSpUdlPc8V/H9CWJNals36mLL2Uql/UjxeTXVWu7nlboruaaGxHF+b7idRY+svghu18MwdI/cl6fBZqOBrtofPOUqRkduyXKzWzUJmrlQiE1JEsyRxPHKOy6i47T0owca5dTNxUlNox5nJHEeUQiXGT0qLewiPTnHoJdyxLc//pjXodGMT5MfNTaNZaOGm4S29SeTSrPxfaeFeElJRK3MLTinVc+Kf7tnCtymTjt4urMc3hnuLjo6q9DLxEYfJj4yMnvsT1XsTx1uNGkUTJGhJiiyMTivP92ejJ9ZfBDr/pMX+H4ZP0+B8uA/jw+TUro1Inlv4GS7tCiaRFik0W2cV5t+UciiictTs4bozOvCLHL2NEuV10FySIi+vdrknRqTI+xpopDJo9D6igpIeJ8rFxE16kc6XoSnZTOEz2tL6ociWaK6nE51LGcArkxwTOIf9Rqy2+vLBkuA2hwROWOJHPjY+Ihe6McoS6FI8Jxe8/uy/CyXWf2F1/0mL0+GT/4Hy4F1lj8naO7NbfNlFGk0EYEcLMuM7NnZ0QxNmhI4qWudrodmvccYe5eMwuPoZcmmNn4l0dvI7SL6jV9CHRDE2IrnXJulyi4y6DiJUNCdIZLciiqZ5GJ2S8w+vJRsaYmQy6XZ+IXwarJSIZNPQWWS9Rvkr5KbSo1HbSqiyxEJuPQ/ESYs8kcQvF/UUbj19SSvUUlfwY6//UnJX9hpeg00cHvkiOHO/Fzo0EMPuKKQyd0xRKXKeWlJr0Ju/wCo1z4ee1exm8jFzWxj6DMfe1b0NO/py4VVEyuotmG3HcyZNCNikaRbDZ6EGSjuTj7DW1ifqJpoyrfuahSIvbkxflIzrfqQXhXySrf5Gl4vgjS//EyTV/blbRwG+aJoNCHjQpRedx+hpiupOUPQeRDyEcrI5URyps4zKo42cNNTgmQld/JJ0j8RKTfsxvwk+fC9WZfKxcvXlifgRfKKf5GFVFHEvwGPaJnd6UxsW+UujDNyjbJTS+5QkUNiIi8LMhJUMQ481uNHXnRRQomkrnk8y+CPRfJLpL5PWXwL/wBpNb/Y9BSP2e/+ojyy5VFoWXc/8W/qKWrcZN1yhEUl2mn6CgzjfIvk/Z+VaNPszhOIfatP1OJzbMj5GPysl058N5jJ5Xz0+vKK8PKzH1/Ix7IzeljdIavIvobGLztmbaLMKqCMm+SJKVHCPw/Jkm1NL3JNJWRdqxVexQy9hdBK0RIxbPQxz0v6Mq1/XlpGpXt056RIfNsydfsQ9PkfSX+Y/wDN8IXT/SZPM+SjZwP/AHCE5N7nGZZLKiUXQ5bnCW0xQZxaowx8KsSiJf8AVfc7SPucdKMkqOHlpyWOVStE8lk/DCmej50cN5iXR8lC2XHp0J49ztFygkRq9vyEcRvNIk1Qt8ppRgbaZl2juJ1E/wDEv2JvwmBOMUSd5vsZ34GR2ijA/E2ZJeNGWemNnoRakrQhbCEJehGdLSWJ0LiERzRfKudcpGSr+xD/AA/IvK/8xLpL7C/9qMvmYoWLFpMHgya/Y/Hv2OJydpK2fi51Q4pmHM43R28vcyzb6kcjrqa2OW42XsQ8w9hmd7Eo7EYticUtztYkcy1JUS6GPI2/Qk2aWRltUt1/Yaa+v1Ll7o3fRo4eVr8jWVeSzqYfO2T6HDeU4nohEXeRmelAxrwoT8bM3l3EzAqV+51yfYz9PuZH4TD5UcO9vuY23KXsKdya9jUro2v6jiep6GxpODjdmkcB7Goc0SnZkik+vRGPGtt/qZMcUvo2SUVqI1T+ETlG3sYoRUdQzCrP8RkJ1WxWxha1bj82xlVGOa0EFZk2ZPJqIRtCnUx5LILYzv0Mk2zUiXKLqRKVIuuWPb+plxp7oU3H4JxvddP7ClpMU0pdDLGUmq6DmkPLvS37saNNOxHDNuzLdP2MTWnYyeJo1GFdX7nE9BdDD55M4jel9SUqMHkIv94ZNmvky3p3LqJhrSYk9/qzH5pEP4jE/wB4/gvxv45XsS5cD5n8cpIaHEZJqjs7ZjS6GT+zH/i+SvC/qkSVMTOsCNqzF1OIi30KkxeQ7NpsjB2ZlZDG9tzHFozU/UjBe5BVEehyIRi1sJUZIpvcnihGNiUfqSxp9GY8UKMnDxW6JyssbT2M0Fp/oT8MeUW1uir3X9CEMb3GjsolLu2NMyHDLwmd+GjGvCjJvkSKRw3Qz2qNzDIybyRkfhZiVRMXnbMn8SJxD8I9omFeFWYN4/cw738mPqyHmYurH0H0JdeXA9WORrs0jgcQ1F11H9OUUtjJuyUfbqZNoE8rMWRv/Yy7RFkdsgcTKjC3L/YydDVu/gxL/g4k4fxNfch5Ti9jDvb+BmbaaOH8q+WM4jqiflZCRqaME7i0TmPldDy7P4FPUn8C5Rg35SOL1fX8jtBycjSQlRl8VCa6EVeWzI/CzB5TJ50iTpHC+UnvlRnpQIRuKMPqbqf2M3RWZvKLaH2MKqBguvuYfX5MT8Un9RPeXySkMfXlw66ilQpURnZkyaYk2n8kSUaIqWr6FIaMjuDJGJ6f9jK7jsafEzHOzikzFcf9iTco7Dj4n8GObv8AocQjCtNNfUwydHEQ9/Ujta+CDuO5kglJIg6RF2jiXVDlakLKhxsUKWx2bNDOzFiNBoRoQlH6GCUYrqJ3+RQk0+UkSptCjTswvxmZOjHLwFrtDNLYxNJF+OzLLVEi9vg4boL+L9jNu0ZnUD/B9jF/DMHkMG6MPR/JjW33Gy+eCdWSkWRekzZb6Dba5UyBOxqzJLwEiCv/AGJeGJFq38mPHRxTMcU7+xJaYkpeJmPGr/oZ3SMFSaXyY4pKziZGLe/sVS2M0nqRg3ivljOK9BT6/UXh3HJm/qOzqLYpclZZqOHgp9SMUug++2aixtEWRMGxmdiXgQvOZJ7UQlUR+aycWJbdSEnpEmmOVzRmnexKTaFLwUY3pjRh2iY5JIxJVsZOppFaLRFIi9jYkarHRbNXsIk3ynprcm1bMC/4MrpEev3RD1OKMHr8oydB+Z/Bj/4RxBw3mX3MflOJexh9fsPoZZeJGBeH/UM4r0ImxCEH6ng9xdn7lYz937kVj6jeM14/qasf1ZGeNkcm/hVkW/VUTdLutbHZ/wBh7C6GgcT4GUfRjlQvc0bCFSGmzTsR6CkNanY4LlLoJbFEOhjarYyR3NIhxRp9iL0l2ehHk17EJpuhVY2L6k6lshoxpxMu8THDdv6mNs4mO5BNdGO3E0q38GO0+pxEdjEqqn7mH5OJgjHBb7+xDoZYJSIpV1F0OJ9C9yXQXJRVixRaJ8LK/D0Fwcq6i4ezJhcd2aTBTIpJlk/Ky9hST6d3KtzEMW41uR3R6kqv6kork3ZA+C0SlfKKFUTb0JD/ANiNiZGiL2JLdlb8oMoR68mmPYc/REIXJsS3LIvca3Jx3+49jV4SD3+5jgkcVIxJO/lElSJT8T+DHFX/AEOIdIweJoxrazipbGGNxKozS8SRhVr78uJ6EVuNFGNb37GFapCbxtR6jiQTS33ILqcUpUrZnVUjh8b02tmY5OasS3GZt8ZjjSXdm9yK5aUaE2LFsdn7jxkoCxjVEI7DQoElTIJ0yCaKOh0GrKojGzQQ2VD3KOrNHqLdjiY0NErJcqdC9CTppCWwtyWPxIY/KYlb+5BM4ox2v6k+hLzP4MbdnEdDh+qr6mP2OIxORj8vLsVdiVDOJWxHqS58IvGT80SVVvyw9H8nE4/Dsx9Thv4aOH6P5Zmy9myGTVGxxuFCH1O08fwube5j6c8Xc3F1NQplmolH1MbNQnvyoon1ML25MomY+vPY25sXPY2NiuSVxMOBR5ZMGpjgeh+HNJkjqRjwaRRp9/Mtjs/qOJoOxOHhUia3RNWhcvw/qdkYVUUYlV/JxMNSMCqFEfKIl1Rjg9f9ebRHoN0PMiKpctxjkIkqI9OTJdDFVctLKKKJY22YsTSOyfPL1MXXvvlXKhx2IwNO4+pjWwuctyhDEhFd/ItjSxwZpZoZBNDnJiyN9REuqK8H1oSZGckKcl6GR2jD0IeUsRFeIk5HbCjYlRQ8PfXlMbGhlGaCbVbV/uNEYsbZlnq8KIy2LJtyQkUiWMnht9UQ4eS3N0y0WOdPnpXsPc0mhk/CiDdb9yEKRpZFL1JRXoKN/Qoorv2uTMnQknQ3MuRqkXI1SI6m+Tk+0+5GUrZrka5Cm6FbIkeSFN9pR2rNf052Y5R9SaSHy7NklQiSIKmXytIU2/Rsv32NRY8UH1XUSSVIvbkmMjsUn0IuuvQycQl7/T6kF2itCVPcjvL4PXm9N7PYRFknZlk0KbINuzFHcqi+5dnaehoaj7nananaI7RCmmKSYopMleq72I1JmX3K1GTH09jSjHFEoWaSN2KVko1Lr6ix7s0W9hxaYo7GPpyS5WS/iEsexq50ep6cktJqYvGtx8O/TclFrryfJQ9+VmXCpr2ZcoP6mLPq2fXm5JFos1Dol0oWKJGOk7RNbpd1S5uIopKieLSY+gtu9qJRi/kflbGyyyzGqf1I1qZe/JSUWZPKb6bXoN7W+WJ39uU+hDqJGWFowJqP3PUyLv6Vy0LuVyoabGqIyogytXU4jg1VxJeLYjHnUtV8uIxKW5KDgzHPVG+Ut2+UehpZfOyxdzSu7PdEdn3756K2HE0MhCmWSXisj1Ze443KzOjHBqI7baNLMcK+Sd1sJU9/US8P15NWVT7r71d+LHywSsgjJ5Wdm4bPr3K5ZOhkjaMFpblmh7l8qIxEu9f5FflaB4zszSUymVsaWUxqTNymJC5VzY99y/0K54eiIE/Q43z8l3JdBHqMXL0/lT7r/L//xABMEAABAwIDBAYFCQQJBAEFAQABAAIRAyESMUEEIlFhEBMycYGRIEJSobEjMDNAYnLB0fCCkqLhBRRDUHODssLxJFNjdDREVGSEs9L/2gAIAQEABj8C6APQn0JV1IVum/zsdPP+78XDNTms9MXh85I+qz/d8/N3/uEXmwP9yAM7R1XWvmJi/p2WXpX+vXyVv5f3JcSsLskS6/BuikXbx9DeyVlkoAso9G+XRl6HJXVvTt89bPRQ8AHl/c8Fdhvkr36LKSrdErLphR9WspWXog8L+Spm/Cf7qt0cvSzWfoZ/UbKSsIC4K4ssllC3VKe7K2H95Z9k/Hoy/ub6Rnaw5H+4p9KyayftOHwRZPan81P9zYGHdGaz1lRr8xdW+qz0X9NxdZrc+5PebOPqp7m/2UOnLVfYNwuH9yYWZkgW4INpN7PagLLl4/X8rfMihRdZ4DqizVZgH0obf7rkGtlzhlHBX+oWVll9SJiSrm3AdNlkpP1q6t8w0DNxjzTi5wAJPFfKVCTa0xfTzW0sp2YQ0Y9cWONecprok6cHTw5pxPE/PRCvZR0k/NRiupdZY53UDiDZzbn5egBMcuKAAhrR6vTl0yejir/X6I+234qlDtN4f5Sb/wDrfw3W0Uzm8gkeLz+IRpVN5sEjlAlVGjJriB5/Ojpsr+hl6MuMAaotY4AwTif2bKRZ2sJv5Qh7SAbcNlNY7dayBKMGe/ognLNBwO7FinAdmzeVl2hnh8eifT5K2SHBW+q39LrGCXMIcB3IOIE5AnuhAY4OQAt+s0GUyeud9GZ1XV7Sw03Ebo0IIiQqjmjN7iPNbygCVvXKsFayz6JfYcFujovl8yfSwvEgr5AZDekq7ZjNctNAh1c5X71vCJ1zhYMe7Eg80IPyg17liGGR2jz4KpIkO7X65IguHO0JtNluPzMq310Y2xOWsxZYR29o7iME+66DgMD3XtosLu04TI5hNAOLg4frRAVpqYuzHqoQ4OkSperenboj0plX6Ofp4Ysc0amAZby5aN6AXDc5aqXNgTDeSJzHHgminAg7xPuTi6nvXLTxnRWOFxV1l8xH15ztoltOkBijO4tA5p1VzAH6RYclvOGISAfgnMxm8+9HCI1BTnUhvOPZdlBRp0+3SvgA0OZPuRqhssEye7PoHD07KT9SLQIkaJrsElt2ysURDZb95DC3PetksABGqMN3QZxJoBBebta74KH7ruAXh6U9EBb2aloyW9b63imI14LuEDnpeF2TcSP0U0YmjrAcM3yVLFV+kf1Yj9FVqmGXUqnV3J4cFSqUxG4CGD3QsMlp9sWTnNOJgAxOHP5rD9U+27JqAIufxyTy4nE0T5IbKTO9dw1sqdMSLe8LFIPJW7Ppx02KyJV7Bbq+0rq6sJ+qkKvhI3aLvMFbICbO2R7j33X9G1HOyFTrB34oWzWJNCu+oT9iVWpXBNQObPs4ALxqmQQ3DAk5WBVobFg7QgR53TtmdZj/ACt8Z+vOqRv4Ynlmi1kS529OnBGpWAAjTWU2pTOETJbzTabfFOvMdmFf56yl28VJYJUBsDkpwCU4MbDjcLC4QR9Sc5vf5XTMT46xkloHjnqhH3bxYKabBa2RIPwTgOzFsgVVBBIwYrE+5NaN7EWOxcbrd9rs6G+nNROEg3lR6Vvq1zHeu9XHNFjQSRco3k8At8+QW6fNWs4ZrToz+aj0+s9V31J72xLSI43VMvJjA237MoCqCC4YmMG6Y5nTuXybRjxEDA84s+CdSxSIkRaRz5qp/gx7woOcs/1BO2ig0wbubxvwTWP7WkZ+HEI/j9Z7+ltYfdKZTxtlovdHq8+yCUXNieBT8TIbBB79FyIC+7EDkh1Y/XNf5mHtfq3zfL5gMH1Kp3hNBybhd+7Tb+JCJ2ks6ujcvyEnj3ZLqtjc3rsm18g08tXfBYg2pj1fVsd4X81WLxv9ULDwQfwLJH7QQgY9Z1iZtzsjibIqEVWn9q/xRboPrLBEtwuyn4dDofgdoU1jOy0Wm57/ABUW5ldd2mZF3NNp03QSc+CNLrA+bOIvPit4T/JYQ0uY42A4JwbY5HD8xKt8xdQoLo+pu5vHwRY/1Sc/utXV7RRwUKjsdGmb9njCDGRTpukMeBYW05ptKo4Of2iRa2khV+TAm/fp/wCoIO/o8Na9r5cKvZcIvHiqdWq49Ye3TLcOG4PE2Tu/6y3kJ/iChBoybI7zxQxOwjV3BOJyuq9IU5aRuHi5dbGB1U4W8UWus4KSJGqZtdBgaawaGNdy7X4K1wbzln8zz+Z5LeVsj9S73/gpBgtJt5I1XuNN5EW96ZTphz3N9Z0wDyW+LkgucYBn4qrHrCNNEyd09Yy0faRY19mwGzlvlBp3muyng5wEecp31lx0FLU27St3253UVGzw5I0nX/FMa8B7S10Nde8LuyhfKARSfiBPL+SbXEudVeb/AILFVc5lV4lv2fBNo12sqPayMUcEzq4aI3hyX0Yy4Dy6OfzufRizXP6lCLCMnW/XcqrsOKpgFcjkY71SaGfTUTVZ4DJbCXMAbtBc1/gmlzxubV1dT7v6C2jhLSE4i8XAB9YZIuoksqDKctOKDatqpe34hO+qXHRy6MTrN4lVXCN6m0A5goPI3gILe4x0MdtIxM6k7vObIPojqNn2e4Azn+fRgqE9XiDntGoGiEtHyb9zlYhOcTDRdxTHOqQH3ai9t2Uxhx6G8gBdoZ4uz/D3fU94rd6ePTf0r5LJSM+jFwTmgtxb4uQMxCp1XV2DFs3UPAveFsrusfUqbOHMIAzBlUmsovf1VTrKLnHjHBVf+nYymXy/FLi1/FVsZDXMwktAGWab14D6epHalAtIcB52gptsQHrfd/mj0W+oz6Fynltza3inV2NLqQaWDgCLjyT6uAOqt1/kgTbiqlR9MOqYDDvBNAMMcYd5dIxvDcWUlOZRfJJwnuXtHROZ4jpn6jZW9CemPmIChMfEioHgjTJUfkhgqfJu1v8Aopj+r3tnq4ahjQraKQ/sagqs8VtLnOllSkKviIVUR/ZUZ/cTcBFgQR5e9WmjV92fvgL5ZsEX6wZcb+aJGRy+pW9KDdFsaulOYfa+PQ6LnCUY0M+KY/UhP6n6TRN+UxPiXk3v3oNJ3SdFgdfCd6ERSbvuzfr3BeM+lcgd/wA7b0LrmrK/Rf5mhHCp8AqJ/wDI3P7y2sTltDAD+7+a24/+Ifiq0abGP9ar/dpf/wA04ZFEEKqx0kZ34EfVrZrLo3rhVG8Kr2qq3mFK3rA2T2hBs3aE1lFrnNN34UKZZhceKIyA1P619G3RKuFhMdkZqoW2Y0aTmVmrX5q6st4wFOnzN/S7lb5il9yp+Coji9lvFbRb/wCsb8WrbvuN/FbT/wCswfxquNfk7/5YR1v0VB3fj9TsFfonXpsnspVMA6/fAz3luCJAJPio0T4OFwggjvThPFQMiPw6GFoIxWFSfMLPp19GTdV/sW8gqzj6z4nuClZKI6MDq9PF94KxluhHTJt80FdQFb08k0cKTj/EFs/32p/2tuj+ILbv2Ats/wACkP41tE232j+AIjn0VuFvh8xCiPFWWSg+nHpTwRe5kF9THj4kIfc/JODnQn0nVTcXgI48btnk5Wdki+ls7y4es56f1dFrTG6SdVjrPxv5+jFakWcxf8ivkqrSeBsff09ZWnPC1ozJVWuXFrqpJiOKOzteOua4uLeXRPQeSqcGl2GQHZ+CoNGeCT+1dYo9Jwa4Etz6L9OXpWRPuUQpU9DjPZpQYvEu1WztnemcMGYAKY0FzsW19Y0htjvZXyW1Dqnw9zRpIIv5LaiWgbtNrrzk4qrJ/tQHQBwHFRWmfajhxU6FVeFvh6UgWWS59EgImELSuzdX+Yw0t+M3aKw3dDKLizdGqqu4NKpMNi1gsoFQtaGgEAA/FdZJtZEmZ5odGCbDPpz6eSgrcquAGmn5L5RjX88vzWJ+Fw0Dcgt5je6E2t1IxC1uBTazTuOFp6XkaNJTiJkwHIDgI6LdG9SlvFrvzC3STr8nf+XvQmYsGtcR/NOdhzM4mn4TCDesLO/dnlwRLpdAkHioHa4elAW8ZUDoMDoHRXs4kUgDA5n8Fs8tdibJiPslUwGEzXlsx7SrHq+3UAz5DktpkCxY0x94qriefpoJy9lRUl320X0XxxjLxCqBwHOO70LZLj03VkXGy3ejK6LtfQuOmVU6pmKRrbVdlje8krsseB6oJB8JRHZ6xzW30koN8PNVWggQfgpdkrTCDlJ0WfoTk3j0dU7L1VgPZPYd+CuAeeS1Hf8AyUth3vUETyU6HMJlGox0MtjC+SqeBsff0dTQa1wI3yfgo6oWvAKxluB0wRzHS9waZHG3mmHjyWN7tzLDl+j3J159khC+l7LiNBqsIuztFmiltqg7U5+Kc2szD7PoyFzUqFZRkVc9G0T/ANpn+5UDycD+6VsJ47Q7yl35J4//ACWD/Stt/wASj8FXB12k/wC1cL/ipjC7iO5VJdLZ4cvQj0LrJYRYdEI+gMFI1XHggBQiMpQ/rb203nmi+jtAFH1YnRYDVc7zAR+4h0S4sc0OxhrpQbUbgOKSNONlWqcXfE9IWFvZ9Et4ZL9lB47uag5rq359F1n5o6QSFi81bJEAnCcwD+S3Se7NWPkVTpA4mvMOBWI5Jjy0hp7YVRgeG2iDnPLjKY5t3suN6bptR1i65H/PS9wMGR8eSa9tQ4cEvEuz8ShhgOAyUNEcL68E4ZYbX43/ACVrDipG9xCuo+Y2o/Zpj3FUf2/gv6O/xXk/xI/+2z8FtXOvRH8Kq2E/1l3+oI96uqvf/tU9HNX9K/p1PZmMoj856JLmYB2ZEoU+HDmmH7P4rlhKE1GjxCs4Hx6MD2m7N1w0I56FHPz4KehpW4PJQfQw+PRGqbDrO4Jrhc5TqsDhcessLs02OCcON0R6r7g811Z7DuweHJQTi4Ygt5pHddYh2TKxtseKjHIPG67AaeLP5oOq7M2GjC2MgEfko7ihs4kOYLTqOiwUHJzmhGdG9EngAWhVCwCRhOHKXI4fpGHDUZ7LldGMjcLLosPNdme5Ahy3una/2B7lSP2X/gv6L73n3FMj/wC8atoHHaqf+hHntDp/eUe5Xun39b/aoVuifnar27xMPEc7BFooQ4XwucBKBZRGHXMkeCIgNOYIYSCmdaTiw5EAEeSYzFhOYPcu1uOjG2BEob2mFxjtDmnMYA9rHcHZclg2qKb5ifVT5d65jz5LTuW+e5ZytzhF1xJz6I16InoaypID8geSaXA7uiI1mY8E4ZZJs5uaLlAsneCtfRNmQ5YfX/FYHfSNUHNeagdpuXgg/wBX1uSh839bNWeFfwRqgYpEQUMYLfei/rGhjczKIZWZ/Vmn5Np+KZJpmi8xU7uPgsgeCsLnNbQGGH9hruG7ZHG0BriS4/q/QDw6Ji/oQRKyWV1tMMN6oactAAvonblJxIteYyWxNbS7DXOpuLuRsVRljBi2jFmSQQ73ozhBdtTAcPdmmzUd9K6YgXxFRWBd9oZ35LddPLXyTj9r/b6EfMYjkPQqVI+jCNNo3G0RhJzJ5q4APLpFQuxdZx5Kkh07wh2jk5vsuItyUR4yhhy9Hmg6RfTVH/uLTyVJgMQAqTHw9pcBcKo1tLcgNhqwUH4dbynN7eGRPcvo90IHWZVo4p5bYgz4Ilro3lMbwOErA4ZSjiP6CNPR36966p2R7KwlEc1J9XtKFYgrsxHBWvyTKJfNUTu6xPRWv68SfsgIZDFlp83XLaY3q955Qqm4LU96+hLeS2aGtBFIuGZthK2dowjHVMWy3kyXZ7U1treqPemOLz9I/XXEUMVzx1Wk6TmnEE4cRGc+g5WHoc1HRzNgpXdboY1udSq0Ob71iLvVIDdM1Yz00u8ql95D0Kzf/I74rIeayb4qGxMzu9NvRIi7Le5Ux7MlVjiHa/kq9UeqxHME34J4fcuf8EAWDOLGE1tEFu4MXenmlUG40ud3KDvFQ5sTee5EOiZkFD4o70ET7kPWOd1vjeai3jCc1GlO+zslYm2e3tNUYvx+Kpz60e8LE0w5hQeDB9YcCnf4zifEwmVWuLdNSN1NbMxr05+nXPHaD/tVc/8AgJWxDjszpPgFsIH/AH3/AO5MnL+vn8FR0+Vef4ip96uJ/QTnCwxu6AOgBx14cEPG/j0NnV46ICd3BZqnPtfgo1zsr+sSfgjcTHxVJrb9XvO5BU5+kLHDwROT8/DLpp/e/BUvvDozXVYh1nDofhgjv43KOEWUwoiI+Yrl/G0C1lULHA4W7lpzPBPc3NzzA8VtdXUtLR4NQxzhyvwWzzk8yqTR7QTxwgeS2ypyhbphUmDNtMC62mq9t6bd0g6lNa1xaSYTqROMtsm4qe7kCnB3eENOaBm5KxtOXHyQfEn10H5B2iaQZiPcE13qvGE/gsDste7+SDh2HS+e5yeXEBsETzWSklXKsfTdz2h3xW1RmNnPvlbJ9nZT/tWwf4tQ/wCpUue3P/BbPzLvi5cE9+jZKw8ySrKPZj3pz/ZBMKpUAMg48uLVy6KTY3Yd5q5v0VAXDtn4BZpmHMB3wsqLmS4tifJP6wZ9ldabMa28HxVWuzsNB99lSbAxPDr8kAR2rSFbob95Uz9odG6ZqHII7QGY2Ou9vrnmETOO2EP1ng5OrvbhZmsgOCaGCSVFQtxHMa/MPOpVerw/BDAd6JlVHOMl03QY5gI5LZeubYDegfZgJnVZZ4Y1hVo9uFUw9p74jxA96p03C7jDU7XCB8FtFQ2xOFlRH2lVd9o37lsjAeJlbU97Q/C2AYvJVOmQWh2bmrBTfeYunCoMWDPD3ShjaW214IDVAGyaA6QePJMqm9QFNFYFjPVtMzpbmm4XE+sMOeV+BWHA90YLugdvs8c1HV31wkFXBHePTK/z3/Fbdx6n/wD0qP8A6n4hf0dPtVT7itn/APcq/ELZucn3uTiTkBM5KoMNiDcqABmohE8h+KIN7ZIiYkfhCF19nUoTpNz0H1jonzrf3BGDnqFy1JUvsBAxHjCFwQLggp5c7dd+viqj8HasI96YfVvJkDRM3ToZtHBONCOq0xOI90K9Rg/eKe57mv5Yf5psGLjJAVKzzyxFNqP+iGQOv8lge9occmyjtGynDVPaZ6r+9O2ZwLY7dE9pvdxCjq6r/BU2CjUGNwYCbCT4qrhp2BO9ewWCBnd2vp9aDnvFVHe0HOHcIH4rfHK4VFhHbj802JVOmNGouPqt+KeYzJk+Kp4Tvudi/FUcRnAe1CrtczHeJ7hCpCpu0XVN4n+XcmdQ4OGAkX9ZO2wPs50x3uWzU+FK/etoqR23hqn2WON1TMyZHxVfvVGkNKA5/FbP8kHOdiJI3Sm7Q4vbjeWxnkjVDwaePCvo/k8UB3NUxUHYdey67ZyalL5X+POVh6m2Gm2D9ns+Sc2lSY9k77y6DZHFQfcgyx05d4CYRabxqORj0Yy7092bgCcKp4aJwve9zL8zmtpaNndvN6pxJsDvcFiwB9alRwvdJEiRoQtkHVskNc+m4k+yc1s+BjWvNao5gvE6rZ8L25dmMs7I7PaCG9Z3BREg2jkUwtGLPFIOuSBNjAPmsQ3hph1/UotqjdGsLAHDrXZDPSUzqbHEcuaBrG8Yj4Imnk2x8xdOe11pEwsTpdMhvgi8WD2k/CVimQRkhSwWtingQjMxaIUC99UaZ7DSYA5I7RQp4wcVPMA+rxWCtR9cubiIPaGSax9IdSIbOIRhCLaTDTpty0bcadDw72UCOyMyutq9nhxXV0e1l3LUk0zVnjBTdk2ierfZmL1T+SxNOCs3sVBn/wALqq9PDX1GjubTxQfTdhc1wcHRkWqo59fFTALiAAJhV31KbX7SThpSL3HuRrMouNMagJ+0bTiotbGER2p+CnT0Ixy3VChm31oyW8NbKiDaSJ/ZbCpdYZE3bKgRZjZ0uq7z2hu/qVjya78Vs1Bz2gBmJzdZ5+asNLwqpJjeK2ZmpT3+zTUYnAOdleFTAN6bGiFBg4qlh3XPwW0QzKkZKoYTPEc1VM51DPmnOpOsxrWz3BbMCJIpSfFbJiydjcqOgdVc5U4tjrG45BbO1mEudWN6gxCAOGqAgdjIWGuQKaIyIGfNVTA3q2h+0uy4dxn2UJJO962eXRAUpt0JOZgLrGvG84CeU3VLCQ5wxOEfa/5VSrYNLhiaZu66qR26tONwWFwtjJ/7R/0lbOxoJ6qrULu4qnh0DhzTnuAxExKxOnq2kB3IYUzA8RBn4J0OHajNCnWeGkT5IfKg/qyLmOG6QfAZqlOIHtZHLNVWh98MBY2gvDyG4Ra/6hVHGm8XgmNYVMCbE4hroobRxsDZnWHae9MB2d0ESI963Ww0w38ERDXMyjWE7rKTdzNPcWziv3J79mY3qaZ+Ux+07gm0aYa3M7vLvXapnvBCbSrM3s2kcYuDCIpnCRcN/wCU+nVYDaC9tr9yBIhgyUcEXVILt5+7Oqe0mQWOdB4gKm0ujE6cQVzKw1NLtcMwup2nup7R6p5OVXqrww9ZTMAwdRa63242A4sBylOqCoQHjC1smAOSAed1uTbwvw9AgPaW+1kFHmm4wMJOZVFjQHQ2RK6x30naVWRL8u6FtVaTF/cEGSCJAAhbK13bjPyVSGwMAk/8LFlVJk58Vs9KcDAzEPcNfFbbDrtBaD4FbOx1xUdAgp0XAa2PJbK3LE4my214/wC3humHMi8rIXqR71VDXEEWdwyCpAHERSAgrZOtZMMyC2IEFo3lsbWGLunvWwXlsVHPHfZNyJMZm0RyWnb/AJrFuxjLpiBmSshyh0JvefQsgx7zip3IbbPmqVE7lJhwyefFS42dbdVYB/ycef6lPe0nISWm6Y2rUdh1gmb5FUyXOOJxnyTdy7puqe8717TngujjviaZlM3BdpH8KZgEBzcR8ygC0dkeWapbgx4KunsSUMOfBUX/AG/zQgQA94j7sKAdez5LaTFxgTwfaKDcWTctdFs9IZGnVk90poN5Isn3nD/ytqZNmtpke5HuK24c2lbKG+ti8RhVtHR4rrBBc2pbCcraqm5rcs17LBn36/zVsuHQIMFUdMWNhLWAcY0WyFwsXQThi8x04dqcA1/qm8p1LZXY6HqF43hOihR6UYt3RZ7yBy5BMLzkAAeXNV8UTTIYYvmVXrBhLMTpIvkU8uzcJ83QqYw3xD4rqzGHADfMdy2ioMo/BDK5CosHqsy8VtlbI5e5Ug+sXNaZuqpBB0I8IWxhwExMeFltILcNPFBPfCf1ZkYLd6ouxWL/AOaqlrrOqR+CI0DWhUWublSH4rY2nLq5Wwtdq1zr8ytlBJDRTc/cJbm7ksWMw+BGdpRO9qcvslNJxAmcxfI5rNuubfvJuXrZd/TJCc/1uy3vKovB+XLfljlvcVizqCJOaHWNxMdu8FVaZFexYeIOibAuBCbbTXgqQGUn4FMng74qlFRkN62ST7VgnU6joIyLTqmS64aS6x4JhFRrsADCL8ymuaT2YeOdlTeSQAypG7o+QsD95glvmFRZBMv3finODn2x1Lgesb66LAWy8OxNPkqzHtcGksa6DdB4BDnl0g8kHtaMTG4TzyH4KjUNK4a/Dc6zIKAY2AC0wfNVKjADIGJv67093VNfiDQTBuPPRVGgRBcFt1GY6wRPCQv6PeXTip4P3UanXB3VVS+Iz6zTwTi1glzy87wXWVHDk3EhAp+aPY816vvKu3+FyxFnduOQ3Dfs7mqgB5PJqw4anC+Efig4037ownX4LeEd6Pp8Qt4ydCs1OWG8qsTV+Uff89E+g2DjEG17prTZzg1scplUTh3g6eSEZBjWzMquPWh8+IgKj1h3MaxM9Vgm6OrqpmP2gpAAwtmTZVSRJe4/FbNTM7tNO+3U/FbRUtIYbqi5wEzPuTsMiasZ/aVQUncGkHwWE7/yQBlUBUbJ6kZLZg5uE9RIH/CouxQBs1mzE35oCDAc3XJObcOwPO8eSpOqnAyMgeWU6pjjUeG1Lg6XlUyDNpV1kt44ZMBf1fqzFNxDyeOSZigngOM+Shuek5IRd6eC8+00aXzVXFasMLWgX+8iIJ5aqmQOZ/dVOD6rviFTaaQcCx+p4mU97NxxuO8mEDA+jdn3JjTTa0OwvtKxBwLHMFueqpRhg03Hs+zNvFEuhhdJta62duLIxKLS4HEXMyHqppk42vnlFltFRj4czA4I06jy4NJiViB7TII7oVANe7eDw+/CYQdUcXGRMqrg7L9/x/QVbASB1bXD71lJvMraYgk4fxWzPBvRJ8Q5Pa7Dfenm1NJptxG+QNlEADkAjbtZxCnjmplbwlRmB2RdBrW4MOoUC51XYHkmMow3HLnOIlYqrsRyTgzMtM90enl+yc1hN/zRjTJSc+COc5Rom4crWVOlr/JREngnxwGfJOf6jvzTaxbuNM4RqnPYSAfVB5LqWvmH2Ds8KxtJFsPmhuSZmUxzi2cPvJlNoeqDJPNbUHdotz5YfzXXEEwCB3rG3jikeaaQfpXiyeInsgeSjD2abWqkcx1YEpvWbmFuFmHggQcT5MzM2VRlbdLKYfPGTzVJtQY+rgNOVvgoc3emTWxXHuut0txEZN4HmmOe4DEYbiCkVARke9MZRypnHUcBkRldH+twNQQB20DTOIgTudmfNDCN68yoeYcLjmE8BsMaN1RJlEOpg+zB99lFOzjqqLCA7ductQqeIH6OqbImJHnqhafkn/BUQGkbjcyg0CN2b5XAVLG2fkqpz80YGjreCoHKX3967AHylTjyQtIc+Cf3VtRsbM3SnWAvosgJpgyO4aLZwL/SAyO9CbHELDvVZtmn7PiqsG3UtdkOIXn8FX7x+KbPGAqnVgF4EgFf/H5NugabLa4oF1vUwAeCOEMjgSocPELAIk6tmVvSB95btVtlhkUzyXV06hcXZwEP607q8PG5Xyb8fG0JjQYJtIT7jMW19Bj8WCD2oDo8DYrI/wDyOu7Lcvz5ZKJj2jwVF9I7rB1bh8EW66KdV8q7C0EQnPacTKl9M0C8ydJXWM0vyhOc4bxOvFMp4hugbvBYZ3RnoutBkG3iFv2IuTpBThA3rOjgg0bzRck8AmVBduR5FNEw5+8TnZFhPaR018l2oWJp3tAsQs+Zc48kHvGPGAJKDLHuGS/6g4nwI+7oqbR2iSt3FjLQ26p5FgO+3nNlVq1SGCj6o9aU2ZtYQsVOCwerJj3qerBPGU3Z6tMdXWzMqqKRmjis7khoofcPvbNNrQerNxymycTa4ELGNTcZJoZbVNq1GW+zqqZDXRgt5hUw0OAwuYZjJ3mn0ocfVv5puOk61MzcZWCYWMe1zd25EJrm0sm4TfhH5Kn1lFx3DG9o/NEMYYnDBPGyogU7FxLb95TvkC3NwOLU56IsDbziE63H5Ks19EPBwtcJPNNf1WBxmblCA0Ftr8ICo/IAk4iJmxkrdp4Mjhunvbh7N2wc8496+ga8YQJLcxZOGENgmy2jwVMam6PMKSF2QbzJUluN8TGjVoW+w5jf+V1j/kqmTqdyD3KKYOH2zwVNr2OqBzQahD4uUWNJEX8FPDhrKzcyfZOEfmU1tSoajW5TdY8JwzGLSVS+8FgYJdMABEVBhcDEejVqY29cXBrWGMsybrrCAHdYIEJpNiseh4JlRrMRqy+OELZ6jrdYCSt6C0kFR6nBOv2iUziTMtzTq7yKri/DcaBMrNaG4qsAZaL5Q73BODt1rrkcVVqVGhzbNIKc6ixrGtc1uIcVgc4A8Tl5rDIdxiMu8ZpznEBrCNYgnlqsIMzM8u5Ntv8ArSnVXs3MWCxj4Jj6bSykHNBJOK6vMTZb7sTuKptbmPBOxDstLjBhO6jHiDSTLsx3KxPVnNpzQtcEXjXuVzuvN3K53JzQA8Fh7XFWs1h7cFU6r95sXDbXPBUmP38QxAg+qDZFts8R8/5rKHZgck5zHRFyFMc7IfYp3Piqbnmo02PazBzhN3yG1WwHG+SG9HyTsuIgpjXukYQ7xIWJrs2Q4dypRUcMVNxN9WqXknitmZiJGKO8XWHrHQ5zmmScmmybLoc12Ie5bSWuMjAWwTzCLC44W5SSVjEh2EtPkEGgkhkg/vFS9xOQVV9PN8fiLIQbmiD+2sRzlbR3BUo16DiCpMJGFz96eAv7095MTdx5lV9q7Dm71Ro58O9Nq4cbWm7HZEcE51Cn1VI9mnMwqVgWta0PAOcKp1bYtOc2WL1hie5p4nL3Qn0XjrKUgP728F1Q3t0OnvuurxfJzijmmd4VVrO1vD3p5fcuKj39Bj1RJ6Osa9jXvJnrKWOw5rqajKTYuHUQWzzhfmsuVk09TSdQYz296DynNU5p4GUxAbMo9X2TobqTPgLLIeYX0ZPcPyVLcw4iXEc54JmyXApw495lEmJUWWPjUN+5UqeI/wBaYZDNMLtT5IY90jMi/wCpQIt6oTnOz5auQk4QOCBab6kpofBbUe73Kns7LY6mKFJdPJF+iH3YW0vtYNE+KrVv/EQe9xUe/W9la0Zc51PNDHZtxe/NE0hhbMSOQQ9/BdnedYDS/eqYc0YqgMRzyTmtmm1kjd1vC3DEX4J7hpwT6k4cJkSdMo81hMGbLFMjVoVRg7LmlpPemDeaW0wfG35rZabBOEOdc81fLqXfgqRbN2DNEgZtmZTNcLerdGmIKI7gVs0e1l5rroNt/wDedBTcjc9+i2nEJacEgcLrGWluJ0X1sLotMAWBd96yOt1IyRpjdaeHfKvc9G0kmNwKj3x0RzzWEQ6U5p+xHmVtMuizSG+1fJbo3psOikRqxpT5YGW0aGGZRdqWfgto++Ux3GjT/wBKezFhcIIdmhs2LUb3eqrwLnGPNXNk5s7z3AxHszqtnLmlp2mpLyRcez5ojgYPRQE+rPmg6m/rBGGxtZHo2eiAJdEn7o/n0DdFtbrsDzIQkZWvcHzRfu9YfWiPCyokneLATYFWNsOllgiSe78kBhFzIAyyNk6nQAfRYSW5zc6+ao5NwB73PPAQsEg2mQrvG7Cz0VzdHeWz95PxWz1QIqPBDnchEdG9f80LranH9WTzOFwLYOQ7UX5K9dvhdduQpbUEidU69rz8CgeOS1C2QyLsYRbVsJ/Jzvj0d6tkm8RkjzQGhN/NUn3lzIdfgQm4WT1LYEnPEmwDTw+9MEXZaeSIFMxG74oBjcQydhObkajRYnE1qptwEQZv4yqtJzCaLxPcD+SxlmLxj9ZJ7XNImNfZuurIJDTZ/wCuS5ejJVfdLpYPcVS3DGIL6NyO6+JXYcnAtdNr+KriJlog8L5oHOHdAcJtrwWBzHGKkYvtBfRuyIVR3tGbqieFJg8k6xNk2pHs+5PwiAZt3hTeVWEb26Z7imQLtqMqeRKLjnn0ACqJaOyJVZwyxdDarIeSfo+XMp2N0taSGDgOhh8H3vKxF2R7HFVS5oxNOAUzniOfksHE6/rkuqc4CGtFNhzyi/NVG4y4A2k5TosNMEv0wqm37TW/6UQbS4D3j8kG9f1lGq3E1ujQTlbuWDCHcLK9KNbO/kmgdygtyUSQqFKo442DehpTG0SAyn2XEwT4IN3f3ghIuhpCr1KrsTicMu7k+nB+W3QdAfQHLPmrHwlXK2acm0vyVdzTumo6PNT5oLS7f4ggTpkhabXKb9owPMKj9w/EI/dC59GG/JF2t81m7XI6LEMU81UZiOK2FNio9xtP4qRPii3RZ+iFUxOgFtk13WtwgglR1gUyD/wu2LIsc7PgngPcQ8YTlxWOm/IzDk7iFWxWOHdVr0nVJB0vqmjEFiLzJ4RoE1vWHcbhGSJDpbHFMPIfFd8fBb3GCntYU35QndbbxUQx+8MJgSs/WxZe7u6L69FBhbhxAwW2HBOKhcAtwyhi11VMHMQSOf6KpYc5gd6NQN+UO89/uTjGIugBHEI7brW8fcm0SSW1HBpveIJOfFVmNpANeIpA3wNmQApcYxbpOVvBBo01RLiQm7UZi+EC6Lpgyd3goDiEC8Yu7NXt3JsmWzYo71+HJClG9JdIGfvQotO9iBIduxHmg0swPYCS/wBpdh3krsPfBQ6uTUvPs/8AKDanytWJwZALstX0zpy8ldjV2PJQaZvdruSp0Zwl5wz3rBRkhowvJ9sZ9DAM5WJ9mUm6zYeS67q3MaRAnWFemfNdhw8V6zZzJyXannB6eXQRoc/RBIIBuD0meChwHlwQKNh5LRQAJF7LRHKdE5TAnAqeED1clp0RCOJNc3xTfBFOTXwMWCfghFraFfSP9C/mUXsO8c2q2Yge5X96yJTIPJROWXw/BYoEt81hZebRmmtOWL3on2reLjiTKNKQScQIztbTxTocGsmGgmShhuc5C5qCgabnDq24ZbaAe5Fz3FxmSTdGc9FmjJ71T1wrGJQAxggRbeW6TnpIse/JOwuBndcZu1dVUbut9ZP6qMWHdQHWTnucOaqEHFOq5oXubKozrOs3d1wHrKZ3lLcuaxs7eX80HVC1wI7N/wAUOsE4RhHctmaxubvxCZS9s4n9w/mmkgNDRAARCML7SbNjkhjqCOWabXbWa+nZ7QRmOap1HHq6dcvDTmBBUtqNLcp7kYe33rC3AXZm/OF6vDNY6uENmM0x9SMNTsQZlNbG7SMA/eTqIp/LugsqT7r2TaVVuEuPI5mFX2aiGvds7sDnARIbYHv4qvBl4NuV1XxfSuJfP4eCvQz58fBVzUZgp04YYvvuuByQlshMa5mEDee0G6Et6p7rF5uOSqXktMe6UWtp2bTuZ5LZ8LIqYRn3QjjjrA3dygwmOwyXCYbEhN3HZZWVhA4dAco0d0S5nqOuhi7NgAuyeHouGTlfW65adAfmWmVjjDyXepIvxWV4N/BBz7Nbgd5NKBNmts0clwK7OeTlipmZ7TIzUvph9J3HMFVKuzOMDNk3Hf0iGm4RaBPcmtqMLSRiHcnVmjdYMXln5JzYn+sDICTxsgcL2s+0IuhqT4INFNrKrBvEGcSaaOLEBvE+1yTqjfWuemALq4t0c1HrBaQt7dMW5wtneLlu9HijUecTjr6QdhtxWB04YgAEiFT2eoS2pTqbr43cD855yurY9xDAN7ECvpXYv1yRLqh5ZfkjUfVIY0S7s/kqdVr5o4owGA7FxtpC2Wt/ZNLmHkZ/FeOPlh7PnKj1QbeK2faqm/Sp1RY8oJW2OZfGdw5+sVVZUbOMZjgRn3LqaO+2N8O96wYRGmasYZtBxvYJjE3X39GKoHCqBhf3xbzCwk5mAqrXAOJtJ0jUIRqmSTAY2VzlQVF1f0J1yRbjdh9mbXUi0Ltn0jz6Lq3Rz0VgnVXMDR2eCFIWoTIbxgZnpp0ySH/ZEwoDSebrfBf1aoQBW7FrY/5oh1wfeE+jeM2HkejZ+4otDsLls7C9zndWBJnu1RaRFIMII+ysLZbUpmxGa+Ve5w4OU8ELxXFm20PNbzZHKEGhhBblZPLrPHZ6YlR0Z9A8QU4YhjixhYXnD6QZMtBkDoBHfHGEazqZuYLRkiDJjmru8AV1bXROc3sgzECqmzeyS7yuqM23beZWBrpPa9WMXxXUwThkHvJT97DOkIsa0uhpbTDJxOxZhbNhbh2hryKjIOLetksNV4xjtNiD700bvUHebBklYa4ljgY71hpENp04l2p7gjLZpNddh4IlSpjfGTuS/wCpPyIY5z4scvzVui6j0DxU/NR+r9y71DvPo6s5DRN6efRsrtMeHzsqLqj8Djkg+mC7d7Q5K+IeCptndpgtjmsbHRPBU31Je4XusD6TgNdUyrs0zGGoHjhkogeCzCHJZLULtKQ9qvB9DVWU3DuIXbd5KZWQPeux5LLoHJZrNS4SvWCzdfNfSk/srtT3tU4qcnVEfJEEzmmvMEAZaL6Ni+jHgjuCSZJlCWzAwi+ikNfP3ip6px74UFkE5JpjLNWUnobeOa6uWxx+Ysj8363gggm55odyHoBUc/pW9nPPRU+32f7fLwQ7OZyy6D9F/mL1f8vJUs/of7X9ZLReqvUWi9VeqvVWnTqtVqtVqtVqtVr6OnRr6eq9Za+hqmZ5+PgnfSZesvyX5p3Yy1Q7Hjmhl+ytMj2v1n8wc1r0f//EACoQAQACAgICAQMEAgMBAAAAAAEAESExQVFhcYEQkaGxwdHwIOEwQPFQ/9oACAEBAAE/ISyE4JyEWJIBUy/UzrgblTXEoepVVxM/0Jf0hrRArHMOvE1uoHyi7ERLlQgV9MSkXX0LcIKqghAKMw2bCRz/APMFuoNG4sF6ag4N7qtNdzxZLR+jFmFPZBXEEzKpeZdMFzO6NWdzkK9zBU0ridLHmANylkvId/RuIJLJQlUz+gvKhUyFgcMCrg//ADqVtMYNW+4hs744n2THf4g1H6E5mZOAj0jDW/slmjMHiWPj6Ftjr6DMtfUufowKnFcw7zCvcu3DpEJmzBLjUSWyyhzLQFN/8JIGSx7/APicrjlmgjt0PJzyePpjn/CpplzFtUplf4YoyIFA13HdF3+j9DZ/gN1LEGpfMJSbIYYoKIt/4yiJXkG40Vpous6a+P8A4gtEOkuLSvBr1HvN0sW8zi3C+X2+lR0AW8S6ogmIOebiUafRHoO0p6iVBNEKbqiCdUQKCIqw8xslw1gi1ODuN3uD/ZnpOJqCEuAPpUx6S/8Air60sZLA8z5Ig783z/8AHNNTPvl6TIZuoduoDHgkwWkKKl62ZaVWIVrM1jUTFMcwmn0DeosywsQ1iL4rEb0zbUIOP8Kg+5Lr9K/wqadcwDm4HbhMsGO5YlHEr6aldsMZ25T9Y222Gy85vt7lwtxM9SoqoM//ABaEaoas7m/pMxMTDH3lO5UifiZRFyuHKuBMZeJrIyNo/S2mJ3UXr/IZ9SvaOdTEZvudhCtr3C1lR1D4Rb8i5oD6gteaXTZWX3kY7TEl7w6bxeIKaFF1Xb52QWp4kR1Fv/4bEVDyz+13ncpL7sjtQKtEredTHUO6de4luWzyR7JSwTByyx53EhGFf8QCXK4DEuhYCbQpnQ+hhEtsRKhFcFdTefDxcwoNAcXW+T+6gOOotf8AxVdU9Fn9TuIHSHd7/wASHKUZ6ypf2ShuNS61Gbl9S/vARTX/ACO5CsG4RIpFGvoDcSIiyOwtOHqYEIvtWj3RLxeVsbCse5xNH3JmUU/+I5tHDsJz+IfS1WqvjPM8zf8Aqit+Y/41OBzLb4lpLldf9PK2hrUKIQWmEIIqh4ixKPx8ua6j3HBWYxXHehZ+8Y2jQst8nitwa1pMf9AhTdblzSq3cbptfJFKSmV/0KBpcHHl8Q7bTWEF9S/LcVW7rj6QiQQDcUSv8KlP/TyEURoQJG5iKjf4ZvzQDzlVLQNKpYarfDfcvDaBgKsHHbUruBhfuPBhGcbPjN9BepsUWT5/5dEYmSjXqocBV+8ozfLDfRC4kbQL5lLjiKJX+WSrz1ONwuXDXlJtaXrWRrmNmrfw+U6KLzX1J1VfMQVoivbIK7d/R1Wpp7JuB4MNLzLSil4JotzPEM19BVYRusy2MbgDz/0TkQkgbzDmlrUw+gqiqW3qN3wlho5tnCuCbfYfKipWyIfdDK9MMUirOXAD9/xKzwsNUf4K/wAaavj6VHA1cICsEFmpiWjGVlPCV3iEpXxFah/weEylaiK8yIwXWM28QQW2l2C/VRsaKKsyB+sw97LRzo1eGGcsDxxfGpfL2TfBnxK4rW1WfpUgFL8Icv3gLcNxg24/on08Tmpk6ShEv0GL1ATz4isqimJN8plNDcC+hEayR/6FwcSDmQ0JfIign6EA3A02uY0Le2h4PCfaUhpQJ6GesPUZ7wNl1M4yQ+h9v2QMzUQQOlVHoridq0yP7aIGA5qAbfrPDCJYM+ZgIDlmG551FGypfHRCxWWMYGMU7lO4DCWjHm/yWcxEwGHrg/PNcQoiplwgDOGLfgiDxhe904+YYQoK4jxXmDEsWEGOFmD1bds+Vc/eA1QfsqbF8wEbIBh3VntiWKpmmFrONe6gKMmUbEUol+fM1naL6lLwdS5ODm4KI8RvBUqGCUX/AKR2mnn6DILnUqg7xmabb3EFlqS+ZQlaLBRqvlV+0QVpcQkvBxmqlsKu8FuDXmohbAGXvDnuEqxaPoMeaNyrpEI2578xFmTQxmhiDL4lX6gSghspctq7OYVrJgMR3r6FRDEfOS/liW8EPb/NVIN1Hm93LjFvAy0fuRaoLaw4uZyhS2LFbKv0lphthyCefdgD2v34ju5LeqOFcwM1BF0Nvju5nQDxnN7/APZnNd9X6nu3/T3DD6ikAIMIM6ZS00D8who3z/jX/WFodxSKYa1y3O1SwSMMzF4aNxuy/eBFLBd4qLJDcOQyxeuYEbCjWV9dxQ8vmKgVjRuXaV1+0zeHCWIgATFbw9MzG5zbMGjzDKEVhCJKkUt7l7V9JV9BXm47xGOftDK3UwF1Ff8AnUrh1GnyGBWo9n0fvbY9nuLNr6nTPqZMbicCu/b4CKHADf61E3wpOyfvAnZ3OLbEojtXMj0/zFeGneJjt3+Jn9KmLdQciuoxhNxGwugjLE7sUtRgiVvrHcqKNypX1r/qFhsx/eidRouZpkrZcwuoVyBWqv0iS2BNqN1Rhw7ZYTU1UGxp6O5XfXEWi8CUDduAbyY70/eYRZU5WKsTjLpiDSzQ6H3w/U1O4OMte8S+YIyUHNQdrZ9AwpC0Ea5iCS3/ADqARblXqflExv4iYbxF136JvAm2tWqlaKzMtHvt5D1NjFOAMvuQ0DHJ5KfuO4IBkaNvWpd2W9XzYSjrj8/SBYXUKOMwfO47lo1KnZL8y4mvijrA5ZuGG8wrVx0m/wAImKACIUKlvdTdFypX/TpCuLXFQmwYNUCbdSjn8MQftG13bnXI5t1A/CIK2+FqFx2aPiUB9xK8iIXqB6LTEA2MLmhA6o/6RVHAVdA3Tii44lLxqETnUY+Z7US4o2T3mUwty6/rHhwlOZzEXsR5/wCFaAPrxGYp9xUYoRkL7y09QFwp0YR17nGUa6P6wsdjby8XMLkxWi3v1BFrTmW9y/Mu/EJgiKxGW/iBhKQsqHRjM61EGoF5UzDtFvxGbh+IIFg17NNEPBnPn+I95CP+kHLjLkrMV5llRmu0qgphXmUqrxWwoxpVqpe3ao0B+0gstkArHOhs+ZQEDFFYL5GzdyiSMMuyVwaSymMFi2FbCyfqRgqxTvj3LPpiE0r6hLGVqBxqzhljFUV8fS2X/wA2TIeVRBzgUJ5hBo03WTP6zgdQrFxuXWmTDxfiKNx+Lx6lOJY15tXUsKhb6z95uU2hlRU+JynxGlgxHDUOIBL13fiOIxtNzarLbGvokpJUmEgWeTUS+B8/WvpX/JUpcAPjndMRTQUZDHW8GZreiSf3afLHMjNEKWbOaIhg7cAyacFYbRzmKvklWBUb5kIzDIFuhSwovWeoRm5ybf0OYe3ftEr/ALGLDm1fGfrcM1jv5P3lkIONsuYBqm0FZ5oc47iRoFNxbUvpNkG38oOLSReqTF/iBdu/3Sy5mWUN68x5v3XX8pwjcIcMisEiWQU9y3KXzkjGLikoyziZRbS3E5EuTYZf+jUVdv8Af9Y2CFbV8UpxUAMyckLLfgOF1DWQYbWroZK1XkxW60TWOgXgWdTCgkUsGDdGxBn1faX8BRYoqwV0LfeCQarqUgrVIH2zUukrX/ZXV3JY3XTHX0TJIr+GBdCaZHPspfGDdcJfNeJQ804dnB8kf76Jba+pdRgu19kZxjE5xQusP9ENdkCS/tzK+XwarqYjUEolzHpUxSiutVmYmDadS4X09P0uMoJGCGntCbc3nf8AhUr/AJaOz8KLNKqKcqoY3piKJZjLCw6ZzTAnp1yjIxyjUSIkyzgNBjqPZgfYNTbYGogPOgqWmjWmYdc2dqxC7BVYahoz/rK+raI5wqXCPqKRbMPEJhXMcGh9DUs7mE4fEVu0pe29fMBxmjFJinWIVVYipJdqVdWlQEmjY4Rq5R2DTs59TK3LAM3Hi0/VBRVANODQeNfQhDC5gxTmUdmHlj2lrmBU/CFAcwq/QAmUxqGBiI55qmXp9R1KjX+Nf8FfXd8P2ER4k+yd9CVi/UonllWh6Z2/+ytp1EhZwlwy2Ti2W7fhAFZP7m83CPIoSH4JcAWK6C6ZVDYaxLrNRWDasPKviZt5lSv8qlSv8K/407ZL1TmsmvmZGFOn9GbldldufRjaM06pxHfiApSxuBjAGjQ9T0okgJXPGTMQhrXgNZc1KjmJXZ3ys3DwIUb5A6U3KgKHEGwh51Pxb/wfmYHNzAyQjqUQyHAMWnn6Ajf0Sa0Nyhdy56QKqMo5dTDn4SssoNfWv8a/xr6VKjCedsRHtdLLNo3DOQVRbLDPC4r/ADrWehXMqEvt03qrlNxwhzTZ8mNN5HlVlYedDgF5LfMtXFrsrLo5SrmC03NNYv1oxUoLzxLViVE/56lTN4HmUbywU+yW50R3QLfBKdo9qq3zevUogEAF2lP4hkE0wmFrtddHlpg0xzTJw9vPxKaSkh9ylPByy/4cCsGhRxWIaUNnog0QZRFNa4+Zn/ZO72RTU/pD8vuibBsnHBLmZYikqnMZ0RstYrlZf6u/eIksQjDrouEuA4qWPIm+xA7Ke5rvlKmkIsMqepX+KB8sE2DqVWAalStNTo8emHkRO85aqiqw4GpiImnayy61LNw0MVJulcRzneEAWtiLTgC2OTnPiUlhnL5PF9zfcimmp3lCKLQyzkUX8wasw8xQ9P8AjVKlK4lfS7TLs1EMViF5TBrMSuDqV2A2aRFnzCn+7ish73A/zhQKFXTP2hBLfy/eAtptspV9oghCnTeA/P0OAy+DWAC17gwbJBdM1NC1cLnxErk3PFNuO1lpaA2MsQIy2S0vGFYk+nKXF9QUtx3oX5hRhPOZbiK5fM8oQMDcGVmXuDehEWtwTDQ13E5LiIkXAy5u49TbMuq9sZtpPpd835h0I72Fp1/RA1R/IZM/+TUbgZixj7TuiFeb+rAIWUE93tYheIFmjVdJRns1Xsa6AZlrgCssU4OIZu2tPT9a/wAa+lfUqVTEOGGE6ip5onMCUNxSJxAPDBxbCO9/xOFuCemJZK1K2TCDgWoiKgK+GIqjg37mhcclqu0vFhA87aUoW1tbMkFC9t+HmWoOG6G4bwW30OPRPl7P7mEy8cS7BGH6T9OPYJnkydy5ua+lIEXcsS0Po2S8yhpuNhdRCghVjidUIE1cIcOpk27gglHzDX8GAN78yhNCdEXiVcFGLe6Idmhv9H7zGERSnYgSa6dNH3tKTNtnyucFPAvyCeYQSB0Aa+4Suan9GwL3zDzwA7q/j/GvpUqVK/xz9C8VK/wQoXwJ3V9w4bC5Sx2QQcH5llH9HkgwG6LiosIrfKx91Lr4YD8418y3+HoYCZtJoMfK+gm1/wAl04+yW5dXjH0uCWs1Evox0BP0UlNyahrtXaC6xKbhxFSzdKnELKYd6UPR1YY2UQ0jqUB6R39H6MMRCPYwQG1RGJcorYl32nuPQgVfGVSloFTrBGcjiWjNRcV9HhC0b7plVW/whl8Lps9EfBqXiCouz3J8TJ6ECmW2kq6Q0GJRkEA1zqK+tfSpUqVKlSv8/wBYIit1K0FHbCuT5lSIURwrCZINlZd3SjVRCWyrag5+ZflsOLxiIu3IBgS1avldR7TIu/P+kMaxLmUACfBvUWsU3lvx9ff8Jz1BTPEVHwbe4qlEKxKpF/SfrDnDCHJ/sgjtMBSjiLUCNUQjLR8UlT94i4tmQV7jRqWdFwf2CV9SWl+YtUS2NsO7F3ACZ+ZgsCWD7oy0+lQLamTVwREHqDD2ufMy9vT3i4HvD9n8EOrTi9v+59lkeVEg0cqtRIBowv1Cx7xv4gpXhLy7SpUr6VKlROMcsD5fKFYc/eGgmK0U9SpUr6+kGBGG5TjEwbltEvW5SycZPxBcg/IbE9alF4zb7QGdDZfXEpkqG6mfJUr+Cywg4ePcGgN/wKYec0LqLXRLzWMquvHR8R+nMxChG4wn7MCjyC/FRlhfMbvMstRoN7FeI+2KrBe6uMeaHKqLL3VUyv3Mo08TZYonUGd21hgvK4mbKjFWcn6xYWTOIq4VN3OxnqONzyxzBRJYHyn6k1AGhTyfRZGLwH1BMHMFryyvpo91KCZNdolZXGv9EEEXmNIJX2SswNWS21Q1eAVeIrIVRrtmli+YlIJ8gAFmfsii1yW+FWCvNxWOejDR6smOpmyx6Thpg4hINOis8EO7V1Tz/gqVCxr4TqB2whQZbgowzE1Nc13A1WObriIrI4NRShT/AIXLZbLj3FAkgBa53HUK5b0v8SlQImQ2HNdRbKv1ZUBcVAcKXM4AF6wVHyjItQApah4x+Ii9s7Z+8Vndp+SotHFvzG7bjTOTRcCqfxA7xDMunUeDPX0KKby/umJ2V1x/JPfaW/tbhCYfAvPzVJQMmTfZF/MKQ0MCZBYRrvV7MPM2wBofI+mahxoI1Q4gdqD0R/1WBjJuj7Coy+JgxTc4Mzzrxf2CUiyo7RO0SC0xNInQceEAW7qK3WqOzGskMh255cgxc4BUmn55iriyrwOfpX1qa3e5mvUIbwQephKPPcAczFHcsYl+Kn5hcnGIL8hCO1a4jncrUXRHlrUEp5F4Tr8ozjKKfzviJApHcL0AusXTNFd8L08h6MkrYL5dLfJ+ZbDtLWNXsu/pUqF2elwyLa5uCKim29QDcP0rMClUiFFacy22MmU+EvgRUqVGlYWWWViVBNEsAxtqbA8gteUAlL+hRAVjlqgHgq5uHRNNHZ4qIEbLMfhNMAo81Co31LcZxMiXZPUQ7KUnxMkWrfuOu0HmZ7+0M7aJg9TtZ1PY+0Nl1NnrqmP2MMfL1FiLqmf4suC7+Bf5myqeh/8AUoTwGX8OYFWMHIerhVwZI3m7prvhgVi3r+GX8RS93LVo4zvH4RNahaLWuYKfYf8Ag8fSoWolrhAPTd1UqTidrtz+sGHCblucmdozmDNLNDvND7zLVkDl4PjOPiUJKTA+Vt+4HRinQd118Ry1GxU+TGzUsAlMb82+S6lSpUqYY+Y11Ah5TOts4CWLCEwmE1HQMhzNkHm9xflU/wBp8ym81d7CZ70g7rG7LikeEfyYswmgfMXO/wBDk/pKbry4uTn8SquGXgcev4+rqiMHiQ0UIum5VaRfDEQYA0VLF/vG9kW2pWGj5iZtQm4gbR8Cu8yuQhLTAt1tmaSLEV8S0mlAVjDsjnGsrYfBepgPa/CQyqtR3G6wLHF1vEF5bRuhdnyTGe4MuGY1u2mE8zvqvU5bxn7yiuBz5WIypUMbYKpavSek7DchUHTTE6sOzJnhmVppge/k5l/PdktGx3z99zAtNrteL8xSX6Kbx4hc1F/Zh11CjGa0j/H8QGiinDEOcfyafzMw9G7L+25ZjyV+zmyOwGm/V7+JR598hTrGu7lVnRXJmvAcvGLlKCqtBRsvmoWtPc/g/iVKlEOyinpaGWJErF00kKtIP5X3cXZ3jcD0ePJLvfmBkfEffQSy+Ggp+O5V3dRajjUZEYgjLDacS5xBH7ShQBNpwTzljWmsChJV+ExOJtjy4fH7Q7QiKEw8ZhQFUWx95SsqvGfiDhrHWqpPUaiwvuWI3lxL+oErIWMMx1L4oGR8RbcsvMoGWUR1KADEtNutcAMZ9GZTzOGwWkd35ZQ6OaootXicRv8AegWLWUuDiIGY5kVfngMbr+ZgcXgSxwIfMXVY9M1UagVUfP2IQDzcKm4DUaBScTJq6jEwPGpyTTZmPxEYpbVlNjUQ0LNksIRRGqGMNRlRVbh96/WMxKc6hRljLn9ckxDgY9nqZfXk1fDuL3YW09sJnUxg9rceymI++Ov5zLG2il417lAzmQ4e/PE2RxP3v9wiC56scmxaVzCWMx3Y5TfzMqPsXieaY/T57LltTbyTuQQ+X9o1YtJxWUZYyBejH4lbeP6gPpYp4Y9By5HRsjrj4mdj+0szCVlYT3DhWJlgC116XGdvuQwSeRiP67+jJncsVhh/NgGLdz1/Vz5zb/t3KdTUHj/yUFmjfoQ5O6Z9ZuBm3L5j+yfaWFvhXik8ag8Oc5iu31gkCjbZN4ynLmU7gGYw62Kq2U8zpbI3Jouri2qYFgeThr3BWS4+Q4XXiI6pwquS2kfiMVwH80GS4+Gl7NUsxriU4XZZB2zecwQbliF4ClarFUxU1NlLdU+vRqBQKQF26V1+k6IKdPaDiCr5kXcxXTZBaAF4SXL2bI5rQWnuNcfmY0fKDu5mlsLZ4ln5qCOO4jfL1CiNppxT6hQUQGHgyqJvo2Hv3OM0MR+YiFkLK3mAkW7Hf2g3kBTWuyp1+yb194z7gOtZzea5/mIsQwk10Z37/DL3VVTd5nnUwXb5cPDjnxGgegFPxTNrH3T9mmKsPtp18SjLo7CmfeJlG83h+M/iEr4tAV94bCSntCledyoo1fFunO0ajQS0ddwxF9A9a5fUsLrpccz96zF1hCdr93B7hbxKJTOBAuNzDJ8oOpApNBPj0lbZSbKnaVl7fqU9xkVjAKNyAHOzmZZnAElQG8cQytMDQZZoGYk3ADyV2XNRFDjuwLGu7qZE1aRthSPN6h+Pnfz7ZgrDN9ekTPUcJLZo4zfGYL9C2Fx19VG2sjc8fVT7WJ5xf7ynFhV6L9YKg3s0+JbnUyjJcnkdPXUVjRafhjCzslFZ5oANTVJnGr7JrczNzapsqEKq2j6ueWW/eG5WkLkNBV8E/wDRbQpLFRfi+OpfrpHFZrPNx4OcFTvTFiVrGmrX35n2+XAPUvgcAs45EeWmtcNfLxucVZZvULiupT2ZlPQJWXpOBsFWbYEHTCOtmeYZUzAby+/5gCAuksxfw6jOWMW4vjvTEr863k8Y/H2jpR7/AHmxqV76/vE8u/QaX95mPJrs/wBepiqpwhT+P9TgS9H6fxCU1Z7f7hNEtTYVjfFTkMvcVu1L7DwvvM3VSDFnweYaYCmYBApKRobl93GnMtC7i3E1HTeYQeBu2mHyS4nHwCrcwC4CoWKdfrKhd6to8ivjqMvMtUomTmE2MClLyVA3qYUcd9gv3mQReppiv9Shsuz4CYftElEr1CQzVH23MxfOEphSr7IbAgW+yNg5xCZb8ffd/pCpdv3hk8ocCxsKRmbGqPrFwgULdGgW6naK7tvmemJ5hzf1ojqnrKleYE6mlThKDKyg42vxMhYUZjcSOrxdE5FP3hXDPczfmVIfL3OTdJqMJfaYBaEO9Nyxitn0S7QqAcX7ahd1fT3/AGo1D5Zh38xQq4vLZl+0pcRqs2x51BTAlm23MNwiauBMeSzN7uB31HAZecS8OyDtLhhg24fiIR0LEHOUaDlh8xjjw3y/eGuZVBOn5ieO7N6w98RgUe8zrB/HqWprFVaeZcsq6cP1S6+Sj9z3zD1YkerzON1dZsmxVandp/ErMBlWSUxcy0xXCBGfogRYpo+8q38Qtz9CqziYQoBRheoDYr8ev4lRq861/vOIxSu6PxhOA4HwSFV7Caez0+f5iWKHnwD+YAHga19/oaFm9+IvAM9zCPfV3PeYSQ2X+RMqsE5gDj39GDwI6HNV/mvzLbttO9bIdl7/AGgp2y+TMa8U6h6VZ6uCEIXpL5B+kRGlq3QU35/mc4nM8Uf1m6DXuD9EKNb4ie8Ta8ot/OL7xR3HuDJBW88/++IOL4h3tDbhehntltBta6+IVEkeLz+kACk+fp+kJ4/wEp46QFGvUtxweAhaquPMTrd2MKqGh0BxmV6M0eY0lZVjYezKoUo42V/uGWbufhuZUL+BQia7H8f9w1lXmAEGMu8wF5mF4seIAZgEUz6gv1ZGlohbEsqE1T+KmJsrkNOL/EV/Vytd35guGJZxmcdjJRjNaFV5/wBxhTsC4fiHKT079ncwmwHjv9UUM0rlbxfcNZJqPkr9Ygz0Np6XK4C9wXSW53nxGjbFGHJLlGD6Am4x5LIFa3ZrWBN7IKvP+iAImPyiAHTTl8xRRFRjJj9sOWt/7zAG+FFQ0MzMIy2PEUmEpqcnh5r+kcRZSuaLgttAFYbMcpzqVUzTTG7hhkskw1hR+8sj2OZdzMFAT2e2eJDFt/cUP5EOEjRnR49y7Hb0Xq5UlrDtDlqOlXJXt3fM1PQeg2yiHoVyGe4UDrxMWYV3R/Rlq3H60U3BtA/VjbMpEutcvcJwbj4ZUKgMqhSk75jnFlHDrjDETuct/nEoT3azFUJ1PPGD8zj6Y/xoxzUt7iPmiU+Cy8GHaVKeeZblGTPNcxSOo7Dj7yoyy5N0Oq6Iy4VRYYKrHFDUGktWWLeXH6Qk4iyHArtu7PHEAupg022G/mGVAGJkSiZNkVJuT0xOTcMesyy7oGvwhUZh+WeYj6FYByXHPJSBx8wEIX/dxmPYqL5GRt8xB0Qhp0uClSAEezqD0paY8Re7BDbP6MQZWIx1k+ZfWOr3HtPhMeULCgYKl0YcTrGMCA9zWIDKNwD9GJ9dwh/qKiplCsFObiWWcEavSv5whjCNj5gK8GN+QgOYUP6eZTGZUgQvuxs7yTEneWjG/tAJ5IiOmoQatZ+al6ORnWPvFm8GCcVlQjaV+8GqKaf7agHY9Mr3yzdm+MTZ9V5K0RN03vuIW2q0xi8GZUbDMdVnbipVadXmNBENYLGZ0mFcJQY00Cz1Hgkq1gsx96S4RWFQMK7cDUPtUQl60YtOYUE78I4Xe5nUm2hnw/dG8yHHR7qHpgbS67cIpoypwTMrQ7f6mFngF2/G804ve4V3TyL8MRT7nPi4fMZr4bF+p/8AIlfHcf2gAGg4LWiKSiZhSzGdeoVqK7czot1Xj6VccfWpUho0aTmBtzvltg6r7ogRb5Zq9wqAvxq7uM5W807xxMfOH8xuWaJ5VLXLUnfSvER2spXOr5X1KqMkAs+1dQrzOJXAMMLtBMGza7Z0IGvKWWiV3AZhMSNIEam3V2Hl3EBTJd6nTc3wupTI+LIjG1cFSmtFcQjQFmwf6u48g3MQsC3OoJ4UXgOjzBcXYIjdLeTxEYvUflXnUdA0KeF/fEQAgDV2tq6PqaC9l0+a+WIk8hSJ0KLXUuprEQqlgbtTepa2sgcjJMsAgKrzENCx/SFpKsHNF1fmPBzQgcs0rFZjTCXXccFe5XdoD4sBlHG4lQqgDmpwU7RuZPncQQK0YyHZ1uL6iK0bGjyOZuOkDD/4Mv59ICsqPzMmQjohCEZLosVV9NQvG/q09kc1AY+37DObai2Wzin7zbxMPFuu8RoG4J50fDMrhujcMluPRMOsw+g/vKqFy52AZ43EFVyhjK4vPiUBVulbQWvMMdg240b44uEF23HwWfmUenlAnmM3pTBXY+4YvQGjWhseoMyWh8Hoc1EcgNSzdg5cZxGy+rqAOss15Cz8y9gUSLfAHKwK27Y/tr9fUQFxt2f4P1jkUAvnQmYxjbsLozy1CXWYB4e/CMUx7Wh/sEx1+RarpJVqNI25o1NnpKA22jQYsCqWJb138QsM0rqwqt6bZYNGm36WZR3LoMyhVlecY+IjXZoXNfEp1bTB69zCC1pfUMNG+I+SK+WHBDeNgpDXgtqccgqTyrkYKmAZNjd+EDQnBozFhxZgQww03I6K59y14x88wGHtGvi/3icuyJOQqL7FaqNWpFF3tzBeoVorEDCqqRKMcc5qvUZbm+bSq65h4mYDyyA+3KfBcrike9XxCItmjiz1CaA90Y/3MbejLwy4Y/RFdikb5jkJlZ3N2zK3zASPvmC/M1zo8C877xqMtLGS4/7S+YsOijj7ypwJ5mdZlbNsVGj/ADAvEGjFEzbpTUHLtHJTvxIPQslkOuirY0UISJX83BtnlsuruhtgVq6oz6jyPIN4NPWm4AEMr8tc+MRbIyVryPiEsdEideXxHcuVwHd8TH8IbyUESzhgIGaLWyrXh5uJJMecgYHgFj5QKDdS1V9pnpKNbxXcfmKgBz69vMEm1DCix35v8QqCoQoD0Q6l4FbbSyU7cJuIF7szSzrJM9qeLSNF/iLqmBYFTLXNY9y6Jyic6S3pgsclM8rxKSI2fAaFsUBBZhg25Drqo4TowABlsC44BNe4nV5AtNdrhY5bmjAcFfEqoQ6PLvPfbCgGAYlqkGtlxM9VLyw/KFyzqzDLy0KqUd3nzLVscrKIZ4CnYz4oow/qwwAgpEWzAVCYAScSOBazjqOUbJQul4fMIdqq5D7rnzLZ3t9sTW49PpdFAxnQ45rMKNrQt6unXn4lFEY5kXfxzHvxAzOhpd4lR3axjZn4tl0mq6KCnzzHcRVFzwc+5YppAPAaqXshLyDNUy9TK0A1aRxg5SZgmlgZs+JnFjQcZuVtuJlw0EsEv4cURGQEcF42PicYwEdNuaeYuRDHYbcfeVCa2fLX3l4gLsEalLc2ht7/ADEwtgcCjELMtZVw5sqtw75PUILg86lVMq6G8mWrh9LunHElSWb1Xi/MwECDsGvlEu27zfOH2l+dvMMKMbvzEV+TcGt+pQGfPnqUczk6Yu8OJbHmbWgjwV/RMZz+TWVrWPUdbmEKkrCNmobBuY8+s7TKGHo1Zbg5sszBkepAFAHiAzc03Yp9SyvMoqG2XWzkHqiEV4j2Vv4nGZ7HB4eS4o0Esu1Qz9oxpj1lqbPELrkworQuaPrdYZxkzjJiCYDT7XFak59liBtsZ28sa+GJJGSGacpEHLSi+iGgGnLTGw+ZgAdnYo9QBKWxzaSh9dLSnkWPJGuYFcWtoVX9GJ8E7us18yjGAXh6X8+YJCdCuzfxjmBmQKyWGKfvFxdZfj91jYpfyOVp77fBKgoAwNTfUr9hi8O/DiayTryQHpagyYBhhZ0FWZqFv9Yhzn1AZcaNRN4LYzfTjgUjx7gmja0RKXlz9CNS/pZeiV/RuA7vbGPMzAWNUBXgmThReLYw8x0WCFukYqn3iWVjYQEM1aGIAwOwrRWrriCXdZR8JYDD0cjZ7QW1B5qtJiltVKunZKI18Dz/AKTYlKb3j+TN4ybo8Y8RPiqi8UI3bjBwA8sXNMqX0sMboYVqNrLxbPXEewya3SEVILNHtgkg3muK+GDLS8DpX2madU/ljxYLQYtutw4Lc6h6ru0K4bazvliLCL1Q8ky5YFJ8RqJWLdP2n9+8qEwVdJzYRXEJwDuDyLy8R9txotR4bNgxNHaMgF1vX6xz2yFWH+JgaTDnatrtqYGiWCuMeIxVvQDVPPbOwgM3K7yj8IwqhQTVq7zBEGS02OckwV4cOt68w1gAZWvIFjcfdCrtAmQfxL+eSza2GeOYoTKMVmg3naQl5zq4Loq4xHXBU2qKNdEVzj+uT+0oNgDXaJhglomCXtWDua23G7AcWvhN8H+JfQZs1GRASLuqcRokTPyW87i01Z+HQxspg0AHgvuYvUDvkF/EoUsVrm5fzGFJmY01m9I5YJ0t6M9TPejpUHV/MLgK8ZlHjW2/UAqPaj9ImuGM5/jl1hS08L8sopc6HMdepbBsFG35Ic0raUyXW4UXcpVKb5JzHCxeFQRoXWWuiaPpfUc/Xapx689Q2M79jiU5yznWYsLcDp5mb3Dui72spZCvFnVd25i/jQernPFzO1UYOGLya3Bum3FYZbVtdzM/c1nS8VTmVUCAoarMuaFccFt3RcVRahs6N91o6m6AYpr3A4xeOEcE+KlCwZMejn1CqtB76px8RJcGer/aWpq+qNWgFrOkWXlO5jRsA3BIDFsG0c/mUMfBxdG68RJQClf38kmA9Kxd8g5KnEQgi1UAJvsjXORKThTfLcelZCy3xckpnwqmi29TaPYcG3oj3QxALcDcClILO3RLgcAFSxVeG5UNI8VJFZw4zHvVeLT9sQSgRxWPGuIjpcdkC7fPEF/sAzS7PDqdAh8TrFxARZY4q8u4SPAXxCoSdZ1TH5qIkGrrdAGcdygC7j80s3zBwSDkDIG3U0lKM1hb8/iAliaw1dI8Q5aNnQs75InbJHxpL8cQhIX60Z1tvMKtqEBtY0ujbLCFoHGbEfmERM3RZcw5/BX7yyikU5F3WkFq2cVwkMqoK0VVHXX6IyADhcJC76gJWKz8MCggPEVYBQ3k+tXAOtTFrKfcxFLbss58S6H3NVr7RoKeANQuSZFZdanOFlNPWpT/AHyy0O22AWZFA1vt5hka9qq/cARr+6U1ZkKxRRdws0yi1g8BFYKDtzkT7TfqMub+n61bJnM+BCno5liwDlSg/hiUGBaFsG5f0X+3UJALT9Ru/wBZvYbNke7mJZ3s6FuZMNCHLjUWsaozXEeIY8W41SCw5+YFn+jVwwtA0GgEKUCvEMnWrjAhyJVh6TIXTezvb73K1oLobENIWbCYLzpOeW7m+jvuDxyIMNTNeHeOJ3lew7tHi4N9LvGrT94CsmN5EZlgJ7ziMI0H3rGjXMowGY/ON8+55ihIYAJnxE6o7bhBjLHJUehtGuthKHsbq5yRVhlTvB8eYJoHpG0LQN0vkh2TlgWsc0o9zJVtgINj5I7y2SZgDhmLTzrY5C401TGxWOhL+bbFbP2ivKtL+kphybldmsuZQIwg8/vBaCYO8F70MXoHYL4O3qWTtkyyUPJnUQZr2qweLNy62sFGMo7j3gBosWc1EVqLMu8ftEjRvsxTXBmMVoS7BGElNZfERW6TIW5M8+j3ErQsH9dy7JFFjsNnfMOZiiQr0dxiWi0NVbjD8xrNBF2zrO5YQF9brf28yi0tgd3enuN0jUlBhxDLqG+XbPdQUIBhTY1aall2/wBKOiiz5vU6zGhk4cey5qIFopd9YIAdYM3oVMXyExp+YpgTWR+GKRs5H/llWtcTg7u6j5TuWtHxd+cQb6FYw/a4uvza3X1r5lijKr49qYh6SoDZXlfcQBg7Kp99xwbabMkpUhS222Y/ebmvpkcLktHKZiu5/QLvuA0wd8NMNeeIWRLtHa5c5FnKDS7azhE/SAHMNCbrm4PbCUovxmZLBTK21jqY2Att1WfMsGFNv5O4XQYud6XfU32Axsytr/ctAHeUpfBnMsXCLF0HY6SPqGqjTJDCzBcvn64h/rDZEQrzAQejXeYQ0LI9VFAb7gceK1MF1X6x53KxeMZerlXTaltmbOP4jM3tTuiFyIDNtYXjpJUs6IycfO4jQppsAvT+V4lBM5I1YvpUpC2F5oG/d3NNZEd2pd23QZ7ileXJwsseZWWwFRiaKxdIWtyiIGOrcZ3MChmSXTL8oXLs3/qCD1Jm1rPjkhHiQTsFb9xyDF62/wBqUR9HwU8+ZghzcWGobG8+pg3BFv15govrBP2HqEzQ2rI/aZOEQsbaxx5JzCgKp+bxF4DkBLfBlikvgHDt7CAQBTCcmVHEziC61M82ZQNAdc6RMp8am40NzLqwEp3NcXAE4Zxoo40xffQVRp6SitGSwtPxGD1opcK9XuGkXhCX8Mq2OxZcb80SnLzzoiIFLviLXzkg8N5g3vk6hiKawFowc5zNJFVujDq4+XF6+8KWLK/zG4gZCvia5F7jtC26X5iFVcq0HVhlXqJAQPCesAPvMmBwMobfXYzQ9WsX2bl7I90bQNYjYoWU9OFiWyE6PAeP4gThcqtcmsnuOCm77l+XMcwHFPZV6uO14fu1B1LLIO6jztke7irXjBMY/P1FlicGAz7QgPJxjOY9aAz5SUyB4Bz+lxTTcAeA/SJR8C2UtVqJdKEa/bkiqbm+dUY3ccrmRV/f1HFq0vtaWZMwYrTzWcLK5ail9i8ZywYeAUQqtf8Ak3yNjgP4IV5gm6XOt4hGkfD/AEYhq1WNw4pZWsynMSzis4yJZJXMMCaD5WOtSo0dCpy4w9RXroUFWHTzxBLMKE4OsLbrMWhE3JfDiILWDUaT7lwB3brttfMe11X7e/0ndnCy1mUVquieCDdggMuE5uGNQJYGOzYw9z08Jvzg8xA1Tga5PNS4hzS1b1Hro7a8VeXyS2VnjQebrqZko11iyntW2ANQMbOAzlbxiOeIQtU7RuAsMS/LnD4lKGUnB13+JaOeCWOKs41COgwG7sNy1iXpclDxZiEe/LxW+U3uCFfJ/UEPdZY12OIuQpWXdM73lucezS5axxmWDTai1/FLL8praNEZ4SxVgNouk58b0pzVZhlp0hWznmPUUQcAd3C2KC6QWyCzgCuWCHWVFVwQ85sOaq2z4TrpWuFVP2JaVVZLmB7P+sAta5HGIW93ctcM1/alH6teBtFvgEex4q7VoD8wP2011qj5P0lDJKYeQvCYlnS3+oo11zEq1u0yWdS35dyMltZLliOcKmTt4QPnIOHRa4q33B6Nleun4SU5VvDQUPumOl/tc6E/XjP9JiulTrWmFKZSKxfEOYxYrWNcypXCMKmzHp6lbgganOx59RCtgkBwlKPgiXNacPIs5dTXTt+RuiGQsJdEEC3TgoBXrvUuBzZSyvv+0r6TvL5xR6l9tJe1X3BK2MuRFAtXsyZlbgOg7KI7bsmn8wBYodFVhRv9oP5iKg+zM3yStXev0XCpbrld1oamkoFwvoHJQ9yqQ0OWtZja4VYm14FM9P0ZRSis8Gb/ABC5fOk0D3OzNZvxHKtUKb8vM9yKao08aiwK5Z6AXg6iJvdCK4ZKaw+ZX76dnk7fKXtA4OmtODmXRDoLDwS0uaKDy5ljV8MroHSogBwJS8L5emNJQrpuIvTx1LVPaugd0NnmYEcGtKWo0AzWjLEH5Zl0A1GqMVR6l6FzOn1g9wT8IKClQ8ctYmEECtclOvSXoZ+Jcbyc4cTOgrgvisPtGwRZLXiq62VmNSOIVTVGTh4ajiNtH2G/PUpYWLRNXnVmooGqqpvIUzzmDWPK64uo/ePVLCpayTjviWmgWlLbl7hEAnyKkLMogomGutzHMVWe8wFWrDXqU4quv6NxItxFrwahv5lIpsfLUoo1XdzFlvHUUq5WPKoG7kDiOYVuj1LAIi1ULkOe4682DyvE48VKB+Bobb6izWxVQ9Gq84ibu17FDVnZ957FPwj9pa7yE43EUVpGUatp4mvFh1kX8RoYDgv9p0F6tfINy54AGlDXhbKUfrE/d+ZgIXH7LCWhkuABr7sMMN8wvP6QACNALo+ff0VFwq8FPfj8yimrtPkfiVpLFram8MagCFt11/p1cG/nLe9l/mPWdRgbb1rmVANTj+SKbUHgONzsHcvAteyql4gxGgrqrX3YPE/BTJcxVg2ayWauDQrovx1Cx0DC0JW5k8HSckbSN/fWm62tZ+ExbKavxNDTnA10PzKCPw95GVM3YHev9pzIHto8O1+JTdHjJGVN46lLQWqBLyXqONpqFlAP4ZdmH8KfkqVMcK4tu6/SOR4OuNxBUY5l2PdzErdK9l2Fz3LH1vEsOSlnziWls0s43Zj3HfK/Mu/5i5mXN55/mJ0gAqsIhvz+RVUMT9dgJL+ah4Usb0rJ6iI4CPJk+0YiFFdZW9dkq4wcwAj8VUajWYmNv4RiSpytjCVfzMmEG7NHxkWQDZ3WlC4AbDFHJy9wzaXRjlrPOj6mRrw9xtfM+8qVKGi71MTRqeBlr11dXc52vxFLMVBWquUDg+L1K3FmLX9XHkqxTcP+kRfIHHMvlKw0qpL2mWrW+i9t53NSi9Twkw+lFNshLdCbXwqPg11PDLkR05hOvjOuSXq1uysSrxVSBcOniFzbhWoFRZNvbKevMHTNMWsaLHUp4EqrxjxAAcRlyrsV9pfiXGJjVXaK9fTWkM0WQ01wajkAlXK0ttFe5jQvjuMKW3z4NUeJEKcTAGFVSnJF0nM5aB2zjUv4eBk49TVNx/tftbM6K7es34jNIuPiHJY5QexwVyHqodnd5L/tXzOtGCXe3r+JkUB4eZVai83L3/VQWnemWPo2Yy3TH3l4FV/TLMsROKyN5wkGrWbLrN79FRNOfWXCgi0doc2Lz7Ikdh6fzCuupiOnZcLaMFV8H8wbc1U1b9pwYBV8ECivK6TSWMUb14T/ANgBlSHxfB8sKusJx8G+5STvdMP9uXCGw87u/wAzGnTc5bMzbwa8Nn7cM37iA5/HMvay88Hc5yqDKsjmjmDVtt8DIx+8tB2XuUZU7c4vF8F7j6CCAla7eZcKzu9sBsNm8OPN94lMQ+7haX94+TKvTZzBBOmNAFCe7lpDsjEwQlDfqahwwgvLwvxFh0w1WjPMFrAjwo2faEtIVBz2S14sa+8WToPdzA5lzm+h1CRK6rfhigcDSmAi5wDGTS+YIHnYP2gCEVTDelM9jsPM2AjNk+0aQC0l8DB0slg2B/eJDsXCCsVrn3P6Jj5x2c8Sx5cokbNxjIJmVNuFckTaZ4iENuI9FtUUKPhcBAA5cpc5Uj4W77gINs9gEg9wbGr/ACVld4ga/PG7hkoijm2hbuxL3XclVa9EE0zhA9vK7ia6dPkM/ZwS4BTnlnh8xTgZDuK2Raa2NPO2zJUZdL5vKv8AybEXHE45i0AfKJVUXl4WwMVxma/1KwSRUZsfxBC1/kRkaqIvahNgS92q9Qt24LM8equanFu7ymg83BZHYpaKBYDu0RBS4bXaNxo0E+r/AFucwIyvYNcYhsF7qpxQ61/W5gynnBp1wkfCjFF8MRevtylB6xiIlWbLI+GI5eM+IJ1rjgApt8qhMCOn5jGoDuHuftPIvV/aADlWrF80FxBNuhg/SAY6q8e4LPzUdXFM14lt/rAgOGj3Tf6zWpcuDJaxtFCeO5XNLLK4hq93O+hw5JFmG9JRWo9lH9ETFr5niN1HIpeO2pibC9QqUkYRY91v5sJpyROXIOp5TF89TpH5lYCk1nzBai9B61OpjTu7mLD7xrPTm2A91saElYm8H+0Ue9+lmluffu57Uy4l1McNJ5P/ACHCoJvsf0i/bCKFaF/mYr07UMD2DEC1V/IvE3MNsOhgdTWN60Qp6LislGZC2dEUJQHHR4rzW4Bznru0fgXNlY3QVyWKrSXKQ0AGLave42jork+ZbgLsiZgwNeMn6zJrgvSrpeFx1Bsal8rOfWwlpXVR7aLr2qEhgPFnEym8oYSvWyGD0QCleTDzKHM0fWaoq8xxpuU8mDlnAzLxwH6y1iV06z+9QcnYkehZ0r4hM97pxfHxqNlNtPiNwQiLwHUDGIYwwt5veJdRtyOgnO0ni+YW8qlBvP6pRXRYs3eLoViOZMLeBoxKZmd9aJ2UaHb4H3FzIyKfmGAKOIqlbLis/jmcOQOvH/srqFRPdygfYtzJq65I2cshovCUFVXIqRNhmAJ4qyM/DMdr1VXTfxM7CKMcbMncDeMWx3+0v9v4VvwbZTVC61HZXEW+UAPZbfNQg6qdKDeT+Up8MYQroPDKh4DMh7C3bKRRuZSGQFrq85gtECLVxoB90CvasNsvSHeG46+YXSAGKA0UhiAIj7COOMY8TM+xgDTSuU4uOXYzWE2D54lDo2orQaPeZfKlLWn/AH9xom0U5QyOs3N8cbHBTqtX6ly3cX8oBS1WFv6IwBrB4gl8zq5ld8XLCwxYuy/4jXCzQHVH5n/sN/S5buWVC0MHwxNjtaIvwS+0FmCN6xmU4K8r8uX3BLK0x7mASjdCmXiWD5N7TkjwXtAe+JgJ1feV5edy5fyP9qLgVB0AfVYYtVyWA98PmVgF4lDCf+RGl3eDk7HUFh+focBBZg47iD0WplRcoH2U3W7PiMcMhWxBT5TDmARq3Shkt63MnLq2tTOL+9RiwuqumYHmAC339uKjmEGCVyw49wnN8wwu8HmPdT1PkjzItEslPfEPETQNsEE/YxcUML0jYlyaonJ5Iu+AeQGcyPL9pVWn1PF9DJa29sxySvNvhGLIksYQnoXWJkdDWpZTIjhFQEAaCqW8SjouuT4hdm6wx9znohBF0ro4zu6Cn2RtqOWOi90Jc33oTA9sv6Erme9dDl94lSv3GdC/ERMzqeDZpOOe4RurKqq1i3g2MpEN/YTRx+0cMdViwVqKjkUCysuen002hVdjNfRQ5ZWALzx8RrAuZ0fJRUZMMDHcPDA3ylHEC9yCHjBmBRsUvwbgy3+X6yyVVdxOEcy07edJ5qpXmC9n8EYN1tJ+t7/P+BjX0fA1SpTlCRxNY4/erlxrkxLDPGn63AHLfDODQltWpeHxH0o7ZUH7EXqc3J1/cysYljYby6ic2nNT7ZfmJRWXEAfwxAvAUhr/AMIauFzN5z+J2vuEURy4vCWmPMZFZiml7Dtri8w9sxG5NCifpCW6JemSbOs1Qljfpg4knCKZ5qKk4tcAqgLTYtK5yz7wnaYg5SNq7rcSy5XscJ9FndQa1iPQjhp4nmW7gleZWLRkbxWdRL7iuVj7msA2pqCwu6as/wAKqvnMLrdk8LuNu9xykpF5F194O4oTQapsq0jl8RSpaTXxKVQorAjss4P7SBZCKPHzHOtgFgVn36iEA0t1RLKxKIuTus8MZgYxAgWHYl+blnNk36jsUZYA8lSqznTNuxpeMYlueiLQ+AMuea67g21r1MQ2LKVsOEljnpaXCtnBzHmwyxmlO6fMrwR17nqEFrh9o1j1FV5eSysA+RL3H4vxM13mZPlzLw0x9RUPtCieUDDbJP05f+QxF0/7lnDxvh15QL9AnExdBxFKyPVFS36PR3e40s9fYo+xPwQGr3KuOn3hhvD2RDQW3FllD8LMukaWIBd+lJdf6Ol+TmpxnvX7XA/vRu9U+typdtG/7S8Q+TbQ1ljLQOYgoz45qOXbiXyc8YmCB7FNS8ujwtQUtq6HcXI32YnjZ7jdpnuKMpDf9hMmobwSyUrm4BzAzi15mofxCf7QP7xgyoVqoVbuw08fky1hh94h0nuEh0KJR0mTUVyANe4cDwYY/Dtlu5we5ecHZFuk/aVyReX/AJAgbUG1v3lIq8WvHHcBM/Ef7nKFXKr95kGwFo2715nudm1b0eu5bKu4j+ZYKlzd+UuVjRv9XMMY83MsU+w0XHKyq23MCBaags03atxLvvaUzvOGAFnHH+G05xEEq9kMC7JYNkt/X/Mjtn6hPt3zuPi/eP2Dep4n9P0dOP3+jh+8IP71/c6n2F9o/wDU/sn689zbn4n3yHjy/wDCH9r3r9E39+J+hzP6E2k0/wDU/q/oPpfn/hhmPvPvPvH+39B8fE4g+fmfaP8Ah/Q+if0/T7w+fo3/AL37597+HzPfv0+ZvxG0D+n5T8rG7+wa4h/wIzTDfm4n9Gfef//aAAwDAQACAAMAAAAQ6cXKHfwMTD28cilCclJptpIEkBv/AOm0vJZLfMy7wv4eFxn0t0a+8sS5BN9ZBYSSbbSLcAkn/tWiQAE2t/YGgD2+MF4Z55RyVvwObCTaSBJYbaSk/vtkgwCDbMiNEaxHs7H2wP8AYpm+1bbZBlCgiWiyEhPb/tNsts228Cw+KorMjyn/AGMo/DAEAE3SPwwkQltpJTX7TfbS2llsf6PvGQtPeSBwSBuyikpRQ8JBXUskhBIF+/76a/kttKBB3Ls2ofJwJsCyOnD+KxDJ2ECf80gBJkn2TbdYmgvIEGicdYV1hNkAJoeAW5okcFlITd/+kIItuaTbf/seXwBx2wvx5+TvJpWT6Zn6c9JpVsACawskAllSST/9fHNELRpt9c/b+yAD8tBjvDhgmAn9mku4ayAklaS2weSaRNsN7eT/AGlmk7ZLU5dVFev/ALJpHhY2Q5hyG0mFnKXxJXH0dYBZob/8fbbHL2nuTFl4PBySJ7btucYTncXFM5up9OoQCQm3ZfrvA15DsWgsOnaf4CFEQmz2fDVV7sJtP/FCk1myIL74OYI0aHzDairBu0Tq1nRuGI3qMOvyest9kSKACSB//wDYq/WQlc+ioOXEO2WybpKf+pxCTNUPI/aFXf7SbJtkscJV873QfPQo3mcEnCv4eXqLkBfUjRYN0i7e7/qSBgkdzHQpmH+i4KFEBbXlQU7EU5WK2rZAIpnNpAL3/wCfyB0R+ER2r0dZbIJKCja+7/8AbA6S0pUKW0uw221mPBVpWTSqUQeHKyc/8kQVSdHezdb4nFtKST6ukWSAG+39FIp5mttWFgN/N422CaJIQOZFpwURITZjPCSwSySSboI5CX/Jrb/bJb/ggEL4FyERPSt7sKB2rQu202//AFj2qNuxfTv/AMWx/wDpigQnZob+oKAYmdt4UVofXL3P/wDeFJOY68mJWwE+Saolr58feRG/hAnisDKuizhde22qM0xTu+biAJpn3V0hLZq55v1ix1APocIJiWJrjdJJrYcGpBCOS3BTdFQ1Mny2QpKELfXvuwAv47HBCPgonS5Bz44k6S6WTNbTFn5AXJprbEb4gaAVThIFMAJuK4EdlKgm2e7OlhMAgLE+sSHD2YgC0OpvMHLadV3jl2j5rmS9xlLIhO+a6yFzq3fIAgkmZcFk2fUycy13OpvSDpzphMEticvgi4qtQQUmC+1GSt0n4mxFdFKwH28Qk/cPj+WmuEBpJAOkbSpWXmPySvMkCHmIK3/+bzJWRxild7ZFaFC2Fy5Bc6dGf9cN+xLGlQSgyuYMSN13EnPJeiU832x1og9QXbU7Pj5jnOTS+W9RwWurQvWpcSBmBsnSiZaz+eTdjFAnw77emmO8T8JJXilLa+PI6SPh49DC0ae9ITfUP1fpaVT2r9iS/FeqM4EzhsQPoFELyWwYIcsOVsHVlx31xm3kFucazMF5DrM9arCRhCckcKJ3uJ9Hd0yZDQVYpKpQwXQKCE0Ektoezu7NafIGONehmvbD2XS2Uz8z+Eh53q/t90mGUy0cMHfcXt+p1mWpenxVqR6uCNDmpOt8p1LTH7rEiCu8yvq0oIN6c6Yg2QA6DSqAqnEwpRUaHiGLjAUb/Umqvs0lr+FYCsEuS2xTYh7zru5wNn4XEqQu/gNZ9/8AhkinrsHx6JkIDjArO0Zda4tdCA7hY5mE387bhU9shkid2ISMNPs1pW1ACjt5zpVkWGUvnxSlFiPgLTd7UFgssyi+jUK5OcX4S+AH88uwLxfk4yITqU8m03JQbgAIyhA4Q1mUUkzir0QbUcwlwxDRKHdzeXkxN245J+lNOYgnbHaWRGBkyH3QUusz2AODlVpqAGJ7dmvXNiz81BIL5UCn+r0/59s6n/0fYKTV+32+icPTqdkprnU0UHnHpJJeEoiNt6vVnHZ8mAYvdCMsqmw2fPHR/cEpG7BS4xmUWUwsXTYvztdYvKoEQ+dJ4KmqVdMVympu9mQSMz4xRC0xuxrmL42ds69G5ReuTRiv6NVSwUErti/ARiLV+WbIJXNhUOubX7s0icmVM0RsMkn33eq8unoBace8LXD7CSXH0ejyqC3O/sgLLgnU+heDXScv1lG6+FT62GDDUgB7Hf3GkXPq9NR8PPIARNSObBRH6k8Gi9V1otnL781eXp2nn6fkQtllkDMpJepVo1W1wQ8xJfsgp0w02SfunyxgfBZOOwL7ksihrFkox6WR3ZxB7UJItUlxsmoTynWB0V/s8AlFJANIDBMhmlJsKqS8XCLwBNciv//EACkRAQACAQMDAwUBAQEBAAAAAAEAESEQMUFRYXEggfAwkaGxwdHh8UD/2gAIAQMBAT8QxxbKwJQSmMy28BBzPTHmBrER2jbgp152y0JFBhosMMMNsLg1LkTYinfQrLS0tLS3oFpb0C3oF46gX0C0tqOhZly/QLlwi5cfKOOqIXidvmokYMtqCpTHaXUGEo6QtmXLRbLhpLIRhhNIzRMowipcapZmOly5f0D1nprVg6EuX6Llw1HQdGAckANKlxlwZei4xBoq9tLqXGOi4voFy0GEKpRLMaSwgyzW5cvW5QF3YP30NTRhL0vQ0v13pf0jSxdhF2FetR0uJsi5T6RnbHGJUIw9BAWJUuLBi3BjF/RuXLhwgubcH6hHUjpely5el6v0DS5fodLhNkajWEuoCusB6dGjEWiXOibYgWJRgMS+JToF6DJnQyy0UzLGYhMG/oDHVz9atLLztAgKx9G9GXKl+k/+MY7snebVO9QmYYYmDbLWIINLuIyKNQ4QAMRamWA3g6VMW3MI6XqBUfWrLZRRBsF0laUDvM8dNFXLVGXq6X67l/8AzogaxMUvvxKIKKdDSInelCdCKRTF0IWLpfovUzoFhfaAMw4BKEbmUDBETJANpjuZD0P3DQuGvnMRxpVl+qvWek1v6Zrcz7Ze5KEvO0tm4SmkUHLc+moRPq3CkG6jEmZiAZgJUSMkxgxHv5iqDFBcWX9O/S41uXH642jL8SiGKjL6bg5gXlg4aMuLpbLh9K9RrMdHN4osIvRhSIrPaPTXEE4X/wCE+rfquvAfvG2eXbxLlxY+kYbS5fruDLly5ely/S6GQDSXjq6WAbwmG7HBAjRY2/7GA7kv0XL+kuyMVpyjePpXUlzj0rGqWiHYJUMmbETNoRmJGOl+i9Ll+i5foJcvS5S8zPaGRuRTqWapY3VIWpcdpxudvztKgbN/iVyj5/sas726X9C5ejcJlqo/fEIzCN4SSuVHQ9BVhiJl2hqfPjMJWagdUGZBKPvpcAfmC3dr10BQ9hLYaQfKRk2WKaWqN1KlK0uX6L1X0jpdQuVG+kWYmVaa1lkTrKw5L/ULxDAHNsYLtAGbW1v6KKUhDQkqCLBnaXbRfQ6oLYy5UMXpOIKUkNIbTdMDEbx/5KgSwlArN56wpe5n7zwdpUroRoksILORCWNpYHiA4jda3L9F+m5cuXoHCkL7wBtoFiLLlonES5q4FaiYO9cMtE6xN0QwSpyY6qIgdJdzGeSwh7GYiby9naEMWIS9XKUqD0LlzmBLcmVxoFTiCMNoGFvRXHjrfaCbcEsFtTdA3X/ffR9VUzCb2JwpdtFxs5mEX0X9C/UVzMVLgyDhYEeKbnW/MXrO/jx1n3IKbeZdCUZgXUK9P7FNwLYgauFBKlRCIMS8ykqHO0awZRGIqC5YCehX6ABbMMMWcSqlaFQLoGBUwv77Rc1o4r9+JbDh/I/0Ztbhl63KQSMkbwipmyu0uX6n6F6XBly5cuXLlxSHLUqgwmz2xX9ha2g6pZVQPSZsEELwHTmCEs7j3xLjo6QYQdFxgw1LMsrixCHQHeAKi6l/QFIZC6mSXhlC8moO9sfaIlc3ApmMcx26tK0WKLHZnEkXQlTO8cKRehCJWm0H6Fy/TfquDCtGMMxG7fPaIrFQMWwoF9agAg5qUrKlCBB0t6CZUuHG6GwYpLDeALozWZairL0vRhL0IulMqJg70oXXyo0QqDSusJQbm8rdRKZ30Y4s0EjDAXULZlmL2YhxiMeBBeiKrDmJFsZ0wSwYy5fquX6rly5cuIV5+hm2ON/EyeSLf0u5SMbWgUR5jK8Rp3mZ8UylwZVvFMpOIh16MMxwiMakTGxZFUV1uDpeqxdCLPdBBM9oKC2/qvn2i4DDn3i2OZlrENGDMBEiRDCiCaKlRtEcQWd2LN0XAoqIbRFoAYjR31Zf0L0HUgHL/gSuD0lWxMRLyOPapTjBFczflrPxBM4gqbaiwglN4QUuc6NTFRgcIXW5ely5cHW4C7EoHpMK4x/P1UAXdXNmUHP32lVES5ZKlmgRV6A3DQab+i4wzG6fQfXFGzeNltKljmDsltriCoJ7YSxjvAcFyqSq0dbly4MvRZet+q9VCOFwhL+QZj4LLpK92EauW05zFxNtQblPHi/n+TKVCEIkEPomAMwkDj1Mv0XLhpcvTFef8lR5iDMWlcS9KhHsglyS5DxGoCzpn7xdb1ZcvS5et63rfoZZu8+K4mbwuAF0Qrr3IOYh9ogXVxfHDfE2QELNHeBDQJUaIECPqY1AwWo86X9K9WGmB7qZlnWKt95dM9xfslZTctqY17agzb0EZcvW/W+k18Nszy9f3zEd3UjogOdypSzG+Y3xC5Oz7sQpLedKlaEJcqXLl+lYsRFlXeHj2YRqvpX6s33f3AI3A6w65hAMR/si3EhKmzBk/LjgI+i5fovQ1vU9XwHSeInHy4ha9usM3hGBIR3lwHP/ABhtGI5xj5mZqHFv/INevz9M/TW/PX/kdQ9DpcXQ+gGQMxozvFeYRY+m/oqQgj8uYoYzVwFCtlzqhlRhrdt+EGnvKEh+UowKzPzkFUYy9RlxY+k0NAgFpvAd4fYgmdoQogqDj8LdvEunl+kd4hMWdV+KlxXOf+wBtKRreoQl4w/n/srUrWLmhbVHgN2/A7zt6W0HQRTUx0XFjobsIyMHMlzzm6YlXdNpzEqbZFGYHpR52hW6grG+iVOIVhzUKSvRDFVrAuKOcRcmjvLTKAVm5YxF9pX3GfPMZAvEvQlwfQaVGULiU6YZjFxCFuZjmfKOqb5jqV1d++f+wlvL+5c+gvzzF7hBu5EhhDDEOYFJT3Ys8zATk0uJKlaXLly4uhh0iCkODEu0LgDicqZDiKmY9cPAwZWZluwjqKsNpE5LGCcAzkLjFRgmAa8WSxVhxABDHXzcek8x/KFjeWbkT4R/eS2X9A0uCRKYicOhuiwoR6kc3gStvFY0bGPLzHBaPVGLnkhq3KydIkcImcUXjx1iouLdjnaOQ8bEuXWY2lxYxcuXFixZcaqVJWJWGM7xpnMCOCJF7RmiBGJ0pCm1scxiZNwAcTHBmd5QgWLiNsw0C2HZEcRY6v5FZERd6n4k5w9/BFxLvDKw4TGSWsvVPoEuC0tlRIS9AqjeW7lQy7lta4QA4SCh+by1Jv8AgqHM8yxXB/yNlTgXEVayDj0ZTdooiJLh8/5KPdDeCBoQ5g3MVY3MzoykyiG0chMm0BVrUJtLFxLoOcxbYIXLglLRtFo4jOSXItwamGtDh5/ibIsM/gR5YO/tDEGYQn016DQIerYyUrUo1Qe+ZRlRYKZOn8JSreRhzrmFbfR0Lsep/ZUwYinnP/kA9bpOT3f3UBLzAkEEY0gOxFo6RBVEgI2EdVUvVSzlI1URWMkKyzeBEZSm0tQnMGS9KQgty5SKnzhvM0RcMHKcvCDeBxBmPk/QSqbKveFeGYK0DEVJmVoajO0pUEo3goG1w0gWpmLhajVPf+ysEdTBDtEYO04QqV6hRK0yUbwih2K4P6xZjFLUeJYF3ohX7YgaCWmJgbgcswS0RVQqVeQjeCu0ds4dM4xfEBM4mMOYyvLpaptSLGs9IFpMxnsQA02PnMMAg4ISiVGguxL0+Y4LglQfe6XqEKgYnF1BaNAXErdStrMRXKI0kNCPsiGANyxAsjs3YjYMsUfDr7wk+KlQlkXiuh4i5m1CBtE9LjRBmDEd3zL8zYnWWMwXHEzggUBAFXusuY0bIrzBbs/Et9MXM43doUrpwP8A7K8B5L6QKNFC+Pd2mUm9N2O1VL0vWhE3ZcqRKjltCgVG7xFj4/sFpLY8ylR3haPEpG+sStoVLe5/UXGoFjEcFzBCIQEZPMsiRqBcbxMypUS3N9G0ECTzA7Y8lQXEwz8MS8nQlA7D9TNnM3kU+kmIPDBbqKJRjKxKy1c4yKX6McRHpN/WOSLCLJX77/eG50P1KytryuH/AN6ys29KPxBUK1Wce+M4/wAljpG1Vs+9+38mIgsm/wAzg3mb3HL8naZaqnLpWlXM8GWGGglQyQTRKYX3mSKoMgwsQwHyTJ9kYCd4Fyr3lzPzjooBAgZFuGEBKxw0VUWnRkG0BuLfZlaWaxvC6GIgYfaN4PxMTodph03H4wNjrR2ljgTd0oqj07S4qSOlCsHeIjCbCZE7ypuBcZsREGQo8xxtVypYnT+viWNrYMfv2r9VFVjeLeqaggl7/qEajdDp+owO4f8AfLzLvu48nH8jj3JxM9sS3EIdacVggVptZgDEUiECP7SJxL15wkvvHjyJz86Bqfl5d0IIt5jkoY30j6FiY2ilQWkgdGISvCNT2k202+6KhOrmPeEHlkcQ0jo/2GkuILSpVeo2RCLIkwy3SYqMSLNQeGKZgOZ3jErpcZSbH8ck3aZd2WdI4y9TsxVqA2LIWXamKQN4cnWE2MPY6f8AesMgOBX6eIytfBmY61s5g9hg4RjFdY5tcAME3bC3DEuC5hoHj9E3Esnvgog/kQ4fMwgStQGY3ZLVlLmODDQZALWUKiu+0N5iir01Z52iqLhbaqBuqFjBBcvB1iDl57xqPmOkaXDudoB8kCA5gBhRsl/f0LKgStAVVeZkJ8xDamArtMNosjAFm8x3IraB+Mu7TA9ogcyxjAWOsat4YU7SrLvAaF/7HMiA24jmEUHTRK27ytyigLgVKjMELiFhAbTPWYA6B+pYariKmktp5ibdxGrfmCwSYN2lzVxYEwHmGIMYAmHCb8wW5lwEpKgEtYjNFvwuIa7KiahCfzR15ptjLxCuHaVM7zmbT01KxcpnR2r0hOGJWgwXGtnYg4uZ3qUyrKlgHmFuEW4fgmKmLKol5RGFcDzLm8TGUu4E3Ipv660WXbV8qPLxL2eIKHlLJ7I9ZeYRp3jKnSU7EvtGC5jSt5v2doTjiOawgiIjBO8oaswZiM+P1FHd/THA6sFFdF8yskeNCfum15lajtDaJK3gVGOlGrXouM5xdTKHCgu6L/ZRB83iuIqHqykOsFjYRgHEWj38wbqHMtaZ+ZN9HimWWQbO5ByQUXadmG9SyvEGrjvpelyiFtCy5aHV+I8HaKoMlXvDeV0m+7f00oXiv3FKDMBgwjBMWWHxocUHjc6+MibVFdAHtmoLgIRSneZkSwHvM3Qn7iLJ5gVKxGgHWVbN+d8zGg+gFje6yhV0+0Kgl13f4QLgA7hKsnfkmS6sVgT6FR1XI0QVbeMnRLBaYYhhgagQbhzH3EasbQio8Os8oShC/GI4E0XV30f9l3LdNkGEYWhaz1m18QwIvPkxRWnmeItRGXt+yFkEahAymBBoupDU3BvUqdLhZ0TFLozTEtd0pRQkBHzX7gE3kwqll5wSwr5xDFMvQ7XtFs6BTKWLMvJ18QXPT7kV2iZYk2je76GCieeX+oEtcTAOv+6Iwrp/n+RC60xFO7EhVhb+faMHzcRO8wjrGO+WC27wjEC0ZsCZc95dSLI3ndGMionEUpjK92UtxVnxFuLgV7xs3i6Gi5lE/mLHhA1qBXuRmPKHCNS5TjL3b/hhzqHABq6xxh6EwbbTZVS44iXE/DgXE2TcdP8AsOUruwTIZ0XsygRJxBekCKu0I5lfmYqvnzrEG+Ihex1dfMEVxbk/pEOGAVSVUDj+QcV67tTLA3f2kRxMfC/UqTufuO14mNu0OsKH7w/sf1AXXVmX+czKc1iGLql1K2KlCnLApXRT7pEre7Ao6BCK8KXOsEKPXiK5JiC1NmZ/UvkJU3H7Qs3NzFdwgWyWiSpIkMbxyzdNgZCFkZg7ssF6ZgGo6BJwjGBwSCEs49YHfGbImxFum4IyuUHtLKU2KkHWEM9P8hsvVGWCjiA40DVdYqojL58+VC+fiK1dl9y4iztE4v8A5HVP+IRCooYWWSqogRQjouAPTZvFCAAYRDm39QGrq++agTMnH6JekXv+CLiCgOz88wZPQl3bDXYfyUD7S1PJN4Ql7vf+wnQgl36S0SLslI6BAU9oMPmFA7sT7rEvEQgQkcFxFhvHTcsTTeTYGlFsg5SXCEVHVCrEyXzHAmEMwA4gKZTzM8JULHCdpcv2hi2ZIaEoqi+YYgcEQVxC7mJcMeEDuwQDdguSMZN/3LNG8wzxCQUcRuXhf5GZc3+orNzDpGkpMR5HY8nmUb7d4bVG8RqooUuPSQLGESPWA+J/YVhxCsd5kPW/8lprrK7JYNtoUQ4hDNxpA3/EATpn7R2DrEeNMV61Nz0GeQJGsIq9ksSoM+Ik6NR4NQrg6w2aBfIy5TOg4lCERcJDblgHMRpphtk6QALEZGZWgD7yzOUBZCxUsqFQrrE5aC892GC3sjX3S6BAg6sZMCmPI7f3OcF+CIT7zZ90oLe0ET3uBT3c/PmIVKgvDFh8fat4da7P1UcTMpdDL1Z93EfWDBIkcby1O8NFN0P3AbvP9hHMx+WA96O17SpPm0tLuRZxWd3/ACbuV1c3CwOP8jGMZhXUqYrW1/iYmoB+sY7tiDI6CN2EMD0nDDabBoZVxGVRhgiC0jCccxQnVjxKIJkYZSzmIVB2mzKBOF3m4zZlQsKioVlcQNTKotHPKwASZA9IY0eYiB0hxXqw8XQitvaHk9IAw6zd8RvDZ+WOOd4wZ7fiO8sqlQgkmZjML/GZbj9mIbj9oqF0zHhijf6F1Kgp1MwJrMRS4R7X6jEHZgqcY/RGyHH9uMlN/wDIgvCKqEyKzgcwPYgQurM2RPYMwK5Y1VdYzjCWdocc4qJa3iiG83GLVSs1DK+JWaKdEZ2bhSBWoNHeN90oUu8pgsx4h0s6/iIVJWC4SCt8xLxLh1z+Jl9o9I4vKQXMl7R6OJejcFh3YszDynMdYd8Erc3r6St73FdOuIBdsRo2MwCzbEW4AwwIhU6xTvG3Mck3X9TeWWvHqplGmZvHpLkmQCEvMVeaF7DAksqjBhe0zejChBmGWNqlYHEFiiRfKTNfSUS95S7uRDS7CMwUwLaVuxbdEyplCBUodog06BcRtiDoCuGIcJw8QVERp0qI+whomJZxBvHSZTjlBi46d9IumBs8R0+SDcccaR3s3PE2zMzdKhVjQiQC2RKeCIoqY4uM0Sld4y6on84QKRjJAoHzf0DU/Z0ffz22l3tNzw/yKJL5mYcRVxTbEGwgXMQvao+W0NJ6wcuII3iI5RpFACD8qWFhXJGWzcivej27swTpAK7pUJVfMAJNyLmBFKiBhFZMLUwcw41DFu4mJmukpE5jMZRNh2hAS5aKhyt8/mUxQYe8asaR2IMUTAQzLqMEsRTlgDAqIl8zJ2vEweKgtEpb3jwlmyBcSr4/ffxBYMrdZHErGMxCnrn8xCvyg8PnzpGjn53lYvEpipu5Bmd2MUlenDDNufBKwS6xAr8X95Uq6QuWAYn3i7Ky9oQF95nBzEjChTCtfWbFcsWEynLvAuBeB7wN9rgKFxVxK5nSSy1gqNk5JT3lB3iqxtiMds2yuRhSphIvSMBJ0hlcICg8RcHYl0Sg7fhGyys+Yal8p9pd5uO3rWPvHw4/5DUZQnS7irMOkTG+L4lqbh/GCpHOY02Y2HYfff8AEQPb7TdiIW+0zMS4CibQdP7GqCsdbl5Td/Bt/fvAGi9r/cpqtpf+R8OsuszF+WfgUStAXQ0Ns5l6njvowBXQ2YVsGQoQcRriWIV0g+ybJdzI33iyI5jOhDtCkzHrBFoLEe0GZOm1iqBKzGoRcyg4g9ITBTKDiOgI9JjEVHudYcQ3CqTpXaVBB+1/Ojs11lw+dB5rDFDoR5eZYuIAPJFXMUde2WvN8sLSbhYy/j+krTdfGIk6kd5UMjEIMdKjxXmfofom87f7KL2f2OFt4+KZe4gXeX3cg/194pfbQ0DoEpZ66C2puhN0xvUKgb6zCFVKWDMCAg+cf7MRHb7sNcfMyhqU7xMqXcBSYaEL7PzpkMKm/Mt1iiVGYS2GxHLrcuXehdRyohBiLKsWrhi0xBzG994CtIS0dpaVHeLcIMWBEbukE6GPMx4lxsw1GfiRB1f+IqV6kdqIpKFDg8CmoRnMuHsfqK+3/TB9iAhOn9YgsfMSqyvwckFXqfgkVq6biZiVfvK7ZMVgLfwy9GJRZ2z5+VCS7246w02twM7d4chzN71+fqWabykhUvf5+oNp5/2INoX0iKosgW4ly5TMwVxoIAhp0y9KuW6BgnMYSo5m28qGybpUJRUqSkzhRzFDmLmFCvUR7+KgtIqiVZS4AT5xGHLtAMjDlio7lGmW4muLoz/JQIha/iMC2x/rD37MxWj83jwexEIAWoimfiorKDglOh840CE7zbAxwO9hOkuBbUeBLYs4gmexFKEBuRUVHWTFH9nInV+xHGCxQ9K4igvWIqjM6Uqm4uvnfMaxVFxBlis+UoekaRVDv+oIcmJQKY7G8xriY7uj07S0qW3mdD/svEsqCYuYN7wvyY7S4Qrfrt2ZTWC+ZV3TroEK9o5iHMMX/qMuY9hhwxNkyxZcuXL0vV1iXMoG7pE0UQ9BFl2j2oWURXaJFnWJsnEthvRoZpbCWlwkKnHxEzoXgzUSDmpfr3j1KjWgZ3JaGnTaVBI0Wu+iesNuIttxHMd4S1FzO7lY16DfP/It070bP2tUGs3rv8NZ7S9Qp7wdyYSlwr2iJXeWb3itpbUN6BeiW3QgK228zdu7VVv0+YhZbHZliRuVB1AdtrlpUoqoLcQICF9rvHk5gxDUEO/AyiOIRKjFDmUEKc6piNuqxATMFxKFzb/IN2ijQ7MGZdpcOiUroyzSvsyyL0uPgm199r/svJk7OzEk44uK7QbeIrgrFv8AJQ2lJw4eVdpejZ1x8YgZbV+S/nWF4a5Qtgyde0yVQictGC//ACVIN9zpDswwCs0VzvNkq/DhiFDsCV6O+iZEOvGtEOz9RqS7QMbBIC5xAJZmAlxlMfdrfPQot+0AbE+B+B7yiLAhL2f8lX379D/HEReYODs+8Nhd8eY6Kwa6NtFhYr2ZWm4wzuHpKfMc25heLUNtQtonSjVjaKHI6Yl+o6RjrcfShd4o22iofDsEBMQJzAluHL8y6K4YJ6ZfayO4YRTOP9gsLN3vniDQm8MI+f7Aoqvn2hS34rQA2CvJ/wBPzCVbZSmrjjb8qj7QyHRT9Tb/AAtmxleJcG2o1xowWpXgcQek7ztAuOXTYJbdwWXEEVwBDS3Fdij2i/sxWVF+B8L9QmIHkqwz5OneIGeUL9D2/qKJym7byph4IG8MubdwTE2lh0HTmFeJSsErQaYRS49KoQAckp9QZ0N4HfYgeY9aEoGKsLi7iXS+ModWA/3RZKkEOB39toFrDd+3+Qp5ItUGOsCl2iyHye21TEKcOWOVl0ahbyWAOv45jhxqsVHS9bwa9K4dpnqBWl5gNjeZZ4b+0FbDF1WGYk8xjootlpCZWjPECzFLboq2RlnECG+lyoOPqGp5RedMeohFEo9PtOoEAZog9G8sTocQ6MvpiGxDMekS8sBc2+0Ktpg0LbcuDmF2hZ6TMQaEPobzVueJu+tBPytLYmxmyD3n8cxnsjH/AO40YQn30O02u0facwjOHzj1HV0//8QAKhEBAAIBAgQGAgMBAQAAAAAAAQARITFBEFFhcSCBkaGx8MHRMEDh8VD/2gAIAQIBAT8QywR1WDWWymcstjLWDFS20tdMuGY+BdpbKiRY8SoDAlwwjfMbyrlmWDo/829Yg5NN/wBwwj9bzqbeiFGEEQuK4iXmVHgR3GVRrgTiVcJ4dUtB8HCEeBuCZRNFf+LX8KZX/sdWwrgSBKg8OcqES6RIIolQJUOCngAEpEXE8AngJg2VsqV/DcNaI9H/AMQwNSali6z90i8LjNJcQawR18QziGOYZyzgcAeBi5UqMFGJDhVK41K8KC3RT6nzn/P/AAq4H4DNskIVV8j9zL1fNwuEErxlWXruNkuYF5lkG4gjdDWJdhjWFUVKkKG2kHEoYxYhwYSvDX8F7001ryjBbFV+ZUr/AMUmGf8AOj9MSpEWYaBKWYhTYQs1gJmWx3xFlXMMBEYPeAQ0R0mjhUqMS5h4a4AaymDFMSRIy+Fr6pcTt+ErLqVWEf4a/pv8DBXmZpXzlusQ14doOBxU6cGjOsBANoEjAQQivBUbSuDZDGIZjTWOURXKy1cNowS1lDhi6pS0TrVvxGOZLg953QSB4K/vqifrT1S+FU6xy5oxC8TEbwxUpKTEZVhBGFP4ki9o1SphpzH0uK4CSVJcth1NB/37vLA4/wBxALUD/wAVVnD3Z81xhvaGK8KRMRdk3UtYDKhxFRah/HUFlQ2sNMcADxBwNjklV7wbtvDO+J/mr+tkDahjlvDAaGaN5+jzlEDwsRhSVAlyv6DKm4REV4MeB4eOUNVJ2wmqpTIYdumOccCaf0B1cMK1gJjw1/JZ0t+6xrLyJbG2TEzB94pRB8V/020TBzHYUYCBwnDiMbj6Q0msVsc/bWGLP0T2lhd5fc9ISrSnx/KUb9wzGYruMKYwhLjMvxhFjaCUXRAdMn/OzGWgwUmXzlh2604eW/UlevmuPhxBg7ac5QFAAafl4xtbKI4xPBGNZ0uALOClw1lkFf6NXEGJXSbSVEIvgMAICy1GT2U+YoX96x2rzqDkoKiuNqPj+NhiMqxJaXLqOEx5rGN4eC8KggY6nX9EvfmNMwClh7wSt3GDSnSN/arXv6EryY3v5HBqhjXpBDaUxTV3i3eo6Ynea1BzRLwdy4RUymbKa7M0iazCHgv+SkTBZyopxA1QFwcKLMyu8qNIw0XKUGQ5h3OICQlrLewhRtiLJc6URMNqiuSAdJWxrEjM1xgcA4Uibix8T07GKUGBu3iNa1wE5QtoYgUNXtLZ4ZreocJbfvyGtso0yvqxmBaAv6zQoUPbpL68B43CWHEoSKc8IBCsyhjwV/M3M3KiGJiiLtHC7p5JjbWBS07hPyKmjWFVG0q3A02QWKDGfx04YAssWal7gvgIK8wa4IrIBEl8NgYHKMMwuXjUSMMayLM+XLvNVojMFkL3BeVQonG45ZsEzwLWvx3mMA3IOO5CNFfrv5ypid/AJUFEWJH2l4ZiS1eI/iqV/DRBRbS3MWRe+7J3xEDlv3iLUMtdhBlzLB0QVoNxd9gNO8pYdBzemdJiKOcaMSWlRJMhcrKJfDfgEjlFNwLhTxjEEplMyxtDUpmYzUWlFjriExAdb/G8LAhj361ApYga181FeciZcEg4JSBxLswJbEuliAaWBebgN+Kv6VQi3OXxFuYmghoeabN9HaES7kVjnANDWKLDsDfb/pwRlyuADUqjTgqYyhvw2MoQlXDCUQPExWUS6lLhrG7qGD7tFD8KrzAFb694XvS4+84vjrM8kvlj70hOYmZUcp4CgmSWS41xKjN0TUoQZjLEgR0gdohiyao1/VHSmlzdBLQaqz6THQ+hLqc8PaNQ2TziFBpTzuFVe/8Ak7J/33iHrk++ULrMSDtCEOmVt8OEyykCCGUFtMlMCfwmBesqVEgS+1xpHjyFxmygbc7/ABLONbHJ1x3g8sfSUUu+3DHBSHYQSmJyiLGkVwiJTrNEYIIoVRS1zCKFUIKuNRB/FUrwVxV9bT3VMt5MzvWFnO/XSAEzmAReuEyDtccy6e5LNFNiMhqiNzXfBJSmiYiBeJWS7EBUKSnAfw14EGrDYS0XfP5+biwSvfeWAWjTl3jMhCxki68jwFmXil1FFTrKCLGLNIsvjrjich39FBKYJGsJsbYl9Df8zMv0Sp0xBEfv/IjnOJ8/zBIMEUqVeGve5c/TBvw1xqV/OoHfRxodhxAoqc5aXs4uDxbvDh7M944vRlR3jKufYPOCMXAoqimUWXCPEmUEWVsv6Tvpfkw9g/Ky60Rlf3ziHVFGuaLHz+JhB5QIiaOsCyLVXg7Er+w29Dbve8wORmpaA7X6I1DO0PPdZUZomlK0xEaQzzlkg5SAHMycRLQefC4scx8K4llEUIeEloEQuaX9JW+hi0cz/sLtEGYKhB3fmM98UxeSo8a2Z7T/AGbDvTqiP1DPS39ek2hbEoIiwOsItIUuY0qH+Kz7yi+CxYvAImYUjmMVAiSpUIMoIS0Gz6nA/oXjy+CC6PX7rF3P1FXiZbMqHf5ldhsplgNRMAefkE9n/sux7fmd0xr9qVY+e5GR3VDaPKFojAXaXK7h1hzCnOpEang3WMESVC+BxA4JwBAHELMaQzwP5b4Eyxer2e0ALaIVIu0yvF0iQcBhsd/rHTzxiNB+JWC5I2m9lMUqly4Z/oXGKHMWZSOcQaiK2K4U15sawSso9F/9glLRcdjk/MRrxUR1l6cpSNfa/wAlrBRBUoNkGVwY6KaTqe0cwQSo8AuHDIIqEEsaEUTAzLCUmydUjV0xNVxCDqgLjxG41j8XG1dOF4bxJSas9o0hbEILQo/EtU7BgWLLWOpA1TFoMmDAeGDs/fxLMfRVctvOoSJWfFXjNVB4UbITRAYphw8FCGS6R9UFr00/yDi6ULaua7bQkd0TjsxvFXMwyg9ZnYtp7TK6wzxcYIF4K4geBKlRvFi9KNrURGYE7xmB4SiRchGUpxKdUEYiCahNAEKcwF5IQ0qC2pmiNAYs9RnOAzEwvlSzykMqGfyJqH7XDtZNGPp/sRqa7Q197Er+RsgxzAZI9polVtXDkQHSJylrSEwkphoxVE5Q0vJ8Qihyi0qBR5wykGHIM9OkzohgjRzMEMu8qMEYQ8AVKlQIECDZblCdYBpALWIpMsAVQS2LOZzoxrrRAM5lQoRaZmWGStJZjEDW4BuLiMQQDEGfQsdbowgOt+ZZHqmg7/iXeaFjMXIYhuR7oAChp/LUR4yAW6TltxipSNE0HpMj62mJ6RKu7LoGUgMn7lYN5qlIiljRfAglalYLBZQj0l3l4C3F2loJ0jVQwgRxwagiXCSaRiXQXLlS9BAqj0gbmMFbsUWwDrCMMowIlwyvhTNEekp6z/Mod1hx9GJV9N5d6xd9JV7D5P6VzW2CqEuix+tpcglhlGBF4ZjHZmNe2I3qm01XkxlrmMG2P+xWXxvwFCWV2IwntFComK1hYirWDZBC5ZqiHUQhwSUu5UwQLtiIAY9KOkQjhBNoAgJXjWuK6MtFzkRj7Sh2GAKvuo/e+JavuxEnr+WLFscx2DrX5OB4stF1uPVATLBFmkuX4FRdxUY5bER9eKoIsadCCi+UFkbzaImbrE7HMdUop1l8DgSF4BhfO0VioKW94EQssIVTrGQObEpcZYylzMzPLRAWwRZmd0K6esLtHdKJY2cAKIPeKMzgxEL7cdeQ5aCWME6kZZTEV1HxB3tVzi7LWKOekFN5BhTW8q9o9JuhZio5Se3/AD4bloOZudxm2ZRCKAXLi2pQ64giyXwZZAiohxGSOD7CPipHllL6xHKHfeimkhGhrDECHMLrBixYNyocLdoUczGMs0YMbuZcJchaTPbLMoyANiEJkZgOagujHvKD1U8qlzVJ6I7Tsyn/ACXRHs0+cTNFbX4JcoPK/e/CS8I3RUsTwBS3hrrhW8Fu0NvoTHOV/wAlKl+yANekAU9X2gXQ1/aabaGAw/fOLA5fnwKUuYgQxFWFxbCkWxFBmKC84lVpLl8BNGXNUZYPSY1qLmHFsz7zBeT9otxdCwYhcykHAkrhbpwOSkd0A6RUSptBRhUb1ggGrneHOhDStuD0g2w9NPTWN7kFbfWAyf8AOUD3Vfz3iYgNcZ+cZiAiJS718lef5jSxWNH3ntMfFOz2Y7WDsfnwXM4dIq43EbuaMSbGyCaRYht9/wCII+0KN0/iKKw5lSVV9ax2SxjaNY+M7mK0BgVF3GC5TmN5dyiExRlwIzUq25o1txvRjaos8peh9ZdlfW5n55p4FokaafqEWzJBQIueMl26+ByiIZTFiwjIVk0hJGa4aYmams0JSC3g0MmhUm0JQXW0GXI5/g784CYKRz8edyjlpBbkhwUQa0+YDFwW84SjovxXY2lI0blbO5E4st/OG5zySkCLORN2tZliJaYiLMRExIMj7YiGbpEW9B8QITqhYMn67zaxbJ9rvO4QZqG0II6QRIx4CYrCsEOSO2RXbLr3SmbyuLB7iIupziVOjHWWCrJHYS8xmChmJF3wIVrDK5UvRuHKaVRtsgxYzVEIlhjki+EBpERreCgVaPvnMc6zbns1NioGhDmwh/cEEIjSpi4NbIHZfmd82ijTk6v66SwVhhAohlbpiL1SLi+c3ygWtd4lZCF4xq0g0UMTJb1+ae4JXyIV+dFj7sx5PvSK43LXX1ctk47KKlhiG4yznCM9VLekE0imkEL4AA7awlwX/Epzo7QNjFNfuKHqlgC6itK7Qs+6zB3fnr+4PXi9IizNhHZG2sqEcriFVNilsyGUtIsUJeeEIXO0YcwqSC6Uz0WCBmt3lOyVtvFFkFtEI1CDFZh17uXYpqM7sQyxRlveaMIblxANGXqbg1G3ArmQVRBSxyhxL7rze7BRNQgCzyvHaLcmQEOSmBA0/EVyMzifqYr7vKjSEWVUdIxJlBLM0aMRYxGEZluLQAjGLae4ZgWXWCvESUy7BUZQ1QiZiciaLzJapulcGDiKQyUjBFT1COY7RjVbDsSXaNlXmNpmyRl8opnHMGms1ELENSEGssRAMYmYl5lJB8IGg5i8G6aS5cvhUCDwEAq7H5YLU/VwUduj5Jc1aPiXMG6EQGz4mrDRMsV+6xvqyvCRe0sjX5GpHVqgmNuR1hmEy8NUFq4IId5YPL8CGjKHdZvTE9wQZ4r0oL7UtcrMWCZDpGCXM8cxvgyuFtFzSYXaItQsEMzMeRGhZvIjoJqqUlGRipcSlZmqjePQ0htsigZg3sxU0wOUrX7vLRsjrcpGIAIYK4Wjwbo8AJUrR9ax2HTHS9Mtex/EVDLX7fgip1QIlP3Rly9N7+IWS4goBz5ii0dkoHclLF1Yhbo5aACdYwjVPPFx3PL2T/IhVHTO8K8jEtTi8u0F9hg5ltyzdMS8SkEHGYZRaoyuC+BIjyjvDrfrtDudCa0+sxtpCuyitdJ5QmC5QNUQtzMK71RxSCfNLELJFxrGAGrtMJygERckQcQL7Jmu5LXH1rB0esq5uVuHNevlKqB0gJXBGGJRHjUPHXOXWh54HLoZfUxR8Exvm5dqBt6NekRCgDsfmGUUMBnJjL6Mcl4l1AyE5sggt3Z23leyXitGYO1VxA1xjluWHUiLysfMciROC3phxdIXG6UBpdjy7/mMU0fZ7RIohWIi78JB4Dzi+1vAZER2/wBQJsgMc5fuiCT0hSk6PxDUnKCTrELpFPaqOt0Ij10esXN3EQEaLMIaDN1MCos5jywlREtOkLlSu1pA5qJ8oU6Q4E4KIauDAckDccbYV9mC0sF7f4iIDnLw3yjD1cncjtCOGLaLWVpNojYczvNB8rZRUtIJuywvJ/cQMwyMV9/4jxl6wZgULiWxqSwgrd2iOz7SgFnr/kQqYMU7QGmsHLC5/wBEdkpNm3N36w5TM30e0pLZWbi+GpRluWPwXFIy76Om9Iav1iwc2GghLdNIFVrFUXNRqzWv1E06ps8paW1XEpdiHicxGF2hAukyHmpeWQxqXECc+rlaLC2syKFxQC2oF0SeSVLcohCwCElJIlvIJXTifpNYfAmoHYfbSA2WhBhsc0s15y1Xoy0jiN2IEm7ZZZvWINdEq8iMcwYHlL9EZW7P7hioL55xD85fFhUekRbNafmAFOarlNRzbEpa9INxwTnDzShonV0jpfQesN3D3jHNanOCNT390s2ZxAlVhkk3RKlLgIyH2lxhLnMgFtrFTBBy/cQu5P8AYA0ozEZ9b+rKShp1e0WPeGgdJn1kaOZBDygvLRuUINAJezm4odIga5Sg5lRweSLK9Jc3IT2L3hWW4wefKYh+znNZY50lxUrGYSwYtmuR99IKujbliAAHTc3laHR8ktawOt9JjAcQe5LFh2lq9olYljJNphWq0hw1CFLVSgARxYXzRuKXHEXN3iFdjAFnMEByhm+ukUO4jb2Q68B8QVkslclPKNOTmKA2qGNYyPK4rENCVFbM/dYDH7/pCaH+SxveFFIMCbDjfBwFEeUvXG005lqt1Y/SuvSMcioqTaokLE25TcrVfeVdgUoZvMQNessQlKMFxZB3YmGykV/Z+OFCI7WiUR6Iu/cNK64iy3uQUpbiECdCVFkOTLMuKYIA5bbS1YYrEE0NoxVZuDC89GYVEATGkKtPKIun+psNpTB3/Mtm41kf+xRbxfuqAxZoGKPT3RY+9Y6PWDQ7RZNbynde+piesVit4aX6ZmyOl6yycdTrElGjZHTe0w4gal3M4hcdwbEz7TymfaJqYJndRLzR31vHiEvjYzWYBgxHZM0NMx0p3hQtkiJsPwSrtSgXvBavuZYPRm71mJ5RIxrDsbV+47SOL/cQJqxbB3/5GCIAmFKEPGsa3AMDPfWJRpwa0I7Y6TQLBWsNUMFmVqd3VmijeAGnOsqXz94otsIvMtTCq+Uct6/jrFvgVhzPmCkpzbFWPlFaVdvs/cuODLUvTKUpXJ6RKu8f9lxF5hFGqCHaJpR4j/xFzOcQF66ysLSl8pQLymV5ME3n6zltg9JdKx5TBB64gTdPpHlMGwDevMibqz1JVonrEdz1it/dFhT73gaGLxeNxD0ZhGCUJpKXvDYGIViWKnGYRt4xEbb5ggsziUs6fMHhKXbgx7QgBzcYIwKbzYrfSHT6w1Dp8xAd5/xAAuFb3vEqHOeYixYza5giMd45VIpVxzTdlmYuoKtiGhVc4CRyfdIrWY+RNQ10fxKEoxn/ACBlAHn7cVr3JfadOTnpBQOZ5AfMuLelkGsm8vWdd3P9TCGaguHaUT0XrGXlUsY1ylZGUjA9G4wtNPlUTUK1ghdsTUOV5coKqpkQJTk+IqUg924ZZRiAZQKzmWMdcMyXG0KbRZ5ILQqADPG5fGx1ltSZ6zRGkpOkVqOsQlZU6QeR/sditSGsuZRiZgVVxXvAfMyNsZlb9qM0gWPuYPSlIXWZywhXeZU7MVd7zKN3SczlwEsGxY+TCrTN9LWSO8awApi26zcZhohqr1YzUjWoy8ugidTWVXXP4zMRl+f5IH1MY67f7BijFVnAAQc6weK7/mZ08/wg9kNXRlE7fxhqCs1zv5Qqd35hoJo4DQb03iGJxUfbBTHtlZqxKapvTUGYbnRgFxAtsqoWHxI38i5cqXUXJiHC5ciX+Gr6f7rAYEJY3lsMS6mNBjeZkokw7koGcVMl5riG28pQJEGM3KhF6SpyLyjCktVTSLQ2gAIjpU2XSHQcwua1Ga94m+iAtgCrgt1ZZKbmGcukZaeUu3nE1Trmqzd2RJuJiBE0Y2liopmWXmm4c6bexC3wKDs+Y9brOYdrdWAoGp7QZbmLNekUvk05Qmy/cw7KICCZHaGu7b/2YQWnmxLObcKDes3gZd/eZnNxUQXoTUqFKlq1CUmQhMQrXtNRy5Sg01WDEP2GYCukOhXzLwFMpcN9hlAqwkVY+HlNCDLekSiGYNG8sXepbqQ5LuUWhAdIVlx5x9OMTJb2m2GIIlZqLWoLT0IiaX2h1sMbPNU03X3rLA56IAXdzGwbNSE4FSlnRjXBlriGtbwooMV7VAu9RaJrhlhneWDrHOMBisecrDFwKiawil9IgznF8CDLVeYKitkuk5p6Rg03mRGgesCklmP6qKtWgvXWWnKoyybt+0ExZYwjt+ZcPOvglRiIs2f1Ea+f0lAUSte8tjkS3aZ3Ivz2jh33m5Jg/wB7RnF1Karc4h3MNmM8qlo9K/7FFtSvPnGCxTXpCy3hsYA70xKMTaEUOFiK06w6sgBLNSaIt3ygFLgHVmcmKwyKk1j5c56Erb4RgRhh7WY1Ucw0B9JhYzDKOkbYwYrQJUIuvODAuA0wQgxC5CNsxVzwHTEV5NpXS2GsNY0AzqpL3iqlTUZTeyJp7zBS8rWn5gBvmx21yYQeofSOQZ9f8QFOYfEFs+7zIdV/hFRldQBTTlBhKazFQspDGOBU1zlOGXi5bmo1Xv8AEvgLy+WNZ2iEui8xCAo53bgE30nsZiH2uIDV2sDH547Sl7IyoFPSVUGfcd4N8Lk9YEo8FqZFwlSusdgxLRi1e8YAg0uKpYqttpXctVyvA1iZmqY7TXmZcsgbKmuGqiryyzlBLlQFosoiM94CpREMRLdYUGUoi4uU6w6r1iq63AqHcu8AKjmkqq4+1ACO5MBdR0ImF95mJtAqMqEWWDO8MsiFK8ydxKds3lmWqfXEsTpNEsJhYQwtdGGepKtyljvys7z1xAptz/suEHrfiZyKomIl5mhKlxWNIIQm6N0rz5Sp8rYs3KQmkIhqYjAbmC7Qq1VmIRLRhjcXrcvuRa/mKLTPWPJhrV8BQhu/gqVGanvAwTLDEZdfP2liDeJnO8vGpwmMYXcDFpjiBW6iO0FEbN+JZeO8s2lTSdKdCOjWkoMGMwChFpDdXOIsdCWtPtw2gJcO5v4iFjMFM+6QV64KJauOLG/7h1ykt7fXjE2KYlrsMqLWZfDFSrGoQVxtFjMG2twsgTR0vfU7wmiEuGQCPaqytcS4ZvGsGaSoFD2lhiZNKYTSJFuJN2SpSNx9p1pTnKAd9JQNRKaieaSooK7S1YfWF4txKNXW0XY0y46SlwasWpcx2H2n+TCGpY2uLjmao0gSpUriM0OkrgTSucDYZ1n1lYawTnDVh1YpOUPS2oyrrwGWSjoqBLuZEuJlslq/3KbHMryaQ6Skl4hoUxSxg4C7RDkmmNWN49jEN1vEpgi1xDMfR1lasPdAB0jclwA2vOcytABsSmNF8+FUQsQaRSLtkz/MPzfxM8FOjRLTzyKFKhLE6Hvyg1aWrcYaLmmtVn9S0W8whZrMTSdYHeCWYGVQEIdCEJdTB1jdwpjZXCTUgOUx3B4hRrEENSXk1YCBh1Pzf4joDvALlvUqBWpWU0ER0YYu7XSc3NGHpmE1uwmF1/cyDk4S69xLsqg+4iEWJfJMOCjgQqre5BVWuZ3nBLlI4gRpGGLcsedDYRTghQqJFRmDdy/Or4lsJxzyP1CrGhCYJBw1RlWWUYEaiTiC3FQEsmalDtRGsc+n1jV4ly4sFa4k4Y1rh5q+koXeILUIKJcuWQqDUEF0RO6rojpF4PiWZd5aMCcTGP8AxKCdWoFXTH5gF3vOoYwNmsCAvqV9xFIQrgo0MmsamGaQs+0UXzQIsVlSq8GINlC+cSdM9OBwQypRS+G2qZdxLgSBFATrAvGm0wUlD2+eICBwcNG36gDPnKoTMD3ZFAzPIjZcAkoeDbK6RvvAjr4AG6mvC+BsIHQYI+HMtVcFwnONWXmxcVCa2JDBamHtlTu/gjW9cFI0BHbu1jz1nLysRHZh5LCF9UMnX+XOK8lJpBFOkMI0jgZtwIOJxry8TzmGO1YM0jGHpLJcyvWD3RQIEqGUSC3FOi2KnzQvpFHDVuJ0gYxLSsplEubcb4CV4KiTsgeF4srBeC+y+sdK42awzBqzqSneagyq1OxKN4evEYJUWJSkUfAeBjL8ehm/D300TV3TS7fb8AjNbtPNDX9ZqJq38p9zPy2hCHE4v8B/UY8CGu01Q4Ov8v8A/8QAJxABAQACAgIBBAMBAQEBAAAAAREAITFBUWFxEIGRobHB8NHh8SD/2gAIAQEAAT8QSAnhxhRoXIo5CvnGjShbmgCPeGa6dnjBhfQ7w5AOuVFh2cFFrp7xkKK6ehhZrVxfIcK7UznEcz1htxvL4yNunWHhoDPMcu3gHjC8HswVZgnWNG8A3h3hmhcZEYeMcAFXmYDr8ZIhjmIMmH10aB5+caq79/XX/wCZ9Z9NZrNcZAzWawnjIfWZrxn2+k39IZDJmshkMhkPGTJkMhkPGQyGKXRlH43hYSXInQT4/wC4wpwibPO3Z4z/ALgf259ZpCN9YFbfnDADZxjGOw0uKbSvKdYfoud58DKWambw1M2ikePxjmh+G8JgBy+WCkbOzNNZDrziFoeGmVHTLINR4yVcBBz3ga84Yry55eHnMexcAacSQyBo4CjzlWnGEF784mRxy5eMDgMNCOx9OTJkyZMmTJkyZMmTJkyZMmTJ9JkyZOsmJkyZMmTJkP8A8Bk+kyZDJk6yJEpJinEb+fnEdClDwrdDjborJ5PPyxFzxm7TBmMag8YuC05zm2UlOsDYl+fObYdOqcDEET5Yp3vh85PKnCYMVW3Gp1ih5HHeA7FmB6aGchg7rj1lBTk6FHvvAqHhHJQwbHnmfGC7b5w1Maz/AOcMGLviGbdguucvytO8gnSayVuTJkyZMTJgUjNTSaml4dfSZMmTJkxyfWZMn0n0mTJ9JkyZMhkyZDHJkyZMmSEFSEaWh1VcCiAlECwYba1m194b2o+Df9Z/yZJgXWMacZR0bZiRWhgzNCxnblcvGTNmAujEQug8PyZyRXKyK2+2PaIdXAWGB4/WBszsXIvsyYGnzielxpHWLwxp6fGONMpU1jU1HvKo35zmWuKrXJrJkyZMmTJj16xCAtl09XEPf4cBCU7tT/3JkybyZMn0mTJkyZMmTJkyZMmTJkyZMmTJkyXJkyZMmbZDYhTuOWUTsSqpvAa6BElsrbBwJNhDgtr6ayejznG42BOGUhhvNfbItlyEV7kwFIIahVXtxokU2eMtwt8YqiZyg/GLKHQ4o4NT5zY79sfqJwMBoasbldTsvrLkuXT+MlvFzm+c7/8AWXVbbxtRN4gA+TO5pyhhkweux8YIBR4cVfZ9JkyZMmTJkypkyYNDqAVq0EN85uAKgQNV0vJ3kyZLoyZMhkxMmTJkyZMmTJkyfSZMjkyZMmTJk+kyZPpN47+l6JRnOf5H9ccY5R6FoMEkiauJ4LkQ0BSecjpRz7xNwIiHL98EFUNjJiV8Bx87CYpDlaTFnGcucI6Q8ZAXvBStPJiX2A5XCIddY26/DkwwV+b2Y+2Q/GUzsL8zBPX3nGV5xVwrWLDe7KV5vGJjGTJk+hgqLtOsWRqlHQTjLzfDf8YJTtMIQJvExZacZMQmTe6ALIBs4jFwQYRQ2FCnOd43aOrgNFX9ZD2vNmIIQH1kyZMePrMmTJkyZMmTJkyZMmTJkyZMmTJkyZPqmTWTPEysDeIaKOMEK9aP/cBJNpFxEFB544uirgqLv1hVrB24DqGOwHfAZtxQ5xFd+HjGuB7x+qI0mpnIb98EeR85VOjw5RomQRr2axV28/SZMNcZweVtO8mEJDCwPjAil3BTGFFTlf5zXZOVwHozhKG0Lzgu4DaOct+RsuftixQG9DeawUF9YOxUo7xeZLoezEuFtRrZDsXaccYwkSNkitpDpAnLiEIgQyE4NdYBNdTEdfz9EyZMmTJkyZMmTJkyZMmTJkyZMTJkyZMmTJkyZMmRSPxD7z/PX9PvjACuHFdNc3nFOPeffEneCLxYxDXwYnfK8YI9zyOOrQdnGAcLh0Ri+W50Nrm/dBzg9Ap8YaMPsQMlyfjNHHE49+cRz34yZMmTJ9JkwU41jwbreIAafvDquui4Srt0GdgvPe8F5XCyaD1h3jWOI8YAuT3lsh6Qs94vJAwYMsSpDNaX3hCKF6soDY4jwuF7ITzynnAQBwb+ca4mJkyZMmTJkyZMmTJkyZMmTJkyZMmTJkyZMmTJkyZN4yvqsHv8jQ4z/wAMYVXiHr5xhvLU41kcmTHFNdY0hwdecA8HhyzOmHrFS/ZiSb14MAu2aXUyzeamBHnEoawxfHOAr0OJnH/cmTJkyZMmTPGBcapx0YpRUx8TFKIlfnFNCZyjWW7wsGnOEI4wAKbQauix4vvH7Ku1OgIMALlv44UgIW7pznig0FaGyocccc5UoHA7uQOcTJkyYmTJkyZMmT6TJkyZMmT6TJkyfSZMn0mTJkxoWAoBHf2x+4dulNo2mf8AEMuyvlidn5xN5POTWTDBgw+3nI4KTrNZt++NNcR2zjXnK4neTV5yYKaMfP0mT6TJkyfSYEyZuN3gcBPLu49hzgF5Jg5KUyYbwe3BQZrydEEpY65HDTIurIBVo0HHRjzVDdzMDisOcWpstZLQEGJdAoekZMTEyZMmTJkyZMmTJkyYgszBzv1nbkmwj+MGvgQVPjEDh5HFGkmTI5MmTJkyZMpZd+MmT6TJhIp+qey8DvHChX42Dk+3ee/s5ecju3EmNBp0HlzbVZTWp84mU6EbikSd4o1MmTDWbcGarNcYCOJk+kyZMn0mTJkyZPpMDEy0NxyVYHE4yfuvgxYVfWL4Uc1K9Ye2Pe8Y51t8Y01GJAAM7i5G9AN/DBsaiqHeDBsKWNhTq2XZiWnew0RCvlImCcqJURC3uK8aHNNNgWK1NY3JkxMmTJvJkyZMmFqhGmO8JU/YFxjWn6EoO8IFe7Kr5yFYJ0d4Nsk3sx4Gh+zFgdVUM5AxRkyZMmTE9Pr2fvM29MCm9XRuTHEgtBsFg+clbd6pmMUmlC5quolRmkClNxplSFBWjXtOsmTKOVoCpr5DU5cRjzG6YEIdsjnYsd4pp5hluSBEPGFhAiTCsNg4CYIusczAaQsIdYoDZymTORg94F1TX3wcrTa44SvyxN6yZMmTJkyYnf0mTJkyYG808im35xCFW8JxhtaRvEFe80ZC8YgTmYfLHc9dY/VZ0ZyPDreW4wuEog8hcFZ3c0Qkq3s6xqqyF8ULK3uGsThAuIYVH7uiMCLgCSgD1DWTJk+iZMmTEghFw5MOzgG0QD84uwhjYDolzmLrxM2ET3iVqh+MaCcAxyVOoe8Yojygu8bQj2+M3aEyZMmA8SrAe1wS1mupFFQntm1+xqlqw3vW/wA5xXAle1T75ziUPROAOzoN3Bv0QC0jR6cv5yNLYTZXYgrwHxm2QjlsnCEAOAyfjF6gOfVLuE4zU6ckAJJ5t3hmmLTwoK6rTWf/AHP+fOCpNe5k3AYE0MUIb8Z0P1hBiwT0uGLsvHmZG6gXeIaRxe8WgDNLN+8O6VIHvEdD1gMmTJkyZMmTJkyZPpMNZcmBIMxa2vvKdhDzmwQvi7wpRuUB23EO+eMLgAkDqHLs8YI8LCqiBW5Pn2mXOURCUR4lNu04x9tylagEIgO8MqSEtClrSfx7pJzhRLh6wkeNoNt94FUKEMgbHnkHrFFB4GX8ZXFBBlPzzi7Qg7FXHgDZ+wxIaCRKPjKndFND8Y+jI5EmDqFlcmEDQ4AHSG8W6YeMANuNppjSRgu4uXkLLXEDtVOcRyZMmTCeTchs2InCZS2sqz9m0ujh5uGUxLaiarvQpiCmFmA9lT/uHQMSqwXho2uMVFVUOaKKHfOsVUiNHYQfW9mVJBpnYu4iTV/+nsl55OgZQ56ZuNdCWk/wo8YwNEugtHrUud5QiV2ix+pOcrz579+fnISGiZYI/GdprBpHhwCq8GBhA6ucF85suTSE4C4lgR49YomjCGieeMvA3p/+5QYVyZMmTJkyZMmTJkyZMmTJmy1PWQaN9seNZxy4saq8+cEQlQFQqza8YprXmpSkMMwFoDIgIA/zZgDo9giweA6wrSZu1uy86njHgFCB3FAbUHeP+EJ3QbRX9s0OAWt2M4PZi9fAGHxHZCZC+37wCUi8D4w34ya4xnJnW5yAonGSIq7jp6yZqU0Y7nmCHOESQZozUG3xiDp6xHI8jITphC9+cMSlDOAX+LE3evOJkyZMmRdCFQeZzcSWjAbMAXg0P2x9SBNCtABCfGBYAJtvOnq+84ow6vAXjhxgK1lUlEZvlG9YjUX2cjyowHZMcfwX0tuO7fXrAxjysQouymI1u+cVWEm6iIQDgjgEBIbNI6rlz/N/xgCLmpDELvJdmLlQySjT1hZDSax5V7Q84vagt0hm/QNfLHGuJifQmTEyOTExMmTJkyePpM4IfnJkyZPpt+M6doLzL8YtDpNyJe0KA+cQRnpAABAs2nY951lA7tFPAfwTFvamBDRuqnDBYhPKFzICGuujA64sA8DeulNTIzPPY3OaEOm8SncEMGasA3kO/wB5q0dD1MFKo6xoQ10YSb0+cQ73iUtmTneIF2FdsPQK8HGFvzXTmkGnHTbiJVHWHCL4yQEhtqaxSeCXu4+KDvT1i7u3EyZMmTJiw2TEIhUInpwdVgLm4p3rnPMBKdgfAXQNOJMQAghAmbAaMtbCUkzbl/IcYQ3nKbGFrzu/GOb+qZvm5OXGAkPpHiSFZs1gzYBLUidhZ7eTIBXRsqtEPwHGX/jf9c+ssc5HvKpeS3BNt+MC3jLXefMYPOK0WD/YcEJxwWfGGISnazxMYu+4H4y7jockuP0JiY+GJiZMnnJkyZMmTJk+kyZMmTN44PIOxDpJsjvJpmYGh5BDt4wPooDJZOrJc4JRC7SJUIPRzkDw+GkCvZ8/vWdFz3MKQob++MyLAxkASkdtD0O7KwxRQB7ZhYnxjawaY7ljXQ8588YY41yd4YvQ8GRBy8ZUh8mEba5UV5yDTjqOLbnY158nkzbvXWQ3jcHF0XWeFGxy3NGsUJfbj9E+kyYVgJDflx3vrBqCr0YsiyPeRtEOMIx1WN0gs6Q5xgPF9tTHaaPOJChxqiBHMH8YlasytgR2ioPmOAVqO7mD06EeDBj+hs68hvCMQ2xoFI7z/wC1+OAnOMZnIM1T0TjHbc9MYCuysAIvhM0Dy9HWDZdese1i5HiYaUU01npzWT0im5cOr6iFr7X+sMQV5OCfzc2LAN1cTuXvAkMLVHzDGL9EyZMmTJMmTJkydZMmTJkyZMJhFsEK1roAx8AVLoIDxOMt5xQIna9Fg9zIMVWGlAOzprxj4FVMEiPEw1+HF2XeRTI3R8rvDhUAckoHqnw5xMUbrQAGENfscZpgMHZCvAwBUbM2tzoOKN8YbWg4clbRqmQJ+LhU70GDAses8zgSUEykGnB8sQC8ZDTJFQTORxcUF4rhGiXeTJkyYmTJgYXUe/OMVeLznFmHIvObPrrB95w9APvm5RkmQHRNPV2TC4CkgoC8gFeOsF+vIqgEbwneRm6WpR99QE3lrLY0NCr0CmD3EBczquftX74HXGmT2yKAecSoNfzmqJz1hbu1tDOLN8GR4u/OAANuWkA77xCCr3nfiesCIDdyuC7gioH0eMT6mNHXnCYfoAb+2UB3qk17xGmJbrx9YW8iLkyYmTJkyZNZMTJkyecn0mTK2Ery0hJvhesAp/PGNEaBuHIpDTKAo4lch1j7CQjhF7HjC+VGADV1AkxrsIAyHt1bBcMJRkLsPEjEY/sfeZ4PZjoTR/fORYBs0wfc8EY5U0V0jSgd4DvNgGe8ecGkX4xErrzjUx4Ybtr1jRfwZc7DKFcqfwMVW4HwuCL7xry/GTEyZMmTJkyZMmTDBG0ID9phvRWhTSWmbAjYKErfov5yOK3aAi6u0NpgIND0BipgUffPBypo20BkjjGYRIIdpABGHK6uBG6oNwODxvvPZw6OPObI48+pmnHXR3gTBfL1iIq2axIPOuNobYjfsV5wYNXI4yXGeusolQ8cfnHOk2+AxyOLV7xLxvLvGcjB6xo3nOANd5Xial1vNVVjRVI3fnEyY4n0TJkyZMmTJkyZMhN95ExSBC7TZs685unSw46rlUv6xtbcWzvgYI1sdjQ58My2pKJXX2xChYYRuJ2U98ZtIYsQkRjBwVEQiEMe0qvQgAjRA4rGUXrhyIWaJv7o5UxVqwbF2ExZveTJkyZWT74//mZMmTJkyZMmTJkyZQ+8DFGKqcaO8mTJcIlnbcD8HLMDko3uLyujHMJI95VEo8Jcpm7lSLQe+cXCAWEUXJCDfjIGy6UCGbvatvI3HhSOnmFPDZj6LBEsoBTILznp/wCXh/f6wBZXkx1N6wyOMC0+MsAR/wBvN4HHOBWx4fGLp5fOsICTzlpHHfjCQKjgyW3eJNaxKnTvG8nDjo85DyjxiFe+/wD3FAH6uLiZMmTJkyZMmTJkMmTJMn0GXhRfMGmLAXyNraiTI+HEZCYpSOTOQC+0wYUXoNBMgdP4DCC/PSX1S5T3cqujIumDSnPOIuDhSVI1WcMIno3lStO26YMF3hyZ0jYE6XEAUiQO1pq47yZMmTJiZMmTJkyZMmTJkyZMmTJkxhQgh+5FQLy3gMLzCzz3zMinadW0oDdTVOMYQAOu9kPzEvzkW3kCWRQ3im1l29ml4ooTrAeoIdQ0jXLvOMjgwZCPArRlqwGFDQlPInnKII02DbFAGzxhMLeQQ0Lt074z/PjGkm15zdvE1uAVxwWr+sGBD3gR0mj+mKwd3XRl5T++ICZFHAG8sTBBrnvNifbFtIecW2EkHGEBOQpPeAYxEHmHDiZMn1JkyZMmTJkyZMmTFVTYOqVnPTch2AJU712mEeMUV9g3Vso30PLg7jZyXRBWAEdnrADLgAAMVHyb++1kDkWblDnuYnJL0pSXj25MlACoHboEIUc5SYCXdqk/ymGA0ID7ZMmSZMmTJkyZMmTJkyZMmTJkyfRHJjC23bVLr0vPHOXivBKprjrK+ECBgu+l5bkBhSpA0M2vgMfzGFWiRedrlHJBqehNteuF94qKqSVN0iDd04xC4ogOAs585CRkVlOA7qd9Yn0bkxQicMjvfbAYBexAaOvyPea894SnnCXCdB8YzRrxk6IM5f0mJWgecOa0CtfjAeXgxUFMbm94Dlbw4ELziHV4ywu3gwlT1iHvKcLhulvII+cHGbv8AwS6wRA3cTJkyfRMmTJkyZPomTDBU45zsYiaaS6I9gq5b4LgEGSaBpux1l45YYKk4BDwu/OvPjGyS7SWigT4Y9BxSJo8o4NQy6COVgCQJJsl+2AJaF2iV0S1qF5chEwkFZaM+0PWLiN9d9GFax8MmTJkyY/UTJk+iZMmTJkyZMmTIPCuAXcNQ4YLeacjRoW97Naw4GvXGey2Zzz6qKXaYVUcNb7QKlB61TGhlYAA8AgY9K1IVFbHQe8qJLam6BspDeyTLtOzrCIjoQzqTOn0EW8CVRNO8THNIIZoXaQ7z/VJ/B+XvLuB4R8YWkBu+sY+OXFcmbyA39soDnsxnVNNZKlNZKOReHAhdTrNY27y5Ch5xQAaa/5hQWKc9YVmzhzjNKeXTkAH3LfvA3rWJv6JkyZPomTJk+iYo5J9Glyg8B5wrAKrtpfBI7LiepolIiqRv8ZO40aRjyngX5w+8VORCC8Tu/ziSGym4DbjsG8AgBpAjU2p1xLgWIDCCFv4AHvJ5vQA0CUUw7drgCoF2FMzxUOSWuMhFHXY+2G4H3inUdc4QwyZLkmGus3cesmTJkyTEyZMrC3DSLaLowGAQ5b/AFknWAT5e8CKJbj4zk8HIBZX7uMxG9Cum1CCVz5ZAcOBPSnKz+mMsoCUjvyPGUEJoUREJIhWYq+atSlXZE02CZvwR6MOUO2d7kJqG9YTtNgWUrXRfjHroZ0Bt/Bj7XJKKlih8xhnW84NCrzlcRz/AJa5/wBO2PgBwP8AGUEB1MalV5yjO0xprhjDU8/G8OkN6y/vPGPZ3grMUMsx60YNtaZMXjJk58dZSYdV3hgFfcn9YZly2vn1iIRfNwSSLvywDWex1ihRN4hLPeOkIU7OVkyZMmPATksQbgFQn4w8BEjx4+hLzkTuuPoGvJpLtzpyCrmCsg/AOLuB3xkIMGgmQmQcGQUPhhlhjoAtkNWa+2Ci5UdcBql8N4WTC9A6VggC4lzVE9EdLCRtOn3j1qOC6jHTObE9GEcRUKjsfWsWPIBx1h8AJyYmTJkyZN5MmT9ZOJ9D/wDGNHlCZM60MHKPIO7gTTGvwu9Y02h3lmWBT7KNIkPTWXh6EaZV3ctd6ynJEZJ24EB4SawkfWABRbNuuVwVTHKiJ3KuGYEaq5b/AIimSCuEM2cTDwoLybaFwTBglJi03SR0X5mWRA2KkJF3roxGXkCgQAC1G9Z8H4P+YGwee8Acg4xSeDF8Y3TiwjkuXBeXEcZtMsylfOUG3nNvvGCvGCaOJKgfcuFvbteMUK45nBhgLXDvDoAvld4uGNNMdQBwON+ct5ueMWBO4eWcqRwwi5bsGwYuAcsxA48FYAayUO8ghPg6xYCN1fWO9c69ZZlPGQmCqva5SSBA5aAAa3a/GaLBNGhuTT23hU4GQ6Aa1CSvhj93FV1oSO4GXkw9lGKI0EXW3fjFkhYHZqPOHeKAND1CisP3i2YHYCWCnIMd9ZKShKigQr2a47mKyc5x2D+MqaOMR7+PomTJkxxJ9EzXjAkvGNCQ7xpGx5+c7TjzgiLxiwi9u8dTSmFMAhZfOcKFnJivevWB1EUhHdNPvN8C8DZBW/DLJ5JlC20PTja0rSPWEgiu2GCmWqN2FBG/hcOhcXmI/wAYCKzCCHkeQF1jpsdiyTmQCyl7whkQ6DpRAQPblNZhtKthPRzivEh8sdhAi3vJ/wCN/wA/LNp3kYmSJiOkplS4l55wRwYFH4FMCRBOA0/JjefDAcAvWCG9Y7kszjOMEGEjvEWO/OUq33rg9YKfssQA56DlzY1vbg2t8C/zlYb4vOIU9Sq/bK0S69MpGJzlCHlzgaMmk6zZl29NZ4QYga+C4beDzhCtDpwoGPk3Osnth0ZDlafJFYcryMgmrvSDpxDF0ZdYXUIW8KbC16RicSGbmC065K+jG7egVvFgZzgAXd5mBtjRHd0Q0OTJluxqClJSIGgesnHZxiZMn0T6nDDEcmTHiGCOM1Ss8Y3oxH2yd5PGTBLm4Crgzu8+OAwG1b+sESkIEm/OAvHg2kOl+c5/U7NRU/nHu2+4mHwpVaogfMwr6rEbHOMHRoLsUv47yoYOVXRM6as7wXW5NAFBbRXElxV0CDZDP8OTSaKjTTzjrj6CarshkMpSGuJ5yvcrFdYgG0KjeQiCb7GRVTU26IiN8fOb3jJvaCY93XWSIKwXHwO/EPtlY6cHjAISmuwng3jAsOl34udyVPaY3bHy50hvNcyPrKXbQxKpilJfeCJU5A5yQ0TY5GmBwuFOWdma+AOHWEomnjFY6eZipqbg9YZWJrzkFw7ZklFeecCo7vnBd4Xh+cRI81ckxtk3PhsImQXgp1PtgZsmms23xrNHI+KAFvbPOJ6VQrAjkePWcmdAKFa0clUgBO7IuJ5pQDXF3fFyUgCAgLHr6EyfRMP/ANgJkyZMmTIlkdHA/OIgg4Da4EQnqMKoB1e4eDLWhrvA8ad4cNVCmMKGjQRrAQ3zlUZpRZaPeA6oDFQ6F7+cvaHcD2vvjATaiijdp5x6/RUtEbgCEHR/9xpJIkDJ7Kd8YUEsRLUQtesurje8n2njl+cNcPtMedEcMyqydXL1CHSzLtHSaB+HAZAKCgp34rDEw2UDbtZu5cgGuEdn2xXpIEzf6IznBELeTnDKipkDJvy1lGwXBvIFEwmDU9TB8ReAwLcHR84w7yZM0a5cDiaxapt4zcSXmYK1rPvjKoeHJKTr1vGM6L/xi0w7p3PeITV8sos7LcDQ45xFa844mRzhw7vjKMQ1E304DzhQnEt70YzFrZLpsXrjBq0kCzWDfRkBqBarvbtwwZ2x9uX/AFiyEKp6ch1iv6HYx8vtxisZwJdACfeY5KA5JBnG/Q/Qn/4AG3lhftnOBhUl95aWGQFL4MWRw0wdfOKUDyufofoQybwA7p6uHRCBrJxD2HNhCLZ1kgIP5xCrLisfliTTC3rZyCyRcyYdpTynOa0QgfNIdj4xmcCgjdIet4JHIaQbHapkrVO3xasDZiUFpLREDQGMcGDZiS6RE6wf0/I3gHyTTFXnK8Gci5PTiZtXIWt/wFwVfROe9TT7YzMo0PGMpaC/+YUQFRWIFgIVXjJE68hBDmKHGMAKxWgWwA8D6wUxoYnWOmo6rjJjh8ZNEMa2vesbnND16TvrenZHBuFWiuqPeRgHBHWzvExQLfu4iRTxswq/S/vICqByugxQkNAKcjsT5yAYTKHA8xBmG1Zrzxth+2YhQLEIj8ZMRhW8PUw6HLlmMAQ20y4mLI57xho7G7n0R4yt5Rw+sSJz5YpELJMUF0zy1kNbTjEG8+MI3MUakGsFMLKmJummxirMMGczDgFG6ZTGRaasOpSQquOMhNLAbrD6GCXA6TTVoLqr/mBILajEqjXasCPOaGSdQRgiacYBqBuA3W8mTJ9BXWFmLdFc7v1afnNvQFeZ3le323BYNyvJ8Z8e67fWALiaaRra5voGpy/GImByOsmTxkmCDgzZrNluUbWB3gciCeHmqtLJl5/6BEr+c5EHiRQ86usKNIvlR+3GpHbG6nzurloDIi/nROMBnjENlQJbXWEjREA5Pa5mMDttXnynImKATjlZ785r/gjSUZaMqge2hP1gvLbjvJIVSimBuhrHP3Mr1zeDnFOB73DfbpnmfOaEHs6PxduF+bEMf4tGb6Y3nw6SywMsZ7Vk93LO1c3AjqE8rvnWJ2BYB6QJ4OObdASRQHkEwJTeL9e8aWNt3GC6pzvf6xUQimIpCfjBeqd1o+H2xmOw6x2ElwTLBYPRcKjaN2mqX4J3zrCagqDsk0ldmEqJZMmAGnA3fnJk9dJoeDcDLjBzUoIDYihXXCDrlXYwbS1N5qaF7BWdh41k+iZPouwgq+GE2rcSH3yfIGnbvziP2VcdMMjAq83K8QW177wZehU+MZpR7uaQgcCQU7Lgi4fKEokHYTVXeMYlQUASgvIuBUm0yipAScGfOFulaV0uyB4W3BwTojRA+yJvJpalAnShRToHnjF2lLYZVZpvLrHuasKLSbHZeucn1A1tU9GPYA6JfjAcAGg4yJidfMyyYOjyYEWTs+sTbrdMg+M7O4Xv5yxcfGpizQLBhORFQxlROM4fRcQ6hS4S9e8sy3XRV4yykCAVALwc4AkogiJqi98TGmkvPcenNoMdDIVEcVMGgoehIbcIVMSZN9EA+jtesI+E2Gy0exmb+XtDauU+DEtngYJE0CnWNgHcnNav4wSxBnQPw+MUW1Gwu8nVLE2U3iuWnhq/LgLQHr/mEOz17Vu+XO8r7V/645xLQzRBZbHx+sEatCG92my9FxI9pVV9ui+DHmQvPK92/ZgI0spI9MQ4k41iDQBkT8HF2eepZ+HrN7+AAVST7jIsWyJfaGFg+Qeb98Du0K7tGJt5YPjYLFVE055zw3oukXtVpcmVNbejGwryQ6VtDwNknzi7QwilvaHFVtxzqTQUAmI7QEc3GgXWoSU1wN5yAeRqCNSOSdO5mw5RXmNmpoFichhrcVqtF7HVhVvIusrMofULAPY8amrM2xIb0A0SICJxXx/+Bv1oyAuOlMPeENXvNhg1bWgzX4YA0DONeMbdcTxi+kG86+cDjgRw4FCECbWpl2syDx6PfFeu0uwGQH9jCwUrNAdzb5cSQPDxq+uu8LAc0popPthDQitbYfA4whCQRUZRo1F2twpIBqiIIxOUxMmA+xn5yofJ7XDNPOMg0OrhxCnn4+cYyrNdxyUyOQduHURoCGLvL8Q3zjK6DhXjA8IJvo+cE5vVfnHwHwes3GA8eco3gEJxeBFpMVP4DKAStCw8Gsq0CCLZCAsPWO5bziW0BqOO5gNwOVkI4vnBJLGr5XhWWN8nY4Qmvrv3vOR+ggEp9kThx54tgbcHx+Kd413twiQETVNacK6yVa01D4xaxDdDs72bwjtNB3+WBhD0r6X/AHHb1HcGy8YLCnnnozY4ub/J85wpouRWqOfH26/PziVotAI2XTeTaiWAn2EcYa31JY6egcM3jNoJLP3/AHiC1mVwvYMM7BmxIQdxk85FBck6E072JjgAaE42Q5m164yEYINg3sLqni4SJ3gPbaEftj7vtHM+LS8MWjVkjlafLxk1TAWAXwAOcdVRoFiAqdVXGjTPGaDW2nR6zVlS1Iak3XyXSgvpSAEIkJsBxQtArBVLps946Z0MVU0EaINAX05Zm+kFJAUu7nMESaQ3tSODvxgK61T9z4odJI8c49ZqOK4FvUJ95IIPkXfwd4zK+YQ8huNAdXl3kUQ0BnJCZBreID0feCL05fOD2LTfTjeRvCbhQhyvnLHwkosPU45whKBBgUnv25d2hSN2t9m1w6TlQQG/ZwIWKYjwD0h3hhJAAQCovxrIwJAtEilON85A2XTbw5zitLJConvzlkTV294MRrw3grRtDzmvUr5w0C4SS5yDvCEDFBFvlw7A5aTYUXKtoeA3jq7Zs9zxMjseDlIgOstTMKVVbSwYHRhRx8MwqLUwrStgp48GbhNbBLhdG+MVkUa/GAkbioDkVddYeykpOflwgGSn8OcjtsokU34w3pZw5fhA03vErarliM2IP/Pzl2SNlKb8KuKK1AoF+CcZD2gBqje5jxYlCl7aYFWcpcmJCQdK6Pu4vBvgm8ZskClvO7rEojF7FSA5PF7cP4+cXgjKbSBW0Mbhh2W6hBd/Gb3eOhsmdmbQ8hSTpo/Ad44y8Q7XhjmgSpEAc72OcoyJNp2R/hyYbqKIKUksdMNW7QnIWoPJrjH5gOUBz2B3hVctk0c6kYX8QZdxvRQ2eclmTa2SESCrzdY4LWiEJKCqX4fxj4WFglaZElCqeHEvCGw1oNeXGmETgA5JQ5wAbbsPmFOBPGCpE1a61go8TvB+8M89EB3o3cV6U6RJOu9Z4M0EJE5envPhJhhWkyNh5PnNaSAipVQ3/SDJdSm7BReO1wmDQBJxxm9UJDjsE95vr+TB1nRy9GBqU+SuJTN28B0ZPJQdePglxRXLA0OVPLvEALbi5sJV1Vu531zxk0AELJD+2EjKCPlpffnDCAGchsWHzjqU7soJ+87jlJpqN94G7CI3Vjb6x+bVeG1vfzjLhEgbX8mRF2t7uME14P8AuIDMecq2FmuNYyN5XY4DY47y/bFJE1MFqvo853NW9bytVrrN45eMJyzHKQPOFSQlr4OclfurAi2eRa1d4BCpauVj5htjbLDb74NPkt9YCjYoINNqyGvvORQ6FUSoJSdznHbYQW03Nt9s2AkMwIMHInECxzGYCVViuBzj3AJFpINcnqjVuVhKCQuBBb0v3Y5UgjRCibHXcxoVFNje+ecsg6lE9r/GDvUdo84G68tbeYHxrFqZIs25DnjziDLnwubVfJR16wBV0ippxFCLRkBd57X7HGEpfVAqpfLVl6ytDOxu0G38MeOtNNhKhvBY0Q7BRA69sCz6oAfQ51gKrlxsIH8apgv7PQ09P8sVZithYid/nEIULnABvY4cEMDfRGnGmuD98Jyl5ETCusUI6b9n6w73c4Oz6TbXvHV0TZqnInJ1y/DAkwck+12ZPPnG+U6g/wAl3gRLKoQLdLXJ5yUqpVhBDrlhZCoIBa8gjAlIBafYhPxjxCDOwO1LF4McHr9AVoLsiHGNkaQUVso1HNAUmpqteqJlfv5egRLOPAxQxEUSgchCfJxgO5yZFIJR95ApeT4zWS2pOVyhyOlM9tcFnn3kAALt5cKnBwHH7xZeqlb+8iro82V22gTzxhdfA4glNF4TEtHZy9WkjenOTkDAEaCxRGc5VNOJZG0HGomA1GMSXGSuvX7whMSrBZmG526ZEPVawkSwP4wklEe1Vc0VuFEjieMKCFGjaOB8kwWIGjDiMelLjYQcatcDcckj5Q0PeWD7Zs9XHtxUaPKBqCN84HCCtgbo8Lt71ghqyKPwNOS0vQw5XdLvjAIV4QWNJCRMdxC35wbNnknQmMBDh1JnAqDgOdYLqII2qA6B87Okxc5qQd6SrplywB4CWfqYupNedg3fzgUeANYAIOtH83CpNbZXC2EG63d+sZe2kA7ofOVagUrSINe5cHWMPZyqtKDSbHPzP+N/rLp1EBlt0niY+d4LsjsRwYa5s9CKW27vCTLijiaqid8Y7YAJFopLPvrLNM2RK7The73hYwh01cM9e83gVOnSeRLvBaYCFAUo86cSbJVoE9bGbxzrepWVq+QV4wKXEwocwYcWjmxoiKGr1jY1Mw3ekRP0x2u7tyizm98POAxScDYhuMRSggKtLNRf5Y4EB4ANGoCaCcMNipYGiu5xVN+XvN1hvvZ3yPvnjFXO/faYLnt8B5bE3w7DHNDpQIm0LU4yRbE3/D5wTqBibCo1YZaKbOj79js5FDH11hg5GUbcDFnvAqzEoCYYFFaDEld85SAhpHCNoWIe3BIuOjeSAeAcXlWBEBNiPRMejT+jMaK14+zDQ21FXbT2RWsHI61fjpQFe+uMVVuvWKIbC/2ytzMUoMOAF3l2A3xRKee9sDUAYnkNHDt9spz+MdZOeMV5DkaQIDd1wv8AWDUCpTjTOcsUFyP/AJhOtofZd5rqB/PnEoqdl1lYVD9qlzaEXuYuwqgWsVx4DhjvBHkCiPw5oIFNEbN3XHObBwCs3wY9c2wRJr4jAgxpcEQ5KiF8YoO26S0gdhHV6wFQpiB08y/8wWxUKvtP6zsBR/In94AhsGvVMUEHN95oPE0PvGkdapg04gjQgS/+MOATLaoF3HWRjktREN7N6w9JwqEicjfOKxtHJ/gyoI+kwuawqUduYmTzTXXS8J61kBcp4K9Z7nN57xX6yChNH9M0x7OnCecHyDHQPBeGQFni1qu5r1yt7i2zjHGv2wD6okdAR4PXEN1UKOBH8mBAJ3tqhT2EDJ/NVQFaByzF9+VWgN1C2mNjOSIIWbOZxg10qAENJuQcS9HOiaFX5MuaYTsSiF/GPaFG25UtdM5ecACoBWwbUXfc73iuY5qrBuhw9zFbJrcDtKA5XjrNmw3BNx5EOeuWVbQUKHTvX8fGdDrX8Jo+zjsVCDq9K9njOxRiRoe9RieLghOSIv6x2L1hEHlKu0F0wOPmKWuIokRef+ZrslvkGjlXg855gOTOUQ8ZrK9YH+BWEUOkiOnH56c5ylvxjt3LRlKjzzlEjB+SHf2xL9EHhXKuWkNkRrev3c42IaSNlHXh1kcK8RQm5DDhOgE7ffrCBoJKoyzjq4YIHTqN0s1iC8Sdtt4PXWTHJhrsDp1eckGKCTz/AMwupIbFhdtpK84EsxQJZIjvRMQc54uUkA9O63etbxiKU1MdDZ2O8BiS1ryL2dAxNoztA2TVCIDlvDEoCQBqVZObxiV24Qk2nwcNCKwpUok3iYcAPhoPkfjCSCMORVQ0ct4siOrwUmmtnEShvlvOQp8chLWF+V/zNoTlviP84deQ8mNPKnZ6xojAqFSh403tZvCIOha+t5QNHa0vvHAeMUJQAG1mo59e8CQSRonaYHzlBWjTwZpBCASZGb4d5ok71X3i9Ouc+/OAG1h685MuKoN4SYUCeGcohqiRcN0Q05xB0Rqm84Rg7mI2BMCFWaLbE2gzZnNkhIAMJxHA9aPRZInnRcubQUuqFyBtbFnIFe9ZzTe6Ix8YbFa9m1LNbzYajS7Cr7twmZdBrEsD4X5zhqDaoEl7w/rhQQygl0s3mufpBhQZtg4atMlgY2QdhMnA1HBejp4UwXb2PB3z3gcFLgKKh86O8Prf6ExqIyO/OTa116LBwltUFYF3t+meUBIdm3fU8GWzAc3Dic8TrjCf7NORoxi8sFLqdIeUE41eckkAE6I7H4wGnkCpoIfnGun4UfvCiEDy4Fp8nOTAhoMMN6XzkvkYhSw94SLwVX7YFjq06h1r1ks4BoNmje/bJ0kNy0FnMg4CinYo6SnmDm50gqVAvyjhqDKmu1jaEcBAIzjGHAL2gNZ5znpTtaeW+bgRK+XnAQg4CnYsl6O5jS3Sqh41XrEKngBMT2NumGcqVyCoWbeXe8NbS9efGK0Ad6RXTwaa/wDSgQHJf2h/GKEmkt1z84UzVMbWA6Q+cRcpEEfIJE4spjuc4aU8Rqt9YccnlsCp0C6xQzjoLoBtF8YMARMJAz2Q/rCA0DLAjAEKHeCsWu0DNvhznwyrZ+nGiiGGqDzjhLDyUpR0U5UBuu3/ALmz3q6A8h8YkhrOgYW/gJvlymr8/AvmeUPR5FD1Y+gC27MZkJT3RZWYRrh5WI+OPuuHiOUQwqA6DbAPJTrrNrHJseMPWMGYo/RjlBy0G+XHVkGhCiB84gSbnCIOHZ1kLChENBJwsx8maZFYx4c6x7QZk2VaIHCmCS601sgbJThxxvZBUGgGgUXwzXEyAvCBYhERWY8Hs+DU2fyywDLFABH0JjLA/wBnsj0zN7EIC2XofbELCVHhINfGUgJ7S4VbzjgImicDYh6xIVYGBakSTJSxOLpionEsMJZYYfkjAWZsM/Ng5JfFPGRfJ5BNKONiY/LXNIxvzu4kekGmpI30TISSCE5JeQM3lOWF2Fph0LeRiA8S7CYYKaaPLgsEWodhSkAjcSrpG0YwPB6x/V8hHpj+2B0iGKJcAq4OOAYKcntiC0EbxNYgkHS+OIm7tDnLvc3HUuS5qQHjaieDsww7BHU8GKgTP+z3eMKCEm1YYxdrWUANafFXfWOISxACsl68vWXgFSRYo+3eRyAKrRU0tUnGS2R6HWjX3i2A89pCF1eGEannzjqLxQTCcDdg3hfjnJgaLqBqA4RLu5WLpIFXWvRsyytVdpBwMAAj9wIoctx7QsYVHwibw0ElLi+haUyPDty6gMDi8uJZWSJNBBwkjiLpSOkxXkHhwKEoK2QGO9S5rbVaDiI2Bo/jN49AFBDPuMQbUAulQMq5MSoiJadlUjXSwcnSAT0rpYVjSppsey8cYv8Abi0OxjHI7xtGl1RAUqv3w0OwgA2iot8i26zV55KnxgPO7e9QAGeGIkdFKBN2j9nM/DDuEeI2PCnk5ruDdZVKg86weEommxiLYMFUaSUdcpELwt31iiTAcK+XrAabMlfnEOsrxhUFdKGUbHr3gRWtWzXEYiDt7wDFABfAIHHtahUwGimShriEOPL/AIypimQw29njKGxNXY9PWBTgqkra8sbfxulnU2QPUxB0lZUWtc+2FIXqg0guCdONFQLkKRqNrSF1vDS5mhlJgQoulwTqafMBmnUXO2qUVnY7F0JzvxhTSYNAC++rvCumLJOaRg3HFQcrNncGL04AraQmh9ncw5br3hWOzfvjwH0LWw4hnznHIDjaGA8u/wC8rUvrSgAah3lFPyYN3XGiZMP21FANpOV0+MW3QgkIU5PDXVcYHABMImi/aUxg7IsBlsEnXPGTaSXUWmhdL3hRNnMjCiDs7ec4Smu7i3n4yxz2wUREv/zI8IlOBgBsZzfgu41gKsvkMc5Eb5MCiumIVCrvQ3OsObsHy9CllhZ+sdkNcuGPk9Z5/vlLQls6EubPA30Ox5B7POPkopaSJCUx8DB16bKABKUQHFpcLQlgEjRz6crQc7BBpVE29mOhIpYKlRG5vZ7yiViuieYRoPXWAEOYh5UJNG+blcFGnJvX3EjfHxhFBSRZI4TSrN4mGQcFcBfCH1iTFTj6S1Nq45nVKA9lqC80yjYhSOJHYr7ORrqUq7Ucu/i4IhJIYaQG15cWABFICUFRV6X3j1m6oI9GmokMgamJKELVdM8ZbIbb3mzxD4uJRjSuHZGxqzLdLcCNOM8y40wOqwQoA5FhR4neyYeuTa85NeBceDaZDntlwfnAUNT7FbHjV/I8DAk3pnBHjgoL8PeCqxSi2uVYXm4PFQVJ93wnh4xNb1P/ACHdrFwveECKPuV/7lsLYkVXw3m45rqVhkS1t3hkngC0JI2V5XjC3oSxA6rN3tM5HhLUiybeD2YhqRDFBeK+8BUwRxDznt8zjvGPJkGk6SuHliDR3aG7A1Iu/wA4iIzAjlQ4BdcuQffkoMTgBDJcWfLDYIUOU/GMFkwMiWTlJq84iYQ4HueBizfrEAkGUC7Rb34yW0BIO7hWUDN4xMw0VEzD8qVEePE8ZQQc7DRpf+sk+PwRrx+sVAoEKgIITWABdM7hS1NBtzgwNwXTa6d01x8YZwXwBTy5aYiFXisFNFHlvC8QQTSRTafnjJoklBQq7CTc/vITor1ylaU3JgfbKVY6XY6phfuGEATZjXHqShNs+c0jaIfCZAqYtk1obuBWcE9YMGkqeBQ+FDrjvEorMF4ZJ5tPlqYe4Byimg13O3l7wt3+4wBvtyb5X6HEVmnz+MQiq3ecaArRS5pBQeKiU0HmYYJFBlRWtacDQSkCDTwhDp5ExKMUSVAmS3PxivJ/qq1orPkg4TVVkE4MzXeWmMEAyp0sXrNHwQgUjeQOfbEWykbFigpALitir4oSq+U11iKincfidzjy+cEwipjDQC4M2cGh3wIB2GuMjRA3wt6SxnNESDm6I0F6MSLcqGaaDRQ99Z1/iFqB6Hox3y8KNI1o2On2ZVbqi1cnVp5V6yeeURRroddTGDmKLTUEEHR5wxCmiVWgN7b6+2E6ShQ4tdyp35xUuhdNSz4PUundzZ/VDE2oGosg3Lm4CAjewz026xuMXapEHDUK/wDMb2zkBLOn8FPGD7SxpLwXk+dYKdoMLntI+GvOAjEh42nTo4wwIEVgLNkHRGMGhgteAxtdHPBrm6oBsyHAYkK0NKV0nAlOsQ65ZbdB2BQmvthz+HGMgCOhzgVrwxCvahrfrBQB1Am6hpHAehEjulfXy/GbQFExpuzbR++sXBUSFdCQH084yZqqNvinS8uRY+f3CMV2nNAFEEOkG08b04Tm+xjaDm/7++RWY7wDDwR0maQyAqqKNgdaH2x9pErCIOYfDAYmq0KCBpwtx8LDsmtwGPQwtCt6OhEapD1xhwReIBexNvhiA8+ABgdhXGrLcChBlh32hgUO7Ky8pByY+nBHvpSfo4cYyG197rUt9pOU6wpF1onZ8kAorcUiaFhT0IsWgHSmiUAHywPAXUA1d4ctMAS8B0wLUQAAV5DJGEFRVYt08shY1k0oCEdG8DUK6BVEEOhMK2nuDoFbBLvOeElgbEULXl645wKZWNUnOoCfvBAWkGo3PJwV/GCdVlA15NCgkLzkPbCAbmxcDyb3jcHyDw8EQJ91cETWjywJA7z/AGYkvnb0QHPjxhToejGRSnc3M1gnk85MeNDxiWW6TFNdGnZtjsBdZCMGlF+WEpKtWG4AgfW2OXgGecKaReYGufGVpPutNfZQzWuOj6gaC/sQRcK8+8dceVT0J5fEwJeFtw1dJNt13cKmsXNDa2iveMn2NmtXXty55xIupRE0QVdBWmmKgE1s1CpTEHbK6iZCKYJ5NY5nYyKBo0KwxSSpQQVxO4RgZEcXCqAASV4y2GEbSBXXT/OQjjEw2L5Nh9sQTZCA0Hd1vvEVEVFpo188YIxuUVFk8Co+EmsSAsmcAD0HxiN+2YAQ9txBd/W6PVwQcYpXPAThyprf9YQc3JxpNQhVdEA5gmQFYkDQhTtCpsLlRFlqIEBVDM7L1k2JoHCk+YbOeAEGjaToJ2GB3jNTyQFGh0PUNHozic2QAGpvxiunyOMUYQmX0nCrnscfuW2/gTQdhd3Fmo3eqXWRs78ZuMQVeOGSNmCgnOAWAIEHoXnzjCSLfbkUdGEcZ1JFkNrDbrPBBSESj5OcXT5ynuZDTz1jjX95sRHKlY155r5xvdoaoew645PeQrJVCjqVNm3n1iO6FdZRBrUoYzWAMDMBDdXArkGeuHQaAdkMuQdoDyEHblJceI6azSVNnWLSJukwVT3T/mMXSvAKcF4zllRiCDRnlwynUNnCaOeMI/pSJA8XeNUxMFhulcxijF60WWPM2YAcpZwdeSNa4hcOseyDXaGufONePZxDYr4srzlEJINclNnOvFw/IhAiMUZyTFzZ6s0KKdmLUTaG7WEeznCGAlDxzx7wEEVjUKUneSN0JACgUQuILxhjgeRG+DFVa6pTlHDZo7nnGmPEZ3doS/13j14SIAYdo8ave8C1Ve1dXFv394aQezvDYNejKEVmCEoYoab2OmsPYeA5IOeJ2EUuMC1KbyKOVvZuUOdFRE1qF7MJpSYdmw4NTo4YGRcojdsU5ptx4AaEGXIB28uyHrCDbUSzmFN83nePpyOXif8ApmjOyXdUEDopxgF1LGTAAlLqYntVQAGmHRFwjkLmIpEDTeHkrsAJDytoMa0wGmhAeYhLju2JG3gaho1xg/ZmoUIAr7sBOaWnIgSoE5y6IiBG0WKd9YlcnSg6QJUEwQ1K85SEjwwN9j4AqooXliIl6mQDBEk1tzkaLRewi06cEPPxNGCB0y5LwbjxBCNM3sdt4AJz3ivoWBKGg5rebe3rhN6X/wA4oae2mlEDdHx4zoFu5QbXhrWL6uWOnpXnTXBjyo4DMImgMsRgNG7a6HWX+rwmDoUtlwA7YEHV3+2HfWkS5gRwwS8C4IlHKcXvAMc0A0Nx5w6J8cxFRtfOa65OSXkR92CC6ylRjLlUpJizkWdGEXUuac00N9rlR+sk+hwRPJVFbP8AzvIupQnLYdnPNyN23ULB074dHvFGkhMFir+cx60+aG6wKUusDFuDUgLB28HX2wcd1Qo7KILquIpo1sQNHRDyx0Ooyp6WHfHsJPMojlVzTG8YdoiQdLYKa61jcOAJaM8ZHjvOxXXHBRwhewOJVY5tmK6mlnOTk6evIkY1yzcHiEI0IdMqbhhMewi5FJEhcFxsO8KpNkAsNdbM1q8F2zgzvWITaFBoJEWsN4tkwIrRzvB2aJAVHQtCdMPhIqHIADIHb3+CNA37czXQQHWsYdxABiGcHB784aoBXRQYAdx+c2TYr/wOdN3zgNjECU2UB2pyQyVk77h2qofbB7AcmM0J8j411hWwmIPEfOCHTnYKjx2BZ1MYBWgNAcexG9XBBtuim626gWnnH8rrUlo1XVeafGCO621XqNmaG+G84NSGi3IE0Qwb47xgS0IAqbsXW/O8LgOARZSEWfvvBsNIG0Fve95FqAQLLOejvFd3QFyLQ8sC83FEhkAU9PGH+G0AN9bl1jCVBiKoBOUb3cc78Xy7HwD46yFuAA40umiYr4EwjEN5aWCXZeQsoOIr7kBcScnd6zdjgqI4jRVkxYuxKiC7A84jcdXQWrXk8E1hkPV+7Ijs286wZjCRgoWuzFULBJKgrFGu1MDwiDDEAWp8Y7gVVVFblXjvCQgBYNBrHjE3pqlJJxChHxgr3mg6g77H6ciAsWNM28o8TWTERQAKw6YdGsCYbpwazTMagFmcDqZUer5xPIHWR0hKo3addYTPeUIhgOiO8WGkUcE1pXWVQAp3LUO5cCG5Uio0avI5vpHBuqRy9Zp0TVBWAAK+MaXCwu0LTtGsKLvaL75yLrWHpTvDY6DF1c9B+0/ObeDswC6aFFtL+82PG8KuU0dB7yqXCQCxKr52Zvd5AUZRJPfJMTcSBo9ta2DETfxkQ5tNuSeQ4fWHsZHKAI5e64YHNqHIBNm9fOISzoB2Iqu245BBVTYIKFP8MUZAcEIlJHS3NnmVkkSyjXOocyCrqxoDjaLjJ690Lk6M9YCZmqIlbDd7Xt3hqwk2CTK128Vms4loFQq4mwGjDvK046jGKRA1XeiTeP1BOkI3aIcrV+2AbKAk7sJavW++MJP7tDRpxDku4MCsjgxLezGrCRdXewO6zN8WzNKWHA8sUzxKCDSLStfLLT6hwxS1YemaPTNJ4BcijRz3hvhMRKEnQvQ8YIoaMlv2BagaYtMkejI+i8h8YBQTC3AekIZ3kQQqaAAURNTJ6KKcKWBLvZl9BLAMEtpCh14x2A7dSQi3ZUcGgCWFWgwdx1T+cVE5NKwixPhv+MnFsiQIbqtS85rUTKgmnsD7xStMBB2URlvIZhrdwtJjsN7PG8N7nVLJpQoXvvJg1AcJmrsHWGVZIAgEPbjK3EggldbI/bAJJoLSBmpd9YESBBZzuO2x944mNxq3jHnp695EYBBwDdhwOPeGUCSVHSaZoZWpAMpyYiGa66zd5WIxRdgb1vAoQIGjUku9EcsFTwJACqL1qYZKAoSZ+Czy1gVG0IqI1xXJlx5QDtkHGO+nYqVvFxWwazbHEXKpF20ORKt00EdoTHDGqtT+mVCwSiBsLrmc468EtEVNpS9YTm+5DyCuLbIN7QGV0PdvoyUbUEQENoIvO9YqTZtGfHK+nCugbeHaIOIAPmi89sXqznIlkQiL4Cv/AI4WOlDtTYkbJgQMNIgNlnnkfGUxxtdESjp4cJA3LMPY169ZV2iC+AVcLi9szc1s54xicABqMnYS3TFqvxkRfP8AWWE7zW8aoukIBMFPoIAXN3V0bhsV8MoZQQHoqUwNM6yTEq74toNs2wagZ5AiC12c8YP9NNyRFs8bZqZEFhkBqXWuCHCe8hNrPQFbR33zh3jGok62St68ZeoQgpEGJteXJ2SZvahScM0m4LqiMs6hstvrCU6qGETQgvnWFtthQCN1rdn/ADByeL0i1WWeG8bJSIctY9ug7xpJup5US6CD4wV1IyUxAQKuz+MOAlJ4qdQ1TPvjAkDdIDgStNbygKGGkxE6dVEac4WlbijUhrnGFd4oyQ9gQ0/bI08Tsjyh28rhiwC6r66tE4fnCfSmXAhDQPCUxavyyiBAEV2SdO8IjWZoleQNBorrNwjtPBSK6TbxjtSC6lhqBApz5zVD7MUIYIPxhbgUYHpC66OjFUd2oqvQtE1jQ40yimqDY7plsoy9AiN8hnJxiRog5XTFbDt3ihatHokxNdymF2sIvFrDpOHznAEHB3NIWrL+sgURSclZCkXr4x1uRqYV0jSHU/nNooECAuhK8BZeMaqZFIgLsBvCyFUOgB7EvnNpZHjhK6k0Ez3oobA7HvjERSMklqBGb1rGbQdzPGdi5d+s084C3wpwvI4rAdIDBCH4t41WkWAoXVpqYWHEVqrkASjWbyoLnBIFuzA0LN1AiATTCYsLGSsTfs1iCs13GsBPsOcU7mKYoCDTjWXu++RI8RTsx3kcqQQRpsn2yL6NLWs8A/WQ0kA2qAV+N5JjasXz74Ke+m61xthN6kL2jtxkhqVzSxtxh42hp5nTJEknKxSNuP23xkl9EAx2Aj8Bx9R2VjQ1djZp5ckn0yvSgW8tbcHmkieoFk8Cn2wL3NDp2dxLkIVgoUarVSCtNC+MN1EBFyCvQMNRyNyAZxYcLMENKiLR0+DcyocLPgF/OG8mpR8Bnr5M8vWpMjlWIeDEiFs5GhvX2zU9/Rmap/2kMo6Nay02JgTyWGuHgymtJiDLK+mYxJMuwxQTX4PziMUtWu50Rrjhy3MS2fRJpJbxcpp3I04Cgp3zkmCTo5WbuE6MPsOVrLtxem8HJ6nsK7Q0aYDaT5XOpTH585yV+lCpaLZR8Z2ZEGGhCbdtw9QOsXRmnDd3eNqUWlBfWKpbSmKxPb4c40QRq5FRrBt5XFekrEw+KW7mG3m+c3ckb+TLrhGiKKA7NEenLQXdw1NIobPu53xFnFFoJC+xxsx2uAtIA9XfvAnb4KMR1Cg55MsUInNEKpdcT+sBMlSqIqTmy6cXKdvvwBYl7YO/OHsBSqV2NTN9DCBETgIC65y855jQofhPuwbsZUCVFpHD4PWW32Bexfk1ouFqVqBAVp4nZdZtkaW0WaBA5tFydnGIAFE7piG7txytE5egKnQNdmE3ZiUmrqAIAG8NBh4XVvsYPtgYWEbtltWk4NM4HuUVKU37OmaJygVFagQ8rO/eb1T0lVZANLx+sZoBd4QQB+wuKKQkUdBUnBobzS3sUUEGh11ljMjJxjCD4zTVhh0iqJkh+8dJQ4BsMGvJOca7OdNQWKDnnBtNApggY6ms0dNgjCJNDt7x+t5Gg8QpXTuYyV6oVBoaqtw+WTwKTfsv5x4+HSAdlt4ffrE2tqpkuk4Ex/GgxQNVXjFOTXAg1jicjmmUgN7jA5SORZFyFP5rgM6jieGnAkWgBEVp6Rc53QRANOC0dpIyLs06wEY9AksQlJ9Obu4qUPsdP/4Y6EwBPMcgmx4+WLbhJhZERr4axg9sM3yDDV/GEvzlmtQpb6WmMZuYd3IQl5rxhCRwDwtHAc9fObz50iSAO33LnnE0lGjJIBBLME4jV9UvghllQBVeCHEsVigpAVKMlY9fV1rgEXiYg2iNafgxTEUMXoXgw5zNAFHYVVADefDK7ZAYAAjwqLHJGetdmi1I9ME+AbLx704Jg5qOHrDE3GHRkth3C8HiO2O9ibetesdlF3odw4enNwADDoruVBpo885RYbjFSxPlhkrNtAFBoDfTLOxEJ4CgoF6x64VetqB+41l9xurCQjXdxLQKaNNHKb83D5fIttXZukvl4w1Go7DFXALpry5w/ApGg6rUdC33rLfMoq+YDlZwTCtH+2/wg8wuSVxgaAA2Eln/AHK7HNUqqVY0PGORFWUDl0xQs41m/QqVaK2sKDHPGNy6cibv4MEpYSC8Tfe5eMZelNRp/Adnj4ylCWeXFVvB2esB9KZzoImimOQ0Z0FgqAsjXPObEKQY6tQLDrlkq4sJA7B5DllnY2K06yVW6yTOcapLUnebEgmwUVIG6Uck31qnwNt4U5NeM8fYjTjnwjhzvLCEAFizQVdD7ZqFJNqJsvt8YU4oDWBQ8iJE8NxaEb00UtDw7/WGYXKCTfMdRseMdQIeDeCbSVQw40UQCFDsR8ORwmB7U9GbdNgkxdbJStU0EqYPnLiTwbIGh0vLrAsbJ28V0cOxMtOCgjcqKqyPuYUaq1sgFNTxTvCA7QWBitiOXEAJwST0zZqPxlTJFpD0Gha+2PSSiqwiEH48YgUAulA0mnZK8e8m/C7G1DrK8cXE3oqpRDqenB7iW2ooUqb3nYdVaa7L3We8JuAlLwDfRmqDk3Pe8Cnv3hxrv3lKwXEDD281xSHbl5PtjvRsyAOnmY20yuRrq/GWOsIStfzrKiGBIM9CKTyXEbdcVoTRmGimySTjFxPQ0DeiNekya9tcAMC/Bggu01Hhu/BijEf4h/vKSnRd6P6wN176Ogug95Wj004KFCt7zcXjmjsD+RgAlGqpx5XFkgIUGBVPRgc/bLh0NiexGly08YoWvMdVOPs9Z8fD/jAGrpCb5j74RSToiAndx+MiSVd8g8GBipDyi3GJUh30Au93xi79GCj6S7DJxr8PL7A7XVaWhUu2r8vrFBFQKIBCaUw5vQZaIBEAoaNgpGPLrLeKJlI8167awLOHRMiUOzJZj50QEUqejDpwzYbwRfMNVx7ywtMWyb1xdTBpWkjYyCs03HWtS+gsolmzIDCpitQhKeubkM7SHWjTXjAFoArQjZ685NibAZUONicdqYDVVHuBufnBc8oxCDuOBrFFXCU+2KivCWDZfKA3ihA2Dtoodkbw+Mg6CxAOJ1PbrT4gGx9soTCjUDsNE53wmc+LI4Q0tOH4yC0BpDzPoMXbCZiS409hgFu8DsKBzut4wBQC0CmjScY5XJgN3v2ejUy/lncHzILvKq7U7iAvfWHKHHRYcGtbMQOvCIkYNQMKl0vd17FV2xK8lnabdv3XBAAiy3CNOHGCDdvS+0JX85USFg0HMqf0YasOWiQgdvD1ikkQtFIfcuNURB1FSbbMBvbSFoEnBs4x6rOqdxGATgDrIL+glMEW2ftiAMgHRQKRt01XG9nB0DaXnaPtjrU6IpAo8kIZZbqqEPSBMdMPGQ7elE0DjvHc+3DzziePDkcs6dt4/wB5ylYM1131kdENfZq5ERs4xEXXFM3rShCpuuhwYEoF5abesmdWBQ4D5feBO1EZ5HjxiAKESAa3XeQSldCMqvGnY85dhrOcR1iGhNqX5yma9w6FeDLIsBStIK0r4wvKMjWlnXjk0q3m/AomVWidYyQM20o79Y7qxrIqnfmZ2kD1Cx+XO26VG2pu3z1jcch4HIm+W5zkG9VoiJrCWL46VVeM9r7MstuXVVoHgD3gkQEIaFQOSeDFN0q8acdjZtSbt1ZNDi+M8G2aE1QmmcvOJsEC2goIBhHLw4RW1FbuANzmz4xgQIYwQYU3q6484W/3RFNFeS++Of0gOlIFpF1gTJRBFE8T7eMAL95BG5fjEqMIzdCmpK14Kc6QCmT8WzdOQgKKkmGnNCQBCYq1t/8AMl6xUC0YjTTQ6dTLpavMegoFdPA5OLjErAFhu7cilJoA7evxgAYAwg0w5M2vnhpU4QSaWG+0i63nwecKYNNBdCht9c4LrITQaBqKIv8AOAdqb9DpGpwYjJg5IZaiIKQeMFGez8n/AJnQFDUdw5KjWgPZH+7kCDQinZ4RZr94Gg/YSxveuzOsd1kCoFPLoOsJLo4kEL9gx+XsTVR67x2EIFLJoR+7ObvbagCoA3g8Y+dhC2JUnsTJWD76chx0fvlJH51IGjfBHvCsIBwycA0mskPDZkhOQvc0385IYqHonY41g8eDtd/nGqRRFvsHngyfINOizD7C5T8PkAO47OXzldk7lTYBJAP3j/Zh0BzNyghxXlngeISbvnFrS2BWbVJ3mtKjYDqgarTeFBXtmv5xREI3PnjjAXhvY+28Q0NJGawE0p9ksxnw2b7jnCimqiz8YvjE2SrAlckhQ1TlTr3nEorVHgdmSfpCPfb98EwbQAoWqck4y8iEWAaE37Y2tsdkN0SUPJjkqHW7K4wAbfwAFdpdJlcNGsnCC6MUx6FUQKDtkLzg3cEkbCKuB9hSIbyK3PGJEiNQS8171MYkUeg9v2wp8mpCQDnE/t4KMBB/ONkLuqFB/GTxMsCpKG5tcczbhgVOxG9493/J/SzRRPMGn5piMhS6757cFL9p444xZtyu11raLxg8DZsdXlne9uHHXAe3Gj3lsVedOsok6kCpu0zQRJNUartSZcExELv7VU15uOSVYohFei7uWXQPUCAOZwngMSGzn1qSx6NOYYiC4fXDGlOmNmCwJtAgR+fODS63x+kOEBe+bibG+nKQBquCdZKJCEYplIjT6wMMgUXy3MoNpsIFgfYHy5wZYQ1GWl1I9p4zk8N2nlq6uBJ6VBOVBsDbgPy1FFAUdXBXKWIOCO2HWbdjsgbSle88fGJFwsmoHeatExatXQkPVb/pjwliBE7BSUD+Zi8esCiQbQxvBKMGBxxJ3g0kxQdhBPtv5wQj7rRqYvR43gLrJOMwWV6nLQKOAMu972r7xzXNho6t/gy5jyIdLCCrvGJV3K6QUWsMJnRcgEQSlYGTAHsHWJdqdogI8n8GRd8ws0lOx5OdZyd7+AhxKmCI85f6rM2x4phf2cheoCSnZ7ngyoUBEeLSq/GEAi+YJWD3T3lGGpX4uDDX4G0EyvRfHpcaUCkGAPPEHWKgGl5ma+eOcibt3g6Xu6+DKRb6D0qej4yxXo/POMk0MN4H6QBHQcUcDkDZEj1pw6y1tQotNWTFtKsEHAd35xs66MANDbkAj4UWFKhzjcYcR7HznGnzNq3ZvrAle0P3jdo0GGt380w9IjHgpo9fGBaQe6P2xiXgRLtsOblSqWlfzw4VSB2r6XjG0OUhJXa/OPnyStxId3iZYRABJUeWRzWuyJ2qtx6EAt1NvnCd7YEKix+M/ReP5wC93lxbhGGhWhyHOeqIL/fkGKBQfQV2JTy3hMCKhBc0S07584sCRzYfvWSIBwET1Uy9SlDRNVw/EkdrBvN8sPYTNfJRNh4uUdnbS6aNNDIRn+aiq9EY3OxGyBjn2DvN2zORkpZDlYi8pUmpwOvJzaP6jsGh9+DxlhyqBpdqc4Bs/IV+0YDc4NmoTS7c0ktBIchXjNcRarfevGMvIMW8ab9sdbMvZ/8ANyJAroEbjOa5wS48N8I0eZ84nQWO21dAew4xhydwCMPMiW3xkx9EgqaLFAm6ZdUUpScBTkIdF+cXVL45pr8GsLNqALUk6E0cJvD2CQaABO2yGAyIhTXCfnL62BbICXork2yzURCIAVaPnWMKCkQakRtW9TLBjg0bAoB8+Mamw3hOg9DGgatA5QrAeLbkxmGA5waaLg7hJ5KJh8uD/m5B3y3r+DELCGUXRtqessgGbLrzl5rH+B7MClO1oidtuxBJM2iNII0VmGyuQEYobsDPxiykeF3qapr+cGEYREoCOfP4yAENS5IIj2c9ZEoFxrcN1MbX+Vaegk4YKlVybgU/hhBQCQl4ODnRm3k9Xnsv3YHJG0gQFqxl6PWbuPWa3gQhpPTHHgbAApRVdh5MrEz2ggKDBMUBbhcbEOmy0LgYxphO/NalsA2eXJ3mvgmhQG9g/ZxpwZDGs1SUOc4eyekWqrfXGB4CRmilKLwwGFlySqAao5Dh+Mf7e11npOGnZvBLIpCGZ7bYuy4DaEBHk1WK0jHgSNCq5ItScnWsinq/mqG0qHsY2QEF2Uuycch8KcNvILz85ecNWzwZiPeBJ4rWu8HTyCBFpRwpwDOiIvRvtj3Aj5RCv7Xefk+v3MDFHGnHEttE1cXKgeTSnq3A5kaJCPLVZgleGv73HarPgyQohO16M6ZjcgmO1QhuuOsm46ewNzrTiIfOpgFkxFsG7O3L9PoWWIIqkHd3jd9CQS85KlzcA6QhflHBb6SDDY0tD6wOFMJNovIh0MehXFm8Xse1cNowCLYvyb04585HiVunkvOCNCf3hY70gLYBSJcb+wStCs695aspY/e45YuqQoLq+Y3CcjqugQagUnyyZgipsSLkM+xgBOxgeFTw7wbFzm2LppB4PjGwZIQOx1v0xQYHpBUJOhues527pB+3Fnnbm4ABxRlm+9YyZwsQXjrGTbR9sUPsB07dHOCgBUCEJzyYX9MmpPgS4fLw7NApKboeMQgGyBpBrzMc4jfA9AIAdBl9TQd7NWL5MrzlRsR5vvLVfnFZ98HzHyg4ZFYZX0cg/Gb2yFAkHaenGEvIBBYPiIhx8ZIYRuBTwBf9wG5I20eEVm68oURV5YG34G+wbVYBklhWqhOMAPBcaMLqNxA2BL0mLMVBA2mgRY0F7Y7Ux7BEHRpvHgTGZbAMND5x0ODJIS7hb5cspi8qKApwLuk6wiu2smCIDR48O5cbu+ghtt10axkIBOnEmaLTj748z7Yk8NF7ZAlhdrRcBVBJcbQhNHleC4IjVIrkQQ2dunK6nYW42P5yPXkAOtrW7MSmPe7TT4OzElvL9TP4cKUE5W7eMdp8y2w4u24xXj0YmPXGKiqk0uOp7OpZohTimcdwEaXInlW4BpV2iPknjPxPfz/bPXeLW/RNVLzjbe8Kp6F3Hk++CPA0GCVNhA6VND39sUM1CIallA2HTvvBrWmnD6/Wbw4aQDvYuvUwCB7QPnTH8YjM9KnijZ5TH5EIAQcxuWOD75N3j72fjCoC3K2P34Y8kVA3VURHstwG5Iek2ysu3vMD0IR5uS2ZtVKRhTvmqfOWiSDRS841a9mcBC9Df5DA+zBGpII0Ej13rFZJMwhdmeF6BvCkVHKKoRTWumtZoBFIQwPk/ec5O8osQOyxXXEYIiFot5yxQSPeuzKnoKi0LkggplyvMiE/nLHOTEJ51W3fOAXxCaVKzZR04rzjGg2NzjFiO0sHAsxfLDNqqqCevnAlaUpitI1coiHIqGPa6zpLUHbWRIiOw0iFK5vB3MpZuZpNRRRnY+P/AMIEUDF8PeAmEZwdh94HXWhhA8GQlbFACVOoyUAwoQItJ0rxgOOBQoEpztvxLgkl2IPKTEmSKwjYAAGg3Lf/AKIJ8NWYtg1CZQorVo5yRQSALYX9YeDJPgagL+BgGDM9hNdc847mKUOijQ8nL2fwmxgbCaPLmgDTDvgB9nmxjS2OkA/JkYu36mPG7gxeFXCy2OgPeBjUCFIZaV9nH3s6Yq1KiP3YkPTVdXnNyaneKdJN8/jOEPgUXaNTSmabpBuBrS0cYOgGx2puL0aOFcLHZ5DWTJVc+MjUesh3hBdCI9dZ5jE1+M0CFEPOe/z75/OeWXOcv0EHeTh14yYnG9HLreDCaVAqnh0aGSZriSLCkVe8S+zKrf0l/eFK7QqK6OH+sN0XTRP+OMAd0NudCR6CYpAfDHwuWjr755wrR0O5m+KzHg8WYu+VgAMLYiTsc05j2a9Z6BcZ8LDCVAkQevvgBQvLj8gfvA7saSQKpoINecgD9g7bFXWBEt5FgGrCawER1WEg+Qr1inAhqHiKhqqPWUX34EbPExmLNm8+dkwJhJRhBovPWL5fkv6Dis/UJlBQeTt+y4cUPF5/eE0t5P8AjGh4eDAnBTzpxQRtuzOGI9E/rD/A9k/Rhih0VOXWzL1KO3mMMS8KtIt3L3iLNyB18cYG2Hwv7crPRTcffIiK7j/zNIUF4LcP6yOILuHkKYuPYipo3DhXDa2qu6N249AFHLSStlYYkFB4OBeuE/eRabAec94TQIU6HM0OFOsNezvGhmpJoQBUN2ecVUFSkfxhFGyETCNGKzQNWF7aZ07TziHsVJKyNGqVBs84bSj60nNYt7LAfYftiQiru28sBH6yTlonty7yBbRBkE2InGSu0cVVatXFp56Oqm9/GGo+QqHpYOriJg9wLiNVzuQTYSG+sfrsKzeJey4RpbjoXEfmYgVGRHmzH+wLrPT1P8n1Pq5x+3efvZxP7Lh49+M5+v8Ah/32z8Nj/Vfz68Z+acc/51nL5ff7Zw+3eOefJx/bn3nLn38c95/vbXF/nn+D/wAZ/srWbi9/j3+s/wBZ/nP23+nefm3pxjc/u/fr1nJ/5Oc7f+32z/bjP/Nw84/5M/wY8/8AM4f1zz/DHn++d57y/wCuH+H0d5/y52/T/wBufZh5xzw6fw4zl/8ALD/HPt+2f7WP+uHPX2+j/wAOf6548v8AXP08cnP9M/Av2MP8N6/1z9z+I/L6F/unh49+M/Hv5XP0k/yZplyePtjzj9OR/nOLnu8Gf5Z++cjD+uf/2Q==\" style=\"border:0px solid black; height:434px; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; width:650px\" vspace=\"0\" /></p>\n\n<p>MKI TRAVEL adalah Perusahaan yang bergerak di bidang travel dan terpercaya untuk segala hal bentuk pengantaran travel dari yang terkecil sampai yang terbesar semoga menjadi pembelajaran menjadi lebih baik lagi dan lagi</p>\n\n<p>Berikut data kami :</p>\n', '1', '2016-06-08 14:10:08', '1', null);

-- ----------------------------
-- Table structure for trx_services
-- ----------------------------
DROP TABLE IF EXISTS `trx_services`;
CREATE TABLE `trx_services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_code` varchar(16) DEFAULT NULL,
  `service_icon` varchar(255) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `service_content` longtext,
  `service_summary` text,
  `service_create_user_id` int(11) DEFAULT NULL,
  `service_create_date` datetime DEFAULT NULL,
  `service_status` int(1) DEFAULT '1' COMMENT '0.Inactive, 1.Active',
  `service_log_id` int(11) DEFAULT NULL COMMENT 'null=Data parent, not null=Histori untuk ID tercantum',
  PRIMARY KEY (`service_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trx_services
-- ----------------------------
INSERT INTO `trx_services` VALUES ('1', 'UMR', 'fa-bed', 'Umroh', '<h2>Apakah Lorem Ipsum itu?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</p>\r\n\r\n<h2>Mengapa kita menggunakannya?</h2>\r\n\r\n<p>Sudah merupakan fakta bahwa seorang pembaca akan terpengaruh oleh isi tulisan dari sebuah halaman saat ia melihat tata letaknya. Maksud penggunaan Lorem Ipsum adalah karena ia kurang lebih memiliki penyebaran huruf yang normal, ketimbang menggunakan kalimat seperti &quot;Bagian isi disini, bagian isi disini&quot;, sehingga ia seolah menjadi naskah Inggris yang bisa dibaca. Banyak paket Desktop Publishing dan editor situs web yang kini menggunakan Lorem Ipsum sebagai contoh teks. Karenanya pencarian terhadap kalimat &quot;Lorem Ipsum&quot; akan berujung pada banyak situs web yang masih dalam tahap pengembangan. Berbagai versi juga telah berubah dari tahun ke tahun, kadang karena tidak sengaja, kadang karena disengaja (misalnya karena dimasukkan unsur humor atau semacamnya)</p>\r\n', 'Ini Umroh.. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad.\r\n', null, null, '1', null);

-- ----------------------------
-- View structure for ta_view_accesses_dt
-- ----------------------------
DROP VIEW IF EXISTS `ta_view_accesses_dt`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `ta_view_accesses_dt` AS SELECT
  `t1`.`access_id` AS `access_id`,
  `t1`.`access_name` AS `access_name`,
  `t1`.`access_status` AS `access_status`,
  count(0) AS `jml_menu`,
  (
    SELECT
      count(0)
    FROM
      `sys_users`
    WHERE
      (
        (
          `sys_users`.`user_access_id` = `t1`.`access_id`
        )
        AND isnull(`sys_users`.`user_log_id`)
      )
  ) AS `jml_pengguna`
FROM
  (
    `sys_access` `t1`
    LEFT JOIN `sys_access_details` `t2` ON (
      (
        `t1`.`access_id` = `t2`.`accessdetail_access_id`
      )
    )
  )
WHERE
  isnull(`t1`.`access_log_id`)
GROUP BY
  `t1`.`access_name` ; ;

-- ----------------------------
-- View structure for ta_view_sys_users
-- ----------------------------
DROP VIEW IF EXISTS `ta_view_sys_users`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `ta_view_sys_users` AS SELECT
  `t1`.`user_id` AS `user_id`,
  `t1`.`user_data_type` AS `user_data_type`,
  `t1`.`user_data_id` AS `user_data_id`,
  `t1`.`user_name` AS `user_username`,
  `t1`.`user_password` AS `user_password`,
  `t1`.`user_access_id` AS `user_access_id`,
  `t1`.`user_online` AS `user_online`,
  `t1`.`user_last_online` AS `user_last_online`,
  `t1`.`user_last_login_ip` AS `user_last_login_ip`,
  `t1`.`user_last_login_date` AS `user_last_login_date`,
  `t1`.`user_create_user_id` AS `user_create_user_id`,
  `t1`.`user_create_date` AS `user_create_date`,
  `t1`.`user_status` AS `user_status`,
  `t1`.`user_log_id` AS `user_log_id`,
  `t2`.`access_id` AS `access_id`,
  `t2`.`access_name` AS `access_name`,
  `t2`.`access_create_user_id` AS `access_create_user_id`,
  `t2`.`access_create_date` AS `access_create_date`,
  `t2`.`access_status` AS `access_status`,
  `t2`.`access_log_id` AS `access_log_id`,
  `t3`.`employee_id` AS `employee_id`,
  `t3`.`employee_nip` AS `employee_nip`,
  `t3`.`employee_fullname` AS `employee_fullname`,
  `t3`.`employee_email` AS `employee_email`,
  `t3`.`employee_address` AS `employee_address`,
  `t3`.`employee_phone` AS `employee_phone`,
  `t3`.`employee_branch_id` AS `employee_branch_id`,
  `t3`.`employee_create_user_id` AS `employee_create_user_id`,
  `t3`.`employee_create_date` AS `employee_create_date`,
  `t3`.`employee_status` AS `employee_status`,
  `t3`.`employee_log_id` AS `employee_log_id`
FROM
  (
    (
      `sys_users` `t1`
      JOIN `sys_access` `t2` ON (
        (
          (
            `t1`.`user_access_id` = `t2`.`access_id`
          )
          AND (`t1`.`user_data_type` = 1)
        )
      )
    )
    JOIN `ref_employees` `t3` ON (
      (
        `t1`.`user_data_id` = `t3`.`employee_id`
      )
    )
  )
WHERE
  isnull(`t1`.`user_log_id`) ; ;

-- ----------------------------
-- View structure for view_product_item_dt
-- ----------------------------
DROP VIEW IF EXISTS `view_product_item_dt`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_product_item_dt` AS SELECT
  `productitem_id` AS `productitem_id`,
  `productitem_name` AS `productitem_name`,
  `productitem_productcate_id` AS `productitem_productcate_id`,
  `productcate_id` AS `productcate_id`,
  `productcate_branch_id` AS `branch_id`,
  `productcate_name` AS `productcate_name`,
  `packageitem_id` AS `packageitem_id`,
  `packageitem_package_id` AS `vpackage_id`
  
FROM
  ref_product_items,ref_product_categories,ref_package_items
WHERE
  productitem_productcate_id=productcate_id
AND
  packageitem_productitem_id=productitem_id
AND
  packageitem_log_id IS NULL ; ;

-- ----------------------------
-- View structure for view_useremployee
-- ----------------------------
DROP VIEW IF EXISTS `view_useremployee`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_useremployee` AS SELECT
  `t1`.`user_id` AS `user_id`,
  `t1`.`user_data_type` AS `user_type`,
  `t1`.`user_data_id` AS `user_data_id`,
  `t1`.`user_name` AS `user_name`,
  `t1`.`user_password` AS `user_password`,
  `t1`.`user_access_id` AS `user_access_id`,
  `t1`.`user_online` AS `user_online`,
  `t1`.`user_last_online` AS `user_last_online`,
  `t1`.`user_last_login_ip` AS `user_last_login_ip`,
  `t1`.`user_last_login_date` AS `user_last_login_date`,
  `t1`.`user_status` AS `user_status`,
  `t3`.`access_name` AS `access_name`,
  `t3`.`access_id` AS `access_id`,
  `t3`.`access_status` AS `access_status`,
  `t1`.`user_log_id` AS `user_log_id`,
  `t2`.`employee_id` AS `employee_id`,
  `t2`.`employee_is_owner` AS `employee_is_owner`,
  `t2`.`employee_fullname` AS `employee_fullname`,
  `t2`.`employee_email` AS `employee_email`,
  `t2`.`employee_nip` AS `employee_nip`,
  `t2`.`employee_profession_id` AS `employee_profession_id`,
  `t2`.`employee_phone` AS `employee_phone`,
  `t2`.`employee_create_user_id` AS `employee_create_user_id`,
  `t2`.`employee_branch_id` AS `employee_branch_id`,
  `t2`.`employee_create_date` AS `employee_create_date`,
  `t2`.`employee_status` AS `employee_status`,
  `t2`.`employee_log_id` AS `employee_log_id`
FROM
  (
    (
      `sys_users` `t1`
      JOIN `ref_employees` `t2` ON (
        (
          (
            `t1`.`user_data_id` = `t2`.`employee_id`
          )
          AND (`t1`.`user_data_type` = 1)
        )
      )
    )
    JOIN `sys_access` `t3` ON (
      (
        `t1`.`user_access_id` = `t3`.`access_id`
      )
    )
  )
WHERE
  isnull(`t1`.`user_log_id`) ; ;

-- ----------------------------
-- View structure for vw_ref_branch_tables
-- ----------------------------
DROP VIEW IF EXISTS `vw_ref_branch_tables`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `vw_ref_branch_tables` AS select `t`.`branchtable_id` AS `branchtable_id`,`t`.`branchtable_room_id` AS `branchtable_room_id`,`t`.`branchtable_code` AS `branchtable_code`,`t`.`branchtable_name` AS `branchtable_name`,`t`.`branchtable_description` AS `branchtable_description`,`t`.`branchtable_map` AS `branchtable_map`,`t`.`branchtable_create_user_id` AS `branchtable_create_user_id`,`t`.`branchtable_create_date` AS `branchtable_create_date`,`t`.`branchtable_status` AS `branchtable_status`,(case when (`t`.`branchtable_status` = 0) then 'Tidak Aktif' when (`t`.`branchtable_status` = 1) then 'Opened' when (`t`.`branchtable_status` = 2) then 'Reserved' when (`t`.`branchtable_status` = 3) then 'Request Bill' else 'Tidak Aktif' end) AS `branchtable_status_name`,`t`.`branchtable_log_id` AS `branchtable_log_id`,`r`.`branchroom_id` AS `branchroom_id`,`r`.`branchroom_branch_id` AS `branchroom_branch_id`,`r`.`branchroom_code` AS `branchroom_code`,`r`.`branchroom_name` AS `branchroom_name`,`r`.`branchroom_description` AS `branchroom_description`,`r`.`branchroom_map` AS `branchroom_map`,`r`.`branchroom_create_user_id` AS `branchroom_create_user_id`,`r`.`branchroom_create_date` AS `branchroom_create_date`,`r`.`branchroom_status` AS `branchroom_status`,`r`.`branchroom_log_id` AS `branchroom_log_id` from (`ref_branch_tables` `t` join `ref_branch_rooms` `r` on((`r`.`branchroom_id` = `t`.`branchtable_room_id`))) where isnull(`t`.`branchtable_log_id`) ; ;

-- ----------------------------
-- View structure for vw_ref_product_items
-- ----------------------------
DROP VIEW IF EXISTS `vw_ref_product_items`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `vw_ref_product_items` AS select `t`.`branchtable_id` AS `branchtable_id`,`t`.`branchtable_room_id` AS `branchtable_room_id`,`t`.`branchtable_code` AS `branchtable_code`,`t`.`branchtable_name` AS `branchtable_name`,`t`.`branchtable_description` AS `branchtable_description`,`t`.`branchtable_map` AS `branchtable_map`,`t`.`branchtable_create_user_id` AS `branchtable_create_user_id`,`t`.`branchtable_create_date` AS `branchtable_create_date`,`t`.`branchtable_status` AS `branchtable_status`,(case when (`t`.`branchtable_status` = 0) then 'Tidak Aktif' when (`t`.`branchtable_status` = 1) then 'Opened' when (`t`.`branchtable_status` = 2) then 'Reserved' when (`t`.`branchtable_status` = 3) then 'Request Bill' else 'Tidak Aktif' end) AS `branchtable_status_name`,`t`.`branchtable_log_id` AS `branchtable_log_id`,`r`.`branchroom_id` AS `branchroom_id`,`r`.`branchroom_branch_id` AS `branchroom_branch_id`,`r`.`branchroom_code` AS `branchroom_code`,`r`.`branchroom_name` AS `branchroom_name`,`r`.`branchroom_description` AS `branchroom_description`,`r`.`branchroom_map` AS `branchroom_map`,`r`.`branchroom_create_user_id` AS `branchroom_create_user_id`,`r`.`branchroom_create_date` AS `branchroom_create_date`,`r`.`branchroom_status` AS `branchroom_status`,`r`.`branchroom_log_id` AS `branchroom_log_id` from (`ref_branch_tables` `t` join `ref_branch_rooms` `r` on((`r`.`branchroom_id` = `t`.`branchtable_room_id`))) where isnull(`t`.`branchtable_log_id`) ; ;

-- ----------------------------
-- View structure for vw_sys_users
-- ----------------------------
DROP VIEW IF EXISTS `vw_sys_users`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `vw_sys_users` AS SELECT
  `u`.`user_id` AS `user_id`,
  `u`.`user_data_type` AS `user_data_type`,
  `u`.`user_data_id` AS `user_data_id`,
  `u`.`user_name` AS `user_name`,
  `u`.`user_password` AS `user_password`,
  `u`.`user_access_id` AS `user_access_id`,
  `u`.`user_online` AS `user_online`,
  `u`.`user_last_login` AS `user_last_login`,
  `u`.`user_last_online` AS `user_last_online`,
  `u`.`user_create_user_id` AS `user_create_user_id`,
  `u`.`user_create_date` AS `user_create_date`,
  `u`.`user_status` AS `user_status`,
  `u`.`user_log_id` AS `user_log_id`,
  `e`.`employee_id` AS `employee_id`,
  `e`.`employee_branch_id` AS `employee_branch_id`,
  `e`.`employee_fullname` AS `employee_fullname`,
  `e`.`employee_nip` AS `employee_nip`,
  `e`.`employee_address` AS `employee_address`,
  `e`.`employee_is_owner` AS `employee_is_owner`,
  `e`.`employee_phone` AS `employee_phone`,
  `e`.`employee_email` AS `employee_email`,
  `e`.`employee_create_user_id` AS `employee_create_user_id`,
  `e`.`employee_create_date` AS `employee_create_date`,
  `e`.`employee_status` AS `employee_status`,
  `e`.`employee_log_id` AS `employee_log_id`,
  `b`.`branch_id` AS `branch_id`,
  `b`.`branch_company_id` AS `branch_company_id`,
  `b`.`branch_code` AS `branch_code`,
  `b`.`branch_name` AS `branch_name`,
  `b`.`branch_description` AS `branch_description`,
  `b`.`branch_rating` AS `branch_rating`,
  `b`.`branch_address` AS `branch_address`,
  `b`.`branch_city` AS `branch_city`,
  `b`.`branch_district` AS `branch_district`,
  `b`.`branch_province_id` AS `branch_province_id`,
  `b`.`branch_country_id` AS `branch_country_id`,
  `b`.`branch_latitude` AS `branch_latitude`,
  `b`.`branch_longitude` AS `branch_longitude`,
  `b`.`branch_create_user_id` AS `branch_create_user_id`,
  `b`.`branch_create_date` AS `branch_create_date`,
  `b`.`branch_status` AS `branch_status`,
  `b`.`branch_log_id` AS `branch_log_id`,
  `c`.`company_id` AS `company_id`,
  `c`.`company_code` AS `company_code`,
  `c`.`company_name` AS `company_name`,
  `c`.`company_description` AS `company_description`,
  `c`.`company_address` AS `company_address`,
  `c`.`company_city` AS `company_city`,
  `c`.`company_district` AS `company_district`,
  `c`.`company_province_id` AS `company_province_id`,
  `c`.`company_country_id` AS `company_country_id`,
  `c`.`company_latitude` AS `company_latitude`,
  `c`.`company_longitude` AS `company_longitude`,
  `c`.`company_create_user_id` AS `company_create_user_id`,
  `c`.`company_create_date` AS `company_create_date`,
  `c`.`company_status` AS `company_status`,
  `c`.`company_log_id` AS `company_log_id`,
  `a`.`access_id` AS `access_id`,
  `a`.`access_name` AS `access_name`,
  `a`.`access_default_menu_id` AS `access_default_menu_id`,
  `a`.`access_create_user_id` AS `access_create_user_id`,
  `a`.`access_create_date` AS `access_create_date`,
  `a`.`access_status` AS `access_status`,
  `a`.`access_log_id` AS `access_log_id`
FROM
  (
    (
      (
        (
          `sys_users` `u`
          JOIN `ref_employees` `e` ON (
            (
              `u`.`user_data_id` = `e`.`employee_id`
            )
          )
        )
        LEFT JOIN `ref_branches` `b` ON (
          (
            `b`.`branch_id` = `e`.`employee_branch_id`
          )
        )
      )
      LEFT JOIN `ref_companies` `c` ON (
        (
          `c`.`company_id` = `b`.`branch_company_id`
        )
      )
    )
    JOIN `sys_access` `a` ON (
      (
        `a`.`access_id` = `u`.`user_access_id`
      )
    )
  )
WHERE
  isnull(`u`.`user_log_id`) ; ;
SET FOREIGN_KEY_CHECKS=1;
